var express = require('express'),
  app = express('localhost'),
  port = process.env.PORT || 3000,
  mongoose = require('mongoose'),
  cors = require('cors');
  models = require('./api/models/model'),
  bodyParser = require('body-parser'),
  fs = require('fs'),
  path = require('path'),
  async = require('async'),
  jwt = require('jsonwebtoken'),
  bcrypt = require('bcryptjs'),
  geodist = require('geodist'),
  sortBy = require('sort-array'),
  randomstring = require('randomstring'),
  request = require('request'),
  cron = require('node-schedule');
 var DataTable = require('mongoose-datatable');

  DataTable.configure({ verbose: false, debug : false });
  mongoose.plugin(DataTable.init);

  mongoose.Promise = global.Promise;
  mongoose.connect('mongodb://localhost/search4pharma');

  var db = mongoose.connection;


app.use(function(req, res, next) {

  return next();
});



//Bind connection to error event (to get notification of connection errors)
db.on('error', console.error.bind(console, 'MongoDB connection error:'));


app.use(function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept');
  res.header('Access-Control-Allow-Credentials', true);
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,PATCH');
  console.log('method =>',req.method);
       next();
})

app.use(cors());
app.use(bodyParser.urlencoded({extended: true, limit: '500mb'}));
app.use(bodyParser.json({extended: true, limit: '500mb'}));
app.use(express.static('public'));

var storeRoutes = require('./api/routes/store_route');
storeRoutes(app);

var customerRoutes = require('./api/routes/customer_route');
customerRoutes(app);

var employeeRoutes = require('./api/routes/employee_route');
employeeRoutes(app);

var superadminRoutes = require('./api/routes/superAdmin_route');
superadminRoutes(app);


app.listen(port);
console.log('Order_Medicine server started on: ' + port);
