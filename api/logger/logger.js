var bunyan = require('bunyan');

exports.log = bunyan.createLogger({
name: 'development',
streams: [{
     type: 'rotating-file',
     path: path.join(__dirname+'/../logger/development.log'),
     period: '1d',
     count: 30
  }]
});
