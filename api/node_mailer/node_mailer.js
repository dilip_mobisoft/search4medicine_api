'use strict';
var smtpTransport = require('nodemailer-smtp-transport');
var nodemailer = require('nodemailer');
var fs = require('file-system');
var config = require('../config/config');
var emailformat = require('string-template');
var path = require('path');



exports.nodeMailer = function(req, cb){
    console.log("======================");
    console.log(req); 
    //return false;
        var htmlURL = path.join(__dirname,'../../public/html_mailer/'+req.templateName+'.html');
        var htmldata = fs.readFileSync(htmlURL).toString();
        if(req.templateName != 'undefined'
                || req.to != 'undefined'
                || req.subject != 'undefined'
                || req.templateData != 'undefined'
                )
        {
                var transporter = nodemailer.createTransport(smtpTransport({
                        service: 'gmail',
                        host: 'smtp.gmail.com',
                        proxy: 'http://localhost:3000/',
                        port: 25,
                        auth: {
                            user: req.senderEmail,
                                pass: req.senderPassword
                        }
                }));

                var mailOptions = {
                  from: req.fromJob+" <"+req.senderEmail+'>',
                  to: req.to,
                  bcc: 'nntpvtltd@gmail.com',
                  subject: req.subject,
                  html: emailformat(htmldata, req.templateData)
                };
                transporter.sendMail(mailOptions, function(error, info){
                  if (error) {
                    console.log("Failed to send mail "+error);
                    cb(error, null);
                  } else {
                    console.log('Upload SUCCESSFULLY');
                    cb(info, error);
                  }
                });
        }
        else{
            var obj = {};
            obj.status = '400';
            cb(obj, null);
        }
}


// //unit case test
//  if(require.main === module){
//     (function(){

//          var templateData = {};

//              templateData.Title_Of_Vacancy = "MY";
//              templateData.For_Position = "2";
//              templateData.Number_Of_Vacancy =  "10";
//              templateData.Total_Experience_Needed = "10 Year";
//              templateData.Salary_Range = "20Lac";
//              templateData.Qualification = "PG";
//              templateData.Joining_On = "20/05/2018";
//              templateData.Age_Limit = "26";
//              templateData.vacancy_id = "123";

//              var requestObj = {
//                 templateName : "ADD_VACANCY",
//                 to :"dilip@mobisoftseo.com,arvind@mobisoftseo.com",
//                 subject : "Pharmacy Job",
//                 templateData : templateData,
//              }

//           nodeMailer(requestObj,function(err,data){
//                 console.log("====ERROR====",err)
//                 console.log("====DATA====",data)
//           });
//     });
// }