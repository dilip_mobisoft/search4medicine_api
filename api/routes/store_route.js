'use strict';
//https://dilip_mobisoft@bitbucket.org/dilip_mobisoft/search4medicine_api.git
//var middleware = require('../middleware/middleware');
// console.log("function", middleware.verifyToken);
module.exports = function(app) {
  var sotre_controller = require('../controllers/Store_controller');
  
  app.route('/store/store_signup')
    .post(sotre_controller.store_signup);

  app.route('/store/OwnerInfo')
    .post(sotre_controller.owner_info);

  app.route('/store/OperationalInfo')
    .post(sotre_controller.OperationalInfo);

  app.route('/store/CreateAcount')
    .post(sotre_controller.CreateAcount);    

  app.route('/store/add_vacancy')
    .post(sotre_controller.AddVacancy);

  app.route('/store/store_login')
    .post(sotre_controller.store_login);

  app.route('/store/getallvacancy')
    .post(sotre_controller.getallvacancy); 

  app.route('/store/GetEmpInfoByMnumber')
    .post(sotre_controller.GetEmpInfoByMnumber);

   app.route('/store/SearchParma')
    .post(sotre_controller.SearchParma);  

   app.route('/store/getmedicineapidata')
    .post(sotre_controller.getmedicineapidata);  

   app.route('/store/getStoredata')
    .post(sotre_controller.getStoredata);      

   app.route('/store/getOperationaldata')
    .post(sotre_controller.getOperationaldata);

   app.route('/store/getownerinfodata')
    .post(sotre_controller.getownerinfodata);

   app.route('/store/getCreateaccountdata')
    .post(sotre_controller.getCreateaccountdata);

   app.route('/store/updateStoredata')
    .post(sotre_controller.updateStoredata);

   app.route('/store/updateOperationaldata')
    .post(sotre_controller.updateOperationaldata);

   app.route('/store/updateownerinfodata')
    .post(sotre_controller.updateownerinfodata); 
                         
//-----------------------------------------------------------------------------------------------------------------------------------------
  // app.route('/store/MyStaff')
  //   .post(sotre_controller.MyStaff);

  // app.route('/store/soft_delete_mystaff')
  //   .post(sotre_controller.soft_delete_mystaff); 
//-----------------------------------------------------------------------------------------------------------------------------------------

  app.route('/store/emp_detail_by_accountid')
    .post(sotre_controller.emp_detail_by_accountid); 

   app.route('/store/SearchMedicine')
    .post(sotre_controller.SearchMedicine);

   app.route('/store/get_order_list')
   .post(sotre_controller.get_order_list);

   app.route('/store/get_order_detail_by_oreder_id')
   .post(sotre_controller.get_order_detail_by_oreder_id);

   app.route('/store/cancel_order')
   .post(sotre_controller.cancel_order);

   app.route('/store/accept_order')
   .post(sotre_controller.accept_order);
     
   app.route('/store/search_by_salt')
    .post(sotre_controller.search_by_salt);

   app.route('/store/search_salt_by_medicineid')
    .post(sotre_controller.search_salt_by_medicineid);
    
   app.route('/store/search_medicine_by_salt')
    .post(sotre_controller.search_medicine_by_salt);    

   // app.route('/store/enter_salt')
   //  .get(sotre_controller.enter_salt);


   app.route('/store/search_salt')
    .post(sotre_controller.search_salt);
      
   app.route('/store/find_nearest_medical')
    .post(sotre_controller.find_nearest_medical);  

   app.route('/store/get_adds_by_store_id')
    .post(sotre_controller.get_adds_by_store_id);    

   app.route('/store/get_all_banner_for_customer')
    .post(sotre_controller.get_all_banner_for_customer); 

   app.route('/store/get_all_banner_for_retailer')
    .post(sotre_controller.get_all_banner_for_retailer); 

   app.route('/store/notification')
    .post(sotre_controller.notification);    

   app.route('/store/get_store_all_notification')
    .post(sotre_controller.get_store_all_notification); 

   app.route('/store/get_customer_email_by_store_id')
    .post(sotre_controller.get_customer_email_by_store_id);

   app.route('/store/save_store_msg')
    .post(sotre_controller.save_store_msg);  

   app.route('/store/update_store_msg')
    .post(sotre_controller.update_store_msg);

   app.route('/store/delete_store_msg')
    .post(sotre_controller.delete_store_msg);

    // app.route('/store/med_finder')   //-----------------------------
    // .post(sotre_controller.med_finder);

    app.route('/store/medicine_finder')
    .post(sotre_controller.medicine_finder);    
 
    app.route('/store/store_response')
    .post(sotre_controller.store_response);  

    app.route('/store/get_order_status_one')
    .post(sotre_controller.get_order_status_one);
  

// medfinder
    // app.route('/store/get_yes_list_of_enquery_id')
    // .post(sotre_controller.get_yes_list_of_enquery_id);

    app.route('/state')
    .get(sotre_controller.state);

    app.route('/city')
    .post(sotre_controller.city);

    app.route('/store/create_med_enquiry')
    .post(sotre_controller.create_med_enquiry);

    app.route('/store/dispatch_order')
    .post(sotre_controller.dispatch_order);    

    app.route('/store/get_emp_profile_according_no_of_view')
    .post(sotre_controller.get_emp_profile_according_no_of_view);

   app.route('/store/pakcage')
    .post(sotre_controller.pakcage);

   app.route('/store/get_all_packages')
    .post(sotre_controller.get_all_packages);

   app.route('/store/update_package_for_store')
    .post(sotre_controller.update_package_for_store); 

   app.route('/store/wrong_med')
    .post(sotre_controller.wrong_med);

   app.route('/store/med_not_found')
    .post(sotre_controller.med_not_found);

   app.route('/store/short_list_candidiate')
    .post(sotre_controller.short_list_candidiate);
  
   app.route('/store/delete_short_list_candidiate')
    .post(sotre_controller.delete_short_list_candidiate);

   app.route('/store/get_short_list_candidiate')
    .post(sotre_controller.get_short_list_candidiate);

   app.route('/store/get_employee_by_employee_id_inbody')
    .post(sotre_controller.get_employee_by_employee_id_inbody);

   app.route('/store/get_store_detail_by_store_id')
    .post(sotre_controller.get_store_detail_by_store_id);

   app.route('/store/delete_add_vacancy')
    .post(sotre_controller.delete_add_vacancy);

   app.route('/store/get_all_employees_applied_for_job')
    .post(sotre_controller.get_all_employees_applied_for_job);

   app.route('/store/emp_find_by_pin_code')
    .post(sotre_controller.emp_find_by_pin_code);

   // app.route('/store/retailer_viewed_profiles')
   //  .post(sotre_controller.retailer_viewed_profiles);

   app.route('/store/adminpackage')
    .post(sotre_controller.adminpackage);

   app.route('/store/get_account_detail_by_account_id')
    .post(sotre_controller.get_account_detail_by_account_id);

   app.route('/store/insert_retailer_viewed_profile')
    .post(sotre_controller.insert_retailer_viewed_profile);

   app.route('/store/create_staff')
    .post(sotre_controller.create_staff);

   app.route('/store/staff_delete')
    .post(sotre_controller.staff_delete);

   app.route('/store/get_staff_by_account_id')
    .post(sotre_controller.get_staff_by_account_id);

   app.route('/store/check_job_posted_by_store')
    .post(sotre_controller.check_job_posted_by_store);   

   app.route('/store/delete_notification')
    .post(sotre_controller.delete_notification);   

   app.route('/store/search_order_detail')
    .post(sotre_controller.search_order_detail);      

   app.route('/store/get_all_enquiry_by_customer')
    .post(sotre_controller.get_all_enquiry_by_customer);     

   app.route('/store/get_all_enquiry_by_retailer')
    .post(sotre_controller.get_all_enquiry_by_retailer); 

   app.route('/store/retailer_response_no')
    .post(sotre_controller.retailer_response_no);

   app.route('/store/cutomer_cancel_request')
    .post(sotre_controller.cutomer_cancel_request);

   app.route('/store/retailer_response_yes')
    .post(sotre_controller.retailer_response_yes);

   app.route('/store/get_found_store_list')
    .post(sotre_controller.get_found_store_list);

   app.route('/store/get_retailerc_notication')
    .post(sotre_controller.get_retailerc_notication);

   app.route('/store/get_retailer_order_notication')
    .post(sotre_controller.get_retailer_order_notication);  
    
   app.route('/store/get_medicine_enquery_for_retailer')
    .post(sotre_controller.get_medicine_enquery_for_retailer);  
   // app.route('/store/storeIsactivestatus')
   //  .post(sotre_controller.storeIsactivestatus);                
    // app.route('/store/search_pharmacy_with_detail')
    // .post(sotre_controller.search_pharmacy_with_detail);

    // app.route('/store/store_status_new')
    // .get(sotre_controller.store_status_new );    

   // app.route('/store/customer_order_second')
   //  .post(sotre_controller.customer_order_second);
     
//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>    END OF SHASHANK API     <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

   // app.route('/store/super_admin_dasboard')                        testing for ajax datatable
   //  .get(sotre_controller.super_admin_dasboard);  
} 