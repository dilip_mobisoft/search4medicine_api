'use strict';
//https://dilip_mobisoft@bitbucket.org/dilip_mobisoft/search4medicine_api.git
// var middleware = require('../middleware/middleware');
// console.log("function", middleware);
module.exports = function(app) {
  var employee_controller = require('../controllers/employee_contorller');
  
  app.route('/candidate/SmEmployee')
    .post(employee_controller.SmEmployee); 

  app.route('/candidate/EmpLogin')
    .post(employee_controller.EmpLogin);     

  app.route('/candidate/get_employee_by_employee_id')
    .get(employee_controller.get_employee_by_employee_id);

  app.route('/candidate/SmEmployeeUpdate')
    .post(employee_controller.SmEmployeeUpdate);    

  app.route('/candidate/getallpharmacy')
    .get(employee_controller.getallpharmacy);

  app.route('/candidate/empchangepass')
    .post(employee_controller.empchangepass);

  app.route('/candidate/EditProfile')
    .post(employee_controller.EditProfile);

  app.route('/candidate/forgot_password')
    .post(employee_controller.forgot_password);

  app.route('/candidate/match_otp_email')
    .post(employee_controller.match_otp_email);

  app.route('/candidate/get_latest_job_detail')
    .get(employee_controller.get_latest_job_detail);                     

  app.route('/candidate/get_job_by_job_id')
    .post(employee_controller.get_job_by_job_id); 

  app.route('/candidate/applyjob')
    .post(employee_controller.applyjob); 

  app.route('/candidate/get_store_detail')
    .post(employee_controller.get_store_detail);

  app.route('/candidate/filter_job')
    .post(employee_controller.filter_job);

   app.route('/candidate/home_page')
    .get(employee_controller.home_page);  
    
   app.route('/candidate/rejection_emp')
    .post(employee_controller.rejection_emp);    

   app.route('/candidate/suspend_emp')
    .post(employee_controller.suspend_emp);
    
   app.route('/candidate/add_activation')
    .post(employee_controller.add_activation);    

   app.route('/candidate/email_veryfication_by_otp')
    .post(employee_controller.email_veryfication_by_otp);  

   app.route('/candidate/change_email_address')
    .post(employee_controller.change_email_address); 

   app.route('/candidate/mobile_veryfication_by_otp')
    .post(employee_controller.mobile_veryfication_by_otp); 
    
   app.route('/candidate/match_otp_mobile')
    .post(employee_controller.match_otp_mobile);

   app.route('/candidate/get_latest_job')
    .get(employee_controller.get_latest_job); 

   app.route('/candidate/change_mobile_number')
    .post(employee_controller.change_mobile_number);

   app.route('/candidate/contact_us')
    .post(employee_controller.contact_us);                   
}