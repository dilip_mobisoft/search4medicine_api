'use strict';
//https://dilip_mobisoft@bitbucket.org/dilip_mobisoft/search4medicine_api.git
//var middleware = require('../middleware/middleware');
// console.log("function", middleware.verifyToken);
module.exports = function(app) {
  var superadmin_controller = require('../controllers/superAdmin_controller');

  app.route('/store/super_admin')
    .post(superadmin_controller.super_admin);  

  app.route('/admin/super_admin_login')
    .post(superadmin_controller.super_admin_login);
    
  app.route('/admin/all_emaployee_list')
    .post(superadmin_controller.all_emaployee_list);

  app.route('/admin/getAllRetailer')
    .post(superadmin_controller.getAllRetailer);

  app.route('/admin/getOneRetailerInformations')
    .post(superadmin_controller.getOneRetailerInformations);

  app.route('/admin/activatedRetailer')
    .post(superadmin_controller.activatedRetailer);

  // app.route('/admin/rejectRetailer')
  //   .post(superadmin_controller.rejectRetailer);

  app.route('/admin/createPackageJob')
    .post(superadmin_controller.createPackageJob); 
       
  app.route('/admin/getAllPackages')
    .get(superadmin_controller.getAllPackages);
  
  app.route('/admin/getPackage')
    .post(superadmin_controller.getPackage);    
    
  app.route('/admin/updatePackageJob')
    .post(superadmin_controller.updatePackageJob);
    
  app.route('/admin/deletePackage')
    .post(superadmin_controller.deletePackage);    

  app.route('/admin/createRejectRetailer')
    .post(superadmin_controller.createRejectRetailer); 

  app.route('/admin/createSuspendRetailer')
    .post(superadmin_controller.createSuspendRetailer); 

  app.route('/admin/get_employee_by_employee_id')
    .get(superadmin_controller.get_employee_by_employee_id);

  app.route('/admin/superAdminDashbord')
    .get(superadmin_controller.superAdminDashbord);

  app.route('/admin/rejectionEmployee')
    .post(superadmin_controller.rejectionEmployee);

  app.route('/admin/suspendEmployee')
    .post(superadmin_controller.suspendEmployee); 
           
  app.route('/admin/activationEmployee')
    .post(superadmin_controller.activationEmployee);

  app.route('/admin/getAllCustomerDetail')
    .post(superadmin_controller.getAllCustomerDetail);  
  
  app.route('/admin/getRetailersPackageHistory')
    .get(superadmin_controller.getRetailersPackageHistory);
      
  app.route('/admin/addRetailerPackage')
    .post(superadmin_controller.addRetailerPackage); 
                 
  app.route('/admin/acceptPackage')
    .post(superadmin_controller.acceptPackage);     

  app.route('/admin/getOrdersOfOrderMedicine')
    .get(superadmin_controller.getOrdersOfOrderMedicine); 
    
  app.route('/admin/getOrderListByOrederId')
    .post(superadmin_controller.getOrderListByOrederId);   
    
  app.route('/admin/getAllordrdetail')
    .post(superadmin_controller.getAllordrdetail); 
          
  app.route('/admin/postAdvertisment')
    .post(superadmin_controller.postAdvertisment);
        
  app.route('/admin/getAdvertisment')
    .get(superadmin_controller.getAdvertisment);

  app.route('/admin/deleteAdvertise')
    .post(superadmin_controller.deleteAdvertise);

  app.route('/admin/flagMedicine')             // wrongMedicine
    .post(superadmin_controller.flagMedicine);
    
  app.route('/admin/medicineNotfound')
    .post(superadmin_controller.medicineNotfound);

  app.route('/admin/solveflagMedicine')
    .post(superadmin_controller.solveflagMedicine);
        
  app.route('/admin/solveMedicineNotfound')
    .post(superadmin_controller.solveMedicineNotfound);

  app.route('/admin/requestForchangepassword')
    .post(superadmin_controller.requestForchangepassword);

  app.route('/admin/matchotpFornewpassword')
    .post(superadmin_controller.matchotpFornewpassword);

  app.route('/admin/getAllStoreStaff')
    .get(superadmin_controller.getAllStoreStaff);

  app.route('/admin/sendNoticationFromSuperAdmin')
    .post(superadmin_controller.sendNoticationFromSuperAdmin);
    
  app.route('/admin/getRetailerViewedProfileDetail')
    .get(superadmin_controller.getRetailerViewedProfileDetail);  
 
   app.route('/admin/entryStoreName')
    .get(superadmin_controller.entryStoreName);   

   app.route('/admin/getCustomerwithStoreId')
    .get(superadmin_controller.getCustomerwithStoreId)         

   app.route('/admin/enterStoreIdByAdmin')
    .post(superadmin_controller.enterStoreIdByAdmin);

   app.route('/admin/storeJobDetail')
    .post(superadmin_controller.storeJobDetail)
    
   app.route('/admin/CandidateDeatilApplyForJob')
    .get(superadmin_controller.CandidateDeatilApplyForJob)    
    
   app.route('/admin/getStorOfStaff')
    .get(superadmin_controller.getStorOfStaff)    
    
         
  // for Tesing    
  // app.route('/admin/sendNotication')
  //   .post(superadmin_controller.sendNotication);    
}