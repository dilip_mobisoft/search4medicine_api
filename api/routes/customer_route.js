'use strict';

module.exports = function(app) {
  var customer_controller = require('../controllers/customer_controller');
  
   app.route('/store/customer_signup')
    .post(customer_controller.customer_signup);

   app.route('/store/customer_profile_update')
    .post(customer_controller.customer_profile_update);
    
   app.route('/store/get_customer_profile_update')
    .post(customer_controller.get_customer_profile_update);

   app.route('/store/add_item_to_cart')
    .post(customer_controller.add_item_to_cart);

   app.route('/store/customer_cart_list')
   .post(customer_controller.customer_cart_list);

   app.route('/store/customer_orders')
   .post(customer_controller.customer_orders);   

   app.route('/store/customer_delivery_detail')
   .post(customer_controller.customer_delivery_detail); 

   app.route('/store/get_customer_delivery_detail')
    .post(customer_controller.get_customer_delivery_detail);   

   app.route('/store/get_order_list_search')
    .post(customer_controller.get_order_list_search);

   app.route('/store/add_prescription_to_healthlocker')
    .post(customer_controller.create_healthlocker);   
    
   app.route('/store/update_prescription_to_healthlocker')
    .post(customer_controller.update_healthlocker);       

   app.route('/store/get_prescription_to_healthlocker_by_customer_id')
    .post(customer_controller.get_healthlocker_by_customer_id);   
    
    app.route('/store/delete_prescription_to_healthlocker')
    .post(customer_controller.delete_healthlocker);     

    app.route('/store/get_nearest_store_list')
    .post(customer_controller.get_nearest_store_list); 
    
   app.route('/store/get_customer_email_by_account_id')
    .post(customer_controller.get_customer_email_by_account_id);
    
   app.route('/store/healthlocker_if_ten')
    .post(customer_controller.healthlocker_if_ten);  

   app.route('/store/get_customer_notication')
    .post(customer_controller.get_customer_notication);

} 