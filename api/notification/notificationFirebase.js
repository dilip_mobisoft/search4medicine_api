var config = require('../config/config');
var rp = require('request-promise');

exports.notificationFirebase = function(req, callback){
  if(req.title != 'undefind'
    || req.bodyText != 'undefind'
    || req.sound != 'undefind'
    || req.fcm != 'undefind')
  {
    var options = {
    uri: 'https://fcm.googleapis.com/fcm/send',
    method: 'POST',
    headers: {
        'Content-Type' : 'application/json',
        'Authorization' : config.serverKey
    },
    json: {"notification":{
        "title": req.title,
        "body": req.bodyText,
        "sound": req.sound,
      },
      "to": req.fcm }
  };
    rp(options)
    .then(function (response) {
      // var data = response.data;
      console.log("SUCCESSFULLY____FireBaseCMF=======>", response); 
      callback(null, response);     
    })
    .catch(function(err){
      console.log("err-------->", err);
      callback(null, err);  
   })
  }
  else{
      var obj = {};
      obj.status = '400';
      callback(null, obj)  
  }
}