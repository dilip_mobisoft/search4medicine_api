'use strict';

var admin = require("firebase-admin");
         
var serviceAccount = require("../config/ordermedicine-53157-firebase-adminsdk-9s0tz-0fc31467ca.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://ordermedicine-53157.firebaseio.com"
});

var moment = require('moment');
var config = require('../config/config');
var logger =  require('../logger/logger');
var node_mailer = require('../node_mailer/node_mailer');
var notificationFirebase = require('../notification/notificationFirebase');
var fs = require('fs');
var rp = require('request-promise');
var sum = require('sum-of-two-numbers');
var async = require('async');
var smtpTransport = require('nodemailer-smtp-transport');
var nodemailer = require('nodemailer');
var regexp = require('node-regexp');
var format  = require('node.date-time');
var emailformat = require('string-template');
var emailExistence = require('email-existence');
var mongoose = require('mongoose'),
   Store = mongoose.model('Store'),
   storeadvertisement = mongoose.model('storeadvertisement'),
   notification = mongoose.model('notification'),
   OwnerInfo = mongoose.model('OwnerInfo'),
   OperationalInfo = mongoose.model('OperationalInfo'),
   CreateAcount = mongoose.model('CreateAcount'),
   AddVacancy = mongoose.model('AddVacancy'),
   SmEmployee = mongoose.model('SmEmployee'),
   email_veryfication = mongoose.model('email_veryfication'),
   mobile_Veryfication = mongoose.model('mobile_Veryfication'),
   medicine_data = mongoose.model('medicine_data'),
   apply_job = mongoose.model('apply_job'),
   my_staff = mongoose.model('my_staff'),
   customer_cart = mongoose.model('customer_cart'),
   customer = mongoose.model('customer'),
   customer_delivery_detail = mongoose.model('customer_delivery_detail'),
   customer_order = mongoose.model('customer_order'),
   medicine_salt = mongoose.model('medicine_salt'),
   rejection = mongoose.model('rejection'),
   suspend = mongoose.model('suspend'),
   health_locker = mongoose.model('health_locker'), 
   medFinder = mongoose.model('medFinder'), 
   state = mongoose.model('state'),
   city = mongoose.model('city'),
   packagejob = mongoose.model('packagejob'),
   medicine_not_found = mongoose.model('medicine_not_found'),
   wrong_medicine = mongoose.model('wrong_medicine'),
   shortlist_candidiate = mongoose.model('shortlist_candidiate'),
   retailer_viewed_profile = mongoose.model('retailer_viewed_profile'),
   staff = mongoose.model('staff'),
   retailerPackagehistory = mongoose.model('retailerPackagehistory'),
   response_store = mongoose.model('response_store'),
   notificationcr = mongoose.model('notificationcr'),
   super_admin = mongoose.model('super_admin');

exports.store_signup = function(req, res){
	var pharmacy_name = req.body.pharmacy_name;
	var address = req.body.address;
	var landmark = req.body.landmark;
	var area = req.body.area;
	var state = req.body.state;
	var city = req.body.city;
	var PinCode = req.body.PinCode;
	// var email = req.body.email;
	var ContactNo_1 = req.body.ContactNo_1;
	var ContactNo_2 = req.body.ContactNo_2;
	var no_of_branches = req.body.no_of_branches;
	var DLN = req.body.DLN;
	var GstNumber = req.body.GstNumber;
	var premise = req.body.premise;
	var latitude = req.body.latitude;
	var longitude = req.body.longitude;
	if(typeof req.body.store_id == 'undefined'){
	    if(typeof pharmacy_name == 'undefined'
	    	|| typeof address == 'undefined'
	     	|| typeof landmark == 'undefined'
	    	|| typeof area == 'undefined'
	    	|| typeof state == 'undefined'
	    	|| typeof city == 'undefined'
	    	|| typeof PinCode == 'undefined'
	    	// || typeof email == 'undefined'
	    	|| typeof ContactNo_1 == 'undefined'
	    	|| typeof no_of_branches == 'undefined'
	    	|| typeof DLN == 'undefined'
	    	|| typeof GstNumber == 'undefined'
	    	|| typeof premise == 'undefined'
	    	|| typeof latitude == 'undefined'
	    	|| typeof longitude == 'undefined'
	    	// || typeof req.body.profileImage == 'undefined'
	    	)
	    {    
	    	console.log("All detail is not provided");
	    	res.send({
	    		statusCode: 400,
	    		message: "All detail is not provided"
	    	});
	    }
	    else{
			var store_detail = new Store();
			store_detail.Pharmacy_name = pharmacy_name;
			store_detail.Address = address;
			store_detail.Landmark = landmark;
			store_detail.Area = area;
		    store_detail.latitude = latitude;
			store_detail.longitude = longitude;
			store_detail.State = state;
			store_detail.City = city;
			store_detail.Pin_code = PinCode;
			store_detail.Contact_No_1 = ContactNo_1;
			// store_detail.Email = email;
			if(typeof ContactNo_2 == 'undefined'){
				store_detail.Contact_No_2 = '';
			}
			else{
				store_detail.Contact_No_2 = ContactNo_2;
			}
			store_detail.no_of_branches = no_of_branches
			store_detail.DLN = DLN;
			store_detail.GST_Number = GstNumber;
			store_detail.Premise = premise;
			store_detail.CreateTime = Date.now();
			if(typeof req.body.profileImage != 'undefined')
			{
				var imagePath = '/images/' + randomstring.generate(10) +'.png';
				store_detail.profile_image = imagePath;
				const base64Data = req.body.profileImage.replace(/^data:([A-Za-z-+/]+);base64,/, '');
				fs.writeFileSync(process.cwd() + '/public' + imagePath, new Buffer(base64Data, 'base64'), 'base64', function(err){
					if (err)
					{
					    store_detail.profile_image = '/images/default.png';
					    store_detail.save();
					}
				});  
			}
			else{
			    store_detail.profile_image = '/images/default.png';
			    store_detail.save();    	    	
			}  
			store_detail.save(function(err){
			    if(err){
			    	logger.log.info(err);
			    	return res.send({
			    		statusCode: 400,
			    		message: "Something Went wrong."
			    	})
			    }
			    console.log("store create Successfully");
			    return res.send({
			    	statusCode: 200,
			    	message: "store create Successfully",
	                data: store_detail
			    })
			})
		}
	}
	else{
		Store.findById({_id: req.body.store_id}, function(err, doc){
			if(err) throw err;
			doc.updateTime = Date.now();
			if(typeof req.body.pharmacy_name != 'undefined')
				{doc.Pharmacy_name = req.body.pharmacy_name;}
			if(typeof req.body.address != 'undefined')
				{doc.Address = req.body.address;}
			if(typeof req.body.landmark != 'undefined')
				{doc.Landmark = req.body.landmark;}
			if(typeof req.body.area != 'undefined')
				{doc.Area = req.body.area;}
			if(typeof req.body.latitude != 'undefined')
			    {doc.latitude = req.body.latitude;}
			if(typeof req.body.longitude != 'undefined')
				{doc.longitude = req.body.longitude;}
			if(typeof req.body.state != 'undefined')
				{doc.State = req.body.state;}
			if(typeof req.body.city != 'undefined')
				{doc.City = req.body.city;}
			if(typeof req.body.PinCode != 'undefined')
				{doc.Pin_code = req.body.PinCode;}
				// store_detail.Email = email;
			if(typeof req.body.no_of_branches != 'undefined')
				{doc.no_of_branches = no_of_branches;}
			if(typeof req.body.ContactNo_1 != 'undefined')
				{doc.Contact_No_1 = req.body.ContactNo_1;}
			if(typeof req.body.ContactNo_2 != 'undefined')
				{doc.Contact_No_2 = req.body.ContactNo_2;}
			if(typeof req.body.DLN != 'undefined')
				{doc.DLN = req.body.DLN;}
			if(typeof req.body.GstNumber != 'undefined')
				{doc.GST_Number = req.body.GstNumber;}
			if(typeof req.body.premise != 'undefined')
				{doc.Premise = req.body.premise;}
				// doc.updateTime = Date.now();
			if(typeof req.body.profileImage != 'undefined')
			{
				var imagePath = '/images/' + randomstring.generate(10) +'.png';
				doc.profile_image = imagePath;
				const base64Data = req.body.profileImage.replace(/^data:([A-Za-z-+/]+);base64,/, '');
				fs.writeFileSync(process.cwd() + '/public' + imagePath, new Buffer(base64Data, 'base64'), 'base64', function(err){});  
			}
			else{
			    doc.save();    	    	
			}  
			doc.save(function(err){
				if(err){
					logger.log.info(err);
					return res.send({
						statusCode: 400,
						message: "Something Went wrong."
					})
				}
				return res.send({
					statusCode: 200,
					message: "Profile Updated Successfully",   //"update store data successfully.",
					data: doc
				})				
			})
		})		
	}
} 

exports.owner_info = function(req, res){ 
  var firm_type = req.body.firm_type;
  var owner_list = req.body.owner_list;
  if(typeof req.body.ownerInfoId == 'undefined'){
		if(typeof firm_type == 'undefined'
			|| typeof owner_list == 'undefined')
		{
			console.log("All detail is not provided");
			return res.send({
				stateCode: 400,
				message: "All detail is not provided"
			})

		}
		else{
			var owner_info = new OwnerInfo();
			owner_info.Firm_Type = firm_type;
			owner_info.Owner_list = owner_list;
			owner_info.save(function(err){
				if(err){
					logger.log.info(err);
					return res.send({
						statusCode: 400,
						message: "Something Went wrong."
					})
				}
				console.log("Owner Info inserted successfully");
				return res.send({
					statusCode: 200,
					message: "Owner Info inserted successfully",
					data: owner_info
				});
			})
		}
	}
	else{
		OwnerInfo.findById({_id: req.body.ownerInfoId}, function(err, doc){
			if(err) throw err;
			doc.updateTime = Date.now();
			if(typeof req.body.firm_type != 'undefined')
				{doc.Firm_Type = req.body.firm_type;}
			if(typeof req.body.owner_list != 'undefined')
				{doc.Owner_list = req.body.owner_list;}
			doc.save(function(err){
				if(err){
					logger.log.info(err);
					return res.send({
						statusCode: 400,
						message: "Something Went wrong."
					})
				}
				console.log("Owner Info update successfully");
				return res.send({
					statusCode: 200,
					message: "Profile Updated Successfully",    //"Owner Info update successfully",
					data: doc
				});
			})
		})		
	}
}

exports.OperationalInfo = function(req, res){
	if(typeof req.body.operationalInfoId == 'undefined'){
		if(typeof req.body.store_start_time == 'undefined'
			|| typeof req.body.store_break_time_from == 'undefined'
			|| typeof req.body.store_break_time_to == 'undefined'
			|| typeof req.body.store_close_time == 'undefined'
			|| typeof req.body.holidy == 'undefined'
			|| typeof req.body.shift_day == 'undefinde'
			|| typeof req.body.number_of_staff == 'undefined'
			|| typeof req.body.number_of_pharmcist == 'undefined'
			|| typeof req.body.number_of_whole_seller == 'undefined'
			|| typeof req.body.home_delevary == 'undefined'
			|| typeof req.body.dicount_scheme == 'undefined'	
			)
		{
			console.log("required detail is not provided");
			return res.send({
				statusCode: 400,
				message: "required detail is not provided"
			})
		}
		else{
			var operational_info = new OperationalInfo();
			operational_info.Store_Start_Time = req.body.store_start_time;
			operational_info.Store_Break_Time_From = req.body.store_break_time_from;
			operational_info.Store_Break_Time_To = req.body.store_break_time_to;
			operational_info.Store_Close_Time = req.body.store_close_time;
			operational_info.Holidays = req.body.holidy;
			operational_info.Shift_Day = req.body.shift_day;
			operational_info.Number_Of_Staff = req.body.number_of_staff;
			operational_info.Number_Of_Pharmcist = req.body.number_of_pharmcist;
			operational_info.Number_Of_Whole_Sellers = req.body.number_of_whole_seller;
			operational_info.Home_Delevary = req.body.home_delevary;
			operational_info.Min_Order_Rs = req.body.min_order_rs;		
			operational_info.Discount_Scheme = req.body.dicount_scheme;
			operational_info.Flate_Discount = req.body.flate_discount;
			operational_info.Slab_Discount = req.body.slab_discount;		
			operational_info.save(function(err){
				if(err){
					logger.log.info(err);
					return res.send({
						statusCode: 400,
						message: "Something Went wrong."
					})
				}
				console.log("Operational Info inserted successfully");
				return res.send({
					statusCode: 200,
					message: "Operational Info inserted successfully",
					data: operational_info
				});
			});	
		}
	}
	else{
		OperationalInfo.findById({_id: req.body.operationalInfoId}, function(err, doc){
			if(err) throw err;
			doc.updateTime = new Date();
			if(typeof req.body.store_start_time != 'undefined')
				{doc.Store_Start_Time = req.body.store_start_time;}
			if(typeof req.body.store_break_time_from != 'undefined')
				{doc.Store_Break_Time_From = req.body.store_break_time_from;}
			if(typeof req.body.store_break_time_to != 'undefined')
				{doc.Store_Break_Time_To = req.body.store_break_time_to;}
			if(typeof req.body.store_close_time != 'undefined')
				{doc.Store_Close_Time = req.body.store_close_time;}
			if(typeof req.body.holidy != 'undefined')
				{doc.Holidays = req.body.holidy;}
			if(typeof req.body.shift_day != 'undefined')
				{doc.Shift_Day = req.body.shift_day;}
			if(typeof req.body.number_of_staff != 'undefined')
				{doc.Number_Of_Staff = req.body.number_of_staff;}
			if(typeof req.body.number_of_pharmcist != 'undefined')
				{doc.Number_Of_Pharmcist = req.body.number_of_pharmcist;}
			if(typeof req.body.number_of_whole_seller != 'undefined')
				{doc.Number_Of_Whole_Sellers = req.body.number_of_whole_seller;}
			if(typeof req.body.home_delevary != 'undefined')
				{doc.Home_Delevary = req.body.home_delevary;}
			if(typeof req.body.min_order_rs != 'undefined')
				{doc.Min_Order_Rs = req.body.min_order_rs;}	
			if(typeof req.body.dicount_scheme != 'undefined')
				{doc.Discount_Scheme = req.body.dicount_scheme;}
			if(typeof req.body.flate_discount != 'undefined')
				{doc.Flate_Discount = req.body.flate_discount;}
			if(typeof req.body.slab_discount != 'undefined')
				{doc.Slab_Discount = req.body.slab_discount;}		
			doc.save(function(err){
				if(err){
					logger.log.info(err);
					return res.send({
						statusCode: 400,
						message: "Something Went wrong."
					})
				}
				console.log("Operational Info update successfully");
				return res.send({
					statusCode: 200,
					message: "Profile Updated Successfully",     //"Operational Info update successfully",
					data: doc
				});
			});	
		})		
	}
}


exports.CreateAcount = function(req, res){
	var storeId = mongoose.Types.ObjectId(req.body.storeId);
	var ownerInfoId = mongoose.Types.ObjectId(req.body.ownerInfoId);
	var operationalInfoId = mongoose.Types.ObjectId(req.body.operationalInfoId);

	if(typeof storeId == 'undefined'
		|| typeof ownerInfoId == 'undefined'
		|| typeof operationalInfoId == 'undefined'
		|| typeof req.body.emailId == 'undefined'
		|| typeof req.body.phoneNumber == 'undefined'
		)
	{
		console.log("email_id or phone number is not provided");
		return res.send({
			statusCode: 400,
			message: "email_id or phone number is not provided"
		});
	}
	else{
		Store.findById({_id:storeId,}, function(err, doc){
			if(err){
					logger.log.info(err);
					return res.send({
						statusCode: 400,
						message: "Something Went wrong."
					})
				}
			else{
				if(doc == null || doc == ''){
					return res.send({
						statusCode: 400,
						message: "store not found."
					})
				}
				else{
					CreateAcount.findOne({Email_Id: req.body.emailId}, function(err, docs){
						if(err){
							console.log("if err ------>", err);
							logger.log.info(err);
							return res.send({
								statusCode: 500,
								message: "Something went wrong"
							})
						}
						if(docs == null){
							customer.findOne({email_id: req.body.emailId}, function(err,cust){
								if(err){
									console.log("if err ------>", err);
									logger.log.info(err);
									return res.send({
										statusCode: 500,
										message: "Something went wrong"
									})
								}
								if(cust == null){
									var store_name = doc.Pharmacy_name;
									var storeStatus;
									doc.Store_status = true;
									doc.save(function(err){
										if(err) throw err;
										storeStatus = doc.Store_status;
										var create_acount = new CreateAcount();
										create_acount.Store_Id = storeId;
										create_acount.OwnerInfo_Id = ownerInfoId;
										create_acount.OperationalInfo_Id = operationalInfoId;
										create_acount.Email_Id = req.body.emailId;
										create_acount.Phone_Number = req.body.phoneNumber;
					                    create_acount.store_fcm = req.body.fcm;
										create_acount.save(function(err){
											if(err){
												console.log("if err ----->", err);
												logger.log.info(err);
												return res.send({
													statusCode: 500,
													message: "something went wrong"
												})
											}
											else{
											    var senderEmail = config.email_m;
											    var senderPassword = config.password_m;
											    var fromJob = "OrderMedicine"; 
											    // node_mailer.nodeMailer
									            var templateData = {};
									            templateData.accountAprove ="";
											    var requestObj = {
											    	templateName : "welcome-pharmacy",
											    	to :req.body.emailId,
											    	subject : 'Welcome to OrderMedicine',
											    	templateData : templateData,
											    	senderEmail : senderEmail,
											    	senderPassword: senderPassword,
											    	fromJob : fromJob
											    }
											     node_mailer.nodeMailer(requestObj,function(err,data){
											    	if(err){console.log("====ERROR====",err); logger.log.info(err);}
											    	console.log("====DATA====",data);
											    	//callback();
											    });																	
												return res.send({
													statusCode: 200,
													message: "Create Acount successfully",
													data: {Account_id: create_acount.id, store_id: storeId, email: req.body.emailId, Pharmacy_name: store_name, is_active: 0, fcm: req.body.fcm, access: "retailer"}
												});												
											}
										});		
									})
								}
								else{
									console.log("this email is already register in customer");
									return res.send({
										statusCode: 400,
										message: "This Email is already registerd"
									})
								}														
							})
						}
						else{
							console.log("this email is already register in store");
							return res.send({
								statusCode: 400,
								message: "This Email is already registerd"
							})
						}
					})					
				}	
			}
		})
	}
}

exports.AddVacancy = function(req, res){
	var AcountId = mongoose.Types.ObjectId(req.body.AcountId);
	if(typeof AcountId == 'undefined'
		|| typeof req.body.title_of_vacancy == 'undefined'
		|| typeof req.body.for_position == 'undefined'
		|| typeof req.body.number_of_vacancy == 'undefined'
		|| typeof req.body.total_experience_needed == 'undefined'
		|| typeof req.body.salary_range == 'undefined'
		|| typeof req.body.qualification == 'undefined'
		|| typeof req.body.joining_on == 'undefined'
		|| typeof req.body.age_limit == 'undefined'
		|| typeof req.body.gender == 'undefined'
		//|| typeof req.body.description == 'undefined'
		)
	{
		console.log('All detail is not provided');
		res.statusCode = 400;
		return res.send({
			statusCode: 400,
			message: "All detail is not provided"
		})
	}
	else{
		var vacancy_id;
		AddVacancy.count({AcountId: AcountId}, function(err, cot){
			if(err) throw err;
			if(cot < 5){
				var add_vacancy = new AddVacancy();
				add_vacancy.AcountId = AcountId;
				add_vacancy.storeName = '';
				add_vacancy.Title_Of_Vacancy = req.body.title_of_vacancy;
				add_vacancy.For_Position = req.body.for_position;
				add_vacancy.Number_Of_Vacancy = req.body.number_of_vacancy;
				add_vacancy.Gender = req.body.gender;
				add_vacancy.Total_Experience_Needed = req.body.total_experience_needed;
				add_vacancy.Salary_Range = req.body.salary_range;
				add_vacancy.Qualification = req.body.qualification;
				add_vacancy.Joining_On = req.body.joining_on; 
				add_vacancy.Age_Limit = req.body.age_limit;
				add_vacancy.Description = req.body.description;
				add_vacancy.applicant_count = 0;
				add_vacancy.CreateTime = Date.now();
				add_vacancy.save(function(err){
					if(err){
						console.log("if err ------>", err);
						logger.log.info(err);
						return res.sedn({
							statusCode: 500,
							message: "Something Went wrong"
						})
					}
					else{
						vacancy_id = add_vacancy.id;
						var Title_Of_Vacancy = add_vacancy.Title_Of_Vacancy;
						var For_Position = add_vacancy.For_Position;
						var Number_Of_Vacancy =  add_vacancy.Number_Of_Vacancy;
						var Total_Experience_Needed = add_vacancy.Total_Experience_Needed;
						var Salary_Range = add_vacancy.Salary_Range;
						var Qualification = add_vacancy.Qualification;
						var Joining_On = add_vacancy.Joining_On;
						var Age_Limit = add_vacancy.Age_Limit;						
						AddVacancy.findById({_id: add_vacancy.id}, function(err, docs){
							if(err){
								console.log("if err ------>", err);
								logger.log.info(err);
								return res.sedn({
									statusCode: 500,
									message: "Something Went wrong"
								})
							}
							else{
								CreateAcount.findById({_id: docs.AcountId}, function(err, cat){
									if(err){
										console.log("if err ------>", err);
										logger.log.info(err);
										return res.sedn({
											statusCode: 500,
											message: "Something Went wrong"
										})								
									}
									if(cat == null){
										console.log("store is not get");
										return res.send({
											statusCode: 400,
											message: "store is not geting"
										})
									}
									else{
										Store.findById({_id: cat.Store_Id}, function(err, doc){
											if(err){
												console.log("if err ------>", err);
												logger.log.info(err);
												return res.sedn({
													statusCode: 500,
													message: "Something Went wrong"
												})
											}
											else{	
												docs.storeName = doc.Pharmacy_name;
												docs.save();
												var store_name = doc.Pharmacy_name;
												var store_pincode = doc.Pin_code;
												var city_Id = doc.City;
												SmEmployee.find({User_PIncode: store_pincode}, function(err, emp_detail){
													if(err){
														console.log("if err ------>", err);
														logger.log.info(err);
														return res.sedn({
															statusCode: 500,
															message: "Something Went wrong"
														})
													}
													else{
														//var emps_email = [];
														city.findOne({city_id: city_Id}, function(err, city_n){
															var cityName = city_n. city_name
															async.each(emp_detail, function(empd, callback){
																if(empd == null){
																	callback();
																}
																else{
																	var	emps_email =empd.User_Email;
																    var templateData = {};
																    templateData.jobLocation = cityName;
																    templateData.Gender = add_vacancy.Gender;
																    templateData.Description = add_vacancy.Description;
																    templateData.Title_Of_Vacancy = add_vacancy.Title_Of_Vacancy;
																    templateData.For_Position = add_vacancy.For_Position;
																    templateData.Number_Of_Vacancy =  add_vacancy.Number_Of_Vacancy;
																    templateData.Total_Experience_Needed = add_vacancy.Total_Experience_Needed;
																    templateData.Salary_Range = add_vacancy.Salary_Range;
																    templateData.Qualification = add_vacancy.Qualification;
																    templateData.Joining_On = add_vacancy.Joining_On;
																    templateData.Age_Limit = add_vacancy.Age_Limit;
																    templateData.vacancy_id = vacancy_id;

																    var senderEmail = config.email;
																    var senderPassword = config.password;
																    var fromJob = "Search4PharmacyJobs"; 
																    // node_mailer.nodeMailer

																    var requestObj = {
																    	templateName : "new-job-alert",
																    	to :emps_email,
																    	subject : "Pharmacy Job",
																    	templateData : templateData,
																    	senderEmail : senderEmail,
																    	senderPassword: senderPassword,
																    	fromJob : fromJob
																    }
																     node_mailer.nodeMailer(requestObj,function(err,data){
																     	if(err){
																     		console.log("====ERROR====",err);
																     		logger.log.info(err);
																     		callback();
																     	}
																     	else{
																	    	console.log("====DATA====",data);
																	    	callback();															     		
																     	}
																    });															    
																}
															}, function(err){
																if(err) throw err;
															    return res.send({
																	statusCode: 200,
																	message: "Vacancy Upload Successfully"
																})															
															})
														})
													}
												})
											}												
										})
									}
								})
							}
						})										
					}
				})
			}
			else{
				return res.send({
					statusCode: 400,
					message: "You can not post Job morethen 5 job"
				})				
			}
		})
	}
}

exports.store_login = function(req, res){
	//var storeId = mongoose.Types.ObjectId(req.body.storeId);
	var email_id = req.body.email_id;
	if( typeof email_id == 'undefined'
		|| typeof req.body.store_id == 'undefined')
	{
		console.log("storeId or email_id is not provided");
		return res.send({
			statusCode: 400,
			message: "storeId or email_id is not provided"
		})
	}
	else{
		var store = [];
		CreateAcount.findOne({Email_Id: email_id, Store_Id: req.body.store_id}, function(err, doc){
			if(err){
				console.log("if err ------>", err);
				logger.log.info(err);
				return res.send({
					statusCode: 500,
					message: "Something Went wrong"
				})
			}
			else{
				if(!doc){
					staff.findOne({customer_email: email_id, customer_store_id: req.body.store_id}, function(err, cust){
						if(err){
							console.log("if err ------>", err);
							logger.log.info(err);
							return res.send({
								statusCode: 500,
								message: "Something Went wrong"
							})
						}
						else{
							if(cust == null){
								return res.send({
									statusCode: 400,
									message: "email id and store is doesn't match"
								})				
							}
							else{
								cust.customer_fcm = req.body.fcm;
								cust.save(function(err){
									Store.findById({_id: req.body.store_id}, function(err, docs){
										if(err){
											console.log("if err ----->", err);
											logger.log.info(err);
											return res.send({
												statusCode: 500,
												message: "something went wrong"
											})
										}
										else{
											CreateAcount.findOne({Store_Id: req.body.store_id}, function(err, store_d){
												if(err) throw err;
												var obj = {};
												obj.accountid = store_d.id;
												obj.store_id = cust.customer_store_id;
												obj.Pharmacy_name = docs.Pharmacy_name;
												obj.email = cust.customer_email;
												//obj.no_of_jprofile_views = cust.no_of_jprofile_views;
												obj.Phone_Number = cust.customer_number;
												obj.is_active = store_d.is_active;
												obj.fcm = cust.customer_fcm;
												obj.access = "staff";
												return res.send({
													statusCode: 200,
													message: "store login successfully",
													data: obj
													//data: {store_id: docs.id, accountid: accountid, Pharmacy_name: docs.Pharmacy_name, email: req.body.email_id, fcm: req.body.fcm}
												})
											})
										}
									})
								})								
							}
						}
					})
				}
				else{
					var accountid = doc.id;
					doc.store_fcm = req.body.fcm;
					doc.save(function(err){
						Store.findById({_id: req.body.store_id}, function(err, docs){
							if(err){
								console.log("if err ----->", err);
								logger.log.info(err);
								return res.send({
									statusCode: 500,
									message: "something went wrong"
								})
							}
							else{
								if(err) throw err;
								var obj = {};
								obj.accountid = doc._id;
								obj.store_id = doc.Store_Id;
								obj.Pharmacy_name = docs.Pharmacy_name;
								obj.email = doc.Email_Id;
								obj.no_of_jprofile_views = doc.no_of_jprofile_views;
								obj.Phone_Number = doc.Phone_Number;
								obj.is_active = doc.is_active;
								obj.fcm = req.body.fcm; 
								// obj.OwnerInfo_Id = doc.OwnerInfo_Id;
								// obj.OperationalInfo_Id = doc.OperationalInfo_Id;
								obj.access = "retailer";
								//var store_login = "store login",						
							    //store.push({store_id: docs.id},{Pharmacy_name: docs.Pharmacy_name}, {Email: docs.Email}, {fcm: req.body.fcm});
								return res.send({
									statusCode: 200,
									message: "store login successfully",
									data: obj
									//data: {store_id: docs.id, accountid: accountid, Pharmacy_name: docs.Pharmacy_name, email: req.body.email_id, fcm: req.body.fcm},
									//data1: {_id: doc._id, Store_Id: doc.Store_Id, OwnerInfo_Id: doc.OwnerInfo_Id, OperationalInfo_Id: doc.OperationalInfo_Id, Email_Id: doc.Email_Id, no_of_jprofile_views: doc.no_of_jprofile_views, Phone_Number: doc.Phone_Number}
								})
							}
						})
					})
				}			
			}
		})
	}
}

exports.getallvacancy = function(req, res){

	var AcountId = mongoose.Types.ObjectId(req.body.AcountId);
	if(typeof AcountId == 'undefined')
	{
		console.log("not provided acount_id");
		return res.send({
			statusCode: 400,
			message: "not provided acount_id"
		})
	}
	else{
		AddVacancy.find({AcountId: AcountId}, function(err, doc){
			if(err) throw err;
			console.log("get all vacancy");
			return res.send({
				statusCode: 200,
				message: "get all vacancy",
				data : doc
			})
		})
	}
}

exports.GetEmpInfoByMnumber = function(req, res){
	var phone_number = req.body.phone_number;
	if(typeof phone_number == 'undefined'){
      console.log("not provided phone number")
      return res.send({
      	statusCode: 400,
      	message: "not provided phone number"
      })
	}
	else{
		 SmEmployee.findOne({User_Contact: phone_number}, function(err, doc){
			if(err) throw err;
			if(doc == null)
			{
				console.log("this type of employee is not exists in our database");
				return res.send({
					statusCode: 400,
					message: "This number is not registered with any staff. Please enter other number of your staff registered with us."
				})
			}
			else{
				console.log("get employee detail by phone number");
				return res.send({
					statusCode: 200,
					message: "get employee detail by phone number",
					data: doc
				})
			}
		})
	}
}

// exports.GetEmpInfoByMnumber = function(req, res){
// 	var phone_number = req.body.phone_number;
// 	if(typeof phone_number == 'undefined'){
//       console.log("not provided phone number")
//       return res.send({
//       	statusCode: 400,
//       	message: "not provided phone number"
//       })
// 	}
// 	else{
// 		  var token = req.headers['x-access-token'];
// 		  if (!token) return res.status(401).send({ auth: false, message: 'No token provided.' });
		  
// 		  jwt.verify(token, config.secret, function(err, decoded) {
// 		    if (err) return res.status(500).send({ auth: false, message: 'Failed to authenticate token.' });

// 		 SmEmployee.findOne({User_Contact: phone_number}, function(err, doc){
// 			if(err) throw err;
// 			if(doc == null)
// 			{
// 				console.log("this type of employee is not exists in our database");
// 				return res.send({
// 					statusCode: 400,
// 					message: "this type of employee is not exists in our database"
// 				})
// 			}
// 			else{
// 				console.log("get employee detail by phone number");
// 				return res.send({
// 					statusCode: 200,
// 					message: "get employee detail by phone number",
// 					data: doc
// 				})
// 			}
// 		})
// 	})
// 		}
// }




exports.SearchMedicine = function(req, res){
  // var medicine_name = req.body.name;
  // console.log('{name: medicine_name}',{name: medicine_name})
//   medicine_data.aggregate([{ 	
//     $match: {
//         "name": {
//             $regex: req.body.name,
//             $options: 'i'
//         }
//     }

// },{ $orderby :{ name : 1}}],function(err, doc){if(err) throw err; return res.send({ statusCode: 200, data: doc})}
// )
  // var query = { name: new RegExp('^' + req.body.name, "i")};
  // var query = { name: { $regex: req.body.name, $options:"i" }};
  // var query = { name: { $regex: new RegExp('^' + medicine_name + '$', "i") }};
  // var query = regexp().start(req.body.name);
  if(typeof req.body.name == 'undefined')
  {
  	console.log("medicine name ia not provided");
  	return res.send({
  		statusCode: 400,
  		message: "medicine name ia not provided"
  	})
  }
  else{
	  var query = { name: { $regex: '^' + req.body.name, $options:"i" }};
	  medicine_data.find(query, function(err, doc){
	  	// console.log("medicine_name ",medicine_name);
	  	if(err) throw err;
	  	if(doc == ''){
	  		console.log("this type of medicine_data is not find");
	  		return res.send({
	  			statusCode: 400,
	  			message: "No medicine by this name found. Please check spelling and find again"
	  		})
	  	}
	  	if(doc == null){
	  		console.log("doc is null");
	  		return res.send({
	  			statusCode: 400,
	  			message: "doc is null"
	  		})
	  	}
	  	else{
	  		return res.send({
	  			statusCode: 200,
	  			message: "get doc value",
	  			data:doc
	  		})
	    }
	  })
  }
}

exports.search_by_salt = function(req, res){
	if(typeof req.body.salt == 'undefined')
	{
		console.log("salt name is not provided");
		return res.send({
			statusCode: 400,
			message: "salt name is not provided"
		})
	}
	else{
		medicine_data.find({strength:{$elemMatch:{salt: { $regex: '^' + req.body.salt, $options:"i" }}}}, function(err, doc){
		  	if(err) throw err;
		  	if(doc == '' || doc == null){
		  		console.log("this type of medicine_data is not find");
		  		return res.send({
		  			statusCode: 400,
		  			message: "No medicine by this name found. Please check spelling and find again"
		  		})
		  	}
		  	else{
		  		return res.send({
		  			statusCode: 200,
		  			message: "get doc value",
		  			data:doc
		  		})
		    }
		})
	}
}


exports.search_salt_by_medicineid = function(req, res){
    if(typeof req.body.medicine_id  ==  'undefined')
    {
        console.log("medicine id is not provided");
        return res.send({
            statusCode: 400,
            message: "medicine id is not provided"
        })
    }
    else{
        var data;
        var array = [];
        medicine_data.findById({_id: req.body.medicine_id}, function(err, med){
            if(err) throw err;
            var med_strength = med.strength;
            var strength_length = med_strength.length;
            // console.log("med_strength  >>>>>", med_strength);
            // console.log("strength_length >>>>>>", strength_length);
           // var query = {strength: {"$size": strength_length,  "$all": med_strength}};
            medicine_data.find().where("strength",med_strength).exec(function(err, doc){
                if(err) throw err;
                if(doc == null)
                {
                    console.log('doc is null 1221');
                }
                if(doc == '')
                {
                    return res.send({
                        statusCode: 400,
                        message: "Found empty data  1230"
                    })
                }
                else{
                	var mdata = [];
                	async.each(doc, function(datam, callback){
                		if(datam._id  == req.body.medicine_id){
 						    callback();
                		}
                		else{
                			mdata.push(datam);
                			callback();
                		}
                	}, function(err){
                		if(err){
                			console.log("if err ----------->",err);
                			logger.log.info(err);
                			return res.send({
                				statusCode: 500,
                				message: "something went wrong"
                			})
                		}
                		else{
                			if(mdata == ""){
								console.log("empty medicine_data");
								return res.send({
									statusCode: 400,
									message: "No alternate found for this medicine"
								})
                			}
                			if(mdata == null){
								console.log("medicine is not found");
								return res.send({
									statusCode: 400,
									message: "No alternate found for this medicine"
								})                				
                			}
                			else{
								return res.send({
									statusCode: 200,
									message: "medicine found",
									data: mdata
								})                				
                			}
                		}
                	})
                }   
            })
        })
    } 
}

exports.search_medicine_by_salt = function(req, res){
	var strength = req.body.strength;
	var array = [];
	async.each(strength, function(str, callback){
		if(str == null)
		{
			callback();
		}
		else{
			array.push(str.salt);
			callback();
            // console.log(array);
		}
	});

    // console.log(array);
    // console.log(array.length);
	if(array == null)
	{
		console.log("salt of array is not provided");
		return res.send({
			statusCode: 400,
			message: "salt of array is not provided"
		})
	}
	else{

		// console.log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  ",array);
		medicine_data.find({'strength.salt': {$all:array}}, function(err, doc){
		// console.log("doc",doc);
		// {strength: {$elemMatch:{salt: str.salt}, $size: strength_length}}
		// {"strength.salt":{$all:array, $eq: array.length}
		//{'strength.salt': {$size: array.length}}

			if(err) throw err;
			var medData = [];
			async.each(doc, function(data, callback){
				if((data.strength).length == array.length){
					medData.push(data);
					callback();
				}else{
						callback();
					}
			}, function(err){
				if(err){
					console.log("if err ----->", err);
					logger.log.info(err);
					return res.send({
						statusCode: 500,
						message: "something went wrong"
					})
				}
				if(medData == null){
					console.log("No medicine found for this combinations of salt");
					return res.send({
						statusCode: 400,
						message: "No medicine found for this combinations of salt"
					})
				}
				if(medData == ''){
					console.log("No medicine found for this combinations of salt");
					return res.send({
						statusCode: 400,
						message: "No medicine found for this combinations of salt"
					})
				}
				else{
					console.log("medicine found for this combinations of salt");
					return res.send({
						statusCode: 200,
						data: medData
					})
				}				
			});
		})
	}	
}

exports.SearchParma = function(req, res){
	//var query = { name: { $regex: '^' + req.body.name, $options:"i" }};
	var query = '^' + req.body.query;  //.replace(/\s/g,'');
	Store.find({ Pharmacy_name: { $regex: query, $options: "i" },"Store_status":true }, function(err, docs){
		if(err) throw err;
		if(docs == null)
		{
			console.log("this type of pharma name is not in our database");
			return res.send({
				statusCode: 400,
				message: "No pharmacy by this name with us. Please check spelling and try again..."
			})
		}
		else{
			var store_detail =[];
			async.each(docs, function(doc, callback){
				//  || doc.id == config.defaultStoreid
				if(doc == null){
					callback();
				}
				else{
					state.findOne({state_id: doc.State}, function(err, state_n){
						city.findOne({city_id: doc.City}, function(err, city_n){
					 		var userStoreDistance = geodist(
			                {lat: req.body.latitude, lon: req.body.longitude}, 
			                {lat: doc.latitude, lon: doc.longitude}, 
			                {exact: true, unit: 'meters'});					
							// var store_detail =[];
							var obj = {};
							obj.distance = userStoreDistance;
							obj._id = doc.id;
							obj.pharmacy_name = doc.Pharmacy_name;
							obj.address = doc.Address;
							obj.landmark = doc.Landmark;
							obj.area = doc.Area;
							obj.state = state_n.state;
							obj.city = city_n.city_name;
							obj.pin_code = doc.Pin_code;
							CreateAcount.findOne({Store_Id: doc.id}, function(err, cat){
								if(err) throw err;
								OperationalInfo.findById({_id: cat.OperationalInfo_Id}, function(err, opi){
									// if(err) throw err;
									// if(opi == null){
									// 	store_detail.push(obj);
									// 	callback();
									// }
									// else{
									// if(opi.Home_Delevary == "yes"){
										obj.home_delevary = opi.Home_Delevary;
										obj.min_order_rs = opi.Min_Order_Rs;
										obj.discount = opi.Discount_Scheme;
										obj.flate_discount = opi.Flate_Discount;
										obj.slab_discount = opi.Slab_Discount;
										store_detail.push(obj);
										callback();
									// }
								})
							})							
						})
					})
				}
			}, function(err){
				if(err) throw err;
				if(store_detail == null || store_detail == ''){
					return res.send({
						statusCode: 400,
						message: "No pharmacy by this name with us. Please check spelling and try again.",
					})					
				}
				else{
					return res.send({
						statusCode: 200,
						message: "get store detail Successfully",
						data: store_detail
					})
				}				
			})
		}
	})
}

 
//-----------------------------------------------------------------------------------------------------------------------------------------
// exports.soft_delete_mystaff = function(req, res){
// 	var account_id = req.body.account_id;
// 	var employee_id = req.body.employee_id;
// 	if(typeof account_id == 'undefined'
// 		|| typeof employee_id == 'undefined')
// 	{
// 		console.log("acount_id and employee_id is not provided")
// 		return res.send({
// 			statusCode: 400,
// 			message: "acount_id and employee_id is not provided"
// 		})
// 	}
// 	else{
// 		my_staff.findOne({account_id: account_id, employee_id: employee_id}, function(err, doc){
// 			if(err) throw err;
// 			if(doc == null)
// 			{
// 				console.log("this type of account is not exists");
// 				return res.send({
// 					statusCode: 400,
// 					message: "this type of account is not exists"
// 				})
// 			}
// 			else{
// 				// if(doc.employee_id != employee_id)
// 				// {
// 				// 	console.log("employee_id is not match with account_id");
// 				// 	return res.send({
// 				// 		statusCode: 400,
// 				// 		message: "employee_id is not match with account_id"
// 				// 	})
// 				// }
// 				// else{
// 					console.log("soft deleted employee");
// 					doc.my_staff_status = false;
// 					doc.save(function(err){
// 						if(err) throw err;
// 						return res.send({
// 						    statusCode:200,
// 						    message: "employee deleted successfully",
// 						    data: doc
// 					    })
// 					})
// 				// }
// 			}
// 		})
// 	}		
// }
//-----------------------------------------------------------------------------------------------------------------------------------------


//--------------------------------------------------------------------------------------------------------------------------------------
// exports.MyStaff = function(req, res){
// 	var account_id = req.body.account_id;
// 	var employee_id = req.body.employee_id;
// 	if(typeof account_id == 'undefined'
// 		|| typeof employee_id == 'undefined')
// 	{
// 		console.log("acount_id and employee_id is not provided")
// 		return res.send({
// 			statusCode: 400,
// 			message: "acount_id and employee_id is not provided"
// 		})
// 	}
// 	else{
// 		my_staff.findOne({account_id: account_id, employee_id: employee_id}, function(err, doc){
// 			if(err) throw err;
// 			if(doc == null)
// 			{
// 				var mystaff = new my_staff();
// 				mystaff.account_id = account_id;
// 				mystaff.employee_id = employee_id;
// 				mystaff.create_date = Date.now();
// 				mystaff.update_date = Date.now();
// 				mystaff.save(function(err){
// 					if(err) throw err;
// 					console.log("my_staff created successfully");
// 					return res.send({
// 						statusCode: 200,
// 						message: "my_staff created successfully",
// 						data:mystaff
// 					})
// 				})
// 			}
// 			else{
// 				if(doc.my_staff_status == false)
// 				{
// 					doc.my_staff_status = true;
// 					doc.save(function(err){
// 						if(err) throw err;
// 						console.log("employee status is true");
//                         return res.send({
//                             statusCode: 200,
//                             message: "employee status is true"
//                          })
// 					})
// 				}
// 				else{
// 					 console.log("you already added this staff account");
// 					 return res.send({
// 						statusCode: 200,
// 						message: "you already added this staff account"
// 					})
// 				}
// 			}
// 		})
// 	}		
// } --------------------------------------------------------------------------------------------------------------------------------------

exports.emp_detail_by_accountid = function(req, res){
	var account_id = req.body.account_id;
	my_staff.find({account_id: account_id, my_staff_status: false}, function(err, doc){
		if(err) throw err;
		if(doc == '')
		{
			console.log("No employee found");
			return res.send({
				statusCode: 400,
				message: "No employee found"
			})
		}
		else{
			var employee_data =[];
			async.each(doc, function(file, callback){
				if(file == null)
				{
					callback();
				}
				else{
					SmEmployee.findOne({_id: file.employee_id}, function(err, edoc){
						if(err) throw err;
						if(edoc == null)
						{
							callback();
						}
						else{
							employee_data.push(edoc);
							callback();
						}
					})                      
				}
			},
			function(err){
				if(err) throw err;
				console.log("get emp detail successfully");
				return res.send({
					statusCode: 200,
					message: "get emp detail successfully",
					data: employee_data
				})
			})
		}
	})
}



//function for mobile number verification by otp


// exports.match_otp_email = function(req, res){
// 	if(typeof req.body.employee_id == 'undefined'
// 		|| typeof req.body.email_id == 'undefined'
// 		|| typeof req.body.otp == 'undefined'
// 		)
// 	{
// 		console.log("email,employee_id  or otp is not provided")
// 		return res.send({
// 			statusCode: 400,
// 			message: "Email or OTP is not provided."
// 		})
// 	}
// 	else{
// 		email_veryfication.findOne({verify_email: req.body.email_id}, function(err, doc){
// 			if(err) throw err;
// 			if(doc.otp != req.body.otp)
// 			{
// 				console.log("otp is not match...... pls enter right otp");
// 				return res.send({
// 					statusCode: 400,
// 					message: "Please check your OTP."
// 				})
// 			}
// 			else{
// 				console.log("otp is match.");
// 				SmEmployee.findById({_id: req.body.employee_id}, function(err, doc){
// 					if(err) throw err;
// 					if(doc == null)
// 					{
// 						console.log("employee_id is not exists");
// 						return res.send({
// 							statusCode: 400,
// 							message: "You are not registered with us."
// 						})
// 					}
// 					else{
// 						doc.User_Email = req.body.email_id;
// 						doc.save(function(err){
// 							if(err) throw err;

// 							console.log("email_id update successfully");
// 							return res.send({
// 								statusCode: 200,
// 								message: "Email Address update successfully."
// 							})
// 						})
// 					}
// 				})
// 			}
// 		})
// 	}
// }


exports.get_store_detail = function(req, res){
	if(typeof req.body.add_vacancy_id == 'undefined')
	{
		console.log("add_vacancy_id is not provided");
		return res.send({
			statusCode: 400,
			message: "add_vacancy_id is not provided"
		})
	}
	else{
		AddVacancy.findById({_id: req.body.add_vacancy_id}, function(err, vacancydoc){
			if(err) throw err;
			CreateAcount.findById({_id: vacancydoc.AcountId}, function(err, acountdoc){
				if(err) throw err;
				Store.findById({_id: acountdoc.Store_Id}, function(err, storedoc){
					if(err) throw err;
					return res.send({
						stataus: 200,
						message: "get store data successfully",
						data: storedoc
					})
				})
			})
		})
	}
}


exports.get_order_list = function(req, res){
	var data=[];
	if(typeof req.body.customer_id != 'undefined')
	{
		customer_order.find({customer_id: req.body.customer_id},{},{ sort :{ create_date : -1}}, function(err, doc){
			if(err) throw err;
			if(doc == '')
			{
				return res.send({
					statusCode: 400,
					message: "No order found"
				})
			}
			if(doc == null)
			{
				return res.send({
					statusCode: 400,
					message: "Order list is Empty"
				})				
			}
			async.each(doc, function(file, callback){
				if(file == null)
				{
					callback();
				}
				else{ 
					var create_account_phone_number;
					CreateAcount.findOne({Store_Id: file.Store_Id}, function(err, datam){
						if(err) throw err;
						if(datam){
							create_account_phone_number = datam.Phone_Number;
							var obj = {};
							obj.id = file._id;
							obj.create_date = file.create_date;
							obj.date = file.update_date;
							obj.customer_orders_status = file.customer_orders_status;
							obj.customer_id = file.customer_id;
							obj.store_id = file.Store_Id;
							obj.store_name = file.store_name;
							obj.comment = file.comment;
							obj.order_item_count = file.order_item_count;
							obj.order_price = file.order_price;
							obj.update = file.update;
							obj.cart_item_list = file.cart_item_list;
							obj.prescription = file.prescription;
							obj.order_no = file.order_no;
							obj.store_number = create_account_phone_number;
							obj.final_bill_amount = file.final_bill_amount;
							var bj = {};

							customer_delivery_detail.findById({_id: file.deliver_address_id}, function(erer, cdd){
							    bj.id = cdd.id;
								bj.customer_id = cdd.customer_id;
								bj.name = cdd.name;
								bj.email_id = cdd.email_id;
								bj.mobile_number = cdd.mobile_number;
								bj.address1 = cdd.address1;
								bj.address2 = cdd.address2;
								bj.landmark = cdd.landmark;
								bj.city = cdd.city;
								bj.state = cdd.state;
								bj.pin_code = cdd.pin_code;
								obj.deliver_address = bj;
								data.push(obj);
		                    	callback();
							})
						}
						else{
							callback();
						}
					})
				}
			},
			function(err){
				if(err) throw err;
					data.sort(function (a, b) {
					  return new Date(b.create_date) - new Date(a.create_date);
					});				
				console.log("get detail successfully");
				return res.send({
					statusCode: 200,
					message: "get detail successfully",
					customer_order_detail: data
				})
			})

		})
	}
	else if(typeof req.body.store_id != 'undefined')
	{
		customer_order.find({Store_Id: req.body.store_id},{},{ sort :{ create_date : -1}}, function(err, doc){
			if(err) throw err;
			if(doc == null)
			{
				return res.send({
					statusCode: 400,
					message: "No order Found"
				})				
			}
			async.each(doc, function(file, callback){
				if(file == null)
				{
					callback();
				}
				else{ 

					var create_account_phone_number;
					CreateAcount.findOne({Store_Id: file.Store_Id}, function(err, datam){
						if(err) throw err;
						 create_account_phone_number = datam.Phone_Number;

						var obj = {};
						obj.id = file._id;
						obj.create_date = file.create_date;
						obj.date = file.update_date;
						obj.customer_orders_status = file.customer_orders_status;
						obj.customer_id = file.customer_id;
						obj.store_id = file.Store_Id;
						obj.store_name = file.store_name;
						obj.comment = file.comment;
						obj.order_item_count = file.order_item_count;
						obj.order_price = file.order_price;
						obj.update = file.update;
						obj.cart_item_list = file.cart_item_list;
						obj.prescription = file.prescription;
						obj.order_no = file.order_no;
						obj.store_number = create_account_phone_number;
						obj.final_bill_amount = file.final_bill_amount;
						//obj.store_phone_number = store_phone_number;
						var bj = {};
						customer_delivery_detail.findById({_id: file.deliver_address_id}, function(err, cdd){
						    bj.id = cdd.id;
							bj.customer_id = cdd.customer_id;
							bj.name = cdd.name;
							bj.email_id = cdd.email_id;
							bj.mobile_number = cdd.mobile_number;
							bj.address1 = cdd.address1;
							bj.address2 = cdd.address2;
							bj.landmark = cdd.landmark;
							bj.city = cdd.city;
							bj.state = cdd.state;
							bj.pin_code = cdd.pin_code;
							obj.deliver_address = bj;
							data.push(obj);
	                    	callback();
						})
					})
				}
			},
			function(err){
				if(err) throw err;
				console.log("get detail successfully");
				data.sort(function (a, b) {
				  return new Date(b.create_date) - new Date(a.create_date);
				});
				return res.send({
					statusCode: 200,
					message: "get detail successfully",
					customer_order_detail: data
				})
			})					
		})		
	}
	else{
		console.log("store_id or customer_id is not provided");
		return res.send({
			statusCode: 200,
			message: "store_id or customer_id is not provided"
		})
	}
}

exports.get_order_detail_by_oreder_id = function(req, res){
    var cart_list;
    //var data = [];
    var order_id = req.body.order_id;
    if(typeof order_id == 'undefined')
    {
    	console.log("order id is not provided");
    	return res.send({
    		statusCode: 400,
    		message: "order id is not provided"
    	})
    }
    else{
    	customer_order.findById({_id: order_id}, function(err, file){
    		if(err){
    			console.log("if err ----->", err);
    			logger.log.info(err);
    			return res.send({
    				statusCode: 500,
    				message: "something went wrong."
    			})
    		}
			var obj = {};
			obj.id = file._id;
			obj.date = file.update_date;
			obj.customer_orders_status = file.customer_orders_status;
			obj.customer_id = file.customer_id;
			obj.store_id = file.Store_Id;
			obj.store_name = file.store_name;
			obj.comment = file.comment;
			obj.order_item_count = file.order_item_count;
			obj.order_price = file.order_price;
			obj.final_bill_amount = file.final_bill_amount;
			obj.order_no = file.order_no;
			obj.update = file.update;
			obj.cart_item_list = file.cart_item_list;
			obj.prescription = file.prescription;
			var bj = {};

			customer_delivery_detail.findById({_id: file.deliver_address_id}, function(err, cdd){
	    		if(err){
	    			console.log("if err ----->", err);
	    			logger.log.info(err);
	    			return res.send({
	    				statusCode: 500,
	    				message: "something went wrong."
	    			})
	    		}				
				bj.id = cdd.id;
				bj.customer_id = cdd.customer_id;
				bj.name = cdd.name;
				bj.email_id = cdd.email_id;
				bj.mobile_number = cdd.mobile_number;
				bj.address1 = cdd.address1;
				bj.address2 = cdd.address2;
				bj.landmark = cdd.landmark;
				bj.city = cdd.city;
				bj.state = cdd.state;
				bj.pin_code = cdd.pin_code;
				obj.deliver_address = bj;
		        return res.send({
		            statusCode: 200,
		            data: obj
		        })						
			})
    	})
    }
}

// Retailer reject order api
exports.cancel_order = function(req, res){
	var order_id = req.body.order_id;
	if(typeof order_id == 'undefined'
		|| typeof req.body.comment == 'undefined'
		|| typeof req.body.status == 'undefined'
		)
	{
		console.log("order id or comment  is not provided");
		return res.send({
			statusCode: 400,
			message: "Please try after some time." //"order id or comment  is not provided"
		})
	}
	else{
		customer_order.findById({_id: order_id}, function(err, doc){
			if(err) throw err;
			doc.customer_orders_status = req.body.status;
			doc.comment = req.body.comment;
			doc.update_date = Date.now();
			doc.save(function(err){
				if(err) throw err;
				customer.findById({_id: doc.customer_id}, function(err, docs){
					if(err) throw err;
					var customerEmail = docs.email_id;
	                var registrationToken = docs.fcm;       // if cancelOrder then send nfction to retailer 
	                // console.log("req.body.status----->", req.body.status);
	                var bodyMsg;
	                if(req.body.status == '3')
	                	{
	                		CreateAcount.findOne({Store_Id: doc.Store_Id}, function(err, cat){
	                			if(err) throw err;
	                			if(cat){
								var payload = {
								      	notification: {
								            title:  "OrderMedicine detail", 
								            body: "Order canceled successfully.",
								            tag: "OrderMedicine",
								            // click_action: "HomeActivity",
								            sound: "sfm_notification"
								        },
								        data:{order_id: order_id},
								   	};
								    admin.messaging().sendToDevice(cat.store_fcm, payload)
								      	.then(function(response) {
								        console.log("Successfully sent message:");
								      	})
								      	.catch(function(err) {
								        console.log("Error sending message:", err);
								        logger.log.info(err);
								    });
								    var notifications = new notificationcr();
									notifications.customer_id = docs.id;	
									notifications.store_id = cat.Store_Id;
									notifications.title = "OrderMedicine detail";
									notifications.message = "Order canceled successfully";
									notifications.flag = '2';
									notifications.save(function(err){
										if(err) throw err;
									})
	                				return res.send({
	                					stataus: 200,
	                					message: "Order canceled successfully"
	                				})											                				
	                			}
	                			else{
	                				return res.send({
	                					stataus: 400,
	                					message: "please try after some time."
	                				})
	                			}
	                		})
	                	}
	                // if(req.body.status == '3')
	                // 	{bodyMsg = "your order has been canceled."}
	                // 	console.log("bodyMsg----->",bodyMsg);
	                
					    var payload = {
					      	notification: {
					            title:  "OrderMedicine detail", 
					            body: "Order Rejected",   //"your order has been Rejected.",
					            tag: "OrderMedicine",
					            // click_action: "HomeActivity",
					            sound: "sfm_notification"
					        },
					        data:{order_id: order_id},
					   	};
					    admin.messaging().sendToDevice(registrationToken, payload)
					      	.then(function(response) {
					        console.log("Successfully sent message:");
					      	})
					      	.catch(function(err) {
					        console.log("Error sending message:", err);
					        logger.log.info(err);
					    });
		        		var today = doc.update_date;
		        		var oreder_date = ('0' + today.getDate()).slice(-2) + '-' + ('0' + (today.getMonth()+1)).slice(-2) + '-' + today.getFullYear();						      	
				      	var templateData = {};
				      	templateData.totalOrderamount = "0.00";
				      	templateData.actiontype = " Rejected.";
				      	templateData.date = oreder_date;
				      	templateData.storeName = doc.store_name;
				      	templateData.orderNo = doc.order_no;
				      	// templateData.totalOrderamount = doc.final_bill_amount;
				      	templateData.totalItem = doc.order_item_count;
				      	Store.findOne({_id: doc.Store_Id}, function(err, sto){
				      		if(err) throw err;
							state.findOne({state_id: sto.State}, function(err, state_n){
								var state_name = state_n.state;
								city.findOne({city_id: sto.City}, function(err, city_n){
									var city_name = city_n.city_name;
						      		templateData.address = sto.address;
						      		templateData.area = sto.Area;
						      		templateData.landmark = sto.Landmark;
						      		templateData.state = state_name;
						      		templateData.city = city_name;
						      		templateData.pin_code = sto.Pin_code;
						      		templateData.phoneNumber = sto.Contact_No_1;
						      		CreateAcount.findOne({Store_Id: doc.Store_Id}, function(err, cta){
						      			if(err) throw err;
						      			templateData.storeContactnumber = cta.Phone_Number;
						      			OperationalInfo.findOne({_id: cta.OperationalInfo_Id}, function(err, opi){
						      				if(err) throw err;
						      				templateData.storeStarttime = opi.Store_Start_Time;
						      				templateData.storeClosetime = opi.Store_Close_Time;
						      				templateData.homeDelevary = opi.Home_Delevary;
										    var requestObj = {
										    	templateName : "update-on-order",
										    	to : customerEmail,
										    	subject : 'Update on Order',
										    	templateData : templateData,
										    	senderEmail : config.email_m,
										    	senderPassword: config.password_m,
										    	fromJob : "OrderMedicine"
										    };
										     node_mailer.nodeMailer(requestObj,function(err,data){
										    	if(err){console.log("====ERROR====",err); logger.log.info(err);}
										    	console.log("====DATA====",data);
										    });								    				      				
						      			})
						      		})									
								})
							})				      		
				      	})					      	
					var notifications = new notificationcr();
					notifications.customer_id = doc.customer_id;	
					notifications.store_id = doc.Store_Id;
					notifications.title = "OrderMedicine detail";
					notifications.message = "your order has been Rejected.";
					notifications.flag = '1';
					notifications.save(function(err){
						if(err) throw err;
					})				    				
					console.log("your order has been Rejected");
					return res.send({
						statusCode: 200,
						message: "Order Rejected Successfully" //"your order has been Rejected."
					})					
				})	
			})
		})
	}
}

exports.accept_order = function(req, res){
	var order_id = req.body.order_id;
	if(typeof order_id == 'undefined'
		|| typeof req.body.comment == 'undefined'
		|| typeof req.body.final_bill_amount == 'undefined'
		)
	{
		console.log("order id or comment  is not provided");
		return res.send({
			statusCode: 200,
			medicinessage: "order id or comment is not provided"
		})
	}
	else{
		customer_order.findById({_id: order_id}, function(err, doc){
			if(err) throw err;
			doc.customer_orders_status = '2';
			doc.final_bill_amount = req.body.final_bill_amount;
			doc.comment = req.body.comment;
			doc.update_date = Date.now();
			doc.save(function(err){
				if(err) throw err;
				customer.findById({_id: doc.customer_id}, function(err, docs){
					if(err) throw err;	
					var customerEmail = docs.email_id		
					var registrationToken = docs.fcm;
				    var payload = {
				      	notification: {
				            title:  "OrderMedicine detail",
				            body: "your order has been accepted",
				            tag: "OrderMedicine",
				            sound: "sfm_notification"
				        },
				        data:{order_id: order_id},
				   	};
				    admin.messaging().sendToDevice(docs.fcm, payload)
				      	.then(function(response) {
				        console.log("Successfully sent message:");
				      	})
				      	.catch(function(err) {
				        console.log("Error sending message:", err);
				        logger.log.info(err);
				    });
		        		var today = doc.update_date;
		        		var oreder_date = ('0' + today.getDate()).slice(-2) + '-' + ('0' + (today.getMonth()+1)).slice(-2) + '-' + today.getFullYear();						      	
				      	var templateData = {};
				      	templateData.date = oreder_date;
				      	templateData.storeName = doc.store_name;
				      	templateData.orderNo = doc.order_no;
				      	templateData.actiontype = "accepted";
				      	templateData.totalOrderamount = doc.final_bill_amount;
				      	templateData.totalItem = doc.order_item_count;
				      	Store.findOne({_id: doc.Store_Id}, function(err, sto){
				      		if(err) throw err;
				      		templateData.address = sto.address;
				      		templateData.area = sto.Area;
				      		templateData.landmark = sto.Landmark;
				      		templateData.state = sto.State;
				      		templateData.city = sto.City;
				      		templateData.pin_code = sto.Pin_code;
				      		templateData.phoneNumber = sto.Contact_No_1;
				      		CreateAcount.findOne({Store_Id: doc.Store_Id}, function(err, cta){
				      			if(err) throw err;
				      			templateData.storeContactnumber = cta.Phone_Number;
				      			OperationalInfo.findOne({_id: cta.OperationalInfo_Id}, function(err, opi){
				      				if(err) throw err;
				      				templateData.storeStarttime = opi.Store_Start_Time;
				      				templateData.storeClosetime = opi.Store_Close_Time;
				      				templateData.homeDelevary = opi.Home_Delevary;
								    var requestObj = {
								    	templateName : "update-on-order",
								    	to : customerEmail,
								    	subject : 'Update on Order',
								    	templateData : templateData,
								    	senderEmail : config.email_m,
								    	senderPassword: config.password_m,
								    	fromJob : "OrderMedicine"
								    };
								     node_mailer.nodeMailer(requestObj,function(err,data){
								    	if(err){console.log("====ERROR====",err); logger.log.info(err);}
								    	console.log("====DATA====",data);
								    });								    				      				
				      			})
				      		})
				      	})				    					
					console.log("Order accepted successfully");
					var notifications = new notificationcr();
					notifications.customer_id = doc.customer_id;	
					notifications.store_id = doc.Store_Id;
					notifications.title = "OrderMedicine detail";
					notifications.message = "your order has been accepted";
					notifications.flag = '2';
					notifications.save(function(err){
						if(err) throw err;
					})					
					return res.send({
						statusCode: 200,
						message: "Order accepted successfully"
					})				
                })
			})
		})
	}
}




// exports.enter_salt = function(req, res){
// 	medicine_data.distinct("strength.salt", function(err, salts){
// 		console.log(salts);
// 		// process.exit();
// 		if(err) throw err;
// 		var array = [];
// 		// console.log(salts);
// 		async.each(salts, function(salt, callback){
// 			medicine_salt.findOne({salt: salt}, function(err, doc){
// 				if(doc != null){
// 					callback();
// 				}
// 				else{
// 					var dalts = new medicine_salt();
// 					dalts.salt = salt;
// 					console.log(salt);
// 					dalts.save();
// 					callback();	
// 				}
// 			})
// 		},function(err,result){
// 			res.send(result)
// 		})	
// 	})

// }


// exports.enter_salt = function(req, res){
// 	medicine_data.distinct("strength", function(err, salts){
// 		console.log(salts);
// 		// process.exit();
// 		var data;
// 		if(err) throw err;
// 		var array = [];
// 		// console.log(salts);
// 		async.each(salts, function(salt, callback){
// 			// console.log(salt);
// 			var data;
// 			medicine_salt.findOne({salt: salt.salt, quantity: salt.quantity}, function(err, doc){
// 				if(doc != null){
// 					callback();
// 				}
// 				else{
// 					data = doc
// 					var dalts = new medicine_salt();
// 					dalts.salt = salt.salt;
// 					dalts.quantity = salt.quantity;
// 					// console.log(doc);
// 					dalts.save();
// 					callback();	
// 				}
// 			})
// 		},function(err,result){
// 			res.send({
// 				data: data
// 			})
// 		})	
// 	})

// }

exports.search_salt = function(req, res){
    if(typeof req.body.name == 'undefined')
    {
  	    console.log("medicine name ia not provided");
  	    return res.send({
  		    statusCode: 400,
  		    message: "medicine name ia not provided"
  	    })
    }
    else{
	    var query = { salt: { $regex: '^' + req.body.name, $options:"i" }};
	    medicine_salt.find(query, function(err, doc){
	  	// console.log("medicine_name ",medicine_name);
	  	    if(err) throw err;
	  	    if(doc == ''){
	  		    console.log("this type of medicine_salt is not find");
	  		    return res.send({
	  			    statusCode: 400,
	  			    message: "This type of medicine salt is not find"
	  		    })
	  	    }
	  	    if(doc == null){
	  		    console.log("This type of medicine salt is not find");
	  		    return res.send({
	  			    statusCode: 400,
	  			    message: "This type of medicine salt is not find"
	  		    })
	  	    }
	  	    else{
	  		    return res.send({
	  			    statusCode: 200,
	  			    message: "get salt Successfully",
	  			    data:doc
	  		    })
	        }
	    })
    }
}


// exports.super_admin_dasboard = function(req, res){
//   SmEmployee.dataTable(req.query, function (err, data) {
//   	console.log(data);
//     return res.send({
//     	data: data
//     }); 
//   });
// }

exports.find_nearest_medical = function(req, res){
	if(typeof req.body.latitude == 'undefined'
		|| typeof req.body.longitude == 'undefined'
		)
	{
		console.log("latitude and longitude is not provided");
		return res.send({
			statusCode: 400,
			message: "latitude and longitude is not provided"
		})
	}
	else{
		//console.log(req.body);
		var stores = [];
		 Store.find({"Store_status":true},function(err, docs){
		 	if(err) throw err;
		 	async.each(docs, function(geo, callback){
		 		//console.log("geo  ",geo);
		 		//console.log(geo.latitude+" <latitude    g> "+geo.longitude)
		 		// geo.id == config.defaultStoreid
		 		if(geo == null)
		 		{
		 			callback();
		 		} 
		 		else{
		 		var userStoreDistance = geodist(
                {lat: req.body.latitude, lon: req.body.longitude}, 
                {lat: geo.latitude, lon: geo.longitude}, 
                {exact: true, unit: 'meters'});
		 		var obj = {};
		 		if(userStoreDistance <= 2500){
		 			obj._id = geo.id;
		 			obj.distance = userStoreDistance;
		 			obj.pharmacy_name =geo.Pharmacy_name;
		 			obj.address = geo.Address;
		 			obj.landmark = geo.Landmark;
		 			obj.area = geo.Area;
		 			obj.state = geo.State;
		 			obj.city = geo.City;
		 			obj.pin_code = geo.Pin_code;	 			
		 			obj.distance = userStoreDistance;
					CreateAcount.findOne({Store_Id: geo._id}, function(err, cat){
						if(err) throw err;
						OperationalInfo.findById({_id: cat.OperationalInfo_Id}, function(err, opi){
							if(err) throw err;
							// if(opi.Home_Delevary == "yes"){
								obj.home_delevary = opi.Home_Delevary;
								obj.min_order_rs = opi.Min_Order_Rs;
								obj.discount = opi.Discount_Scheme;
								obj.flate_discount = opi.Flate_Discount;
								obj.slab_discount = opi.Slab_Discount;
					 			stores.push(obj);
					 			callback();
							// }
							// else{
							// 	obj.flate_discount = opi.Flate_Discount;
							// 	obj.slab_discount = opi.Slab_Discount;
							// 	store_detail.push(obj);
							// 	callback();
							// }
						})
					})		 			
		 		}
		 		else{
		 			callback();
		 		}
		 	}
		 	},
		 	function(err){
		 		if(err) throw err;
		 		var nearest_med = sortBy(stores, 'distance');
			 	return res.send({
			 		statusCode: 200,
			 		message: "get all nearest medical",
			 		data: nearest_med
			 	})		 			
		 	})
		})
	}
}




// exports.save_store_msg = function(req, res){
// 	if(typeof req.body.store_id == 'undefined'
// 		//|| typeof req.body.advertisement_image == 'undefined'
// 		|| typeof req.body.advertisement_message == 'undefined'
// 		)
// 	{
// 		console.log("store id is not provided");
// 		return res.send({
// 			statusCode: 400,
// 			message: "store id is not provided"
// 		})
// 	}
// 	else{
// 		Store.findById({_id: req.body.store_id}, function(err, doc){
// 			if(err) throw err;
// 			var storeadds = new storeadvertisement();
// 			storeadds.store_id = req.body.store_id;
// 			storeadds.advertisement_image = req.body.advertisement_image;
// 			storeadds.advertisement_message = req.body.advertisement_message;
// 			storeadds.save(function(err){
// 				if(err) throw err;
// 				return res.send({
// 					statusCode: 200,
// 					message: "successfully save addvertisement",
// 					data: storeadds
// 				})
// 			})
// 		})
// 	}
// } 



exports.save_store_msg = function(req, res){
	if(typeof req.body.store_id == 'undefined'
		//|| typeof req.body.advertisement_image == 'undefined'
		|| typeof req.body.advertisement_message == 'undefined'
		)
	{
		console.log("store id is not provided");
		return res.send({
			statusCode: 400,
			message: "store id is not provided"
		})
	}
	else{
		storeadvertisement.count({store_id: req.body.store_id}, function(err, doc){
			if(err) throw err;
			if(doc < 3){
				var storeadds = new storeadvertisement();
				storeadds.store_id = req.body.store_id;
				storeadds.advertisement_image = req.body.advertisement_image;
				storeadds.advertisement_message = req.body.advertisement_message;
				storeadds.save(function(err){
					if(err) throw err;
					return res.send({
						statusCode: 200,
						message: "successfully save addvertisement",
						data: storeadds
					})
				})				
			}
			else{
				return res.send({
					statusCode: 400,
					message: "you can add only 3 adds.",
					// data: storeadds
				})				
			}
		})
	}
} 

exports.get_adds_by_store_id = function(req, res){
	if(typeof req.body.store_id == 'undefined')
	{
		console.log("store id is not provided");
		return res.send({
			statusCode: 400,
			message: "store id is not provided"
		})
	}
	else{
		storeadvertisement.find({store_id: req.body.store_id}, function(err, doc){
			if(err){
				logger.log.info(err);
				return res.send({
					statusCode: 400,
					message: "Something Went wrong."
				})
			}
			return res.send({
				statusCode: 200,
				message: "get advertisement successfully",
				data: doc
			})				
		})
	}
} 

// exports.get_adds_by_store_id =  function(req, res){
// 	if(typeof req.body.store_id == 'undefined')
// 	{
// 		console.log("store id is not provided");
// 		return res.send({
// 			statusCode: 400,
// 			message: "store id is not provided"
// 		})
// 	}
// 	else{//  { $or : [{ advertisement_for: '1'}, { store_id: req.body.store_id}]},
// 		storeadvertisement.find({store_id: req.body.store_id},{create_date:0, update_date:0}, function(err, docs){
// 			if(err) throw err;
// 			if(docs == ''){
// 				console.log("emty data");
// 				return res.send({
// 					statusCode: 400,
// 					message: "No addvertisement."
// 				})				
// 			}
// 			else{
// 				var data = [];
// 				async.each(docs, function(doc, callback){
// 					if(doc == null){
// 						callback();
// 					}
// 					else{
// 						data.push(doc);
// 						callback();
// 					}
// 				},
// 				function(err){
// 					storeadvertisement.find({advertisement_for: '1', softDelete:'1'},{create_date:0, update_date:0}).sort({update_date: -1}).limit(2).exec(function(err, advs){
// 						if(err) throw err;
// 						async.each(advs, function(adv, call){
// 							if(adv == null){
// 								call();
// 							}
// 							else{
// 								data.push(adv);
// 								call();
// 							}
// 						},
// 						function(err){
// 							return res.send({
// 								statusCode: data
// 							})
// 						})
// 					}) 
// 				})
// 			}
// 		})
// 	}	
// }


// for customer end showing advertisement
exports.get_all_banner_for_customer =  function(req, res){
	if(typeof req.body.store_id == 'undefined')
	{
		console.log("store id is not provided");
		return res.send({
			statusCode: 400,
			message: "store id is not provided"
		})
	}
	else{//  { $or : [{ advertisement_for: '1'}, { store_id: req.body.store_id}]},
			storeadvertisement.find({advertisement_for: '1', softDelete:'1'},{create_date:0, update_date:0}).sort({update_date: -1}).limit(2).exec(function(err, advs){
				if(err) throw err;
				var data = [];
				async.each(advs, function(adv, call){
					if(adv == null){
						call();
					}
					else{
						data.push(adv);
						call();
					}
				},
				function(err){
				storeadvertisement.find({store_id: req.body.store_id, softDelete:'1'},{create_date:0, update_date:0}, function(err, docs){
					if(err) throw err;
					if(docs == '' && data != null){
						console.log("emty data");
						return res.send({
							statusCode: 200,          					//400,
							message: "addvertisement found.",           //"No addvertisement."
							data: data
						})				
					}
					else if(docs == '' && data == ''){
						console.log("emty data");
						return res.send({
							statusCode: 400,          					
							message: "No addvertisement."
						})				
					}				
					else{
						// var data = [];
						async.each(docs, function(doc, callback){
							if(doc == null){
								callback();
							}
							else{
								data.push(doc);
								callback();
							}
						},
						function(err){
							return res.send({
								statusCode: 200,
								message: "data found",
								data: data
							})
						})
					}
				})
			})
		})
	}	
}

// for retailer end showing advertisement
exports.get_all_banner_for_retailer =  function(req, res){
	if(typeof req.body.store_id == 'undefined')
	{
		console.log("store id is not provided");
		return res.send({
			statusCode: 400,
			message: "store id is not provided"
		})
	}
	else{//  { $or : [{ advertisement_for: '1'}, { store_id: req.body.store_id}]},
			storeadvertisement.find({advertisement_for: '2', softDelete:'1'},{create_date:0, update_date:0}).sort({update_date: -1}).limit(2).exec(function(err, advs){
				if(err) throw err;
				var data = [];
				async.each(advs, function(adv, call){
					if(adv == null){
						call();
					}
					else{
						data.push(adv);
						call();
					}
				},
				function(err){
				storeadvertisement.find({store_id: req.body.store_id, softDelete:'1'},{create_date:0, update_date:0}, function(err, docs){
					if(err) throw err;
					if(docs == '' && data != null){
						console.log("emty data");
						return res.send({
							statusCode: 200,          					//400,
							message: "addvertisement found.",           //"No addvertisement."
							data: data
						})				
					}
					else if(docs == '' && data == ''){
						console.log("emty data");
						return res.send({
							statusCode: 400,          					
							message: "No addvertisement."
						})				
					}
					else{
						// var data = [];
						async.each(docs, function(doc, callback){
							if(doc == null){
								callback();
							}
							else{
								data.push(doc);
								callback();
							}
						},
						function(err){
							return res.send({
								statusCode: 200,
								message: "data found",
								data: data
							})
						})
					}
				})
			})
		})
	}	
}



exports.notification =  function(req, res){
	if(typeof req.body.store_id == 'undefined'
		|| typeof req.body.title == 'undefined'
		|| typeof req.body.message == 'undefined'
		)
	{
		console.log("store id or title or message is not provided");
		return res.send({
			statusCode: 400,
			message: "store id or title or message is not provided"
		})
	}
	else{
		var store_id = req.body.store_id;
		var title = req.body.title;
		var message = req.body.message;
		var store_notification = new notification();
		store_notification.store_id = store_id;
		store_notification.title = title;
		store_notification.message = message;
		store_notification.save(function(err){
			if(err) throw err;
			sendnotification(store_id, res, title, message);
			return res.send({
				statusCode: 200,
				message: "send notification successfully"
			})
		})
	}	
}

exports.get_store_all_notification = function(req, res){
	if(typeof req.body.store_id == 'undefined')
	{
		console.log("store id is not provided");
		return res.send({
			statusCode: 400,
			message: "store id is not provided"
		})
	}
	else{
		notification.find({store_id: req.body.store_id},{},{ sort :{ create_time : -1}},  function(err, doc){
			if(err){
				console.log("if err ----->", err);
				logger.log.info(err);
				 return res.send({
				 	statusCode: 500,
				 	message: "Something Went wrong"
				 })
			}
			if(doc == null){
				console.log("There is no any notification");
				 return res.send({
				 	statusCode: 400,   // 500,
				 	message: "No Notification Available"
				 })
			}
			if(doc == ''){
				console.log("There is no any notification");
				 return res.send({
				 	statusCode: 400,   //500
				 	message: "No Notification Available"
				 })
			}						
			return res.send({
				statusCode: 200,
				message: " get store all notification Successfully",
				data: doc
			})
		})
	}
}

exports.get_customer_email_by_store_id = function(req, res){
	if(typeof req.body.store_id == 'undefined')
	{
		console.log("store_id is not provided");
		return res.send({
			statusCode: 400,
			message: "store_id is not provided"
		})
	}
	else{
		customer.find({Store_Id: req.body.store_id}, function(err, docs){
			if(err) throw err;
			var email_box = [];
			async.each(docs, function(doc, callback){
				if(doc == null)
				{
					callback();
				}
				else{
					var obj = {};
					obj.email = doc.email_id
					email_box.push(obj);
					callback();
				}
			},
			function(err){
				if(err) throw err;
				return res.send({
					statusCode: 200,
					message: "get all customer email_id successfully",
					data: email_box
				})
			})
		})
	}
}

exports.update_store_msg = function(req, res){
	if(typeof req.body.store_id == 'undefined'
		|| typeof req.body.advertisement_id == 'undefined'
		)
	{
		console.log("store id or advertisement id is not provided");
		return res.send({
			statusCode: 400,
			message: "store id or advertisement id is not provided"
		})
	}
	else{
		storeadvertisement.findById({_id: req.body.advertisement_id, store_id: req.body.store_id}, function(err, doc){
			if(err) throw err;
			doc.advertisement_image = req.body.advertisement_image;
			doc.advertisement_message = req.body.advertisement_message;
			doc.save(function(err){
				if(err) throw err;
				return res.send({
					statusCode: 200,
					message: "successfully update addvertisement",
					data: doc
				})
			})
		})
	}
}

exports.delete_store_msg = function(req, res){
	if(typeof req.body.store_id == 'undefined'
		|| typeof req.body.advertisement_id == 'undefined'
		)
	{
		console.log("store id or advertisement id is not provided");
		return res.send({
			statusCode: 400,
			message: "store id or advertisement id is not provided"
		})
	}
	else{
		storeadvertisement.remove({_id: req.body.advertisement_id, store_id: req.body.store_id}, function(err, doc){
			if(err) throw err;
			if(doc.n == 0){
				console.log("no advertisement found");
				return res.send({
					statusCode: 400,
					message: "no advertisement found"
				})
			}
			else{
				return res.send({
					statusCode: 200,
					message: "successfully delete addvertisement",
					//data: doc
				})				
			}			
		})
	}
} 


// exports.med_finder = function(req, res){
// 	var med_name = req.body.med_name;
// 	var quantity = req.body.quantity;
// 	var customer_id = req.body.customer_id;
// 	if(typeof customer_id == 'undefined'
// 		|| typeof med_name == 'undefined'
// 		|| typeof quantity == 'undefined'
// 		|| typeof req.body.latitude == 'undefined'
// 		|| typeof req.body.longitude == 'undefined')
// 	{
// 		console.log("customer_id or med_name or quntity is not provided");
// 		return res.send({
// 			statusCode: 400,
// 			message: "customer_id or med_name or quntity is not provided"
// 		}) 
// 	}
// 	else{
// 		Store.find({Store_status: true}, function(err, docs){
// 			if(err) throw err;
// 			//console.log(docs);
// 			// var registrationToken = [];
// 			async.each(docs, function(doc, callback){
// 				if(doc == null)
// 				{
// 					callback();
// 				}
// 				else{
// 			 		var userStoreDistance = geodist(
// 	                {lat: req.body.latitude, lon: req.body.longitude}, 
// 	                {lat: doc.latitude, lon: doc.longitude},
// 	                {exact: true, unit: 'meters'});
// 			 		//var obj = {};
// 			 		//console.log("Distance  >>>>>>>> ",userStoreDistance);
// 			 		if(userStoreDistance <= 500){	
// 			 			var store_id = doc._id;
// 			 			med_finder_notification(store_id, med_name, quantity, callback);
// 			 			callback();
// 			 			// CreateAcount.find({Store_Id: doc._id}, function(err, stores){
// 			 			// 	if(err) throw err;
// 			 				// var registrationToken = [];
// 			 				// async.each(stores, function(store, innercallback){
// 			 				// 	registrationToken.push(store.store_fcm);
//         //                         med_finder_notification(registrationToken, med_name, quantity);
// 			 				// 	console.log(registrationToken);
// 			 				// 	innercallback();
// 			 				// })
// 			 				//console.log(store.store_fcm);
// 			 			// 	callback();
// 			 			// })
// 			 			// CreateAcount.find({Store_Id: doc._id}, function(err, store){
// 			 			// 	if(err) throw err;
// 				 		// 	registrationToken.push(store.fcm);
// 				 			//med_finder_notification(registrationToken, med_name, quantity);
// 				 			//callback();
// 			 			// })
// 			 		}
// 			 		else{
// 			 			callback();
// 			 		}				
// 				}
// 			}, function(err){
// 				if(err) throw err;
// 				console.log(""+med_name+" is Searching from your location to 500 meter");
// 				return res.send({
// 					statusCode: 200,
// 					message: ""+med_name+" is Searching from your location to 500 meter"
// 				})
// 			})
// 		})
// 	}
// }



//{ $text: { $search: req.body.name}}

function sendnotification(store_id, res, title, message) {
  	customer.find({Store_Id: store_id}, function(err, customer_doc) {
  		if(err) throw err;
   		if(customer_doc == null || customer_doc == '')
    	{
    		console.log("store id is not present in any customer table line no.");
    		return res.send({
    			statusCode: 400,
    			message: "store id is not present in any customer table"
    		})    	
    	}
    	else{
    		//var registrationToken = customer_doc.fcm;
	    	var registrationToken = [];
	    	async.each(customer_doc, function(doc, callback){
	    		if(doc == null)
	    		{
	    			callback();
	    		}
	    		else{
					var notifications = new notificationcr();
					notifications.customer_id = doc.id;	
					notifications.store_id = store_id;
					notifications.title = title;
					notifications.message = message;
					notifications.flag = '1';
					notifications.save(function(err){
						if(err) throw err;
					})	    			
	    			registrationToken.push(doc.fcm);
	    			callback();
	    		}
	    	}, function(err){
	    		if(err) throw err;
	    		if(registrationToken.length == 0)
	    		{
	    			console.log("store id is not present in any customer table");
	    			return res.send({
	    				statusCode: 400,
	    				message: "store id is not present in any customer table"		
	    			})
	    		}
	    		else{
			    	var payload = {
			      		notification: {
			          	title:  title, //"Notification from OrderMedicine",
			          	body: message, // "Campaign",
			          	tag: "OrderMedicine",
			          	// click_action: "HomeActivity",
			          	sound: "sfm_notification"
			        	},
			   		 };
			    	admin.messaging().sendToDevice(registrationToken, payload)
			      		.then(function(response) {
			        	console.log("Successfully sent message:");
			      		})
			      		.catch(function(err) {
			        	console.log("Error sending message:", err);
			        	logger.log.info(err);
			    	});
	    		}
	    	})
    	}
  	});
}

// exports.create_med_enquiry = function(req, res){
// 	var med_name = req.body.med_name;
// 	var quantity = req.body.quantity;
// 	var customer_id = req.body.customer_id;
// 	if(typeof customer_id == 'undefined'
// 		|| typeof med_name == 'undefined'
// 		|| typeof quantity == 'undefined'
//       )
// 	{
// 		console.log("customer_id or med_name or quantity is not provided");
// 		return res.send({
// 			statusCode: 400,
// 			message: "customer_id or med_name or quantity is not provided"
// 		})
// 	}
// 	else{
// 		var enquiry_id;
// 		//var distance = [];
// 		var med_finder = new medFinder();
// 		med_finder.customer_id = customer_id;
// 		med_finder.med_name = med_name;
// 		med_finder.quantity = quantity;
// 		med_finder.save(function(err){
// 			if(err) throw err;
// 			enquiry_id = med_finder.id;
// 			console.log("create request successfully");
// 			return res.send({
// 				statusCode: 200,
// 				message: "create request successfully",
// 				enquiry_id: enquiry_id
// 			})
//         })
//     }
// }



// exports.medicine_finder = function(req, res){
// 	var enquiry_id = req.body.enquiry_id;
// 	if(typeof enquiry_id == 'undefined'
// 		|| typeof req.body.latitude == 'undefined'
// 		|| typeof req.body.longitude == 'undefined')
// 	{
// 		console.log("enquiry_id or latitude or longitude is not provided");
// 		return res.send({
// 			statusCode: 400,
// 			message: "enquiry_id or latitude or longitude is not provided"
// 		})
// 	}
// 	else{
// 		var distance = [];
// 		var med_name;
// 		var quantity;
// 		medFinder.findById({_id: enquiry_id}, function(err, med_f){
// 			if(err) throw err;
// 			med_name = med_f.med_name;
// 			quantity = med_f.quantity;
// 			console.log("med_f ----->", med_f);
// 			// enquiry_id = med_finder.id;
// 			//console.log(customer_id);
//             //return false;
// 			Store.find({Store_status: true}, function(err, docs){
// 				if(err) throw err;
// 				//console.log(docs);
// 				// var registrationToken;
// 				async.each(docs, function(doc, callback){
// 					if(doc == null)
// 					{
// 						callback();
// 					}
// 					else{
// 						var fcm;
// 				 		var userStoreDistance = geodist(
// 		                {lat: req.body.latitude, lon: req.body.longitude}, 
// 		                {lat: doc.latitude, lon: doc.longitude},
// 		                {exact: true, unit: 'meters'});
// 		                //console.log("doc___>", doc);
// 		                CreateAcount.findOne({Store_Id: doc._id}, function(err, s_fcm){
// 		                	//if(err) throw err;
// 		                	//console.log(s_fcm);
// 		                	//return false;
// 		                	fcm = s_fcm.store_fcm;
// 		                    distance.push({distance: userStoreDistance, store_id: doc._id, fcm: fcm});
// 		                   // console.log("distance >>>>>>>>", distance);
// 		                    callback();
// 		                })			
// 					}
// 				}, function(err){
// 					if(err) throw err;
// 					// sort by distance
// 					distance.sort(function (a, b) {
// 					  return a.distance - b.distance;
// 					});
// 					console.log("distance >>>>>>>>", distance);
// 					async.eachSeries(distance, function (near, call) {
// 					    setTimeout(function () {
// 					    	medFinder.findById({_id: enquiry_id}, function(err, mf){
// 					    		var data = mf.request;
// 					    		console.log("array.length-----------------> ", data.length)
// 					    		if(data.length <= 1){
// 					    			var registrationToken = near.fcm;

//                                      var payload = {
//                                          notification: {
//                                          title:  med_name+'(QTY : '+quantity+')', //"Notification from OrderMedicine",
//                                          body: " Do you have this medicine ?\nPlease respond within 30 second.", // "Campaign",
//                                          tag: "OrderMedicine",
//                                          //click_action: "HomeActivity"
//                                          },
//                                          data:{enquiry_id: enquiry_id},
//                                       };
//                                       // console.log("array.length-----------------> ", data.length)
//                                      // console.log(new Date(), "eachSeries------> ",near);
//                                      console.log("near.store_id-----> ", near.store_id);
//                                      admin.messaging().sendToDevice(registrationToken, payload)
//                                          .then(function(response) {
//                                          	 call();
//                                          console.log("Successfully sent message:", response);
//                                          //console.log("payload: ", payload);
//                                          })
//                                          .catch(function(err) {
//                                          	 call();
//                                          console.log("Error sending message:", err);
//                                      });
                                        
// 					    		}
// 					    		else{
// 					    			console.log(">2 ------>",data.length);
// 									 return call({flag:true});
// 					    			// return res.send({
// 					    			// 	statusCode: 200,
// 					    			// 	message: "successfully send notification for searching med",
// 					    			// 	enquiry_id: enquiry_id
// 					    			// })
// 					    			// false;
// 					    		}
// 					    	}) 
// 					    	//console.log(new Date(), "eachSeries------> ",near);
// 					    	//call();
// 					    }, 10000);
// 					}, function(err, result){
// 						if(err){
// 							// console.log("if err ----->", err);
// 							// return res.send({
// 							// 	statusCode: 500,
// 							// 	message: "something went wrong"
// 							// })	
// 							medFinder.findById({_id: enquiry_id}, function(err, s_detail){
// 								if(err) throw err;
// 								if(s_detail.request == ''){
// 									console.log("emty store !!!");
// 									return res.send({
// 										statusCode: 400,
// 										message: "emty store !!!"
// 									})									
// 								}
// 								if(s_detail.request  == null){
// 									console.log("store not found !!!");
// 									return res.send({
// 										statusCode: 400,
// 										message: "store not found !!!"
// 									})									
// 								}
// 								else{
// 									console.log("Successfully found store data");
// 									return res.send({
// 										statusCode: 200,
// 										message: "successfully send notification for searching medicine",
// 										data: s_detail.request
// 									})
// 								}								
// 							})							
// 						}
// 						else{
// 							console.log("else ---------------------- else");
// 							// medFinder.findById({_id: enquiry_id}, function(err, s_detail){
// 							// 	if(err) throw err;
// 							// 	if(s_detail.request == ''){
// 							// 		console.log("emty store !!!");
// 							// 		return res.send({
// 							// 			statusCode: 400,
// 							// 			message: "emty store !!!"
// 							// 		})									
// 							// 	}
// 							// 	if(s_detail.request  == null){
// 							// 		console.log("store not found !!!");
// 							// 		return res.send({
// 							// 			statusCode: 400,
// 							// 			message: "store not found !!!"
// 							// 		})									
// 							// 	}
// 							// 	else{
// 							// 		console.log("Successfully found store data");
// 							// 		return res.send({
// 							// 			statusCode: 200,
// 							// 			message: "successfully send notification for searching medicine",
// 							// 			data: s_detail.request
// 							// 		})
// 							// 	}								
// 							// })
// 						}
// 					})				
// 				})
// 			})
// 		})
// 	}
// }



exports.store_response = function(req, res){
	if(typeof req.body.store_id == 'undefined'
		|| typeof req.body.enquiry_id == 'undefined')
	{
		console.log("request is not provided");
		return res.send({
			statusCode: 400,
			message: "request is not provided"
		})
	}
	else{
		medFinder.findById({_id: req.body.enquiry_id}, function(err, doc){
			if(err){
				console.log("if err ------>", err);
				logger.log.info(err);
				return res.send({
					statusCode: 500,
					message: "something went wrong"
				})
			}
			else{
				// doc.request.store_id = req.body.request;
				doc.request.push({store_id: req.body.store_id});
				doc.save(function(err){
					if(err){
						console.log("if err ------>", err);
						logger.log.info(err);
						return res.send({
							statusCode: 500,
							message: "something went wrong"
						})
					}
					else{
						// console.log("send request successfully", doc.request.length);
						return res.send({
							statusCode: 200,
							message: "send request successfully"
						})						
					}
				})
			}
		})
	}
}



// exports.get_yes_list_of_enquery_id = function(req,res){
// 	if(typeof req.body.enquiry_id == 'undefined')
// 	{
// 		console.log("enquiry_id is not provided");
// 		return res.send({
// 			statusCode: 400,
// 			message: "enquiry_id is not provided"
// 		})
// 	}
// 	else{
// 		medFinder.findById({_id: req.body.enquiry_id}, function(err, docs){
// 			if(err){
// 				console.log("if err ------>", err);
// 				return res.send({
// 					statusCode: 500,
// 					message: "something went wrong"
// 				})
// 			}
// 			if(docs == null){
// 				console.log("not found data");
// 				return res.send({
// 					statusCode: 400,
// 					message: "not found data"
// 				})
// 			}
// 			if(docs == ''){
// 				console.log("empty data found");
// 				return res.send({
// 					statusCode: 400,
// 					message: "empty data found"
// 				})
// 			}
// 			else{
// 				console.log(docs.request);
// 				//return false;
// 				var request = docs.request;
// 				var store_detail = [];
// 				async.each(request, function(doc, callback){
// 					var docid = doc.store_id;
// 					Store.findById({_id: docid}, function(err, store_file){
// 							store_detail.push(store_file);
// 							callback();
// 					})
// 				}, function(err){
// 					if(err) throw err;
// 					if(store_detail == null){
// 						console.log("not found request");
// 						return res.send({
// 							statusCode: 400,
// 							message: "not found request"
// 						})
// 					}
// 					if(store_detail == ''){
// 						console.log("empty request");
// 						return res.send({
// 							statusCode: 400,
// 							message: "empty request"
// 						})
// 					}
// 					else{
// 						console.log("Successfully get store detail");
// 						return res.send({
// 							statusCode: 200,
// 							message: "Successfully get store detail",
// 							data: store_detail
// 						})						
// 					}
// 				})
// 			}			
// 		})
// 	}
// }

// async.eachSeries(TheUrl, function (eachUrl, done) {
//     setTimeout(function () {
//         var url = 'www.myurl.com='+eachUrl;
//         request(url, function(error, resp, body) { 
//             if (error) return callback(error); 
//             var $ = cheerio.load(body);
//             //Some calculations again...
//             done();
//         });
//     }, 10000);
// }, function (err) {
//     if (!err) callback();
// });




// cron({ on: '*/1 * * * *'}, function() {
// 	console.log("cron start");
// 	//'*/45 * * * * *'
// 	//'0 0 * * *'
// });

// var rule = new cron.RecurrenceRule();
// rule.second = 10;
// cron.scheduleJob(rule, function(){
//     console.log(new Date(),  'The 30th second of the minute.');
// });

//let startTime = new Date(Date.now() + 5000);
//let endTime = necmdw Date(startTime.getTime() + 5000);
// var j = cron.scheduleJob({rule: '*/30 * * * * *' }, function(){
//   console.log(new Date(), 'Time for tea!');
// });

// var j = cron.scheduleJob({rule: '0 */10 * * * *' }, function(){
//   console.log(new Date(), 'Time for tea!');
// });
cron.scheduleJob({rule: '0 */10 * * * *' }, function(){
// exports.store_status_new = function(req, res){
	console.log(new Date(), 'Time for tea!');
	customer_order.find({customer_orders_status: '1'}, function(err, docs){
		if(err){
			console.log("if err ------>", err);
			return res.send({
				statusCode: 500,
				message: "something went wrong"
			})
		}
		if(docs == null){
			console.log("docs == null ------------- CRON -------------");
			return false;
		}
		if(docs == ''){
			console.log("docs == '' ************* CRON *************");
			return false;
		}		
		else{
			var array = [];
			async.each(docs, function(doc,callback){
				if(doc == null){
					callback();
				}
				else{
		            var notifications = new notificationcr();
					notifications.customer_id = doc.customer_id;	
					notifications.store_id = doc.Store_Id;
					notifications.title = "New order reminder";
					notifications.message = "New order is pending in your order list please take any action";
					notifications.flag = '3';
					notifications.save(function(err){
						if(err) throw err;
					})	

					array.push(doc.Store_Id);
					callback();					
				}
			}, function(err){
				if(err) throw err;
				var hash = {}, newarr = [];
				for (var i = 0; i < array.length; i++) {
				    var v = array[i];
				    if (v in hash) continue;
				    newarr.push(v);
				    hash[v] = true;
				}
                //console.log(newarr);
                var registrationToken = [];
                async.each(newarr, function(storeid, call){
                	if(storeid == null){
                		call();
                	}
                	else{
	               		//console.log("storeid--->", storeid);
	               		CreateAcount.findOne({Store_Id: storeid}, function(err, store){
	               			if(err){
	               				//console.log("store....n  ", store);
	               				call();
	               			}
	               			if(store == null){
	               				call();
	               			}
	               			else{
	               				//console.log("store_id ----------->", store);
	               				if(store.store_fcm!=null){
	               					registrationToken.push(store.store_fcm);
	               					call();
	               				}
	               				else{
	               					call();
	               				}		
	               			}
	               		})                		
                	}
                }, function(err){
               		if(err) throw err;
               		if(registrationToken == null){
						console.log("this there is no any new order");
						return false;              		
               		}
               		else{
	               		// console.log("fcm-------------> ", registrationToken);
	                	var payload = {
			                notification: {
				                title:  "New order reminder", //"Notification from OrderMedicine",
				                body: "New order is pending in your order list please take any action", // "Campaign",
				                tag: "OrderMedicine",
				                // click_action: "OrderActivity",
				                sound: "retailer_ring"
			                },
		                // data:{enquiry_id: med_finder.id},
		                };
		               // console.log(new Date(), "eachSeries------> ");
		                admin.messaging().sendToDevice(registrationToken, payload)
		                .then(function(response) {
		                console.log("Successfully sent message for New Order reminder.");
		               // console.log("payload: ", payload);
		                    })
		                .catch(function(err) {
		                console.log("Error sending message:");
		                logger.log.info(err);
		               })
	            	}    
				})
			})
		}
	})
}) 

exports.get_order_status_one = function(req, res){
	if(typeof req.body.store_id == 'undefined'){
		console.log("store id not provided");
		return res.send({
			statusCode: 400,
			message: "store id not provided"
		})
	}
	else{
		customer_order.find({Store_Id: req.body.store_id, customer_orders_status: 1},{},{sort: {update_date: -1}}, function(err, docs){
			if(err){
				console.log("if err ------>", err);
				logger.log.info(err);
				return res.send({
					statusCode: 500,
					message: "something went wrong"
				})
			}
			else{
				if(docs == null){
					console.log("order not found");
					return res.send({
						statusCode: 400,
						message: "order not found"
					})
				}
				if(docs == ''){
					console.log("order not found");
					return res.send({
						statusCode: 400,
						message: "order not found"
					})
				}				
				else{
					console.log("found data Successfully");
					return res.send({
						statusCode: 200,
						message: "found data Successfully",
						data: docs
					})
				}
			}
		})
	}
}

exports.state = function(req, res){
	state.find({}, function(err, doc){
		if(err) throw err;
		if(doc == null){
			return res.send({
				statusCode: 400,
				message: "error"
			})
		}
		return res.send({
			statusCode: 200,
			message: "Successfully get all state of india",
			data: doc
		})
	})
}

exports.city = function(req, res){
	if(typeof req.body.state_id == 'undefined')
	{
		console.log("state_id is not provided");
		return res.send({
			statusCode: 400,
			message: "state_id is not provided"
		})
	}
	else{
		city.find({state_id: req.body.state_id},{},{ sort :{ city_name : 1}}, function(err, doc){
			if(err) throw err;
			if(doc == null){
				return res.send({
					statusCode: 400,
					message: "error"
				})
			}
				return res.send({
					statusCode: 200,
					message: "Successfully get all city of selected state",
					data: doc
				})
		})		
	}
}

exports.dispatch_order = function(req, res){
	//console.log("body----->", body);
	if( typeof req.body.comment == 'undefined'
		// || typeof req.body.delivery_boy_name == 'undefined'
		// || typeof req.body.delivery_boy_number == 'undefined'
		|| typeof req.body.delivery_time == 'undefined'
		|| typeof req.body.order_id  == 'undefined'   // 5aedb73002c6af1246a69c2f  live
		|| typeof req.body.final_bill_amount == 'undefined' 
		)
	{
		console.log("final_bill_amount or order_price or delivery_time or order_id is not provided");
		return res.send({
			statusCode: 400,
			message: "final_bill_amount or order_price or delivery_time or order_id is not provided."
		})
	}
	else{
		// var string = "Not Required";
		// req.body.delivery_time = string;
		customer_order.findById({_id: req.body.order_id}, function(err, doc){
			if(err){
				console.log("if err ----->", err);
				logger.log.info(err);
				return res.send({
					statusCode: 500,
					message: "something went wrong.6098"
				})
			}
			if(doc == null){
				return res.send({
					statusCode: 400,
					message: "data not found"
				})
			}
			if(doc == ''){
				return res.send({
					statusCode: 400,
					message: "empty data found"
				})
			}
			else{
				customer.findById({_id: doc.customer_id}, function(err, cust){
					if(err) throw err;
					var customerEmail = cust.email_id;			
	        		var today = doc.update_date;
	        		var oreder_date = ('0' + today.getDate()).slice(-2) + '-' + ('0' + (today.getMonth()+1)).slice(-2) + '-' + today.getFullYear();						      	
			      	var templateData = {};
			      	templateData.date = oreder_date;
			      	// templateData.finalBillamount = req.body.final_bill_amount;
			      	templateData.totalOrderamount = req.body.final_bill_amount;
			      	templateData.storeName = doc.store_name;
			      	templateData.orderNo = doc.order_no;
			      	// templateData.totalOrderamount = doc.final_bill_amount;
				    var payload = {
				      	notification: {
				            title:  "OrderMedicine detail",
				            body: "your order has been dispatched.",
				            tag: "OrderMedicine",
				            // click_action: "HomeActivity",
				            sound: "sfm_notification"
				        }
				   	};
				    admin.messaging().sendToDevice(cust.fcm, payload)
				      	.then(function(response) {
				        console.log("Successfully sent message:");
				      	})
				      	.catch(function(err) {
				        console.log("Error sending message:");
				        logger.log.info(err);
				    });

			      	Store.findOne({_id: doc.Store_Id}, function(err, sto){
			      		if(err) throw err;
			      		city.findOne({city_id: sto.City}, function(err, city_n){
			      			state.findOne({state_id: sto.State}, function(err, state_n){
					      		templateData.address = sto.Address;
					      		templateData.area = sto.Area;
					      		templateData.landmark = sto.Landmark;
					      		templateData.state = state_n.state;
					      		templateData.city =  city_n.city_name;
					      		templateData.pin_code = sto.Pin_code;
					      		templateData.phoneNumber = sto.Contact_No_1;
					      		CreateAcount.findOne({Store_Id: doc.Store_Id}, function(err, cta){
					      			if(err) throw err;
					      			templateData.storeContactnumber = cta.Phone_Number;
					      			OperationalInfo.findOne({_id: cta.OperationalInfo_Id}, function(err, opi){
					      				if(err) throw err;
					      				templateData.storeStarttime = opi.Store_Start_Time;
					      				templateData.storeClosetime = opi.Store_Close_Time;
					      				templateData.homeDelevary = opi.Home_Delevary;
					      				templateData.actiontype = "Dispatched"
									    var requestObj = {
									    	templateName : "update-on-order",
									    	to : customerEmail,
									    	subject : 'Update on Order',
									    	templateData : templateData,
									    	senderEmail : config.email_m,
									    	senderPassword: config.password_m,
									    	fromJob : "OrderMedicine"
									    };
									     node_mailer.nodeMailer(requestObj,function(err,data){
									    	if(err){console.log("====ERROR====",err); logger.log.info(err);}
									    	console.log("====DATA====",data);
									    });							    				      				
					      			})
					      		})
			      			})
			      		})
			      	})
		      	})			
				customer_delivery_detail.findById({_id: doc.deliver_address_id}, function(err, datam){
					if(err){
						console.log("if err ----->", err);
						logger.log.info(err);
						return res.send({
							statusCode: 500,
							message: "something went wrong.. 6164"
						})
					}
					else{
						//request('http://www.global91sms.in/API/WebSMS/Http/v1.0a/index.php?username='+config.msgUname+'&password='+config.msgPass+'&sender=ORDMED&to='+req.body.delivery_boy_number+'&message=Customer+Detail:+'+datam.name+'+('+datam.mobile_number+'),++Adddress:+'+datam.address1+',+'+datam.address2+',+'+datam.landmark+',+'+datam.city+',+'+datam.state+',+'+datam.pin_code+',+Order+Price:+'+req.body.final_bill_amount+'', function (err, done, body) {
						request('http://sms.global91sms.in/api/mt/SendSMS?user='+config.msgUname+'&password='+config.msgPass+'&senderid=ORDMED&channel=trans&DCS=0&flashsms=0&number='+datam.mobile_number+'&text=Customer+Detail:+'+datam.name+'+('+datam.mobile_number+'),++Adddress:+'+datam.address1+',+'+datam.address2+',+'+datam.landmark+',+'+datam.city+',+'+datam.state+',+'+datam.pin_code+',+Order+Price:+'+req.body.final_bill_amount+'', function (err, done, body) {
							if(err){
								console.log('err:', err); // Print the error if one occurred
								logger.log.info(err);
								return res.send({
									statusCode: 500,
									message: "something went wrong 6173"
								})
							}
							if(!done){
								console.log("Fail to hitt sms url");
								return res.send({
									statusCode: 400,
									message: "Fail to hitt sms url"
								})
							}
							else{
								if( typeof req.body.delivery_boy_name == 'undefined'
									|| typeof req.body.delivery_boy_number == 'undefined')
								{
									if(req.body.delivery_time != 'Not Required'){
										console.log("successfully hitt sms url")
										//request('http://www.global91sms.in/API/WebSMS/Http/v1.0a/index.php?username='+config.msgUname+'&password='+config.msgPass+'&sender=ORDMED&to='+datam.mobile_number+'&message=Dear+Customer,++Your+order+number:+'+doc.order_no+',+has+been+dispatched.+Delivery+Time:'+req.body.delivery_time+'.+Amount+Payable:+Rs.+'+req.body.final_bill_amount+'.', function (err, done, body) {
										request('http://sms.global91sms.in/api/mt/SendSMS?user='+config.msgUname+'&password='+config.msgPass+'&senderid=ORDMED&channel=trans&DCS=0&flashsms=0&number='+datam.mobile_number+'&text=Dear+Customer,++Your+order+number:+'+doc.order_no+',+has+been+dispatched.+Delivery+Time:'+req.body.delivery_time+'.+Amount+Payable:+Rs.+'+req.body.final_bill_amount+'.', function (err, done, body) {
											if(err){
												console.log('err:', err); // Print the error if one occurred
												logger.log.info(err);
												return res.send({
													statusCode: 500,
													message: "something went wrong"
												})
											}
											if(!done){
												console.log("Fail to hitt sms url");
												return res.send({
													statusCode: 400,
													message: "Fail to hitt sms url"
												})
											}									
											else{
												doc.comment = req.body.comment;
												doc.customer_orders_status = '4';
												doc.final_bill_amount = req.body.final_bill_amount;
												doc.delivery_time = req.body.delivery_time;
												doc.update_date = Date.now();
												doc.save(function(err){
													if(err) throw err;
													return res.send({
														statusCode: 200,
														message: "customer order updated successfully"
													})
												})	
											}	  
										});
									}
									else{
										console.log("successfully hitt sms url")
										// request('http://www.global91sms.in/API/WebSMS/Http/v1.0a/index.php?username='+config.msgUname+'&password='+config.msgPass+'&sender=ORDMED&to='+datam.mobile_number+'&message=Dear+Customer,++Your+order+number:+'+doc.order_no+',+has+been+dispatched.+It+will+soon+be+delivered.+Amount Payable: Rs.'+req.body.final_bill_amount+'.', function (err, done, body) {
										request('http://sms.global91sms.in/api/mt/SendSMS?user='+config.msgUname+'&password='+config.msgPass+'&senderid=ORDMED&channel=trans&DCS=0&flashsms=0&number='+datam.mobile_number+'&text=Dear+Customer,++Your+order+number:+'+doc.order_no+',+has+been+dispatched.+It+will+soon+be+delivered.+Amount Payable: Rs.'+req.body.final_bill_amount+'.', function (err, done, body) {
											if(err){
												console.log('err:', err); // Print the error if one occurred
												logger.log.info(err);
												return res.send({
													statusCode: 500,
													message: "something went wrong....6226"
												})
											}
											if(!done){
												console.log("Fail to hitt sms url");
												return res.send({
													statusCode: 400,
													message: "Fail to hitt sms url"
												})
											}									
											else{
												doc.comment = req.body.comment;
												doc.customer_orders_status = '4';
												doc.final_bill_amount = req.body.final_bill_amount;
												doc.delivery_time = req.body.delivery_time;
												doc.update_date = Date.now();
												doc.save(function(err){
													if(err) throw err;
													return res.send({
														statusCode: 200,
														message: "customer order updated successfully"
													})
												})	
											}	  
										});										
									}																				
								}
								else{
									if(req.body.delivery_time != 'Not Required'){
										console.log("successfully hitt sms url")
										var customer_number = Number(datam.mobile_number);
										// request('http://www.global91sms.in/API/WebSMS/Http/v1.0a/index.php?username='+config.msgUname+'&password='+config.msgPass+'&sender=ORDMED&to='+datam.mobile_number+'&message=Dear+Customer,++Your+order+number:+'+doc.order_no+'+has+been+dispatched.+Delivery+Time:+'+req.body.delivery_time+'.+Delivery+Boy+:'+req.body.delivery_boy_name+'+('+req.body.delivery_boy_number+'),+Amount+Payable:+Rs.+'+req.body.final_bill_amount+'', function (err, done, body) {
										request('http://sms.global91sms.in/api/mt/SendSMS?user='+config.msgUname+'&password='+config.msgPass+'&senderid=ORDMED&channel=trans&DCS=0&flashsms=0&number='+datam.mobile_number+'&text=Dear+Customer,++Your+order+number:+'+doc.order_no+'+has+been+dispatched.+Delivery+Time:+'+req.body.delivery_time+'.+Delivery+Boy+:'+req.body.delivery_boy_name+'+('+req.body.delivery_boy_number+'),+Amount+Payable:+Rs.+'+req.body.final_bill_amount+'', function (err, done, body) {
											if(err){
												console.log('err:', err); // Print the error if one occurred
												logger.log.info(err);
												return res.send({
													statusCode: 500,
													message: "something went wrong.....6260"
												})
											}
											if(!done){
												console.log("Fail to hitt sms url");
												return res.send({
													statusCode: 400,
													message: "Fail to hitt sms url"
												})
											}									
											else{
												doc.comment = req.body.comment;
												doc.customer_orders_status = '4';
												doc.final_bill_amount = req.body.final_bill_amount;
												doc.delivery_time = req.body.delivery_time;
												doc.delivery_boy_name = req.body.delivery_boy_name;
												doc.delivery_boy_number = req.body.delivery_boy_number;
												doc.update_date = Date.now();
												doc.save(function(err){
													if(err) throw err;
													return res.send({
														statusCode: 200,
														message: "customer order updated successfully"
													})
												})	
											}	  
										});
								    }
								    else{
								    	// if()
								    	// request('http://www.global91sms.in/API/WebSMS/Http/v1.0a/index.php?username='+config.msgUname+'&password='+config.msgPass+'&sender=ORDMED&to='+datam.mobile_number+'&message=Dear+Customer,++Your+order+number:+'+doc.order_no+'+has+been+dispatched.+It will soon be delivered.+Delivery+Boy+:'+req.body.delivery_boy_name+'+('+req.body.delivery_boy_number+'),+Amount+Payable:+Rs.+'+req.body.final_bill_amount+'', function (err, done, body) {
										request('http://sms.global91sms.in/api/mt/SendSMS?user='+config.msgUname+'&password='+config.msgPass+'&senderid=ORDMED&channel=trans&DCS=0&flashsms=0&number='+datam.mobile_number+'&text=Dear+Customer,++Your+order+number:+'+doc.order_no+'+has+been+dispatched.+It will soon be delivered.+Delivery+Boy+:'+req.body.delivery_boy_name+'+('+req.body.delivery_boy_number+'),+Amount+Payable:+Rs.+'+req.body.final_bill_amount+'', function (err, done, body) {
											if(err){
												console.log('err:', err); // Print the error if one occurred
												logger.log.info(err);
												return res.send({
													statusCode: 500,
													message: "something went wrong......6293"
												})
											}
											if(!done){
												console.log("Fail to hitt sms url");
												return res.send({
													statusCode: 400,
													message: "Fail to hitt sms url"
												})
											}									
											else{
												doc.comment = req.body.comment;
												doc.customer_orders_status = '4';
												doc.final_bill_amount = req.body.final_bill_amount;
												doc.delivery_time = req.body.delivery_time;
												doc.delivery_boy_name = req.body.delivery_boy_name;
												doc.delivery_boy_number = req.body.delivery_boy_number;
												doc.update_date = Date.now();
												doc.save(function(err){
													if(err) throw err;
													return res.send({
														statusCode: 200,
														message: "customer order updated successfully"
													})
												})	
											}	  
										});								    	
								    }
								}									
								// console.log('done:', done);
								// console.log('body:', body); // Print the error if one occurred
								// return res.send({
								// 	statusCode: 200,
								// 	message: "send message Successfully"
								// })		
							}	  
						});						
					}
				})
				// doc.order_price = req.body.order_price;
				// doc.customer_orders_status = 4;
				// doc.delivery_time = req.body.delivery_time;
				// doc.delivery_body_name = req.body.delivery_body_name;
				// doc.delivery_body_number = req.body.delivery_body_number;
				// doc.save(function(err){
				// 	if(err) throw err;
				// 	return res.send({
				// 		statusCode: 200,
				// 		message: "customer order updated successfully"
				// 	})
				// })
			}			
		})
	}
}



exports.pakcage = function(req, res){
  var name_of_package = req.body.name_of_package;
  var price_of_package = req.body.price_of_package;
  var number_of_view = req.body.number_of_view;
	if(typeof name_of_package == 'undefined'
		|| typeof price_of_package == 'undefined')
	{
		console.log("All detail is not provided");
		return res.send({
			statusCode: 400,
			message: "All detail is not provided"
		})

	}
	else{
		var packages = new packagejob();
		packages.name_of_package = name_of_package;
		packages.price_of_package = price_of_package;
		packages.number_of_view = number_of_view;
		packages.save(function(err){
			if(err) throw err;
			console.log("inserted successfully");
			return res.send({
				statusCode: 200,
				message: "inserted successfully",
				data: packages
			});
		})
	}
}

exports.get_all_packages = function(req, res){
	packagejob.find({package_status: 1}, function(err, doc){
		if(err) throw err;
		if(doc == null){
			return res.send({
				statusCode: 400,
				message: "error"
			})
		}
		else{
			return res.send({
				statusCode: 200,
				message: "packege List found.",
				data: doc
			})
		}
	})
}

exports.update_package_for_store = function(req, res){
	// if(typeof req.body.packagejob_id == 'undefined'
	// 	|| typeof req.body.AcountId == 'undefined'
	// 	)
	// {
	// 	console.log("packagejob_id or AcountId is not provided");
	// 	return res.send({
	// 		statusCode: 400,
	// 		message:"packagejob_id or AcountId is not provided"
	// 	})
	// }
	// else{
	// 	packagejob.findById({_id: req.body.packagejob_id}, function(err, pkj){
	// 		if(err) throw err;
	// 		var pview = pkj.number_of_view;
	// 		var cast = parseInt(pview);
	// 		CreateAcount.findOne({_id: req.body.AcountId}, function(err, cat){
	// 			if(err) throw err;
	// 			var cview = cat.no_of_jprofile_views;
	// 			//var tview = cview + cast;
	// 			var tview = sum(cview, cast);
	// 			console.log("tview ------>", tview);
	// 			cat.no_of_jprofile_views = tview;
	// 			cat.save(function(err){
	// 				if(err) throw err;
	// 				return res.send({
	// 					statusCode: 200,
	// 					message:"Successfully Subscribed to package.",
	// 					data: cat
	// 				})
	// 			})
	// 		})			
	// 	})
	// }
}

// exports.insert_retailer_viewed_profile = function(req, res){
// 	if(typeof req.body.AcountId == 'undefined')
// 	{
// 		console.log("AcountId is not provided");
// 		return res.send({
// 			statusCode: 400,
// 			message:"AcountId is not provided"
// 		})
// 	}
// 	else{
// 		// packagejob.findById({_id: req.body.packagejob_id}, function(err, pkj){
// 			// if(err) throw err;
// 			// var pview = pkj.number_of_view;
// 			// var cast = parseInt(pview);


// 			CreateAcount.findOne({_id: req.body.AcountId}, function(err, cat){
// 				if(err) throw err;
// 				var cview = cat.remaining_view;
// 				//var tview = cview + cast;
// 				//var tview = sum(cview, cast);
// 				var tivew = ciew - 1
// 				console.log("tview ------>", tview);
// 				cat.remaining_view = tview;
// 				cat.save(function(err){
// 					if(err) throw err;
// 					return res.send({
// 						statusCode: 200,
// 						message:"Successfully Subscribed to package.",
// 						data: cat
// 					})
// 				})
// 			})			
// 		//})
// 	}
// }


exports.get_emp_profile_according_no_of_view = function(req, res){
	if(typeof req.body.AcountId == 'undefined')
	{
		console.log('create account id is not provided');
		return res.send({
			statusCode: 400,
			message:'create account id is not provided'
		})
	}
	else{
		CreateAcount.findById({_id: req.body.AcountId}, function(err, doc){
			if(err)
			{
				console.log("if err----->", err);
				logger.log.info(err);
				return res.sedn({
					statusCode: 500,
					message: "something went wrong"
				})
			}
			else{
				var nofview = doc.no_of_jprofile_views;
				// console.log(nofview);
				if(nofview == 0)
				{
					console.log("get zero emp list");
					return res.send({
						statusCode: 400,
						message: "Please Subscribe any package to get employee list",
					})
				}
				else{
					SmEmployee.find({}).limit(nofview).exec(function(err, docs){
						if(err)
						{
							console.log("if err----->", err);
							logger.log.info(err);
							return res.sedn({
								statusCode: 500,
								message: "something went wrong"
							})						
						}
						else{
							console.log("get_emp_profile_according_no_of_view");
							return res.send({
								statusCode: 200,
								message: "get employee profile list successfully",
								data: docs
							})
						}
					})
				}
			}
		})
	}
}

exports.med_not_found = function(req, res){
	if(typeof req.body.customer_id == 'undefined'
		|| typeof req.body.store_id == 'undefined'
		|| typeof req.body.search_for == 'undefined'
		|| typeof req.body.query == 'undefined'
		)
	{
		console.log('customer_id or store_id or search_for or query is  not provided');
		return res.send({
			statusCode: 400,
			message: "customer_id or store_id or search_for or query is  not provided"
		})
	}
	else{
		Store.findById({_id: req.body.store_id}, function(err,sto){
			if(err) throw err;
			customer.findById({_id: req.body.customer_id}, function(err, cst){
				if(err) throw err;
				var mednotfound = new medicine_not_found();
				mednotfound.customer_id = req.body.customer_id;
				mednotfound.store_id = req.body.store_id;
				if(cst == null || cst.phone_number == null)
					{mednotfound.customerContactnumber = ''}
				else{
					mednotfound.customerContactnumber = cst.phone_number;
				}
				if(sto == null)
					{mednotfound.storeContactnumber = '';
					mednotfound.storeName = ''; }
				else{
				    mednotfound.storeContactnumber = sto.Contact_No_1;	
				    mednotfound.storeName = sto.Pharmacy_name;
				}				
				// mednotfound.storeName = sto.Pharmacy_name;
				// mednotfound.storeContactnumber = sto.Contact_No_1;
				// mednotfound.customerContactnumber = cst.phone_number;				
				mednotfound.search_for = req.body.search_for;
				mednotfound.query = req.body.query;
				mednotfound.save(function(err){
					if(err){
						console.log("if err ---->", err);
						logger.log.info(err);
						return res.send({
							statusCode: 500,
							message: " something went wrong"
						})
					}
					else{
						return res.send({
							statusCode: 200,
							message: "Yor enquery save successfully" 		
						})
					}
				})
			})
		})			
	}
}


exports.wrong_med = function(req, res){
	if(typeof req.body.customer_id == 'undefined'
		|| typeof req.body.store_id == 'undefined'
		|| typeof req.body.medicine_id == 'undefined'
		//|| typeof req.body.query == 'undefined'
		)
	{
		console.log('customer_id or store_id or medicine_id is not provided');
		return res.send({
			statusCode: 400,
			message: "customer_id or store_id or medicine_id is not provided"
		})
	}
	else{
		Store.findById({_id: req.body.store_id}, function(err,sto){
			if(err) throw err;
			customer.findById({_id: req.body.customer_id}, function(err, cst){
				if(err) throw err;
				var wrongmed = new wrong_medicine();
				wrongmed.customer_id = req.body.customer_id;
				if(cst == null || cst.phone_number == null)
					{wrongmed.customerContactnumber = ''}
				else{
					wrongmed.customerContactnumber = cst.phone_number;
				}
				if(sto == null)
					{wrongmed.storeContactnumber = '';
					wrongmed.storeName = ''; }
				else{
				    wrongmed.storeContactnumber = sto.Contact_No_1;	
				    wrongmed.storeName = sto.Pharmacy_name;
				}
				wrongmed.store_id = req.body.store_id;
				// wrongmed.storeName = sto.Pharmacy_name;
				// wrongmed.storeContactnumber = sto.Contact_No_1;
				// wrongmed.customerContactnumber = cst.phone_number;
				// wrongmed.query = req.body.query;
				wrongmed.medicine_id = req.body.medicine_id;
				wrongmed.save(function(err){
					if(err){
						console.log("if err ---->", err);
						logger.log.info(err);
						return res.send({
							statusCode: 500,
							message: " something went wrong"
						})
					}
					else{
						return res.send({
							statusCode: 200,
							message: "Your enquiry sent successfully." 		
						})
					}
				})				
			})
		})
	}
}

exports.short_list_candidiate = function(req, res){
	if(typeof req.body.Account_id == 'undefined'
		|| typeof req.body.employee_id == 'undefined'
		)
	{
		console.log("Account_id or employee_id is not provided");
		return res.send({
			statusCode: 400,
			message: "Account_id or employee_id is not provided"
		})
	}
	else{
		shortlist_candidiate.findOne({account_id: req.body.Account_id, employee_id: req.body.employee_id}, function(err, doc){
			if(err){
				console.log("if err ------->", err);
				logger.log.info(err);
				return res.send({
					statusCode: 500,
					message: "something went wrong :"+err
				})
			}
			if(doc == null){
				var shortlistcandidate = new shortlist_candidiate();
				shortlistcandidate.account_id = req.body.Account_id;
				shortlistcandidate.employee_id = req.body.employee_id;
				shortlistcandidate.save(function(err){
					if(err){
						console.log("if err ------->", err);
						logger.log.info(err);
						return res.send({
							statusCode: 500,
							message: "something went wrong :"+err
						})
					}
					else{
						console.log("Candidate Shot listed Successfully");
						return res.send({
							statusCode: 200,
							message: "Candidate Shot listed Successfully"
						})
					}
				})
			}
			else{
				return res.send({
					statusCode: 400,
					message: "Already Shortlisted"
				})
			}
		})
	}
}


exports.delete_short_list_candidiate = function(req, res){
	if(typeof req.body.Account_id == 'undefined'
		|| typeof req.body.employee_id == 'undefined'
		)
	{
		console.log("Account_id or employee_id is not provided");
		return res.send({
			statusCode: 400,
			message: "Account_id or employee_id is not provided"
		})
	}
	else{
		shortlist_candidiate.remove({account_id: req.body.Account_id, employee_id: req.body.employee_id}, function(err, doc){
			if(err){
				console.log("if err ------->", err);
				logger.log.info(err);
				return res.send({
					statusCode: 500,
					message: "something went wrong :"+err
				})
			}
			if(doc.n == 0){
				console.log("NOt Found Shortlisted Candidate");
					return res.send({
					statusCode: 400,
					message: "NOt Found Shortlisted Candidate"
				})
			}
			else{
				return res.send({
					statusCode: 200,
					message: "Delete Shortlisted Candidate Successfully"
				})
			}
		})
	}
}

exports.get_short_list_candidiate = function(req, res){
	if(typeof req.body.Account_id == 'undefined'
		)
	{
		console.log("Account_id is not provided");
		return res.send({
			statusCode: 400,
			message: "Account_id is not provided"
		})
	}
	else{
		var employee_detail =[];
		shortlist_candidiate.find({account_id: req.body.Account_id}, function(err, docs) {
			if(err){
				console.log("if err ------->", err);
				logger.log.info(err);
				return res.send({
					statusCode: 500,
					message: "something went wrong"
				})
			}
			else{
				async.each(docs, function(doc, callback){
					if(doc == null){
						callback();
					}
					else{
						SmEmployee.findById({_id: doc.employee_id}, function(err, emp_detail){
							if(err) throw err;
							state.findOne({state_id: emp_detail.User_State}, function(err, state_n){
								city.findOne({city_id: emp_detail.User_City}, function(err, city_n){
									var obj = {};
									obj.is_email_valid = emp_detail.is_email_valid;
									obj.is_contact_valid = emp_detail.is_contact_valid;
									obj.User_Experience = emp_detail.User_Experience;
									obj.Software_Data = emp_detail.Software_Data;
									obj.Profile_Status = emp_detail.Profile_Status;
									obj.Emp_Status = emp_detail.Emp_Status;
									obj._id = emp_detail._id;
									obj.User_Fname = emp_detail.User_Fname;
									obj.User_Lname = emp_detail.User_Lname;
									obj.User_Dob = emp_detail.User_Dob;
									obj.User_Gender = emp_detail.User_Gender;
									obj.User_Email = emp_detail.User_Email;
									obj.User_Pwd = emp_detail.User_Pwd;
									obj.User_Contact = emp_detail.User_Contact;
									obj.User_Image = emp_detail.User_Image;
									obj.User_Address1 =emp_detail.User_Address1;
									obj.User_Address2 = emp_detail.User_Address2;
									obj.User_Aadhar_No = emp_detail.User_Aadhar_No;
									obj.User_State = state_n.state;
									obj.User_City = city_n.city_name;
									obj.User_PIncode = emp_detail.User_PIncode;
									obj.User_Education = emp_detail.User_Education;
			                        obj.Certi_Image = emp_detail.Certi_Image;
			                        obj.User_College = emp_detail.User_College;
			                        obj.User_University = emp_detail.User_University;
			                        obj.User_Passing_Year = emp_detail.User_Passing_Year;
			                        obj.User_Percentage = emp_detail.User_Percentage;
			                        obj.User_Experience_Type = docs.User_Experience_Type;
			                        obj.Fresher_Expected_Ctc = emp_detail.Fresher_Expected_Ctc;
			                        obj.Current_Ctc = emp_detail.Current_Ctc;
			                        obj.Expected_Ctc = emp_detail.Expected_Ctc;
			                        obj.Notice_Period = emp_detail.Notice_Period;
			                        obj.Computer_Knowledge = emp_detail.Computer_Knowledge;
			                        obj.Billing_Software = emp_detail.Billing_Software;
			                        obj.User_Create = emp_detail.User_Create;
			                        obj.User_Profile = emp_detail.User_Profile;
			                        obj.User_Update = emp_detail.User_Update;
			                        obj.view_status = emp_detail.view_status;
			                        obj._v = emp_detail._v;
			                        employee_detail.push(obj);
									callback(); 								
								})
							})	
						})
					}
				}, function(err){
					if(err){
						console.log("if err ------->", err);
						logger.log.info(err);
						return res.send({
							statusCode: 500,
							message: "something went wrong"
						})
					}
					if(employee_detail == null || employee_detail == ''){
						console.log("Not Found employee");
						return res.send({
							statusCode: 400,
							message: "Not Found employee"
						})
					}					
					else{
						console.log("get Shortlisted Candidate Successfully");
						return res.send({
							statusCode: 200,
							message: "get Shortlisted Candidate Successfully",
							data: employee_detail
						})	
					}
				})
			}
		})
	}
}


exports.get_employee_by_employee_id_inbody = function(req, res){
	if(typeof req.body.employee_id == 'undefined'
		|| typeof req.body.Account_id == 'undefined')
	{
		console.log("employee_id is not provided");
		return res.send({
			statusCode: 400,
			message: "employee_id is not provided"
		})
	}
	else{
		SmEmployee.findById({_id: req.body.employee_id}, function(err, docs){
			if(err) throw err;
			if(docs == null)
			{
				return res.send({
					statusCode: 400,
					message: "employee id is not exists",
					//employee: doc
				})
			}
			else{
				var state_name;
				var city_name;
				state.findOne({state_id: docs.User_State}, function(err, state_n){
					if(err){
						console.log("if err ------>", err);
						logger.log.info(err);
						return res.send({
							statusCode: 500,
							message: "something Went wrong"
						})
					}
					else{
						state_name = state_n.state;
					}
				})
				city.findOne({city_id: docs.User_City}, function(err, city_n){
					if(err){
						console.log("if err ------>", err);
						logger.log.info(err);
						return res.send({
							statusCode: 500,
							message: "something Went wrong"
						})
					}
					else{
						city_name = city_n.city_name;
					}
				})
				// console.log("state_name -----> * ", state_name);
				// console.log("city_name -----> * ", city_name);					
				retailer_viewed_profile.findOne({account_id: req.body.Account_id, employee_id:  req.body.employee_id}, function(err, doc){
					if(err){
						console.log("if err ------>", err);
						logger.log.info(err);
						return res.send({
							statusCode: 500,
							message: "something Went wrong"
						})
					}
					if(doc == null){
						var obj = {};
						obj.is_email_valid = docs.is_email_valid;
						obj.is_contact_valid = docs.is_contact_valid;
						obj.User_Experience = docs.User_Experience;
						obj.Software_Data = docs.Software_Data;
						obj.Profile_Status = docs.Profile_Status;
						obj.Emp_Status = docs.Emp_Status;
						obj._id = docs._id;
						obj.User_Fname = docs.User_Fname;
						obj.User_Lname = docs.User_Lname;
						obj.User_Dob = docs.User_Dob;
						obj.User_Gender = docs.User_Gender;
						obj.User_Email = docs.User_Email;
						obj.User_Pwd = docs.User_Pwd;
						obj.User_Contact = docs.User_Contact;
						obj.User_Image = docs.User_Image;
						obj.User_Address1 =docs.User_Address1;
						obj.User_Address2 = docs.User_Address2;
						obj.User_Aadhar_No = docs.User_Aadhar_No;
						obj.User_State = state_name;
						obj.User_City = city_name;
						obj.User_PIncode = docs.User_PIncode;
						obj.User_Education = docs.User_Education;
                        obj.Certi_Image = docs.Certi_Image;
                        obj.User_College = docs.User_College;
                        obj.User_University = docs.User_University;
                        obj.User_Passing_Year = docs.User_Passing_Year;
                        obj.User_Percentage = docs.User_Percentage;
                        obj.User_Experience_Type = docs.User_Experience_Type;
                        obj.Fresher_Expected_Ctc = docs.Fresher_Expected_Ctc;
                        obj.Current_Ctc = docs.Current_Ctc;
                        obj.Expected_Ctc = docs.Expected_Ctc;
                        obj.Notice_Period = docs.Notice_Period;
                        obj.Computer_Knowledge = docs.Computer_Knowledge;
                        obj.Billing_Software = docs.Billing_Software;
                        obj.total_experience = docs.total_experience;
                        obj.User_Create = docs.User_Create;
                        obj.User_Profile = docs.User_Profile;
                        obj.User_Update = docs.User_Update;
                        obj.view_status = "0";
                        //obj._v = doc._v; 
                        return res.send({
                        	statusCode: 200,
                        	message: "",
                        	data: obj
                        })
					}
					else{
						var obj = {};
						obj.is_email_valid = docs.is_email_valid;
						obj.is_contact_valid = docs.is_contact_valid;
						obj.User_Experience = docs.User_Experience;
						obj.Software_Data = docs.Software_Data;
						obj.Profile_Status = docs.Profile_Status;
						obj.Emp_Status = docs.Emp_Status;
						obj._id = docs._id;
						obj.User_Fname = docs.User_Fname;
						obj.User_Lname = docs.User_Lname;
						obj.User_Dob = docs.User_Dob;
						obj.User_Gender = docs.User_Gender;
						obj.User_Email = docs.User_Email;
						obj.User_Pwd = docs.User_Pwd;
						obj.User_Contact = docs.User_Contact;
						obj.User_Image = docs.User_Image;
						obj.User_Address1 =docs.User_Address1;
						obj.User_Address2 = docs.User_Address2;
						obj.User_Aadhar_No = docs.User_Aadhar_No;
						obj.User_State = state_name;
						obj.User_City = city_name;
						obj.User_PIncode = docs.User_PIncode;
						obj.User_Education = docs.User_Education;
                        obj.Certi_Image = docs.Certi_Image;
                        obj.User_College = docs.User_College;
                        obj.User_University = docs.User_University;
                        obj.User_Passing_Year = docs.User_Passing_Year;
                        obj.User_Percentage = docs.User_Percentage;
                        obj.User_Experience_Type = docs.User_Experience_Type;
                        obj.Fresher_Expected_Ctc = docs.Fresher_Expected_Ctc;
                        obj.Current_Ctc = docs.Current_Ctc;
                        obj.Expected_Ctc = docs.Expected_Ctc;
                        obj.Notice_Period = docs.Notice_Period;
                        obj.Computer_Knowledge = docs.Computer_Knowledge;
                        obj.Billing_Software = docs.Billing_Software;
                        obj.total_experience = docs.total_experience;
                        obj.User_Create = docs.User_Create;
                        obj.User_Profile = docs.User_Profile;
                        obj.User_Update = docs.User_Update;
                        obj.view_status = "1";
                        //obj._v = doc._v;
                        return res.send({
                        	statusCode: 200,
                        	message: "",
                        	data: obj
                        })                        
					}
				})
				// console.log(doc);
				// return res.send({
				// 	statusCode: 200,
				// 	message: "successfully get employee by employee id",
				// 	doc.push("view_status","1");
				// 	data: doc
				// })
			}
		})
	}
}

exports.adminpackage = function(req, res){
	if(typeof req.body.Account_id == 'undefined'
		|| typeof req.body.package_id == 'undefined'
		)
	{
		console.log("Account_id or package_id is not provided");
		return res.send({
			statusCode: 400,
			message: "Account_id or package_id is not provided"
		})
	}
	else{
		var adminpackage = new retailerPackagehistory();
		adminpackage.account_id = req.body.Account_id;
		adminpackage.package_id = req.body.package_id;
		adminpackage.save(function(err){
			if(err){
				console.log("if err ----->", err);
				logger.log.info(err);
				return res.send({
					statusCode: 500,
					message: "something went wrong"
				})
			}
			else{
				async.waterfall([
					function(callback){
						CreateAcount.findById(req.body.Account_id, function(err, cat){
							if(err) throw err;
							callback(null, cat);
						})
					},
					function(cat, callback){
						Store.findById(cat.Store_Id, function(err, sto){
							if(err) throw err;
							city.findOne({city_id: sto.City}, function(err, city_n){
							callback(null, sto, cat, city_n);
							})
						})
					},
					function(sto, cat, city_n, callback){
						packagejob.findById(req.body.package_id, function(err, pac){
							if(err) throw err;
			            var transporter = nodemailer.createTransport(smtpTransport({
			                service: 'gmail',
			                host: 'smtp.gmail.com',
			                proxy: 'http://localhost:3000/',
			                    port: 25,
			                auth: {
			                  user: config.email,
			                  pass: config.password
			                }
			            }));

			            var mailOptions = {
			              from: 'OrderMedicine <'+config.email+'>',
			              to: 'ravnir1987@gmail.com',//'ravnir1987@gmail.com',
			              subject: 'Request for packages',
			              html:'Dear Admin,\
			              <br><b>'+sto.Pharmacy_name+' retailer from '+city_n.city_name+'('+sto.Pin_code+') request to you for packages '+pac.price_of_package+' </b>'
			            };

				         transporter.sendMail(mailOptions, function(error, info){
				            if(error){console.log("Failed to send mail"); logger.log.info(error);}
				            	else{console.log("mail send successfully")}
				            })
				            callback();							
						})
					}])

				return res.send({
					statusCode: 200,
					message: "Your, Request has been  send successfully, Our team will get back to you soon, Thank you for Subscribtion."
				})
			}
		})
	}
}

exports.get_account_detail_by_account_id = function(req, res){
	if(typeof req.body.Account_id == 'undefined')
	{
		console.log("Account_id is not provided");
		return res.send({
			statusCode: 400,
			message: "Account_id is not provided"
		})
	}
	else{
		CreateAcount.findById({_id: req.body.Account_id}, function(err, doc){
			if(err) throw err;
			if(doc == null)
			{
				return res.send({
					statusCode: 400,
					message: "Account_id id is not exists",
					employee: doc
				})
			}
			else{
				return res.send({
					statusCode: 200,
					message: "get Accountdetail uccessfully",
					data: doc
				})
			}
		})
	}		
}

exports.get_store_detail_by_store_id = function(req, res){
	if(typeof req.body.store_id == 'undefined')
	{
		console.log("store_id is not provided");
		return res.send({
			statusCode: 400,
			message: "store_id is not provided"
		})
	}
	else{
		Store.findById({_id: req.body.store_id}, function(err, doc){
			if(err) throw err;
			if(doc == null)
			{
				return res.send({
					statusCode: 400,
					message: "store_id id is not exists",
					employee: doc
				})
			}
			else{
			    var state_name;
				var city_name;
				state.findOne({state_id: doc.State}, function(err, state_n){
					if(err){
						console.log("if err ------>", err);
						logger.log.info(err);
						return res.send({
							statusCode: 500,
							message: "something Went wrong"
						})
					}
					else{
						state_name = state_n.state;
						city.findOne({city_id: doc.City}, function(err, city_n){
							if(err){
								console.log("if err ------>", err);
								logger.log.info(err);
								return res.send({
									statusCode: 500,
									message: "something Went wrong"
								})
							}
							else{
								city_name = city_n.city_name;
								var obj = {};
								obj._id = doc._id;
								obj.CreateTime = doc.CreateTime;
								obj.Store_status = doc.Store_status;
								obj.Pharmacy_name = doc.Pharmacy_name;
								obj.Address = doc.Address;
								obj.Landmark = doc.Landmark;
								obj.Area = doc.Area;
								obj.latitude = doc.latitude;
								obj.longitude = doc.longitude;
								obj.State = state_name;
								obj.City = city_name;
								obj.Pin_code = doc.Pin_code;
								obj.Contact_No_1 = doc.Contact_No_1;
								obj.DLN = doc.DLN;
								obj.GST_Number = doc.GST_Number;
								obj.Premise = doc.Premise;
								obj.profile_image = doc.profile_image;
								obj.__v = doc.__v;
								// console.log("state_name----->", state_name);
								// console.log("city_name----->", city_name);								
								CreateAcount.findOne({Store_Id: req.body.store_id},function(err, cat){
									if(err) throw err;
									var operationalInfoId = cat.OperationalInfo_Id;
									OperationalInfo.findById({_id: operationalInfoId}, function(err, opifo){
										if(err) throw err;
										return res.send({
											statusCode: 200,
											message: "get store & operational information successfully",
											store_info: obj,
											operational_info: opifo
										})
									})
								})								
							}
						})							
					}
				})
			}
		})
	}	
}

exports.delete_add_vacancy = function(req, res){
	if(typeof req.body.add_vacancy_id == "undefined")
	{
		console.log("add_vacancy_id is not provided");
		return res.send({
			statusCode: 400,
			message: "add_vacancy_id is not provided"
		})
	}
	else{
		AddVacancy.remove({_id: req.body.add_vacancy_id}, function(err, doc){
			if(err){
				console.log("if err ------>", err);
				logger.log.info(err);
				return res.send({
					statusCode: 500,
					message: "something Went wrong"
				})
			}
			if(doc.n == 0){
				console.log("Vacancy not found");
				return res.send({
					statusCode: 400,
					message: "Vacancy not found"
				})
			}
			else{
				console.log("Vacancy delete successfully");
				return res.send({
					statusCode: 200,
					message: "Vacancy delete successfully"
				})
			}
		})
	}
}

exports.get_all_employees_applied_for_job = function(req, res){
	if(typeof req.body.vacancy_id == "undefined")
	{
		console.log("vacancy_id is not provided");
		return res.send({
			stataus: 400,
			message: "vacancy_id is not provided"
		})
	}
	else{
		apply_job.find({add_vacancy_id: req.body.vacancy_id}, function(err, docs){
			if(err){
				console.log("if err ----->",err);
				logger.log.info(err);
				return res.send({
					statusCode: 500,
					message: "something went wrong"
				})
			}
			else if(docs == null || docs == ""){
				return res.send({
					statusCode: 400,
					message: "NO any candidates apply for this vacancy yet."
				})
			}
			else{
				if(docs == null){
					console.log("No any candidate applied for this job yet");
					return res.send({
						statusCode: 400,
						message: "No any candidate applied for this job yet."
					})
				}
				else{
					var emp_data = [];
					async.each(docs, function(doc, callback){
						if(doc == null){
							call();
						}
						else{
							SmEmployee.findById({_id: doc.employee_id}, function(err, emp_detail){
								if(err){
									callback();
								}
								else{
									state.findOne({state_id: emp_detail.User_State}, function(err, state_n){
										city.findOne({city_id: emp_detail.User_City}, function(err, city_n){
											var obj = {};
											obj.is_email_valid = emp_detail.is_email_valid;
											obj.is_contact_valid = emp_detail.is_contact_valid;
											obj.User_Experience = emp_detail.User_Experience;
											obj.Software_Data = emp_detail.Software_Data;
											obj.Profile_Status = emp_detail.Profile_Status;
											obj.Emp_Status = emp_detail.Emp_Status;
											obj._id = emp_detail._id;
											obj.User_Fname = emp_detail.User_Fname;
											obj.User_Lname = emp_detail.User_Lname;
											obj.User_Dob = emp_detail.User_Dob;
											obj.User_Gender = emp_detail.User_Gender;
											obj.User_Email = emp_detail.User_Email;
											obj.User_Pwd = emp_detail.User_Pwd;
											obj.User_Contact = emp_detail.User_Contact;
											obj.User_Image = emp_detail.User_Image;
											obj.User_Address1 =emp_detail.User_Address1;
											obj.User_Address2 = emp_detail.User_Address2;
											obj.User_Aadhar_No = emp_detail.User_Aadhar_No;
											obj.User_State = ''; //state_n.state;
											obj.User_City = ''; //city_n.city_name;
											obj.User_PIncode = emp_detail.User_PIncode;
											obj.User_Education = emp_detail.User_Education;
					                        obj.Certi_Image = emp_detail.Certi_Image;
					                        obj.User_College = emp_detail.User_College;
					                        obj.User_University = emp_detail.User_University;
					                        obj.User_Passing_Year = emp_detail.User_Passing_Year;
					                        obj.User_Percentage = emp_detail.User_Percentage;
					                        obj.User_Experience_Type = docs.User_Experience_Type;
					                        obj.Fresher_Expected_Ctc = emp_detail.Fresher_Expected_Ctc;
					                        obj.Current_Ctc = emp_detail.Current_Ctc;
					                        obj.Expected_Ctc = emp_detail.Expected_Ctc;
					                        obj.Notice_Period = emp_detail.Notice_Period;
					                        obj.Computer_Knowledge = emp_detail.Computer_Knowledge;
					                        obj.Billing_Software = emp_detail.Billing_Software;
					                        obj.User_Create = emp_detail.User_Create;
					                        obj.User_Profile = emp_detail.User_Profile;
					                        obj.User_Update = emp_detail.User_Update;
					                        obj.view_status = emp_detail.view_status;
					                        obj._v = emp_detail._v;
					                        emp_data.push(obj);
											callback(); 								
										})
									})									
									// emp_data.push(empd);
									// callback();
								}
							})							
						}
					}, function(err){
						if(err){
							console.log("if err ------->", err);
							logger.log.info(err);
							return res.send({
								statusCode: 500,
								message: "something went wrong"
							})
						}
						else{
							if(emp_data == null || emp_data == ""){
								return res.send({
									statusCode: 400,
									message: "NO any candidates apply for this vacancy yet."
								})
							}
							else{
								console.log("get_all_employees_applied_for_job");
								return res.send({
									statusCode: 200,
									message: "get all employee detail successfully",
									data: emp_data
								})
							}
						}
					})
				}
			}
		})
	}
}


// exports.emp_find_by_pin_code = function(req, res){
//     //var pincode = req.body.pincode;
// 	// for(var i = 0, i< pincode, i++){
// 		// SmEmployee.find({User_PIncode: req.body.pincode}, function(err, fist){

// 		// })
// 	// }
// 	// var secon = pincode -1;
// 	// console.log(secon);
// 	Store.findById({_id: req.body.store_id}, function(err, store){
// 		if(err){
// 			console.log("if err ----->", err);
// 			return res.send({
// 				statusCode: 500,
// 				message: " Something went wrong"
// 			})			
// 		}
// 		else{
// 			var pincode = store.Pin_code;
// 			var pincode1 = store.Pin_code;
// 			var pincode2 = store.Pin_code;
// 			var pincode3 = store.Pin_code;									
// 			var pincode4 = store.Pin_code;

// 			SmEmployee.find({User_PIncode: pincode}, function(err, codone){
// 				if(err){
// 					console.log("if err ----->", err);
// 					return res.send({
// 						statusCode: 500,
// 						message: " Something went wrong"
// 					})
// 				}
// 				else{
// 					var all_pincode_epm = [];
// 					async.each(codone, function(doco, callback){
// 						if(doco == null){
// 							callback();
// 						}
// 						else{
// 							all_pincode_epm.push(doco);
// 							callback();
// 						}
// 					},
// 					function(err){
// 						if(err){
// 							console.log("if err ----->", err);
// 							return res.send({
// 								statusCode: 500,
// 								message: " Something went wrong"
// 							})					
// 						}
// 						else{
// 							var secon = pincode1 -1;
// 							SmEmployee.find({User_PIncode: secon}, function(err, codsec){
// 								if(err){
// 									console.log("if err ----->", err);
// 									return res.send({
// 										statusCode: 500,
// 										message: " Something went wrong"
// 									})					
// 								}
// 								else{
// 									async.each(codsec, function(cods, call){
// 										if(cods == null){
// 											call();
// 										}
// 										else{
// 											all_pincode_epm.push(cods);
// 											call();					
// 										}
// 									},function(err){
// 										if(err){
// 											console.log("if err ----->", err);
// 											return res.send({
// 												statusCode: 500,
// 												message: " Something went wrong"
// 											})					
// 										}
// 										else{
// 											var pinc = sum(pincode2, 1);
// 											SmEmployee.find({User_PIncode: pinc}, function(err, codlsec){
// 												if(err){
// 													console.log("if err ----->", err);
// 													return res.send({
// 														statusCode: 500,
// 														message: " Something went wrong"
// 													})					
// 												}
// 												else{
// 													async.each(codlsec, function(codls, calls){
// 														if(codls == null){
// 															calls();
// 														}
// 														else{
// 															all_pincode_epm.push(codls);
// 															calls();
// 														}
// 													}, function(err){
// 														if(err){
// 															console.log("if err ----->", err);
// 															return res.send({
// 																statusCode: 500,
// 																message: " Something went wrong"
// 															})					
// 														}
// 														else{
// 															var pinco = sum(pincode3, 2)
// 															SmEmployee.find({User_PIncode: pinco}, function(err, codlths){
// 																if(err){
// 																	console.log("if err ----->", err);
// 																	return res.send({
// 																		statusCode: 500,
// 																		message: " Something went wrong"
// 																	})					
// 																}
// 																else{
// 																	async.each(codlths, function(codlth, callcom){
// 																		if(codlth == null){
// 																			callcom();
// 																		}
// 																		else{
// 																			all_pincode_epm.push(codlth);
// 																			callcom();
// 																		}
// 																	}, function(err){
// 																		if(err){
// 																			console.log("if err ----->", err);
// 																			return res.send({
// 																				statusCode: 500,
// 																				message: " Something went wrong"
// 																			})					
// 																		}
// 																		else{
// 																			var pinco = pincode4 - 2;
// 																			SmEmployee.find({User_PIncode: pinco}, function(err, codths){
// 																				if(err){
// 																					console.log("if err ----->", err);
// 																					return res.send({
// 																						statusCode: 500,
// 																						message: " Something went wrong"
// 																					})					
// 																				}
// 																				else{
// 																					async.each(codths, function(codth, callm){
// 																						if(codth == null){
// 																							callm();
// 																						}
// 																						else{
// 																							all_pincode_epm.push(codth);
// 																							callm();
// 																						}
// 																					}, function(err){
// 																						if(err){
// 																							console.log("if err ----->", err);
// 																							return res.send({
// 																								statusCode: 500,
// 																								message: " Something went wrong"
// 																							})					
// 																						}
// 																						// else{
// 																							var emp_pincode = [];
// 																							async.each(all_pincode_epm, function(docs, callback){
// 																								if(docs == null){
// 																									callback();
// 																								}
// 																								else{
// 																									var state_name;
// 																									var city_name;
// 																									state.findOne({state_id: docs.User_State}, function(err, state_n){
// 																										state_name = state_n.state;
// 																										city.findOne({city_id: docs.User_City}, function(err, city_n){
// 																											city_name = city_n.city_name;
// 																											console.log("city----->",city_name);
																								
// 																									var obj = {};
// 																									obj.is_email_valid = docs.is_email_valid;
// 																									obj.is_contact_valid = docs.is_contact_valid;
// 																									obj.User_Experience = docs.User_Experience;
// 																									obj.Software_Data = docs.Software_Data;
// 																									obj.Profile_Status = docs.Profile_Status;
// 																									obj.Emp_Status = docs.Emp_Status;
// 																									obj._id = docs._id;
// 																									obj.User_Fname = docs.User_Fname;
// 																									obj.User_Lname = docs.User_Lname;
// 																									obj.User_Dob = docs.User_Dob;
// 																									obj.User_Gender = docs.User_Gender;
// 																									obj.User_Email = docs.User_Email;
// 																									obj.User_Pwd = docs.User_Pwd;
// 																									obj.User_Contact = docs.User_Contact;
// 																									obj.User_Image = docs.User_Image;
// 																									obj.User_Address1 =docs.User_Address1;
// 																									obj.User_Address2 = docs.User_Address2;
// 																									obj.User_Aadhar_No = docs.User_Aadhar_No;
// 																									obj.User_State = state_name;
// 																									obj.User_City = city_name;
// 																									obj.User_PIncode = docs.User_PIncode;
// 																									obj.User_Education = docs.User_Education;
// 																			                        obj.Certi_Image = docs.Certi_Image;
// 																			                        obj.User_College = docs.User_College;
// 																			                        obj.User_University = docs.User_University;
// 																			                        obj.User_Passing_Year = docs.User_Passing_Year;
// 																			                        obj.User_Percentage = docs.User_Percentage;
// 																			                        obj.User_Experience_Type = docs.User_Experience_Type;
// 																			                        obj.Fresher_Expected_Ctc = docs.Fresher_Expected_Ctc;
// 																			                        obj.Current_Ctc = docs.Current_Ctc;
// 																			                        obj.Expected_Ctc = docs.Expected_Ctc;
// 																			                        obj.Notice_Period = docs.Notice_Period;
// 																			                        obj.Computer_Knowledge = docs.Computer_Knowledge;
// 																			                        obj.Billing_Software = docs.Billing_Software;
// 																			                        obj.User_Create = docs.User_Create;
// 																			                        obj.User_Profile = docs.User_Profile;
// 																			                        obj.User_Update = docs.User_Update;
// 																			                        //obj.view_status = "1";
// 																									emp_pincode.push(obj);
// 																									callback();
// 																									})
// 																								})
// 																								}
// 																							}, function(err){
// 																								if(err) throw err;
// 																								if(emp_pincode == null){
// 																									return res.send({
// 																										statusCode: 400,
// 																										message: "you have no any Nearest candidates"
// 																									})																									
// 																								}
// 																								return res.send({
// 																									statusCode: 200,
// 																									message: "gett employee by Pin code successfully",
// 																									data: emp_pincode
// 																								})																								
// 																							})
// 																							// return res.send({
// 																							// 	statusCode: 200,
// 																							// 	message: "get employee by Pin code successfully",
// 																							// 	data: all_pincode_epm
// 																							// })
// 																					    // }												
// 																					})
// 																				}									
// 																			})																	
// 																		}												
// 																	})
// 																}									
// 															})
// 														}												
// 													})
// 												}									
// 											})
// 										}
// 									})
// 								}						
// 							})				
// 						}
// 					})
// 				}
// 			})
// 		}
// 	})
// }

exports.emp_find_by_pin_code = function(req, res){
	SmEmployee.find({Profile_Status: '4'},function(err, doc){
		if(err) throw err;
		var emp_pincode = [];
		async.each(doc, function(docs, callback){
			if(docs == null || docs == ''){
				callback();
			}
			else{
				if(docs.User_State == '' || docs.User_City == '' || docs.User_State == null || docs.User_City == null){
					callback();
				}
				else{
					var state_name;
					var city_name;
					state.findOne({state_id: docs.User_State}, function(err, state_n){
						if(state_n == null || state_n == '' ){
							callback();
						}
						else{
							state_name = state_n.state;
							city.findOne({city_id: docs.User_City}, function(err, city_n){
								if(city_n == null || city_n == '' ){
									callback();
								 }
								else{
									city_name = city_n.city_name;	
									var obj = {};
									obj.is_email_valid = docs.is_email_valid;
									obj.is_contact_valid = docs.is_contact_valid;
									obj.User_Experience = docs.User_Experience;
									obj.Software_Data = docs.Software_Data;
									obj.Profile_Status = docs.Profile_Status;
									obj.Emp_Status = docs.Emp_Status;
									obj._id = docs._id;
									obj.User_Fname = docs.User_Fname;
									obj.User_Lname = docs.User_Lname;
									obj.User_Dob = docs.User_Dob;
									obj.User_Gender = docs.User_Gender;
									obj.User_Email = docs.User_Email;
									obj.User_Pwd = docs.User_Pwd;
									obj.User_Contact = docs.User_Contact;
									obj.User_Image = docs.User_Image;
									obj.User_Address1 =docs.User_Address1;
									obj.User_Address2 = docs.User_Address2;
									obj.User_Aadhar_No = docs.User_Aadhar_No;
									obj.User_State = state_name;
									obj.User_City = city_name;
									obj.User_PIncode = docs.User_PIncode;
									obj.User_Education = docs.User_Education;
					                obj.Certi_Image = docs.Certi_Image;
					                obj.User_College = docs.User_College;
					                obj.User_University = docs.User_University;
					                obj.User_Passing_Year = docs.User_Passing_Year;
					                obj.User_Percentage = docs.User_Percentage;
					                obj.User_Experience_Type = docs.User_Experience_Type;
					                obj.Fresher_Expected_Ctc = docs.Fresher_Expected_Ctc;
					                obj.Current_Ctc = docs.Current_Ctc;
					                obj.Expected_Ctc = docs.Expected_Ctc;
					                obj.Notice_Period = docs.Notice_Period;
					                obj.Computer_Knowledge = docs.Computer_Knowledge;
					                obj.Billing_Software = docs.Billing_Software;
					                obj.User_Create = docs.User_Create;
					                obj.User_Profile = docs.User_Profile;
					                obj.User_Update = docs.User_Update;
					                obj.total_experience = docs.total_experience;
					                //obj.view_status = "1";
									emp_pincode.push(obj);
									callback();
								}
							})
						}
						})
			       	}
					}
				},
			 function(err){
				if(err) throw err;
				// if(emp_pincode == null){
				// 	return res.send({
				// 		statusCode: 400,
				// 		message: "you have no any Nearest candidates"
				// 	})																									
				// }
				emp_pincode.sort(function (a, b) {
				  return b.User_Update - a.User_Update;
				});				
				return res.send({
					statusCode: 200,
					message: "get all candidates successfully.",
					data: emp_pincode
				})																								
			})
	})
}

exports.insert_retailer_viewed_profile = function(req, res){
	if(typeof req.body.Account_id == 'undefined'
		|| typeof req.body.employee_id == 'undefined'
		)
	{
		console.log("Account_id or employee_id is not provided");
		 return res.send({
		 	statusCode: 400,
		 	message: "Account_id or employee_id is not provided"
		 })
	}
	else{
		var retailer_viewed = new retailer_viewed_profile();
		retailer_viewed.account_id = req.body.Account_id;
		retailer_viewed.employee_id = req.body.employee_id;
		retailer_viewed.save(function(err){
			if(err){
				console.log("if err ----->", err);
				logger.log.info(err);
				return res.send({
					statusCode: 500,
					message: "something went wrong."
				})
			}
			else{
				CreateAcount.findOne({_id: req.body.Account_id}, function(err, cat){
					if(err) throw err;
					var cview = cat.remaining_view;
					var tivew = cview - 1
					// console.log("tview ------>", tivew);
					cat.remaining_view = tivew;
					cat.save(function(err){
						if(err) throw err;
						return res.send({
							statusCode: 200,
							message:"One view used",
							data: cat
						})
					})
				})
			}
		})
	}
}



// exports.insert_retailer_viewed_profile = function(req, res){
// 	if(typeof req.body.AcountId == 'undefined')
// 	{
// 		console.log("AcountId is not provided");
// 		return res.send({
// 			statusCode: 400,
// 			message:"AcountId is not provided"
// 		})
// 	}
// 	else{
// 			CreateAcount.findOne({_id: req.body.AcountId}, function(err, cat){
// 				if(err) throw err;
// 				var cview = cat.remaining_view;
// 				var tivew = ciew - 1
// 				console.log("tview ------>", tview);
// 				cat.remaining_view = tview;
// 				cat.save(function(err){
// 					if(err) throw err;
// 					return res.send({
// 						statusCode: 200,
// 						message:"Successfully Subscribed to package.",
// 						data: cat
// 					})
// 				})
// 			})			
// 	}
// }

exports.create_staff = function(req, res){
	if(typeof req.body.Account_id == 'undefined'
		|| typeof req.body.customer_id == 'undefined')
	{
		console.log("Account_id or customer_id is not provided");
		return res.send({
			statusCode: 400,
			message: "Account_id or customer_id is not provided"
		})
	}
	else{
		var account_id = req.body.Account_id;
		var customer_id = req.body.customer_id;
		CreateAcount.findById({_id: account_id}, function(err, docs){
			if(err){
				console.log("if err -------> ", err);
				logger.log.info(err);
				return res.send({
					statusCode: 500,
					message: "something went wrong"
				})
			}
			else{
				if(docs){
					// console.log("c=> ",customer_id," ++++++ ","s=> ", docs.Store_Id);
					customer.findOne({_id: customer_id, Store_Id: docs.Store_Id}, function(err, doc){
						if(err){
							console.log("if err ------> ", err);
							logger.log.info(err);
							return res.send({
								statusCode: 500,
								message: "Something Went wrong"
							})
						}
						else{
							if(!doc || doc == null){
								console.log("This Customer belongs to other Pharmacy");
								return res.send({
									statusCode: 400,
									message: "This Customer belongs to other Pharmacy"
								})
							}
							else{
								staff.findOne({customer_email: doc.email_id}, function(err, staf){
									if(err){
										console.log("if err ------> ",err);
										logger.log.info(err);
										return res.send({
											statusCode: 500,
											message: "Something Went wrong"
										})
									}
									if(staf == null){
										var staff_table = new staff();
										staff_table.account_id = account_id;
										staff_table.customer_id = customer_id;
										staff_table.customer_store_id = doc.Store_Id;								
										staff_table.customer_email = doc.email_id;
										staff_table.customer_number = doc.phone_number;
										staff_table.customer_address = doc.address;
										staff_table.customer_fcm = doc.fcm;
										staff_table.save(function(err){
											if(err){
												console.log("if err ------> ",err);
												logger.log.info(err);
												return res.send({
													statusCode: 500,
													message: "Something Went wrong"
												})
											}
											else{
												console.log("Staff added successfully");
												return res.send({
													statusCode: 200,
													message: "Staff added successfully",
													data: staff_table
												})
											}
										})		
									}
									else{
										console.log("staff is aleady added");
										return res.send({
											statusCode: 400,
											message: "Staff already added"
										})
									}
								})						
							}
						}
					})
				}
				else{
					console.log("Create account not found");
					return res.send({
						statusCode: 400,
						message: "Create account not found"
					})
				}
			}
		})
	}
}

exports.staff_delete = function(req, res){
	if(typeof req.body.staff_id == 'undefined'
		|| typeof req.body.Account_id == 'undefined'
		)
	{
		console.log("staff_id or Account_id is not provided");
		return res.send({
			statusCode: 400,
			message: "staff_id or Account_id is not provided"
		})
	}
	else{
		staff.remove({_id: req.body.staff_id, account_id: req.body.Account_id}, function(err, doc){
			if(err) throw err;
			if(doc.n == 0){
				console.log("no staff found");
				return res.send({
					statusCode: 400,
					message: "no staff found"
				})
			}
			else{
				return res.send({
					statusCode: 200,
					message: "successfully delete staff",
					//data: doc
				})				
			}			
		})
	}
} 

exports.get_staff_by_account_id = function(req, res){
	if(typeof req.body.Account_id == 'undefined')
	{
		console.log("Account_id is not provided");
		return res.send({
			statusCode: 400,
			message: "Account_id is not provided"
		})
	}
	else{
		staff.find({account_id: req.body.Account_id}, function(err, doc){
			if(err) throw err;
			if(doc == null){
				return res.send({
					statusCode: 400,
					message: "No staff assign"
				})
			}
			if(doc == ''){
				return res.send({
					statusCode: 400,
					message: "No staff assign"
				})
			}			
			else{
				return res.send({
					statusCode: 200,
					message: "get  all staff successfully.",
					data: doc
				})
			}
		})
	}
}

exports.check_job_posted_by_store = function(req, res){
	if(typeof req.body.Account_id == 'undefined'){
		console.log("account is is not provided");
		return res.send({
			statusCode: 400,
			message: "account is is not provided"
		})
	}
	else{
		AddVacancy.find({AcountId: req.body.Account_id}, function(err, doc){
			if(err){
				console.log("if err ------> ", err);
				logger.log.info(err);
				return res.send({
					statusCode: 500,
					message: "Something Went wrong"
				})
			}
			else{
				if(doc.length < 5){
					console.log("Status true for less then 5 job post");
					return res.send({
						statusCode: 200,
						message: "You can post 5 jobs.",
						status: "true"
					})
				}
				else{
					console.log("Status false for more then 5 job post");
					return res.send({
						statusCode: 400,
						message: "You can post only 5 jobs.",
						status: "false"
					})					
				}
			}
		})
	}
}


exports.delete_notification = function(req, res){
	if( typeof req.body.notification_id != "undefined"
		&& typeof req.body.store_id != "undefined"
		)
	{
		var notification_id = mongoose.Types.ObjectId(req.body.notification_id);
		notification.remove({_id: notification_id,  store_id: req.body.store_id}, function(err, doc){
			if(err){
				console.log("if err ------> ", err);
				logger.log.info(err);
				return res.send({
					statusCode: 500,
					message: "Something Went wrong"
				})
			}
			else{
				// if(doc.n == 0){
				// 	console.log("no notification found");
				// 	return res.send({
				// 		statusCode: 400,
				// 		message: "no notification found - 1"
				// 	})
				// }
				// else{
					console.log("Delete notification of store successfully");
					return res.send({
						statusCode: 200,
						message: "notification deleted successfully"
					})					
				// }				
			}
		})
	}
	else if(typeof req.body.store_id != "undefined")
	{
		notification.remove({store_id: req.body.store_id}, function(err, doc){
			if(err){
				console.log("if err ------> ", err);
				logger.log.info(err);
				return res.send({
					statusCode: 500,
					message: "Something Went wrong"
				})
			}
			else{
				// if(doc.n == 0){
				// 	console.log("no notification found");
				// 	return res.send({
				// 		statusCode: 400,
				// 		message: "no notification found - 2"
				// 	})
				// }
				// else{
					console.log("all notification deleted successfully");
					return res.send({
						statusCode: 200,
						message: "all notification deleted successfully"
					})					
				// }				
			}
		})
	}
	else{
		console.log("notification_id or store_id is not provided");
		return res.send({
			statusCode: 400,
			message: "notification_id or store_id is not provided"
		})	
	}
}						
			

exports.search_order_detail = function(req, res){
	if(typeof req.body.store_id == 'undefined'
		|| typeof req.body.query == 'undefined'
		)
	{
		console.log("store_id or query is not provided");
		 return res.send({
		 	statusCode: 400,
		 	message: "store_id or query is not provided"
		 })
	}
	else{
	     //, { update_date: { $regex: '^' + req.body.query, $options:"i" }}
		//{ $or : [ {store_name: { $regex: '^' + req.body.name, $options:"i" }}, { comment: { $regex: '^' + req.body.name, $options:"i" }} ] },
		//var query = { store_name: { $regex: '^' + req.body.name, $options:"i" } comment: { $regex: '^' + req.body.name, $options:"i" }};
		var query = {Store_Id: req.body.store_id, $and: [ { $or : [ {store_name: { $regex: '^' + req.body.query, $options:"i" }}, { comment: { $regex: '^' + req.body.query, $options:"i" }}, { order_item_count: { $regex: '^' + req.body.query, $options:"i" }}, { order_price: { $regex: '^' + req.body.query, $options:"i" }}, { order_no: { $regex: '^' + req.body.query, $options:"i" }}] }]};
		customer_order.find(query, function(err, doc){
			if(err){
				console.log("if err ----->", err);
				logger.log.info(err);
				return res.send({
					statusCode: 500,
					message: "Something went wrong"
				})
			}
			else{
				if(doc == null){
					console.log("data not found");
					return res.send({
						statusCode: 400,
						message: "data not found"
					})
				}
				if(doc == ''){
					console.log("data not found");
					return res.send({
						statusCode: 400,
						message: "data not found"
					})
				}				
				else{
					console.log("data found successfully");
					return res.send({
						statusCode: 200,
						message: "data found successfully",
						data: doc
					})					
				}
			}
		})
	}
}

//{ $text: { $search: req.body.name}}

// exports.create_med_enquiry = function(req, res){
// 	var med_name = req.body.med_name;
// 	var quantity = req.body.quantity;
// 	var customer_id = req.body.customer_id;
// 	if(typeof customer_id == 'undefined'
// 		|| typeof med_name == 'undefined'
// 		|| typeof quantity == 'undefined'
//       )
// 	{
// 		console.log("customer_id or med_name or quantity is not provided");
// 		return res.send({
// 			statusCode: 400,
// 			message: "customer_id or med_name or quantity is not provided"
// 		})
// 	}
// 	else{
// 		var enquiry_id;
// 		//var distance = [];
// 		var med_finder = new medFinder();
// 		med_finder.customer_id = customer_id;
// 		med_finder.med_name = med_name;
// 		med_finder.searchingradius = 0;
// 		med_finder.status = 'SEARCHING';
// 		med_finder.save(function(err){
// 			if(err) throw err;
// 			enquiry_id = med_finder.id;
// 			console.log("create request successfully");
// 			return res.send({
// 				statusCode: 200,
// 				message: "create request successfully",
// 				enquiry_id: enquiry_id
// 			})
//         })
//     }
// }

exports.create_med_enquiry = function(req, res){
	//var enquiry_id = req.body.enquiry_id;
	if( typeof req.body.latitude == 'undefined'
		|| typeof req.body.longitude == 'undefined'
		|| typeof req.body.customer_id == 'undefined'
		|| typeof req.body.med_name == 'undefined'
		|| typeof req.body.quantity == 'undefined'
	  )
	{
		console.log("enquiry_id or latitude or longitude is not provided");
		return res.send({
			statusCode: 400,
			message: "enquiry_id or latitude or longitude is not provided"
		})
	}
	else{
		var med_finder = new medFinder();
		med_finder.customer_id = req.body.customer_id;
		med_finder.med_name = req.body.med_name;
		med_finder.quantity = req.body.quantity;
		med_finder.latitude = req.body.latitude;
		med_finder.longitude = req.body.longitude;
		med_finder.searchingradius = 2000;
		// med_finder.status = '0';
		med_finder.save(function(err){
			if(err) throw err;
			var enquiry_id = med_finder.id;
			console.log("create request successfully");
			Store.find({Store_status: true}, function(err, docs){
				if(err) throw err;
				async.each(docs, function(doc, callback){
					if(doc == null)
					{
						callback();
					}
					else{
						var store_id = doc.id;
					 	var userStoreDistance = geodist(
			            {lat: req.body.latitude, lon: req.body.longitude}, 
			            {lat: doc.latitude, lon: doc.longitude},
			            {exact: true, unit: 'meters'});
				        if(userStoreDistance <= 2000){
				            // console.log(doc.store_fcm, "userStoreDistance <= 2000  store_id", doc.id);
		                	CreateAcount.findOne({Store_Id : doc.id}, function(err, crt){
		                		if(err || typeof crt == 'undefined' || crt == null ||  crt.store_fcm == null){ 
		                			callback();
		                		 }
	                			else{
	                				var fcmToken = crt.store_fcm;
				                	var responsestore = new response_store();
				                	responsestore.enquiry_id = enquiry_id;
				                	responsestore.store_id = store_id;
				                	responsestore.status = '0';
				                	responsestore.save(function(err){
				                		if(err){ logger.log.info(err); 
				                			return res.send({statusCode: 200, message: "please try after some time."}) }
										var notifications = new notificationcr();
										notifications.customer_id = req.body.customer_id;	
										notifications.store_id = store_id;
										notifications.title = "Med Enquiry";
										notifications.message = "Medicine request from customer. Please check and respond.";
										notifications.flag = '1';
										notifications.save();				                			
					                    var payload = {
					                       notification: {
					                          title:  "Med Enquiry",
					                          body: "Medicine request from customer. Please check and respond.",
					                          tag: "Search4Medicine",
					                          sound: "retailer_ring"
					                       }
					                    };
					                 admin.messaging().sendToDevice(fcmToken, payload)
				                    .then(function(response) {console.log("medfinder sent msg to retailer:"); callback();} )
				                    .catch(function(err) {console.log("Error sending message:"); logger.log.info(err); callback();} );	
				                    })					                				
	                			}
		                	})
				        }
				        else{
				            callback();
				        }
					}
				},
				function(err){
					if(err) throw err;
					return res.send({
						statusCode: 200,
						message: "Searching medicine ......."
					})
				})
			})
		})  
	}
}


exports.medicine_finder = function(req, res){
	var enquiry_id = req.body.enquiry_id;
	if(typeof enquiry_id == 'undefined')
	{
		console.log("enquiry_id is not provided");
		return res.send({
			statusCode: 400,
			message: "enquiry_id is not provided"
		})
	}
	else{
		var quantity;
		medFinder.findById({_id: enquiry_id}, function(err, med_f){
			if(err) throw err;
			else{
				var latitude = med_f.latitude;
				var longitude = med_f.longitude;
				var final_distance = sum(med_f.searchingradius, 2000);
				med_f.searchingradius = final_distance;
				med_f.create_date = Date.now();
				med_f.save(function(err){
					if(err) throw err;
					// console.log(final_distance);
					Store.find({Store_status: true}, function(err, docs){
						if(err) throw err;
						async.each(docs, function(doc, callback){
							if(doc == null)
							{
								callback();
							}
							else{
								var store_id = doc.id;
						 		var userStoreDistance = geodist(
				                {lat: latitude, lon: longitude}, 
				                {lat: doc.latitude, lon: doc.longitude},
				                {exact: true, unit: 'meters'});
				               if(final_distance == 4000){
				                	console.log("userStoreDistance == 4000");
					                if(userStoreDistance > 2000 && userStoreDistance <= 4000 ){
					                	CreateAcount.findOne({Store_Id : doc.id}, function(err, crt){
					                		if(err || typeof crt == 'undefined' ||  crt.store_fcm == null){ 
					                			callback();
					                		 }
				                			else{
				                				var fcmToken = crt.store_fcm;
							                	var responsestore = new response_store();
							                	responsestore.enquiry_id = enquiry_id;
							                	responsestore.store_id = store_id;
							                	responsestore.status = '0';
							                	responsestore.save(function(err){
							                		if(err){ return res.send({statusCode: 200, message: "please try after some time."}) }
													var notifications = new notificationcr();
													notifications.customer_id = req.body.customer_id;	
													notifications.store_id = store_id;
													notifications.title = "Med Enquiry";
													notifications.message = "Medicine request from customer. Please check and respond.";
													notifications.flag = '1';
													notifications.save();							                			
								                    var payload = {
								                       notification: {
								                          title: "Med Enquiry",
								                          body: "Medicine request from customer. Please check and respond.",
								                          tag: "OrderMedicine",
								                          sound: "retailer_ring"
								                       }
								                    };
								                 admin.messaging().sendToDevice(fcmToken, payload)
							                    .then(function(response) {console.log("medfinder sent msg to retailer:"); callback();} )
							                    .catch(function(err) {console.log("Error sending message:"); logger.log.info(err); callback();} );	
							                    })					                				
				                			}
					                	})
					                }
					                else{
					                	callback();
					                }			                	
				                }
				                else if(final_distance == 6000){
				                	console.log("userStoreDistance == 6000");
					                if(userStoreDistance > 4000 && userStoreDistance <= 6000 ){
					                	CreateAcount.findOne({Store_Id : doc.id}, function(err, crt){
					                		if(err || typeof crt == 'undefined' ||  crt.store_fcm == null){ 
					                			callback();
					                		 }
				                			else{
				                				var fcmToken = crt.store_fcm; 
							                	var responsestore = new response_store();
							                	responsestore.enquiry_id = enquiry_id;
							                	responsestore.store_id = store_id;
							                	responsestore.status = '0';
							                	responsestore.save(function(err){
							                		if(err){logger.log.info(err);
							                		 return res.send({statusCode: 200, message: "please try after some time."}) }
													var notifications = new notificationcr();
													notifications.customer_id = req.body.customer_id;	
													notifications.store_id = store_id;
													notifications.title = "Med Enquiry";
													notifications.message = "Medicine request from customer. Please check and respond.";
													notifications.flag = '1';
													notifications.save();							                			
								                    var payload = {
								                       notification: {
								                          title:  "Med Enquiry",
								                          body: "Medicine request from customer. Please check and respond.",
								                          tag: "OrderMedicine",
								                          sound: "retailer_ring"
								                       }
								                    };
								                 admin.messaging().sendToDevice(fcmToken, payload)
							                    .then(function(response) {console.log("medfinder sent msg to retailer:"); callback();} )
							                    .catch(function(err) {console.log("Error sending message:"); logger.log.info(err); callback();} );	
							                    })					                				
				                			}
					                	})
					                }
					                else{
					                	callback();
					                }			                	
				                }
				                else if(final_distance == 8000){
				                	console.log("userStoreDistance == 8000");
					                if(userStoreDistance > 6000 && userStoreDistance <= 8000 ){
					                	CreateAcount.findOne({Store_Id : doc.id}, function(err, crt){
					                		if(err || typeof crt == 'undefined' ||  crt.store_fcm == null){ 
					                			callback();
					                		 }
				                			else{
				                				var fcmToken = crt.store_fcm;
							                	var responsestore = new response_store();
							                	responsestore.enquiry_id = enquiry_id;
							                	responsestore.store_id = store_id;
							                	responsestore.status = '0';
							                	responsestore.save(function(err){
							                		if(err){ return res.send({statusCode: 200, message: "please try after some time."}) }
													var notifications = new notificationcr();
													notifications.customer_id = req.body.customer_id;	
													notifications.store_id = store_id;
													notifications.title = "Med Enquiry";
													notifications.message = "Medicine request from customer. Please check and respond.";
													notifications.flag = '1';
													notifications.save();							                			
								                    var payload = {
								                       notification: {
								                          title:  "Med Enquiry",
								                          body: "Medicine request from customer. Please check and respond.",
								                          tag: "OrderMedicine",
								                          sound: "retailer_ring"
								                       }
								                    };
								                 admin.messaging().sendToDevice(fcmToken, payload)
							                    .then(function(response) {console.log("medfinder sent msg to retailer:"); callback();} )
							                    .catch(function(err) {console.log("Error sending message:"); logger.log.info(err); callback();} );	
							                    })					                				
				                			}
					                	})
					                }
					                else{
					                	callback();
					                }			                	
				                }
				                else if(final_distance == 10000){
				                	console.log("userStoreDistance == 10000");
					                if(userStoreDistance > 8000 && userStoreDistance <= 10000 ){
					                	CreateAcount.findOne({Store_Id : doc.id}, function(err, crt){
					                		if(err || typeof crt == 'undefined' ||  crt.store_fcm == null){ 
					                			callback();
					                		 }
				                			else{
				                				var fcmToken = crt.store_fcm;
							                	var responsestore = new response_store();
							                	responsestore.enquiry_id = enquiry_id;
							                	responsestore.store_id = store_id;
							                	responsestore.status = '0';
							                	responsestore.save(function(err){
							                		if(err){ return res.send({statusCode: 200, message: "please try after some time."}) }
													var notifications = new notificationcr();
													notifications.customer_id = req.body.customer_id;	
													notifications.store_id = store_id;
													notifications.title = "Med Enquiry";
													notifications.message = "Medicine request from customer. Please check and respond.";
													notifications.flag = '1';
													notifications.save();							                			
								                    var payload = {
								                       notification: {
								                          title:  "Med Enquiry",
								                          body: "Medicine request from customer. Please check and respond.",
								                          tag: "OrderMedicine",
								                          sound: "retailer_ring"
								                       }
								                    };
								                 admin.messaging().sendToDevice(fcmToken, payload)
							                    .then(function(response) {console.log("medfinder sent msg to retailer:"); callback();} )
							                    .catch(function(err) {console.log("Error sending message:"); logger.log.info(err); callback();} );	
							                    })					                				
				                			}
					                	})
					                }
					                else{
					                	callback();
					                }			                	
				                }
				                else{
				                	callback();
				                }			                			                			                			                			                
							}
						}, function(err){
							if(err) throw err;
							return res.send({
								statusCode: 200,
								message: "Searching medicine ......."
							})
					    })
				    })					
				});
			}
	    })
	}	    
}

exports.get_all_enquiry_by_customer = function(req, res){
	if(typeof req.body.customer_id == 'undefined')
	{
		console.log("customer_id is not provided");
		return res.send({
			statusCode: 400,
			message: "customer_id is not provided"
		})
	}
	else{
		medFinder.find({customer_id: req.body.customer_id},{},{ sort :{ create_date : -1}}, function(err, docs){
			if(err){
				console.log("if err ------>", err);
				logger.log.info(err);
				 return res.send({
				 	statusCode: 400,
				 	message: "something went wrong"
				 })
			}
			if(docs == null){
				 return res.send({
				 	statusCode: 400,
				 	message: "No MedEnquiry"
				 })
			}			
			if(docs == ''){
				 return res.send({
				 	statusCode: 400,
				 	message: "No MedEnquiry"
				 })
			}			
			else{
				async.each(docs, function(doc, callback){
					if(doc == null){
						callback();
					}
					else{
						var c_date = Date.now();
						var p_date = doc.create_date;
						var tdaye = c_date - p_date;
						var t_time = Math.floor(tdaye / 60e3);
						if(doc.status == '3'){
							callback();
						}
						else{
							// console.log("response_count    ==========>>>>>>>",doc.response_count);
							if(doc.response_count > 0){
								doc.status = '1';
								doc.save(function(err){
									if(err) throw err;
									callback();
								})
							}
							else{
								if(t_time <= 10){
									doc.status = '0'
									doc.save(function(err){
										if(err) throw err;
										callback();
									})
								}
								else{
									doc.status = '2'
									doc.save(function(err){
										if(err) throw err;
										callback();
									})							
								}							
							}							
						}
					}
				},
				function(err){
					if(err) throw err;
					console.log("get_all_enquiry_by_customer");
					return res.send({
						statusCode: 200,
						message: "get enquiry detail successfully",
						data: docs
					})					
				})
				// var c_date = new Date();
				// var p_date = doc.create_date;
				// var tdaye = c_date - p_date;
				// console.log('TTTTTTTTTTT------------------->',tdaye);
				// // if (tdaye > 60e3) 
				// console.log(
		  //             Math.floor(tdaye / 60e3), 'minutes ago'
		  //       );
			}
		})
	}
}

exports.get_all_enquiry_by_retailer = function(req, res){
	if(typeof req.body.store_id == 'undefined')
	{
		console.log("store_id is not provided");
		return res.send({
			statusCode: 400,
			message: "store_id is not provided"
		})
	}
	else{
		response_store.find({store_id: req.body.store_id}, function(err, docs){
			if(err){
				console.log("if err ------>", err);
				logger.log.info(err);
				 return res.send({
				 	statusCode: 400,
				 	message: "something went wrong"
				 })
			}
			else{
				var res_data = []
				console.log("get_all_enquiry_by_retailer");
				async.each(docs, function(doc, callback){
					if(doc == null){
						callback();
					} 
					else{
						medFinder.findOne({_id: doc.enquiry_id}, function(err, medf){
							if(err) throw err;
							if(medf.response_count == 2){
								if(doc.response_status == '1'){
									var obj = {};
									var enq_data;
									medFinder.findOne({_id: doc.enquiry_id}, function(err, enq){
										if(err) throw err;
										if(enq == null){
											callback();
										}
										else{
											enq_data = enq;
											// console.log("enq_data ----->", enq_data);
											obj._id = doc.id;
											obj.timestamp = doc.timestamp;
											obj.enquiry_data = enq_data;
											obj.store_id = doc.store_id;
											obj.response_status = doc.response_status;
											res_data.push(obj);
											callback();								
										}							 
									})
								}
								else{
									var obj = {};
									var enq_data;
									medFinder.findOne({_id: doc.enquiry_id}, function(err, enq){
										if(err) throw err;
										if(enq == null){
											callback();
										}
										else{
											enq_data = enq;
											// console.log("enq_data ----->", enq_data);
											obj._id = doc.id;
											obj.timestamp = doc.timestamp;
											obj.enquiry_data = enq_data;
											obj.store_id = doc.store_id;
											obj.response_status = doc.response_status;
											res_data.push(obj);

											doc.response_status = '3'
											doc.save(function(err){
												if(err) throw err;
												callback();
											})									
											//callback();								
										}							 
									})							
								}														
							}
							else{
								var obj = {};
								var enq_data;
								medFinder.findOne({_id: doc.enquiry_id}, function(err, enq){
									if(err) throw err;
									if(enq == null){
										callback();
									}
									else{
										enq_data = enq;
										// console.log("enq_data ----->", enq_data);
										obj._id = doc.id;
										obj.timestamp = doc.timestamp;
										obj.enquiry_data = enq_data;
										obj.store_id = doc.store_id;
										obj.response_status = doc.response_status;
										res_data.push(obj);
										callback();;								
									}							 
								})								
							}
						})

						// var res_data = [];
						// obj._id = doc.id;
						// obj.timestamp = doc.timestamp;
						// obj.enquiry_data = enq_data;
						// obj.store_id = doc.store_id;
						// res_data.push(obj);
						// callback();
					}
				}, function(err){
					if(err){
						console.log("if err ------>", err);
						 return res.send({
						 	statusCode: 500,
						 	message: "something went wrong"
						 })
					}
					if(res_data == null || res_data == ''){
						// console.log("res_data = null ------>", res_data);
						 return res.send({
						 	statusCode: 400,
						 	message: "No MedEnquiry Till Now",     // "Data not found"
						 })
					}								
					else{
						res_data.sort(function (a, b) {
						  return new Date(b.timestamp) - new Date(a.timestamp);
						});							
						return res.send({
							statusCode: 200,
							message: "get enquiry detail successfully",
							data: res_data
						})
					}
				})
			}
		})
	}
}

exports.get_medicine_enquery_for_retailer = function(req, res){
	if(typeof req.body.store_id == 'undefined'){
		return res.send({
			statusCode: 400,
			message: "store_id is not provided."
		})
	}
	else{
		response_store.find({store_id: req.body.store_id, response_status: '0'}, function(err, docs){
			if(err){
				return res.send({
					statusCode: 500,
					message: "Something went wrong."
				})
			}
			else if(docs == ''){
				return res.send({
					statusCode: 400,
					message: "No MedEnquiry Till Now",  //"data not found."
				})
			}
			else{
				return res.send({
					statusCode: 200,
					message: "get all not response on med enquiry",
					data: docs
				})
			}
		})		
	}
}

exports.retailer_response_no = function(req, res){
	if(typeof req.body.enquiry_id == 'undefined'
	   || typeof req.body.store_id == 'undefined')
	{
		console.log("enquiry_id is not provided");
		return res.send({
			statusCode: 400,
			message: "enquiry_id is not provided"
		})
	}
	else{
		response_store.findOne({enquiry_id: req.body.enquiry_id, store_id: req.body.store_id}, function(err, docs){
			if(err){
				console.log("if err ------>", err);	
				logger.log.info(err);
				 return res.send({
				 	statusCode: 400,
				 	message: "something went wrong"
				 })
			}
			else{
				docs.response_status = '2';
				docs.save(function(err){
					if(err) throw err;
					return res.send({
						statusCode: 200,
						message: "NO found",
						data: docs
					})
				})
			}
		})
	}		
}


exports.cutomer_cancel_request = function(req, res){
	if(typeof req.body.enquiry_id == 'undefined')
	{
		console.log("enquiry_id is not provided");
		return res.send({
			statusCode: 400,
			message: "enquiry_id is not provided"
		})
	}
	else{
		medFinder.findOne({_id: req.body.enquiry_id}, function(err, medf){
			if(err){
				console.log("if err ------>", err);
				logger.log.info(err);
				 return res.send({
				 	statusCode: 400,
				 	message: "something went wrong"
				 })
			}
			else{
				medf.status = '3';
				medf.save(function(err){
					if(err) throw err;
					response_store.find({enquiry_id: req.body.enquiry_id}, function(err, docs){
						if(err) throw err;
						async.each(docs, function(doc, callback){
							if(doc == null){
								callback();
							}
							else{
								if(doc.response_status == '1'){
									callback();
								}
								else{
									doc.response_status = '3'
									doc.save(function(err){
										if(err) throw err;
										callback();
									})
								}
							}
						},
						function(err){
							if(err) throw err;
							return res.send({
								statusCode: 200,
								message: "canceled successfully"
							})
						})
					})
				})
				// docs.response_status = '2';
				// doc.save(function(err){
				// 	if(err) throw err;
				// 	return res.send({
				// 		statusCode: 200,
				// 		message: "NO found",
				// 		data: docs
				// 	})
				// })
			}
		})
	}		
}



exports.retailer_response_yes = function(req, res){
	if(typeof req.body.enquiry_id == 'undefined'
		|| typeof req.body.store_id == 'undefined'
		)
	{
		console.log("enquiry_id is not provided");
		return res.send({
			statusCode: 400,
			message: "enquiry_id is not provided"
		})
	}
	else{
		medFinder.findOne({_id: req.body.enquiry_id}, function(err, medf){
			if(err) throw err;
			if(medf.response_count == 2){
				return res.send({
					statusCode: 400,
					message: "Already closed"
				})
			}
			else{
				var med_name = medf.med_name;
				var quantity = medf.quantity;

				response_store.findOne({enquiry_id:  req.body.enquiry_id, store_id:  req.body.store_id}, function(err, docs){
					if(err) throw err;
					// if(docs.response_status == '1'){
					// 	var final_count = sum(medf.response_count, 1);
					// 	medf.medf.response_count = final_count;
					// 	medf.save(function(err){
					// 		if(err) throw err;
					// 		return res.send({
					// 			statusCode: 200,
					// 			message: "get response successfully"
					// 		})
					// 	})
					// }

					docs.response_status = '1';
					docs.save(function(err){
						if(err) throw err;
						var final_count = sum(medf.response_count, 1);
						medf.response_count = final_count;
						medf.save(function(err){
							if(err) throw err;
							customer.findOne({_id: medf.customer_id}, function(err, cust){
								if(err) throw err;
								var c_mail = cust.email_id;
								Store.findOne({_id: req.body.store_id}, function(err, str){
									if(err) throw err;
									var store_name = str.Pharmacy_name;
									var store_number = str.Contact_No_1;
									var store_address = str.Address;
									var store_pincode = str.Pin_code;

								    var senderEmail = config.email_m;
								    var senderPassword = config.password_m;
								    var fromJob = 'OrderMedicine'; 
								    // node_mailer.nodeMailer
								    var templateData = {};
								    templateData.store_name = store_name;
								    templateData.store_number = store_number;
								    templateData.store_address = store_address;
								    templateData.store_pincode = store_pincode;
								    templateData.med_name = med_name;
								    templateData.quantity = quantity;
								    var requestObj = {
								    	templateName : "medfinder-pharmacyfound",
								    	to : c_mail,
								    	subject : 'MedFinder - Pharmacy Found',
								    	templateData : templateData,
								    	senderEmail : senderEmail,
								    	senderPassword: senderPassword,
								    	fromJob : fromJob
								    }
								    //console.log("node_mailer=======+++++++++++", node_mailer.nodeMailer);
								     node_mailer.nodeMailer(requestObj,function(err,data){
								    	if(err){console.log("====ERROR====",err); logger.log.info(err);}
								    	//if(err) throw err;
								    	console.log("====DATA====",data);
										return res.send({
											statusCode: 200,
											message: "send yes response successfully"
										})								    	
								    });	
								})
							})							
						})							
					})
				})		
			}	
		})
	}
} 


exports.get_found_store_list = function(req, res){
	if(typeof req.body.enquiry_id == 'undefined')
	{
		console.log("enquiry_id is not provided");
		 return res.send({
		 	statusCode: 400,
		 	message: "enquiry_id is not provided"
		 })
	}
	else{
		response_store.find({enquiry_id: req.body.enquiry_id, response_status: '1'}, function(err, docs){
			if(err){
				console.log("if err ----->", err);
				logger.log.info(err);
				 return res.send({
				 	statusCode: 500,
				 	message: "something went wrong"
				 })
			}
			if(docs == null || docs == ''){
				console.log("store list is not found");
				 return res.send({
				 	statusCode: 400,
				 	message: "store list is not found"
				 })				
			}			
			else{
				var array_list = [];
				async.each(docs, function(doc, callback){
					if(doc == null){
						callback();
					}
					else{
						var obj = {};
						Store.findById({_id: doc.store_id, response_status: '1'}, function(err, str){
							if(err) throw err;
							if(str == null){
								callback();
							}
							else{
								obj.store_detail = str;
								CreateAcount.findOne({Store_Id: str.id}, function(err, cat){
									if(cat == null){
										callback();
									}
									else{
										obj.phone_number = cat.Phone_Number;
										array_list.push(obj);
										callback();
									}
								})
							}
						})
					}
				},function(err){
					if(err){
						console.log("if err ------>", err);
						logger.log.info(err);
						 return res.send({
						 	statusCode: 500,
						 	message: "something went wrong"
						 })
					}
					else{
						return res.send({
							statusCode: 200,
							message: "get yes response list of store successfully",
							data: array_list
						})						
					}					
				})
			}
		})
	}
}

exports.get_retailerc_notication = function(req, res){
	if(typeof req.body.store_id == 'undefined')

	{
		console.log("store_id is not provided");
		return res.send({
			statusCode: 400,
			message: "store_id is not provided"
		})
	}
	else{
		notificationcr.find({store_id: req.body.store_id, flag: '1' },{},{ sort :{ create_time : -1}}, function(err, docs){
			// console.log("docs.l ---->",docs.length);
			if(err){
				console.log("if err ------>", err);
				logger.log.info(err);
				 return res.send({
				 	statusCode: 500,      //400,
				 	message: "something went wrong"
				 })
			}
			if(docs == null || docs == ''){
				console.log("No Notification Available");
				 return res.send({
				 	statusCode: 400,
				 	message: "No Notification Available"
				 })				
			}				
			else{
				return res.send({
					statusCode: 200,
					message: "get notication successfully"
				})
			}
		})
	}		
}


exports.get_retailer_order_notication = function(req, res){
	if(typeof req.body.store_id == 'undefined')

	{
		console.log("store_id is not provided");
		return res.send({
			statusCode: 400,
			message: "store_id is not provided"
		})
	}
	else{
		var query = {store_id: req.body.store_id, $and: [ { $or : [{ flag: '2'}, { flag: '3'}] }]};
		notificationcr.find(query,{},{ sort :{ create_time : -1}}, function(err, docs){
			// console.log("docs.l ---->",docs.length);
			if(err){
				console.log("if err ------>", err);
				logger.log.info(err);
				 return res.send({
				 	statusCode: 500,    //400,
				 	message: "something went wrong"
				 })
			}
			if(docs == null || docs == ''){
				console.log("No Notification Available");
				 return res.send({
				 	statusCode: 400,
				 	message: "No Notification Available"
				 })				
			}						
			else{
				return res.send({
					statusCode: 200,
					message: "get notication successfully",
					data: docs
				})
			}
		})
	}		
}

//Weekly job alert
// cron.scheduleJob({hour: 21, minute: 1, dayOfWeek: 6}, function(){

// 	AddVacancy.count({job_status: 1}, function(err, jobis){
// 		if(err){
// 			console.log("something went wrong");
// 			return res.send({
// 				statusCode: 400,
// 				message: "something went wrong"
// 			})
// 		}
// 		if(jobis == 0){
// 			console.log("no any job here");
// 			return res.send({
// 				statusCode: 400,
// 				message: "no any job here"
// 			})			
// 		}
// 		else{
// 			var totalJob = jobis;
// 			Store.count({Store_status: true}, function(err, storeis){
// 				if(err){
// 					console.log("something went wrong");
// 					return res.send({
// 						statusCode: 400,
// 						message: "something went wrong"
// 					})
// 				}
// 				else{
// 					var totalPharmacy = storeis;
// 					SmEmployee.find({}, function(err, emps){
// 						async.each(emps, function(emp, callback){
// 							if(emp == null){
// 								callback();
// 							}
// 							else{
// 								var emp_email = emp.User_Email;

// 							    var senderEmail = config.email;
// 							    var senderPassword = config.password;
// 							    var fromJob = "Search4PharmacyJobs"; 
// 							    // node_mailer.nodeMailer
// 							    var templateData = {};
// 							    templateData.totalJob = totalJob;
// 							    templateData.totalPharmacy = totalPharmacy;

// 							    var requestObj = {
// 							    	templateName : "Weeklyjob-alert",
// 							    	to : emp_email,
// 							    	subject : 'Weekly Job Alert',
// 							    	totalJob : totalJob,
// 							    	templateData : templateData,
// 							    	senderEmail : senderEmail,
// 							    	senderPassword: senderPassword,
// 							    	fromJob : fromJob
// 							    }
// 							    //console.log("node_mailer=======+++++++++++", node_mailer.nodeMailer);
// 							     node_mailer.nodeMailer(requestObj,function(err,data){
// 							    	console.log("====ERROR====",err)
// 							    	console.log("====DATA====",data);
// 							    });									
// 							}
// 						})
// 					})					
// 				}				
// 			})
// 		}
// 	})
// })


exports.getStoredata = function(req, res){
	if(typeof req.body.store_id == 'undefined'){
		console.log("store_id is not provided");
		return res.send({
			statusCode: 400,
			message: "store_id is not provided"
		})
	}
	else{
		Store.findById({_id: req.body.store_id}, function(err, doc){
			if(err) throw err;
			return res.send({
				statusCode: 200,
				message: "Get store data successfully.",
				data: doc
			})
		})
	}
}

exports.getOperationaldata = function(req, res){
	if(typeof req.body.operationalInfoId == 'undefined'){
		console.log("operationalInfoId is not provided");
		return res.send({
			statusCode: 400,
			message: "operationalInfoId is not provided"
		})
	}
	else{
		OperationalInfo.findById({_id: req.body.operationalInfoId}, function(err, doc){
			if(err) throw err;
			return res.send({
				statusCode: 200,
				message: "Get operational data successfully.",
				data: doc
			})
		})
	}
}

//not working on live
exports.getownerinfodata = function(req, res){
	if(typeof req.body.ownerInfoId == 'undefined'){    
		console.log("ownerInfoId is not provided");
		return res.send({
			statusCode: 400,
			message: "ownerInfoId is not provided"
		})
	}
	else{
		OwnerInfo.findById({_id: req.body.ownerInfoId}, function(err, doc){
			if(err) throw err;
			return res.send({
				statusCode: 200,
				message: "Get ownerInfoId data successfully.",
				data: doc
			})
		})
	}
}

exports.getCreateaccountdata = function(req, res){
	if(typeof req.body.account_id == 'undefined'){
		console.log("account_id is not provided");
		return res.send({
			statusCode: 400,
			message: "account_id is not provided"
		})
	}
	else{
		CreateAcount.findById({_id: req.body.account_id}, function(err, doc){
			if(err) throw err;
			if(doc == null){
				return res.send({
					statusCode: 400,
					message: "No data found."
				})
			}
			if(doc == ''){
				return res.send({
					statusCode: 400,
					message: "No data found."
				})				
			}
			else{
				return res.send({
					statusCode: 200,
					message: "Get account data successfully.",
					data: doc
				})				
			}
		})
	}
}

exports.updateStoredata = function(req, res){
	if(typeof req.body.store_id == 'undefined'){
		console.log("store_id is not provided");
		return res.send({
			statusCode: 400,
			message: "store_id is not provided"
		})
	}
	else{
		Store.findById({_id: req.body.store_id}, function(err, doc){
			if(err) throw err;
			doc.Pharmacy_name = req.body.pharmacy_name;
			doc.Address = req.body.address;
			doc.Landmark = req.body.landmark;
			doc.Area = req.body.area;
		    doc.latitude = req.body.latitude;
			doc.longitude = req.body.longitude;
			doc.State = req.body.state;
			doc.City = req.body.city;
			doc.Pin_code = req.body.PinCode;
			// store_detail.Email = email;
			doc.Contact_No_1 = req.body.ContactNo_1;
			doc.DLN = req.body.DLN;
			doc.GST_Number = req.body.GstNumber;
			doc.Premise = req.body.premise;
			doc.updateTime = Date.now();
			if(typeof req.body.profileImage != 'undefined')
			{
				var imagePath = '/images/' + randomstring.generate(10) +'.png';
				doc.profile_image = imagePath;
				const base64Data = req.body.profileImage.replace(/^data:([A-Za-z-+/]+);base64,/, '');
				fs.writeFileSync(process.cwd() + '/public' + imagePath, new Buffer(base64Data, 'base64'), 'base64', function(err){});  
			}
			else{
			    doc.save();    	    	
			}  
			doc.save(function(err){
				if(err) throw err;
				return res.send({
					statusCode: 200,
					message: "update store data successfully.",
					data: doc
				})				
			})
		})
	}
}

exports.updateOperationaldata = function(req, res){
	if(typeof req.body.operationalInfoId == 'undefined'){
		console.log("operationalInfoId is not provided");
		return res.send({
			statusCode: 400,
			message: "operationalInfoId is not provided"
		})
	}
	else{
		OperationalInfo.findById({_id: req.body.operationalInfoId}, function(err, doc){
			if(err) throw err;
			doc.Store_Start_Time = req.body.store_start_time;
			doc.Store_Break_Time_From = req.body.store_break_time_from;
			doc.Store_Break_Time_To = req.body.store_break_time_to;
			doc.Store_Close_Time = req.body.store_close_time;
			doc.Holidays = req.body.holidy;
			doc.Shift_Day = req.body.shift_day;
			doc.Number_Of_Staff = req.body.number_of_staff;
			doc.Number_Of_Pharmcist = req.body.number_of_pharmcist;
			doc.Number_Of_Whole_Sellers = req.body.number_of_whole_seller;
			doc.Home_Delevary = req.body.home_delevary;
			doc.Min_Order_Rs = req.body.min_order_rs;		
			doc.Discount_Scheme = req.body.dicount_scheme;
			doc.Flate_Discount = req.body.flate_discount;
			doc.Slab_Discount = req.body.slab_discount;		
			doc.save(function(err){
				if(err) throw err;
				console.log("Operational Info update successfully");
				return res.send({
					statusCode: 200,
					message: "Operational Info update successfully",
					data: operational_info
				});
			});	
		})
	}
}


exports.updateownerinfodata = function(req, res){
	if(typeof req.body.ownerInfoId == 'undefined'){
		console.log("ownerInfoId is not provided");
		return res.send({
			statusCode: 400,
			message: "ownerInfoId is not provided"
		})
	}
	else{
		OwnerInfo.findById({_id: req.body.ownerInfoId}, function(err, doc){
			if(err) throw err;
			doc.Firm_Type = req.body.firm_type;
			doc.Owner_list = req.body.owner_list;
			doc.save(function(err){
				if(err) throw err;
				console.log("Owner Info update successfully");
				return res.send({
					statusCode: 200,
					message: "Owner Info update successfully",
					data: owner_info
				});
			})
		})
	}
}

// console.log("city name****************",cityFunction());

// function cityFunction() {				
// 	var objName =
// 	city.findOne({city_id: 1}, function(err, city_n){
// 		console.log("-----",city_n.city_name);
//         return city_n;
// 	})

// }

// function stateFunction(state_id) {
//     var state1 = state_id.toString();																			
// 	state.findOne({state_id: state1}, function(err, state_n){
// 		console.log("=====",state_n.state);
//         return  state_n.state;
// 	})
// }

// // var city_name;
// // var state_name;

// var city_name;
// var state_name;
// city_name = stateFunction(21);
// city_name = cityFunction(21);
// console.log("city_name----->", city_name);
// console.log("state_name----->", state_name);


// var query = require('array-query');
exports.getmedicineapidata = function(req, res){
  var options = {
      uri: config.baseUrl + req.body.name,
      headers: {
          'api-key': config.apikey
      },
      json: true
  };
    rp(options)
    .then(function (response) {
    	var data = response.data;
    	if(data == null || data == ''){
    		return res.send({
    			statusCode: 400,
    			message: "medicine not found"
    		})
    	}
		return res.send({
			statusCode: 200,
			data: response.data
		})    	
    })
    .catch(function(err){
    	console.log("err-------->", err);
    })
}


cron.scheduleJob({hour: 23, minute: 59, dayOfWeek: [0,2,4]}, function(){
	console.log("cron for every 2 days--------:");	
	notificationcr.find({}, function(err, docs)	{
		if(err) throw err;
		async.each(docs, function(doc, callback){
			if(doc == null){
				callback();
			}
			else{
				var today = new Date();
				var current_date = ('0' + today.getDate()).slice(-2) + '-' + ('0' + (today.getMonth()+1)).slice(-2) + '-' + today.getFullYear();				var tod = doc.create_time
				var doc_date = ('0' + tod.getDate()).slice(-2) + '-' + ('0' + (tod.getMonth()+1)).slice(-2) + '-' + tod.getFullYear();
				if(current_date == doc_date){
					console.log("-----equal-----");
					callback();
				}
				else{
					console.log("------not_equal-----");
					console.log("doc.id--------->", doc.id);
					notificationcr.remove({_id: doc.id}, function(err, notif){
						if(err) throw err;
						callback();
					})
				}				
			}
		})		
	})
})

// exports.storeIsactivestatus = function(req, res){
// 	if(typeof req.body.account_id == 'undefined'){
// 		console.log("account_id is not provided");
// 		return res.send({
// 			statusCode: 400,
// 			message: "account_id is not provided"
// 		})
// 	}
// 	else{
// 		CreateAcount.findById({_id: req.body.account_id}, function(err, doc){
// 			if(err) throw err;
// 			return res.send({
// 				statusCode: 200,
// 				message: "get account status successfully.",
// 				data: doc.is_active;
// 			})
// 		})
// 	}
// }
