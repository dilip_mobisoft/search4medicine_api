'use strict';
var config = require('../config/config');
var logger =  require('../logger/logger');
var node_mailer = require('../node_mailer/node_mailer');
var fs = require('fs');
var sum = require('sum-of-two-numbers');
var async = require('async');
var smtpTransport = require('nodemailer-smtp-transport');
var nodemailer = require('nodemailer');
var regexp = require('node-regexp');
var format  = require('node.date-time');
var emailformat = require('string-template');
var emailExistence = require('email-existence');
var mongoose = require('mongoose'),
   Store = mongoose.model('Store'),
   storeadvertisement = mongoose.model('storeadvertisement'),
   notification = mongoose.model('notification'),
   OwnerInfo = mongoose.model('OwnerInfo'),
   OperationalInfo = mongoose.model('OperationalInfo'),
   CreateAcount = mongoose.model('CreateAcount'),
   AddVacancy = mongoose.model('AddVacancy'),
   SmEmployee = mongoose.model('SmEmployee'),
   email_veryfication = mongoose.model('email_veryfication'),
   mobile_Veryfication = mongoose.model('mobile_Veryfication'),
   medicine_data = mongoose.model('medicine_data'),
   apply_job = mongoose.model('apply_job'),
   my_staff = mongoose.model('my_staff'),
   customer_cart = mongoose.model('customer_cart'),
   customer = mongoose.model('customer'),
   customer_delivery_detail = mongoose.model('customer_delivery_detail'),
   customer_order = mongoose.model('customer_order'),
   medicine_salt = mongoose.model('medicine_salt'),
   rejection = mongoose.model('rejection'),
   suspend = mongoose.model('suspend'),
   health_locker = mongoose.model('health_locker'), 
   medFinder = mongoose.model('medFinder'), 
   state = mongoose.model('state'),
   city = mongoose.model('city'),
   packagejob = mongoose.model('packagejob'),
   medicine_not_found = mongoose.model('medicine_not_found'),
   wrong_medicine = mongoose.model('wrong_medicine'),
   shortlist_candidiate = mongoose.model('shortlist_candidiate'),
   retailer_viewed_profile = mongoose.model('retailer_viewed_profile'),
   staff = mongoose.model('staff'),
   retailerPackagehistory = mongoose.model('retailerPackagehistory'),
   response_store = mongoose.model('response_store'),
   notificationcr = mongoose.model('notificationcr'),
   super_admin = mongoose.model('super_admin');
   var admin = require("firebase-admin");


// var testing = "dilip";
// console.log(testing.length);

   exports.customer_signup = function(req, res){
   var store_id = req.body.store_id;
   var email_id = req.body.email_id;
   var mobile_number = req.body.mobile_number  ;
   var fcm = req.body.fcm;
   if(typeof store_id == 'undefined'
      || typeof email_id == 'undefined'
      // || typeof mobile_number == 'undefined'
      || typeof fcm  == 'undefined'
      )
   {
      
      console.log("store_id or email_id or fcm is not provided");
      return res.send({
         statusCode: 400,
         message: "store_id or email_id or fcm is not provided"
      })
   }
   else{
        if(mobile_number.length < 10 || mobile_number.length >10){
          return res.send({
            statusCode: 400,
            message: "Please enter valid mobile number."
          })
        }
        customer.findOne({email_id: email_id}, function(err, doc){
         if(err) throw err;
         if(doc == null)
         {
            CreateAcount.findOne({Email_Id: email_id}, function(err, cat){
               if(cat == null){
                Store.findById(store_id, function(err, sto){
                  var customer_detail = new customer();
                  customer_detail.Store_Id = store_id;
                  customer_detail.email_id = email_id;
                  customer_detail.storeName = sto.Pharmacy_name
                  customer_detail.fcm = fcm;
                  customer_detail.fname = '';
                  customer_detail.lname = '';
                  customer_detail.image = '';
                  customer_detail.phone_number = mobile_number;
                  customer_detail.address = '';
                  customer_detail.area = '';
                  customer_detail.city = '';
                  customer_detail.state = '';
                  customer_detail.pin_code = '';
                  customer_detail.save(function(err){
                     if(err) throw err;
                      var requestObj = {
                        templateName : "welcome-customer",
                        to : email_id,
                        subject : 'Welcome to Order Medicine - "The Med App"',
                        senderEmail : config.email_m,
                        senderPassword: config.password_m,
                        fromJob : "OrderMedicine"
                      }
                       node_mailer.nodeMailer(requestObj,function(err,data){
                        if(err){console.log("====ERROR====",err); logger.log.info(err);}
                        console.log("====DATA====",data);
                      });                 
                      Store.findById({_id: store_id}, function(err, file){
                        if(err) throw err;
                        console.log("customer signup successfully");
                        return res.send({
                           statusCode: 200,
                           message: "customer signup successfully",
                           data: {customer_detail: customer_detail, store_id: store_id, email_id: email_id, store_name: file.Pharmacy_name, fcm: fcm}
                        })                   
                     }) 
                  })
                })
               }
               else{
                  return res.send({
                     statusCode: 400,
                     message: "This email id is already registerd"
                  })
               }
            })          
         }
         else{console.log(doc.Store_Id+"<<<<<<<<>>>>>>"+config.defaultStoreid);
            if(doc.Store_Id == config.defaultStoreid){

               if(config.defaultStoreid == store_id){
                console.log("2. fcm======>", fcm);
                doc.fcm = fcm;
                doc.save(function(err){
                  if(err) throw err;
                  Store.findById({_id: doc.Store_Id}, function(err, file){
                     if(err) throw err;
                     console.log("customer login successfully+++++++++++");                  
                     return res.send({
                        statusCode: 200,
                        message: "customer login successfully",
                        data: {customer_detail: doc, store_id: doc.Store_Id, email_id: email_id, store_name: file.Pharmacy_name}
                     })
                  })                  
                })                
               }
               else{
                  console.log("3. fcm======>", fcm);
                  doc.fcm = fcm;
                  doc.Store_Id = store_id;
                  doc.save(function(err){
                     if(err) throw err;
                     Store.findById({_id: store_id}, function(err, file){
                        if(err) throw err;
                        console.log("customer login successfully-------");
                        return res.send({
                           statusCode: 200,
                           message: "customer login successfully",
                           data: {customer_detail: doc, store_id: doc.Store_Id, email_id: email_id, store_name: file.Pharmacy_name, fcm: fcm}
                        })                   
                     })                
                  })
               }                                   
            } 
            else{
               console.log("4. fcm======>", fcm);
               doc.fcm = fcm;
               doc.save(function(err){
                  if(err) throw err;
                  Store.findById({_id: doc.Store_Id}, function(err, file){
                     if(err) throw err;
                     console.log("customer login successfully@@@@@@");                 
                     return res.send({
                        statusCode: 200,
                        message: "customer login successfully",
                        data: {customer_detail: doc, store_id: doc.Store_Id, email_id: email_id, store_name: file.Pharmacy_name}
                     })
                  }) 
               })
            }
         }
      })
   }
}

exports.customer_profile_update = function(req, res){
   if(typeof req.body.customer_id == 'undefined'){
      console.log("customer_id is not provided");
      return res.send({
         statusCode: 200,
         message:"customer_id is not provided"
      })
   }
   else{
      customer.findById({_id: req.body.customer_id}, function(err, doc){
         if(err){
            console.log("if err ----->", err);
            logger.log.info(err);
            return res.send({
               statusCode: 400,
               message: "something went wrong"
            })
         }
         if(doc == null || doc == ''){
            console.log("customer not found");
            return res.send({
               statusCode: 400,
               message: "customer not found"
            })
         }                          
         else{
          if(typeof req.body.name != 'undefined')
            {doc.name = req.body.name;}
          if(typeof req.body.image != 'undefined')
           { doc.image = req.body.image;}
          if(typeof req.body.phone_number != 'undefined')
           { doc.phone_number = req.body.phone_number;}
          if(typeof req.body.address != 'undefined')
            {doc.address = req.body.address;}
          if(typeof req.body.area != 'undefined')
           { doc.area = req.body.area;}
          if(typeof req.body.state != 'undefined')
           { doc.state = req.body.state;}
          if(typeof req.body.city != 'undefined')
           { doc.city = req.body.city;}
          if(typeof req.body.pin_code != 'undefined')
           { doc.pin_code = req.body.pin_code;}
            doc.update_date = Date.now();
            doc.save(function(err){
               if(err) throw err;
               return res.send({
                  statusCode: 200,
                  message: "profile update successfully"
               })
            })
         }
      })
   }
}


exports.get_customer_profile_update = function(req, res){
   if(typeof req.body.customer_id == 'undefined'){
      console.log("customer_id is not provided");
      return res.send({
         statusCode: 200,
         message:"customer_id is not provided"
      })
   }
   else{
      customer.findById({_id: req.body.customer_id}, function(err, doc){
         if(err){
            console.log("if err ----->", err);
            logger.log.info(err);
            return res.send({
               statusCode: 400,
               message: "something went wrong"
            })
         }
         if(doc == null || doc == ''){
            console.log("customer not found");
            return res.send({
               statusCode: 400,
               message: "customer not found"
            })
         }                          
         else{
               return res.send({
                  statusCode: 200,
                  message: "profile update successfully",
                  data: doc
            }) 
         }
      })
   }
}


exports.add_item_to_cart = function(req, res){
   if(typeof req.body.customer_id == 'undefined'
      || typeof req.body.medicine_id == 'undefined'
      || typeof req.body.quantity == 'undefined'
      || typeof req.body.price == 'undefined'
      || typeof req.body.med_name == 'undefined'
      ){
      console.log("customer_id or medicine_id or quntity or price is not provided");
      return res.send({
         statusCode: 400,
         message: "customer_id or medicine_id or quntity or price is not provided"
      })
   }
   else{
      var add_to_cart = new customer_cart();
      add_to_cart.customer_id = req.body.customer_id;
      add_to_cart.medicine_id = req.body.medicine_id;
      add_to_cart.med_name = req.body.med_name;
      add_to_cart.quantity = req.body.quantity;
      add_to_cart.price = req.body.price;
      add_to_cart.date = Date.now();
      add_to_cart.save(function(err){
         if(err) throw err;
         console.log("item add to card successfully");
         return res.send({
            statusCode: 200,
            message: "item add to card successfully"
         })
      })
   }
}

exports.customer_cart_list = function(req, res){
   var customer_id = req.body.customer_id;
   if(typeof customer_id == 'undefined')
   {
      console.log("customer_id is not provided");
      return res.send({
         statusCode: 400,
         message: "customer_id is not provided"
      })
   }
   else{
      customer_cart.find({customer_id: customer_id, cart_status: '0'}, function(err, doc){
         if(err) throw err; 
         if(doc == '')
         {
            console.log("doc is blanck");
            return res.send({
               statusCode: 400,
               message: "this type of customer is not exists"
            })

         }
         if(doc == null){
            console.log("doc is null");
            return res.send({
               statusCode: 400,
               message: "not found this type of customer"
            })
         }
         else{
            return res.send({
               statusCode: 200,
               message: "get list of customer cart successfully",
               data: doc
            })
         }
      })
   }
} 


exports.customer_orders = function(req, res){
   if(
      typeof  req.body.customer_id == 'undefined'
      // || typeof req.body.store_name == 'undefined'
      || typeof  req.body.store_id == 'undefinde'
      || typeof req.body.deliver_address_id == 'undefined'
      )
   {
      console.log("customer_id or store_id or deliver_address_id is not provided");
      return res.send({
         statusCode: 400,
         message: "customer_id or store_id or deliver_address_id is not provided"
      })
   }
   else{
      var storeN;
      Store.findById({_id: req.body.store_id}, function(err, sname){
        if(err) throw err;
        if(sname == null){
          return res.send({
            statusCode: 400,
            message: "somthing is not right at this moment. Please try after some time."
          })
        }
        customer.findById({_id: req.body.customer_id}, function(err, cname){
          if(err) throw err;
          if(cname == null){
            return res.send({
              statusCode: 400,
              message: "somthing is not right at this moment. Please try after some time."
            })
          }
          customer_order.count({customer_id: req.body.customer_id}, function(err, coc){
            if(err) throw err;                  
          storeN = sname.Pharmacy_name;
          console.log("storeN",storeN);
          // return false;
          var customerorder = new customer_order();
          customerorder.customer_id = req.body.customer_id;
          customerorder.Store_Id = req.body.store_id;
          customerorder.deliver_address_id = req.body.deliver_address_id;
          customerorder.store_name = storeN;
          customerorder.storeContactNumber = sname.Contact_No_1;
          customerorder.customerContactNumber = cname.phone_number;
          if(cname.phone_number == null){
           customerorder.customerContactNumber = '';
          }          
          if(typeof req.body.cart_item_list == 'undefined'){
           customerorder.cart_item_list = '';
          }
          if(typeof req.body.comment == 'undefined'){
           customerorder.comment = '';
          }
          if(typeof req.body.order_item_count == 'undefined'){
           customerorder.order_item_count = "0";
          }  
          if(typeof req.body.order_price == 'undefined'){
           customerorder.order_price = "0.00";
          }
          if(typeof req.body.medicine_manufacturer == 'undefined'){
           customerorder.medicine_manufacturer = '';
          }                   
          customerorder.cart_item_list = req.body.cart_item_list;
          // customerorder.prescription = req.body.prescription;
          customerorder.comment = req.body.comment;
          customerorder.order_item_count = req.body.order_item_count;
          customerorder.order_price = req.body.order_price;
          customerorder.medicine_manufacturer = req.body.medicine_manufacturer; 

          var presc = req.body.prescription;
          async.each(presc, function(image, callback){
              if(image == null){
                 callback();
              }
              else{
                 var imagePath = '/images/' + randomstring.generate(10) +'.png';
                 var final_image = req.protocol + '://' + req.get('host') + imagePath;
                 customerorder.prescription.push({image_url: final_image});
                 const base64Data = image.image_url.replace(/^data:([A-Za-z-+/]+);base64,/, '');
                 fs.writeFileSync(process.cwd() + '/public' + imagePath, new Buffer(base64Data, 'base64'), 'base64', function(err){
                    if (err) throw err;
                    callback();
                    //console.log("prescriptions ------->", prescriptions);
                 });            
              }
          })
          // })
          // customerorder.update_date = Date.now();
          var today = new Date();
          var output   = today.getFullYear() + ''
          + ('0' + (today.getMonth()+1)).slice(-2) + ''
          + ('0' + today.getDate()).slice(-2);
          // console.log(output); 
          var id = customerorder._id;
          var myid = id.toString().slice(22);
          // console.log(output+''+myid);
          var order_no = output+''+myid+''+coc;
          // console.log(order_no);
          customerorder.order_no = order_no;
          // customerorder.update_date = Date.now();
          // customerorder.create_date = Date.now();
          customerorder.save(function(err){
              if(err){
                 console.log("if err ----->", err);
                 logger.log.info(err);
                 return res.send({
                    statusCode: 500,
                    message: "something went wrong"
                 })
              }
              else{
                 // console.log(customerorder);
                 var today = customerorder.update_date;
                 var oreder_date = ('0' + today.getDate()).slice(-2) + '-' + ('0' + (today.getMonth()+1)).slice(-2) + '-' + today.getFullYear();
                // var oreder_date = today.getFullYear() + '-' + ('0' + (today.getMonth()+1)).slice(-2) + '-' + ('0' + today.getDate()).slice(-2);
                 console.log(oreder_date);               
                 var templateData = {};
                 templateData.date = oreder_date;
                 templateData.order_no = customerorder.order_no;
                 templateData.total_order_amount = customerorder.order_price;
                 templateData.totale_item = customerorder.order_item_count;
                 templateData.store_name = customerorder.store_name;
                 CreateAcount.findOne({Store_Id: req.body.store_id}, function(err, doc){
                    if(err) throw err;
                    templateData.Phone_Number = doc.Phone_Number;
                    var store_email = doc.Email_Id;
                    var OperationalInfo_id = doc.OperationalInfo_Id;
                    var registrationToken = doc.store_fcm;
                    var payload = {
                       notification: {
                          title:  "OrderMedicine detail", //"Notification from Search4Medicine",
                          body: "You have new order", // "Campaign",
                          // tag: "Search4Medicine",
                          // click_action: "HomeActivity",
                          sound: "retailer_ring"
                       },
                     data:{order_id: customerorder.id},
                    };
                    admin.messaging().sendToDevice(registrationToken, payload)
                    .then(function(response) {console.log("Successfully sent message:", response);})
                    .catch(function(err) {console.log("Error sending message:", err);}); 
                     
                    var notifications = new notificationcr();
                    notifications.customer_id = customerorder.customer_id;   
                    notifications.store_id = doc.Store_Id;
                    notifications.title = "OrderMedicine detail";
                    notifications.message = "You have new order";
                    notifications.flag = '1'
                    // notifications.flag = '2';
                    notifications.save(function(err){
                       if(err) throw err;
                    })                   
                    customer.findOne({_id: req.body.customer_id}, function(err, cut){
                       if(err) throw err;
                       var cut_mail = cut.email_id;
                       console.log("cut_mail---->",cut_mail);
                       OperationalInfo.findOne({_id: OperationalInfo_id}, function(err, opr){
                          if(err) throw err;
                          templateData.home_delevary = opr.Home_Delevary;
                          templateData.store_Start_Time = opr.Store_Start_Time;
                          Store.findOne({_id: req.body.store_id}, function(err, sto){
                             if(err) throw err;
                             templateData.store_name = sto.Pharmacy_name;
                             templateData.store_address = sto.Address;
                             templateData.store_landmakr = sto.Landmark;
                             templateData.store_area = sto.Area;
                             var state_name;
                             var city_name;
                             state.findOne({state_id: sto.State}, function(err, state_n){
                                if(err){
                                   console.log("if err ------>", err);
                                   logger.log.info(err);
                                   return res.send({
                                      statusCode: 500,
                                      message: "something Went wrong"
                                   })
                                }
                                else{
                                   state_name = state_n.state;                                                         
                                   city.findOne({city_id: sto.City}, function(err, city_n){
                                      if(err){
                                         console.log("if err ------>", err);
                                         logger.log.info(err);
                                         return res.send({
                                            statusCode: 500,
                                            message: "something Went wrong"
                                         })
                                      }
                                      else{
                                         city_name = city_n.city_name;
                                         templateData.store_state = state_name;
                                         templateData.store_city = city_name;
                                         templateData.store_pcode = sto.Pin_code;
                                          var requestObj = {
                                            templateName : "order-place-successfully",
                                            to : cut_mail,
                                            subject : 'Order Placed Successfully',
                                            templateData : templateData,
                                            senderEmail : config.email_m,
                                            senderPassword: config.password_m,
                                            fromJob : "OrderMedicine"
                                          };
                                           node_mailer.nodeMailer(requestObj,function(err,data){
                                            if(err){console.log("====ERROR====",err); logger.log.info(err);}
                                            console.log("======DATA=====", data);
                                            //if(err) throw err;
                                         });                                       
                                      }
                                   })
                                }
                             })

                             customer_delivery_detail.findOne({_id: req.body.deliver_address_id}, function(err, cdd){
                                templateData.cName = cdd.name;
                                templateData.cMobile_number = cdd.mobile_number
                                templateData.cAddress1 = cdd.address1;
                                templateData.cAddress2 = cdd.address2;
                                templateData.cLandmark = cdd.landmark;
                                templateData.cCity = cdd.city;
                                templateData.cState = cdd.state;
                                templateData.cPin_code = cdd.pin_code;
                                templateData.cComment = req.body.comment;
                                templateData.cart_item_list = req.body.cart_item_list;
                                var cart = templateData.cart_item_list;
                                var orderData;
                                // console.log("cart----->", cart);
                                for (var i = 0; i< cart.length; i++) {
                                  var obj = cart[i]; 
                                  orderData+='<tr style="border:1px solid #333;"><td colspan="2" style=" border:1px solid #333;  border-collapse: collapse;padding-top:14px!important;  padding-bottom:12px;padding-right:60px;padding-left:20px;color:#333;font-size:14px!important">'+
                                  obj.med_name +'</td><td colspan="2" style=" border:1px solid #333; border-collapse: collapse; padding-top:14px!important;  padding-bottom:12px;padding-right:60px;padding-left:20px;color:#666;font-size:14px!important">'+
                                  obj.price +'</td><td colspan="2" style=" border:1px solid #333; border-collapse: collapse; padding-top:14px!important;  padding-bottom:12px;padding-right:60px;padding-left:20px;color:#666;font-size:14px!important">'+
                                  obj.quantity +'</td></tr>';
                                }
                                templateData.orderData = orderData;
                                // console.log("orderData---->", orderData);
                                 var requestObj = {
                                   templateName : "new-order-recieved",
                                   to : store_email,
                                   subject : 'New Order Received',
                                   templateData : templateData,
                                   senderEmail : config.email_m,
                                   senderPassword: config.password_m,
                                   fromJob : "OrderMedicine"
                                 };
                                 node_mailer.nodeMailer(requestObj,function(err,data){
                                   if(err){console.log("====ERROR====",err); logger.log.info(err);}
                                      console.log("====DATA====",data);                                    
                                 })
                             })
                             console.log("order successfully pleced");
                             return res.send({
                                statusCode: 200,
                                message: "order successfully pleced"
                             })                                                                                                                
                          })
                       })                
                    }) 
                 })             
              }
            });
          });
        })
      })   
   }
} 


exports.customer_delivery_detail = function(req, res){
   var customer_id = req.body.customer_id;
   if(typeof customer_id == 'undefined'
      || typeof  req.body.email_id == 'undefined'
      || typeof  req.body.name == 'undefined'
      || typeof  req.body.mobile_number == 'undefined'
      || typeof  req.body.address1 == 'undefined'
      || typeof  req.body.address2 == 'undefined'
      || typeof  req.body.city == 'undefined'
      || typeof  req.body.state == 'undefined'
      || typeof  req.body.pin_code == 'undefined'
      )
   {
      console.log("customer_id is not provided");
      return res.send({
         statusCode: 400,
         message: "customer_id or rmail_id or name or mobile_number or address1 or address2 or city or state or pin_code is not provided"
      })
   }
   else{
        if(req.body.mobile_number.length < 10 || req.body.mobile_number.length > 10){
          return res.send({
            statusCode: 400,
            message: "Please enter valid mobile number."
          })
        }    
      customer.findById({_id: customer_id}, function(err, doc){
         if(err) throw err;
         var customer_info = new customer_delivery_detail();
         customer_info.customer_id = customer_id;
         customer_info.name = req.body.name;
         customer_info.email_id = req.body.email_id;
         customer_info.mobile_number = req.body.mobile_number;
         customer_info.address1 = req.body.address1;
         customer_info.address2 = req.body.address2;
         customer_info.landmark = req.body.landmark;
         customer_info.city = req.body.city;
         customer_info.state = req.body.state;
         customer_info.pin_code = req.body.pin_code;
         customer_info.save(function(err){
            console.log("customer_detail fill successfully");
            if(err) throw err;
            return res.send({
               statusCode: 200,
               message: "thank you for your detail"
            })
         })
      })
   }
}

exports.get_customer_delivery_detail = function(req, res){
   var customer_id = req.body.customer_id;
   if(typeof customer_id == 'undefined')
   {
      console.log("customer_id is not provided")
      return res.send({
         statusCode: 400,
         message: "customer_id is not provided"
      })
   }
   else{
      customer_delivery_detail.find({customer_id: customer_id}, function(err, doc){
         if(err) throw err;
         if(doc == null || doc == '')
         {
            return res.send({
               statusCode: 400,
               message: "Please fill your delivery address."
            })
         }
         return res.send({
            statusCode: 200,
            message: "customer_id is not provided",
            data: doc
         })
      })
   }
}

exports.get_order_list_search = function(req, res){
   var data=[];
   if(typeof req.body.customer_id != 'undefined')
   {   

      customer_order.find({customer_id: req.body.customer_id},{},{ sort :{ update_date : -1}}, function(err, doc){
         if(err) throw err;
         if(doc == '')
         {
            return res.send({
               statusCode: 400,
               message: "No order found"
            })
         }
         if(doc == null)
         {
            return res.send({
               statusCode: 400,
               message: "Order list is Empty"
            })          
         }
         async.each(doc, function(file, callback){
            if(file == null)
            {
               callback();
            }
            else{ 
               var create_account_phone_number;
               CreateAcount.findOne({Store_Id: file.Store_Id}, function(err, datam){
                  if(err) throw err;

                  if(datam){
                     create_account_phone_number = datam.Phone_Number;
               
                     // console.log("create_account_phone_number >>>>>>>>>>>> "+create_account_phone_number);
                     var obj = {};
                     obj.id = file._id;
                     obj.date = file.update_date;
                     obj.customer_orders_status = file.customer_orders_status;
                     obj.customer_id = file.customer_id;
                     obj.store_id = file.Store_Id;
                     obj.store_name = file.store_name;
                     obj.comment = file.comment;
                     obj.order_item_count = file.order_item_count;
                     obj.order_price = file.order_price;
                     obj.update = file.update;
                     obj.cart_item_list = file.cart_item_list;
                     obj.prescription = file.prescription;
                     obj.order_no = file.order_no;
                     obj.store_number = create_account_phone_number;
                     var bj = {};

                     customer_delivery_detail.findById({_id: file.deliver_address_id}, function(err, cdd){
                        // console.log("111111&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&111"+cdd);

                               bj.id = cdd.id;
                              bj.customer_id = cdd.customer_id;
                              bj.name = cdd.name;
                              bj.email_id = cdd.email_id;
                              bj.mobile_number = cdd.mobile_number;
                              bj.address1 = cdd.address1;
                              bj.address2 = cdd.address2;
                              bj.landmark = cdd.landmark;
                              bj.city = cdd.city;
                              bj.state = cdd.state;
                              bj.pin_code = cdd.pin_code;
                        // console.log("22222BJ......",bj);
                        obj.deliver_address = bj;
                        data.push(obj);
                        // console.log("333333^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^"+obj.deliver_address);
                        callback();
                     })
                  }
               })
            }
         },
         function(err){
            if(err) throw err;
            console.log("get detail successfully");
            return res.send({
               statusCode: 200,
               message: "get detail successfully",
               customer_order_detail: data
            })
         })

      })
   }
   else if(typeof req.body.store_id != 'undefined')
   {
      customer_order.find({Store_Id: req.body.store_id},{},{ sort :{ update_date : -1}}, function(err, doc){
         if(err) throw err;
         if(doc == null)
         {
            return res.send({
               statusCode: 400,
               message: "No order Found"
            })          
         }
         async.each(doc, function(file, callback){
            if(file == null)
            {
               callback();
            }
            else{ 

               var create_account_phone_number;
               CreateAcount.findOne({Store_Id: file.Store_Id}, function(err, datam){
                  if(err) throw err;
                   create_account_phone_number = datam.Phone_Number;

               var obj = {};
               obj.id = file._id;
               obj.date = file.update_date;
               obj.customer_orders_status = file.customer_orders_status;
               obj.customer_id = file.customer_id;
               obj.store_id = file.Store_Id;
               obj.store_name = file.store_name;
               obj.comment = file.comment;
               obj.order_item_count = file.order_item_count;
               obj.order_price = file.order_price;
               obj.update = file.update;
               obj.cart_item_list = file.cart_item_list;
               obj.prescription = file.prescription;
               obj.order_no = file.order_no;
               obj.store_number = create_account_phone_number;
               //obj.store_phone_number = store_phone_number;
               var bj = {};
               customer_delivery_detail.findById({_id: file.deliver_address_id}, function(err, cdd){
                  //console.log(cdd);

                         bj.id = cdd.id;
                        bj.customer_id = cdd.customer_id;
                        bj.name = cdd.name;
                        bj.email_id = cdd.email_id;
                        bj.mobile_number = cdd.mobile_number;
                        bj.address1 = cdd.address1;
                        bj.address2 = cdd.address2;
                        bj.landmark = cdd.landmark;
                        bj.city = cdd.city;
                        bj.state = cdd.state;
                        bj.pin_code = cdd.pin_code;
                  //console.log("22222BJ......",bj);
                  obj.deliver_address = bj;
                  data.push(obj);
                  //console.log(obj.deliver_address);
                     callback();
               })
            })
            }
         },
         function(err){
            if(err) throw err;
            console.log("get detail successfully");
            //console.log(data);
            data.find({ $text: { $search: req.body.name}}, function(err, doc){
               if(err) throw err;
               console.log(value);
               return res.send({
                  statusCode: 200,
                  message: "get detail successfully",
                  //data: data,
                  customer_order_detail: doc
               })
            })
         })             
      })    
   }
   else{
      console.log("store_id or customer_id is not provided");
      return res.send({
         statusCode: 200,
         message: "store_id or customer_id is not provided"
      })
   }

}


exports.create_healthlocker = function(req, res){
   //var customer_id = mongoose.Types.ObjectId(req.body.customer_id);
   if(typeof req.body.customer_id == 'undefined'
       || typeof req.body.img_url == 'undefined'
       || typeof req.body.patient_name == 'undefined'
       )
   {
      console.log('customer_id or img_url or pacent_name is not provided');
      return res.send({
         statusCode: 400,
         message: 'customer_id or img_url or pacent_name is not provided'
      })
   }
   else{
      health_locker.find({customer_id: req.body.customer_id}, function(err, doc){
         if(err){
            console.log("if err ------>", err);
            logger.log.info(err);
            return res.send({
               statusCode: 500,
               message: "Something Went wrong"
            })
         }
         else{
            if(doc.length < 10){
               var healthlocker = new health_locker();
               healthlocker.customer_id = req.body.customer_id;
               // healthlocker.img_url = req.body.img_url;
               healthlocker.patient_name = req.body.patient_name;
               healthlocker.doc_name = req.body.doc_name;
               healthlocker.type = req.body.type;
               healthlocker.create_date = Date.now();

               var imagePath = '/images/' + randomstring.generate(10) +'.png';
               var final_image = req.protocol + '://' + req.get('host') + imagePath;
               healthlocker.img_url = final_image;
               const base64Data =  req.body.img_url.replace(/^data:([A-Za-z-+/]+);base64,/, '');
               fs.writeFileSync(process.cwd() + '/public' + imagePath, new Buffer(base64Data, 'base64'), 'base64', function(err){
                  if (err) throw err;
               });               
               healthlocker.save(function(err){
                  if(err) throw err;
                  return res.send({
                     statusCode: 200,
                     message: "Health Locker save Successfully"
                  })
               })
            }
            else{
               console.log("You can put max to max 10 helth record");
               return res.send({
                  statusCode: 400,
                  message: "Max 10 prescription can we uploaded. Delete prescription to add new"
               })
            }
         }
      })
   }
}

exports.update_healthlocker = function(req, res){
   if(typeof req.body.customer_id == 'undefined'
       || typeof req.body.img_url == 'undefined'
       || typeof req.body.patient_name == 'undefined'
       || typeof req.body.doc_name == 'undefined'
       || typeof req.body.type == 'undefined'
       )
   {
      console.log('customer_id or img_url or pacent_name or doc_name or type is not provided');
      return res.send({
         statusCode: 400,
         message: 'customer_id or img_url or pacent_name or doc_name or type is not provided'
      })
   }
   else{
      health_locker.findById({_id: req.body.healthlocker_id}, function(err, doc){
         if(err){
            console.log("if err >>>>>", err);
            logger.log.info(err);
            return res.send({
               statusCode: 500,
               message: "something went wrong"
            })
         }
         if(doc == null || doc == ''){
            console.log("no health_locker found -----> on update line");
            return res.send({
               statusCode: 400,
               message: "no Health Locker found"
            })
         }       
         else{
            doc.customer_id = req.body.customer_id;
            doc.img_url = req.body.img_url;
            doc.patient_name = req.body.patient_name;
            doc.doc_name = req.body.doc_name;
            doc.type = req.body.type;
            doc.update_date = Date.now();
            doc.save(function(err){
               if(err) throw err;
               return res.send({
                  statusCode: 200,
                  message: "healthlocker save Successfully"
               })
            })
         }
      })
   }
}

exports.get_healthlocker_by_customer_id = function(req, res){
   if(typeof req.body.customer_id == 'undefined'
      || typeof req.body.type == 'undefined')
   {
      console.log('customer-id is not provided');
       return res.send({
         statusCode: 400,
         message: 'customer-id is not provided'
       })
   }
   else{
      health_locker.find({customer_id: req.body.customer_id, type: req.body.type},function(err, doc){
         if(err) throw err;
         if(doc == null){
            console.log("no health_locker found -----> on get line");
            return res.send({
               statusCode: 400,
               message: "Health Locker is empt"
            })
         }
         if(doc == ''){
            console.log("health_locker is empty -----> on get line");
            return res.send({
               statusCode: 400,
               message: "Health Locker is empty"
            })          
         }
         else{
            console.log("health_lockern found successfully");
            return res.send({
               statusCode: 200,
               message: "Health Lockern found successfully",
               data: doc
            })
         }
      })
   }
}

exports.delete_healthlocker = function(req, res){
   if(typeof req.body.customer_id == 'undefined'
      || typeof req.body.healthlocker_id == 'undefined')
   {
      console.log('custome_id is not provided');
       return res.send({
         statusCode: 400,
         message: 'customer_id is not provided'
       })
   }
   else{
      health_locker.remove({_id: req.body.healthlocker_id, customer_id: req.body.customer_id}, function(err, doc){
         if(err) throw err;
         if(doc.n == 0){
            console.log("no health_locker found");
            return res.send({
               statusCode: 400,
               message: "no Health Locker found"
            })
         }
         else{
            console.log("health_locker delete successfully");
            return res.send({
               statusCode: 200,
               message: "Health Locker delete successfully"
               //data: doc
            })
         }
      })
   }
}

exports.get_nearest_store_list = function(req, res){
   if(typeof req.body.latitude == 'undefined'
      || typeof req.body.longitude == 'undefined'
      // || typeof req.body.customer_id == 'undefined'
      )
   {
      console.log("customer_id or latitude or longitude is not provided");
      return res.send({
         statusCode: 400,
         message: "customer_id or latitude or longitude is not provided"
      })
   }
   else{
      var stores_name = [];
         Store.find({}, function(err, docs){
            if(err){
               console.log("if err ----->", err);
               logger.log.info(err);
               return res.send({
                  statusCode: 500,
                  message: "something went wrong"
               })
            }
            else{
               var stores_name = [];
            async.each(docs, function(doc, callback){

               var userStoreDistance = geodist(
                   {lat: req.body.latitude, lon: req.body.longitude}, 
                   {lat: doc.latitude, lon: doc.longitude},
                   {exact: true, unit: 'meters'});

               if(userStoreDistance <= 2500){
               stores_name.push({store_name: doc.Pharmacy_name}); 
                  callback();
               } 
               else{
                  callback();
               }        
            }, function(err){
               if(err){
                  console.log("if err ------>", err);
                  logger.log.info(err);
                  return res.send({
                     statusCode: 500,
                     message: "something went wrong"
                  })
               }
               else{
                  return res.send({
                     statusCode: 200,
                     message: "successfully get nearest all store name",
                     data: stores_name
                  })
               }
            }) 
         }
      })
   }
}

exports.get_customer_email_by_account_id = function(req, res){
   if(typeof req.body.Account_id == 'undefined')
   {
      console.log("Account_id is not provided");
      return res.send({
         statusCode: 400,
         message: "Account_id is not provided"
      })
   }
   else{
      CreateAcount.findById({_id: req.body.Account_id}, function(err, doc){
         if(err){
            console.log("if err ------->", err);
            logger.log.info(err);
            return res.send({
               statusCode: 500,
               message: "something went wrong"
            })
         }
         else{
            // var query = { email_id: { $regex: '^' + req.body.email_id, $options:"i" }};
            customer.find({Store_Id: doc.Store_Id, email_id: { $regex: '^' + req.body.email_id, $options:"i" }}, function(err, docs){
               if(err) throw err;
                if(docs == ''){
                   console.log("customer not found");
                   return res.send({
                      statusCode: 400,
                      message: "Staff not found"
                   })
                }
                if(docs == null){
                   console.log("customer not found null");
                   return res.send({
                      statusCode: 400,
                      message: "customer not found null"
                   })
                }
                else{
                   return res.send({
                      statusCode: 200,
                      message: "customer found Successfully",
                      data:docs
                   })
                 }               
            })
         }
      })
   }
}


exports.healthlocker_if_ten = function(req, res){
   if(typeof req.body.customer_id == 'undefined')
   {
      console.log("customer_id is not provided");
      return res.send({
         statusCode: 400,
         message: "customer_id is not provided"
      })
   }
   else{
      health_locker.find({customer_id: req.body.customer_id}, function(err, docs){
         if(err){
            console.log("if err ------>", err);
            logger.log.info(err);
             return res.send({
               statusCode: 400,
               message: "something went wrong"
            })
         }
         // if(docs == ''){
         //    console.log("------>", err);
         //     return res.send({
         //       statusCode: 400,
         //       message: "healthlocker is empty"
         //     })
         // }
         else if(docs.length < 10 || docs.length > 10){
            return res.send({
               statusCode: 200,
               message: "found",
               data: docs
            })          
         }     
         else{
            return res.send({
               statusCode: 400,
               message: "Max 10 prescription can we uploaded. Delete prescription to add new."
            })
         }
      })
   }     
}


exports.get_customer_notication = function(req, res){
   if(typeof req.body.customer_id == 'undefined')
   {
      console.log("customer_id is not provided");
      return res.send({
         statusCode: 400,
         message: "customer_id is not provided"
      })
   }
   else{
      notificationcr.find({customer_id: req.body.customer_id,  flag: '2' },{},{ sort :{ create_time : -1}}, function(err, docs){
         if(err){
            console.log("if err ------>", err);
            logger.log.info(err);
             return res.send({
               statusCode: 400,
               message: "something went wrong"
             })
         }
         if(docs == null || docs == ''){
            console.log("No Notification Available");
             return res.send({
               statusCode: 400,
               message: "No Notification Available"
             })            
         }
         else{
            return res.send({
               statusCode: 200,
               message: "get notication successfully",
               data: docs
            })
         }
      })
   }     
}