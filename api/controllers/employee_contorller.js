'use strict';
var admin = require("firebase-admin");
var config = require('../config/config');
var logger =  require('../logger/logger');
var fs = require('fs');
var sum = require('sum-of-two-numbers');
var async = require('async');
var smtpTransport = require('nodemailer-smtp-transport');
var nodemailer = require('nodemailer');
var regexp = require('node-regexp');
var format  = require('node.date-time');
var emailExistence = require('email-existence');
var mongoose = require('mongoose'),
   Store = mongoose.model('Store'),
   storeadvertisement = mongoose.model('storeadvertisement'),
   notification = mongoose.model('notification'),
   OwnerInfo = mongoose.model('OwnerInfo'),
   OperationalInfo = mongoose.model('OperationalInfo'),
   CreateAcount = mongoose.model('CreateAcount'),
   AddVacancy = mongoose.model('AddVacancy'),
   SmEmployee = mongoose.model('SmEmployee'),
   email_veryfication = mongoose.model('email_veryfication'),
   mobile_Veryfication = mongoose.model('mobile_Veryfication'),
   medicine_data = mongoose.model('medicine_data'),
   apply_job = mongoose.model('apply_job'),
   my_staff = mongoose.model('my_staff'),
   customer_cart = mongoose.model('customer_cart'),
   customer = mongoose.model('customer'),
   customer_delivery_detail = mongoose.model('customer_delivery_detail'),
   customer_order = mongoose.model('customer_order'),
   medicine_salt = mongoose.model('medicine_salt'),
   rejection = mongoose.model('rejection'),
   suspend = mongoose.model('suspend'),
   health_locker = mongoose.model('health_locker'), 
   medFinder = mongoose.model('medFinder'), 
   state = mongoose.model('state'),
   city = mongoose.model('city'),
   packagejob = mongoose.model('packagejob'),
   medicine_not_found = mongoose.model('medicine_not_found'),
   wrong_medicine = mongoose.model('wrong_medicine'),
   shortlist_candidiate = mongoose.model('shortlist_candidiate'),
   super_admin = mongoose.model('super_admin');

exports.SmEmployee = function(req, res){
	if(typeof req.body.user_fname == 'undefined'
		|| typeof req.body.user_lname == 'undefined'
		|| typeof req.body.user_dob == 'undefined'
		|| typeof req.body.user_gender == 'undefined'
		|| typeof req.body.user_email == 'undefined'
		|| typeof req.body.user_pwd == 'undefined'
		|| typeof req.body.user_contact == 'undefined'
		)
	{
       console.log("employee not provided all data");
       return res.send({
       	statusCode: 400,
       	message: "employee not provided all data"
       })
	}
	else{
        var user_email = req.body.user_email;
        var user_contact = req.body.user_contact;
		SmEmployee.findOne({User_Email: user_email}, function(err, doc){
			if(err){
				console.log("if err >>>>>", err);
				logger.log.info(err);
				return res.send({
					statusCode: 500,
					message: "something went wrong"
				})
			}
			if(doc == null){
				SmEmployee.findOne({User_Contact: user_contact}, function(err, doc){
					if(err) throw err;
					if(doc == null){
                       // var hashedPassword = bcrypt.hashSync(req.body.user_pwd, 8);
                        var sm_employee = new SmEmployee();
							sm_employee.User_Fname = req.body.user_fname;
							sm_employee.User_Lname = req.body.user_lname;
							sm_employee.User_Dob = req.body.user_dob;
							sm_employee.User_Gender = req.body.user_gender;
							sm_employee.User_Email = req.body.user_email;
							sm_employee.User_Pwd = req.body.user_pwd;
							sm_employee.User_Contact = req.body.user_contact;
							sm_employee.User_Image = '';
							sm_employee.User_Address1 = '';
							sm_employee.User_Address2 = '';
							sm_employee.User_Aadhar_No = '';
							sm_employee.User_State = '';
							sm_employee.User_City = '';
							sm_employee.User_PIncode = '';
							sm_employee.User_Education = '';
							sm_employee.Certi_Image = '';
							sm_employee.User_College = '';
							sm_employee.User_University = '';
							sm_employee.User_Passing_Year = '';
							sm_employee.User_Percentage = '';
							sm_employee.User_Experience_Type = '';
							sm_employee.Fresher_Expected_Ctc = '';
							sm_employee.User_Experience = '';
							sm_employee.Current_Ctc = '';
							sm_employee.Expected_Ctc = '';
							sm_employee.Notice_Period = '';
							sm_employee.Current_Ctc = '';
							sm_employee.Computer_Knowledge = '';
							sm_employee.Billing_Software = '0';
							sm_employee.Software_Data = '';
							sm_employee.User_Signup = '';
							sm_employee.current_employeer = '';
				            sm_employee.current_employeer_contact = '';
				            sm_employee.current_join_date = '';
				            sm_employee.total_experience = '';
				            sm_employee.current_employeer_person = '';
							sm_employee.User_Profile = '';
							sm_employee.User_Create = Date.now();
							sm_employee.User_Update = Date.now();
							sm_employee.save(function(err){
								if(err)
								{
									console.log("if err >>>>>", err);
									logger.log.info(err);
									return res.send({
										statusCode: 500,
										messag: "something went wrong"
									})
								}
								//var token = jwt.sign({ id: sm_employee._id }, config.secret, {expiresIn: 86400});
								// if(err){
								// 	console.log("datatype is not right");
								// 	return res.send({
								// 		statusCode: 500,
								// 		message: "datatype is not right"
								// 	})
								// };
					           // var otp = randomstring.generate({length: 6, charset: 'numeric'}); 
								var transporter = nodemailer.createTransport(smtpTransport({
									 service: 'gmail',
									 host: 'smtp.gmail.com',
									 proxy: 'http://localhost:3000/',
					                 port: 25,
									 auth: {
									   user: config.email,
									   pass: config.password
									 }
								}));

								var mailOptions = {
								  from: 'Search4PharamcyJobs<'+config.email+'>',
								  to: req.body.user_email,
								  bcc: 'nntpvtltd@gmail.com',
								  subject: 'Welcome to Search4PharamcyJobs',
								  //text: ' Thank You For SignUp In nSearch 4 Pharmacy'
								  html:'Dear '+sm_employee.User_Fname+' '+sm_employee.User_Lname+',\
								  <br><br>Thank you for  signing up  with \
								  <b>Search4PharamcyJobs.</b>\
								  <br><br>\
								  <table style="border: 1px solid black;border-collapse: collapse;">\
									<tr><td style="border: 1px solid black;border-collapse: collapse;"><b>Username</b></td><td style="border: 1px solid black;border-collapse: collapse;">' +sm_employee.User_Email+'</td></tr>\
									<tr><td style="border: 1px solid black;border-collapse: collapse;"><b>Password</b></td><td style="border: 1px solid black;border-collapse: collapse;">'+sm_employee.User_Pwd+'</td></tr>\
								  </table><br>\
								  <p>Welcome to the Family.</p>\
                                  <br><br>Thanks & Regards,<br>Customer Support<br><b>Seach4PharmacyJobs</b>'								  
								};

								transporter.sendMail(mailOptions, function(error, info){
								   if(error){
									    console.log("Failed to send mail "+error);
									    logger.log.info(error);
									}
									else{
										console.log("sm_employee sign_up successfully");
									}
								});  
								return res.send({
									statusCode: 200,
									message: "Thank you for information.",
									data: sm_employee
								});								
							});
					}
					else{
						console.log("This phone number is already resgister");
						return res.send({
							statusCode: 400,
							message: "This phone number is already resgister."
						})
					}
				})
			}
			else{
				console.log("Email id is already registerd");
				return res.send({
					statusCode: 400,
					message: "Email Address is Already Registerd."
				})
			}
		})
	}
}   

exports.get_employee_by_employee_id = function(req, res){
	if(typeof req.query.employee_id == 'undefined')
	{
		console.log("employee_id is not provided");
		return res.send({
			statusCode: 400,
			message: "employee_id is not provided"
		})
	}
	else{
		SmEmployee.findById({_id: req.query.employee_id}, function(err, doc){
			if(err) throw err;
			if(doc == null)
			{
				return res.send({
					statusCode: 400,
					message: "employee id is not exists",
					employee: doc
				})
			}
			else{
				return res.send({
					statusCode: 200,
					message: "successfully get employee by employee id",
					employee: doc
				})
			}
		})
	}
}

exports.EmpLogin = function(req, res){
	var email_id = req.body.email_id;
	var password = req.body.password;
	if(typeof email_id == 'undefined'
		|| typeof password == 'undefined')
		{
			console.log("email_id and password is not provided");
			return res.send({
				statusCode: 400,
				message: "email_id and password is not provided"
			})
		}
	else{
		SmEmployee.findOne({User_Email: email_id, User_Pwd: password}, function(err, doc){
			if(err)
			{
				console.log("if err >>>>>", err);
				logger.log.info(err);
				return res.send({
					statusCode: 500,
					message: "something went wrong"
				})
			}
			if(doc == null)
				{
					console.log("User Name or Password is Not Match");
					return res. send({
						statusCode: 400,
						message: "User Name & Password Does Not Match"
					})
        		}
			else{
				return res.send({
					statusCode: 200,
					message: "USER LOGIN SUCCESSFULLY",
					employee_id:doc.id,
					employee_email: doc.User_Email,
	                employee_fname: doc.User_Fname,
	                employee_lname: doc.User_Lname,
	                employee_pimg: doc.User_Image,
	                is_email_valid: doc.is_email_valid,
	                is_contact_valid: doc.is_contact_valid
				})
			}
		})
	}
}


exports.SmEmployeeUpdate = function(req, res){
	// console.log(req.body);
	var employee_id = mongoose.Types.ObjectId(req.body.employee_id);
	if(typeof req.body.employee_id == 'undefined' 
		//|| typeof req.body.user_address1 == 'undefined'
		//|| typeof req.body.user_address2 == 'undefined'
		// || typeof req.body.user_aadhar == 'undefined'
		// || typeof req.body.user_state == 'undefined'
		// || typeof req.body.user_city == 'undefined'
		// || typeof req.body.user_pincode == 'undefined'
		// || typeof req.body.user_profile == 'undefined'
		// || typeof req.body.user_education == 'undefined'
		// // || typeof req.body.certi_image == 'undefined'
		// || typeof req.body.user_college == 'undefined'
		// || typeof req.body.user_university == 'undefined'
		// || typeof req.body.user_passing_year == 'undefined'
		// || typeof req.body.user_percentage == 'undefined'
		// || typeof req.body.user_experience_type == 'undefined'
		// //|| typeof req.body.fresher_expected_ctc == 'undefined'
		// //|| typeof req.body.user_experience == 'undefined'
		// || typeof req.body.current_ctc == 'undefined'
		// || typeof req.body.expected_ctc == 'undefined'
		// || typeof req.body.notice_period == 'undefined'
		// || typeof req.body.computer_knowledge == 'undefined'
		//|| typeof req.body.billing_software == 'undefined'
       // || typeof req.body.software_data == 'undefined'
		//|| typeof req.body.user_image == 'undefined'
		//|| typeof req.body.user_signup == 'undefined'
		// || typeof req.body.profile_status == 'undefined'
        // || typeof req.body.user_create == 'undefined'
        // || typeof req.body.user_update == 'undefined'
        // || typeof req.body.emp_status == 'undefined'
		)
	{
		console.log("employee not provided all data");
		return res.send({
			statusCode: 400,
			message: "Please fill out all detail."
		})
	}
	else{
		SmEmployee.findById({_id: employee_id}, function(err, doc){
			if(err)
			{
				console.log("if err >>>>>", err);
				log.info("if err >>>>>", err);
				return res.send({
					statusCode: 500,
					message: "something went wrong"
				})
			}
            doc.User_Address1 = req.body.user_address1;
		    doc.User_Address2 = req.body.user_address2;
		    doc.User_Aadhar_No = req.body.user_aadhar;
		    doc.User_State = req.body.user_state;
		    doc.User_City = req.body.user_city;
		    doc.User_PIncode = req.body.user_pincode;
		    doc.User_Profile = req.body.user_profile;
		    doc.User_Education = req.body.user_education;
		    doc.Certi_Image = req.body.certi_image;
		    doc.User_College = req.body.user_college;
		    doc.User_University = req.body.user_university;
		    doc.User_Passing_Year = req.body.user_passing_year;
		    doc.User_Percentage = req.body.user_percentage;
		    doc.User_Experience_Type = req.body.user_experience_type;
		    doc.Fresher_Expected_Ctc = req.body.fresher_expected_ctc;
		    doc.User_Experience = req.body.user_experience;
		    doc.Current_Ctc = req.body.current_ctc;
		    doc.Expected_Ctc = req.body.expected_ctc;
		    doc.Notice_Period = req.body.notice_period;
		    doc.Computer_Knowledge = req.body.computer_knowledge;
		    doc.Billing_Software = req.body.billing_software;
		    doc.Software_Data = req.body.software_data;
		    doc.User_Image = req.body.user_image;
			doc.current_employeer = req.body.current_employeer;
			doc.current_employeer_contact = req.body.current_employeer_contact;
			doc.current_join_date = req.body.current_join_date;
			doc.total_experience = req.body.total_experience;
			doc.current_employeer_person = req.body.current_employeer_person;
		    doc.User_Signup = req.body.user_signup;
		    doc.Profile_Status = '1';
		    // doc.User_Create:{type:Date},
		    doc.User_Update = Date.now();
		    // doc.Emp_statusCode:{type: Boolean, default: true}
		    doc.save(function(err){
		    	if(err) throw err;
		    	console.log("Your Profile updated successfully");
		    	return res.send({
		    		statusCode: 200,
		    		message: "Your Profile updated successfully"
		    	})
		    })
		})
	}
}

exports.getallpharmacy = function(req, res){
	Store.find({Store_status: true}, function(err, doc){
		if(err) throw err;
		else if(doc == ''){
			return res.send({
				statusCode: 400,
				message: "No any store are registered."
			})			
		}
		else{
			return res.send({
				statusCode: 200,
				message: "get all pharmacy",
				data: doc
			})
		}
	})
}

exports.empchangepass = function(req, res){
	var employee_id = mongoose.Types.ObjectId(req.body.employee_id);
	var old_password = req.body.old_password;
	var new_password =  req.body.new_password;
	if(typeof employee_id == 'undefined'
		|| typeof old_password == 'undefined'
		|| typeof new_password == 'undefined'
		)
	{
		console.log("employee_id, old_password and new_password is not provided");
		return res.send({
			statusCode: 400,
			message: "employee_id, old_password and new_password is not provided"
		})
	}
	else{
		SmEmployee.findById({_id: employee_id}, function(err, doc){
			if(err) throw err;
			if(doc.User_Pwd == old_password)
			{
				doc.User_Pwd = new_password;
				doc.save(function(err){
					if(err) throw err;
					console.log("Passsword Change Successfully");
					return res.send({
						statusCode: 200,
						message: "Passsword Change Successfully"
					})
				})
			}
			else{
				console.log("Please Check your Current Password");
				return res.send({
					statusCode: 400,
					message: "Please Check your Current Password"
				})
			}
		})
	}
}

exports.EditProfile = function(req, res){
	var employee_id = mongoose.Types.ObjectId(req.body.employee_id);
	if(typeof employee_id == 'undefined'
		|| typeof req.body.user_fname == 'undefined'
		|| typeof req.body.user_lname == 'undefined'
		|| typeof req.body.user_dob == 'undefined'
		|| typeof req.body.user_gender == 'undefined'){
		console.log("employee_id is not provided")
		return res.send({
			statusCode: 400,
			message: "employee_id is not provided"
		})
	}
	else{
		SmEmployee.findById({_id: employee_id}, function(err, doc){
		      if(err) throw err
				doc.User_Fname = req.body.user_fname;
				doc.User_Lname = req.body.user_lname;
				doc.User_Dob = req.body.user_dob;
				doc.User_Gender = req.body.user_gender;
				doc.save(function(err){
				if(err) throw err;
				console.log("Your Profile Update Successfully");
					return res.send({
					 statusCode: 200,
					 message:"Your Profile Updated Successfully"
				 })
			});
		});
	}
}


exports.forgot_password = function(req, res){	
	// var email_id = req.body.employee_id;
	if(typeof req.body.user_email == 'undefined'){
		console.log("user_email is not provided")
		return res.send({
			statusCode: 400,
			message: "user_email is not provided"
		})
	}
	else{
		SmEmployee.findOne({User_Email: req.body.user_email}, function(err, doc){
		      if(err) throw err;
		      if(doc == null){
		      	console.log("You are not registerd with us");
		      	return res.send({
		      		statusCode: 400,
		      		message: "You are not registerd with us."
		      	})
		      }
		      else{
					var transporter = nodemailer.createTransport(smtpTransport({
					  service: 'gmail',
					  host: 'smtp.gmail.com',
					  proxy: 'http://localhost:3000/',
	                  port: 25,
					  auth: {
					    user: config.email,
					    pass: config.password
					  }
					}));

					var mailOptions = {
					  from: 'Seach4PharmacyJobs <'+config.email+'>',
					  to: req.body.user_email,
					  subject: 'Login Credential',
					  // text: 'your old passsword is  ' +doc.User_Pwd,
					  html:'Dear '+doc.User_Fname+' '+doc.User_Lname+',\
					  <br><br>Your login credentials are as below.<br> \
						<br><table style="border: 1px solid black;border-collapse: collapse;">\
						<tr><td style="border: 1px solid black;border-collapse: collapse;"><b>Username</b></td><td style="border: 1px solid black;border-collapse: collapse;">' +doc.User_Email+'</td></tr>\
						<tr><td style="border: 1px solid black;border-collapse: collapse;"><b>Password</b></td><td style="border: 1px solid black;border-collapse: collapse;">'+doc.User_Pwd+'</td></tr>\
						</table><br>\
						<br><span>Thanks & Regards,</sapn><br><spane>Customer Support</span><br><span><b>Seach4PharmacyJobs</b></span>'
					};

					transporter.sendMail(mailOptions, function(error, info){
					  if (error) {
					    console.log("Failed to send mail "+error);
					    logger.log.info(error);
					    return res.send({
					    	statusCode: 400,
					    	message: "Failed to send mail"
					    })
					  } 
					  else {
					    return res.send({
					    	statusCode: 200,
					    	message: "Password has been send to your registered Email Address."
					    })
					  }
					});
				}  
		});
	}
}


exports.match_otp_email = function(req, res){
	if(typeof req.body.email_id == 'undefined'
		|| typeof req.body.otp == 'undefined'
		)
	{
		console.log("email_id or otp is not provided")
		return res.send({
			statusCode: 400,
			message: "email or otp is not provided"
		})
	}
	else{
		email_veryfication.findOne({verify_email: req.body.email_id}, function(err, doc){
			if(err) throw err;
			if(doc.otp != req.body.otp)
			{
				console.log("OTP is not match...... please enter right otp");
				return res.send({
					statusCode: 400,
					message: "OTP is not match. Please enter right OTP"
				})
			}
			else{
				console.log("otp is match.");
				SmEmployee.findOne({User_Email: req.body.email_id}, function(err, doc){
					if(err) throw err;
					if(doc == null)
					{
						console.log("email_id is not exists");
						return res.send({
							statusCode: 400,
							message: "email_id is not exists"
						})
					}
					else{
						doc.is_email_valid = 1;
						doc.save(function(err){
							if(err) throw err;

							console.log("Your email verifivcation is done.");
							return res.send({
								statusCode: 200,
								message: "Your Email Verification Is Done."
							})
						})
					}
				})
			}
		})
	}
}


exports.get_latest_job_detail = function(req, res){
	var sales_person;
	var pharmacist;
	var latest_job;
	var pharmacies;
	var account= [];
	AddVacancy.count({For_Position: 'pharmacist', job_status: 1}, function(err, pharmacistcoount){
		if(err) throw err;
		pharmacist = pharmacistcoount;
		AddVacancy.count({For_Position: 'sales executive', job_status: 1}, function(err, sales_personcoount){
			if(err) throw err;
			sales_person = sales_personcoount;
			Store.find({},null,{sort: {CreateTime: -1}}, function(err, stores){
				if(err) throw err;	
				pharmacies = stores;
			
				AddVacancy.find({job_status: 1},null,{ sort :{ CreateTime : -1}}, function(err, docs){
					if(err) throw err;
					async.each(docs,function(doc, callback){
						if(doc == null)
						{
							callback();
						}
						else{
							var obj = {};
							obj._id = doc.id;
							obj.Title_Of_Vacancy = doc.Title_Of_Vacancy;
							obj.For_Position = doc.For_Position;
							obj.Number_Of_Vacancy = doc.Number_Of_Vacancy;
							obj.Total_Experience_Needed = doc.Total_Experience_Needed;
							obj.Salary_Range = doc.Salary_Range;
							obj.Qualification = doc.Qualification;
							obj.Joining_On = doc.Joining_On;
							obj.Age_Limit = doc.Age_Limit;
							obj.CreateTime = doc.CreateTime;
							CreateAcount.findById({_id: doc.AcountId}, function(err, datam){
								if(err) throw err;
								if(datam != null){
									obj.AcountId_detail = datam;
									Store.findById({_id: datam.Store_Id}, function(err, s_detail){  
										if(err)
										{
											console.log("if err ------>",err);
											logger.log.info(err);
											return res.send({
												statusCode: 500,
												message: "something Went wrong"
											})
										}
										if(s_detail != null){
											obj.store_detail = s_detail;
											account.push(obj);
											callback();											
										}
										else{
											callback();
										}
									});
								}
								else{
									callback();
								}	
							});
						}
					},
					function(err){
						if(err){
							console.log("if err --------->", err);
							logger.log.info(err);
							return res.send({
								statusCode: 500,
								message: "something went wrong"
							})
						}
						if(account == null || account == ''){
							return res.send({
								statusCode: 400,
								message: 'no vacancy'
							})
						}						
						else{
							account.sort(function (a, b) {
							  return new Date(b.CreateTime) - new Date(a.CreateTime);
							});							
							return res.send({
								statusCode: 200,
								sales_person: sales_person,
								pharmacist: pharmacist,
								latest_job: account,
								pharmacies: pharmacies
							})
						}
					})
				})
			})
		})
	})
}

exports.get_job_by_job_id = function(req, res){
	var add_vacancy_id = req.body.add_vacancy_id;
	if(typeof add_vacancy_id == 'undefined')
	{
		console.log("add_vacancy_id is not provided");
		return res.send({
			statusCode: 400,
			message: "add_vacancy_id is not provided"
		})
	}
	else{
		//var store_detail;
		var obj = {};
		AddVacancy.findById({_id: add_vacancy_id}, function(err, doc){
			if(err) throw err;
			obj._id = doc.id;
			obj.Title_Of_Vacancy = doc.Title_Of_Vacancy;
			obj.For_Position = doc.For_Position;
			obj.Number_Of_Vacancy = doc.Number_Of_Vacancy;
			obj.Total_Experience_Needed = doc.Total_Experience_Needed;
			obj.Salary_Range = doc.Salary_Range;
			obj.Qualification = doc.Qualification;
			obj.Joining_On = doc.Joining_On;
			obj.description = doc.Description;
			obj.Age_Limit = doc.Age_Limit;
			obj.CreateTime = doc.CreateTime;
			CreateAcount.findById({_id: doc.AcountId}, function(err, docs){
				if(err) throw err;
				Store.findById({_id: docs.Store_Id}, function(err, store_d){
					if(err) throw err;
					obj.store_detail = store_d;
					return res.send({
						statusCode: 200,
						message: "successfully get job by add_vacancy_id",
						data: obj
					})					
				})
			})
		})
	}
}


exports.applyjob = function(req,res){
	if(typeof req.body.add_vacancy_id == "undefined"
		|| typeof req.body.employee_id == "undefined"
		)
	{
		console.log("add_vacancy_id or email_id is not provided");
		return res.send({
			statusCode: 400,
			message: "add_vacancy_id or email_id is not provided"
		})
	}
	else{
		SmEmployee.findOne({_id: req.body.employee_id}, function(err, sme){
			if(err){
				console.log("if err ----->", err);
				logger.log.info(err);
				return res.send({
					statusCode: 500,
					message: "something went wrong"
				})
			}
			if(sme){
				if(sme.is_email_valid == 1){
					if(sme.is_contact_valid == 1){
						apply_job.findOne({ $and : [ {employee_id : req.body.employee_id},{add_vacancy_id : req.body.add_vacancy_id} ] }, function(err, doc){
							if(err){
								console.log("if err ------>", err);
								logger.log.info(err);
								return res.send({
									statusCode: 500,
									message: "something went wrong"
								})
							}
							if(doc)
							{
								console.log("You have already applied for this job.");
								return res.send({
									statusCode: 400,
									message: "You have already applied for this job."
								})
							}
							else{
								var applyJob = new apply_job();
								applyJob.add_vacancy_id = req.body.add_vacancy_id;
								applyJob.employee_id = req.body.employee_id;
								applyJob.apply_time = Date.now();
								applyJob.save(function(err){
									if(err) throw err;
									AddVacancy.findById({_id: req.body.add_vacancy_id}, function(err, adc){
										if(err) throw err;
										// console.log("adc------->", adc.applicant_count);
										var vac = adc.applicant_count;
										// console.log("vac ----->", vac);
										var tc = sum(vac, 1);
										// console.log("tc ------>", tc);
										adc.applicant_count = tc;
										adc.save(function(err){
											if(err) throw err;
											var jobtitel = adc.Title_Of_Vacancy;
											CreateAcount.findById({_id: adc.AcountId}, function(err, act){
							                    var registrationToken = act.store_fcm;
							                    var payload = {
							                       notification: {
							                          title: "Job application received.",
							                          body: "Job seeker applied on your job post title "+jobtitel+"",
							                          tag: "OrderMedicine",
							                          sound: "retailer_ring"
							                       }
							                    };
							                    admin.messaging().sendToDevice(registrationToken, payload)
							                    .then(function(response) {console.log("Successfully sent message:");})
							                    .catch(function(err) {console.log("Error sending message:"); logger.log.info(err);}); 												
											})
									        console.log("data enter in apply job successfully");							
											return res.send({
												statusCode: 200,
												message: "Job Applied Successfully"
											})
										})
									})
								})
							}
						})						
					}
					else{
						console.log("Please verify your mobile number then apply for this job.");
						return res.send({
							statusCode: 400,
							message: "Please verify your mobile number then apply for this job."
						})
					}
				}
				else{
					console.log("Please verify your Email address then apply for this job.");
					return res.send({
						statusCode: 400,
						message: "Please verify your Email address then apply for this job."
					})					
				}
			}
			else{
				console.log("employee is not exists in our database");
				return({
					statusCode: 400,
					message: "This candidate is not exists in our database"
				})
			}
		})
	}
}

exports.get_store_detail = function(req, res){
	if(typeof req.body.add_vacancy_id == 'undefined')
	{
		console.log("add_vacancy_id is not provided");
		return res.send({
			statusCode: 400,
			message: "add_vacancy_id is not provided"
		})
	}
	else{
		AddVacancy.findById({_id: req.body.add_vacancy_id}, function(err, vacancydoc){
			if(err) throw err;
			CreateAcount.findById({_id: vacancydoc.AcountId}, function(err, acountdoc){
				if(err) throw err;
				Store.findById({_id: acountdoc.Store_Id}, function(err, storedoc){
					if(err) throw err;
					return res.send({
						stataus: 200,
						message: "get store data successfully",
						data: storedoc
					})
				})
			})
		})
	}
}

exports.filter_job = function(req, res){
	if(typeof req.body.year_of_exp != 'undefined' && typeof req.body.salary != 'undefined')
	{
		AddVacancy.find({Total_Experience_Needed: req.body.year_of_exp, Salary_Range: req.body.salary}, function(err, yoedoc){
			if(err) throw err;

			return res.send({
				statusCode: 200,
				message: 'get job data successfully',
				data: yoedoc
			})
		})
	}
	else if(typeof req.body.year_of_exp != 'undefined')
	{
		AddVacancy.find({Total_Experience_Needed: req.body.year_of_exp}, function(err, yoedoc){
			if(err) throw err;
			return res.send({
				statusCode: 200,
				message: 'get year of experience job data successfully',
				data: yoedoc
			})
		})
	}
	else if(typeof req.body.salary != 'undefined')
	{
		AddVacancy.find({Salary_Range: req.body.salary}, function(err, yoedoc){
			if(err) throw err;
			return res.send({
				statusCode: 200,
				message: 'get salary job data successfully',
				data: yoedoc
			})
		})
	}
}

exports.home_page = function(req, res){
	     var account= [];
		AddVacancy.find({},null,{ sort :{ CreateTime : -1}}, function(err, docs){
		if(err) throw err;
		async.each(docs,function(doc, callback){
			if(doc == null)
			{
				callback();
			}
			else{
				var obj = {};
				// obj.AcountId = doc.AcountId;
				obj.Title_Of_Vacancy = doc.title_of_vacancy;
				obj.For_Position = doc.For_Position;
				obj.Number_Of_Vacancy = doc.Number_Of_Vacancy;
				obj.Total_Experience_Needed = doc.Total_Experience_Needed;
				obj.Salary_Range = doc.Salary_Range;
				obj.Qualification = doc.Qualification;
				obj.Joining_On = doc.Joining_On;
				obj.Age_Limit = doc.Age_Limit;
				obj.CreateTime = doc.CreateTime;
				CreateAcount.findById({_id: doc.AcountId}, function(err, datam){
					if(err) throw err;
					obj.AcountId_detail = datam;
					account.push(obj);
					Store.findById({_id: datam.Store_Id}, function(err, s_detail){
						if(err) throw err;
						obj.store_detail = s_detail;
						account.push(obj);
						callback();
					})			
				});
			}
		},
		function(err){
			if(err) throw err;
				return res.send({
				statusCode: 200,
				recent_job: account
			})
		})
	})
}

exports.rejection_emp = function(req, res){
	if(typeof req.body.employee_id == 'undefined'
		|| typeof req.body.reject_reason == 'undefined'
		)
	{
		console.log('employee_id or reject_reason is not provided');
		return res.send({
			statusCode: 400,
			message: 'employee_id or reject_reason is not provided'
		})
	}
	else
	{
		var emp_rejection = new rejection();
		emp_rejection.employee_id = req.body.employee_id;
		emp_rejection.reject_reason = req.body.reject_reason;
		emp_rejection.save(function(err){
			if(err) throw err;
			var employee_email;
			SmEmployee.findById({_id: req.body.employee_id}, function(err, doc){
				if(err) throw err;
				employee_email =doc.User_Email;
				doc.Profile_Status = 2;
				doc.save(function(err){
					if(err) throw err;
					var transporter = nodemailer.createTransport(smtpTransport({
						 service: 'gmail',
						 host: 'smtp.gmail.com',
						 proxy: 'http://localhost:3000/',
		                 port: 25,
						 auth: {
						   user: config.email,
						   pass: config.password
						 }
					}));

					var mailOptions = {
					  from: 'Info@Search4PharamcyJobs.com <'+config.email+'>',
					  to: employee_email,
					  subject: 'Search4PharamcyJobs reject reason',
					  text: req.body.rejection_reason
					};

					transporter.sendMail(mailOptions, function(error, info){
						if(error)
						{
							console.log("Failed to send mail "+error);
							logger.log.info(error);
							return res.send({
							    statusCode: 400,
							    message: "Failed to send mail"
							})
						}
						else{
							console.log("successfully send reject reason");
							return res.send({
								statusCode: 200,
								message: "Successfully Send Reject Reason To Registered User"
							})
						}
					});               
				})
			})
		})
	}
}

exports.suspend_emp = function(req, res){
	if(typeof req.body.employee_id == 'undefined'
		|| typeof req.body.suspend_reason == 'undefined'
		)
	{
		console.log('employee_id or suspend_reason is not provided');
		return res.send({
			statusCode: 400,
			message: 'employee_id or suspend_reason is not provided'
		})
	}
	else
	{
		var emp_suspend = new suspend();
		emp_suspend.employee_id = req.body.employee_id;
		emp_suspend.suspend_reason = req.body.suspend_reason;
		emp_suspend.save(function(err){
			if(err) throw err;
			var employee_email;
			SmEmployee.findById({_id: req.body.employee_id}, function(err, doc){
				if(err) throw err;
				employee_email =doc.User_Email;
				doc.Profile_Status = 3;
				doc.save(function(err){
					if(err) throw err;
					var transporter = nodemailer.createTransport(smtpTransport({
						 service: 'gmail',
						 host: 'smtp.gmail.com',
						 proxy: 'http://localhost:3000/',
		                 port: 25,
						 auth: {
						   user: config.email,
						   pass: config.password
						 }
					}));

					var mailOptions = {
					  from: 'Info@Search4PharamcyJobs.com <'+config.email+'>',
					  to: employee_email,
					  subject: 'Search4PharamcyJobs suspend reason',
					  text: req.body.suspend_reason
					};

					transporter.sendMail(mailOptions, function(error, info){
						if(error)
						{
							console.log("Failed to send mail "+error);
							logger.log.info(errot);
							return res.send({
							    statusCode: 400,
							    message: "Failed to send mail"
							})
						}
						else{
							console.log("successfully send suspend reason");
							return res.send({
								statusCode: 200,
								message: "Successfully Send Suspend Reason To Registered User"
							})
						}
					});               
				})
			})
		})
	}
}

exports.add_activation = function(req, res){
	if(typeof req.body.employee_id == 'undefined')
	{
		console.log("employee_id is not provided");
		return res.send({
			statusCode: 400,
			message: "employee_id is not provided"
		})
	}
	else{
		SmEmployee.findById({_id: req.body.employee_id}, function(err, doc){
			if(doc == null)
			{
				console.log("this type of employee id not exists");
				return res.send({
					stataus: 400,
					message: "this type of employee id not exists"
				})
			}
			else{
				console.log("Successfully update statusCode 4");
				doc.Profile_Status = 4;
				doc.save(function(err){
					if(err) throw err;
					console.log("Successfully update statusCode 4");
					return res.send({
						statusCode: 200,
						message: "Employee Account Successfully Activated"
					})
				})
			}
		})
	}
}


exports.email_veryfication_by_otp = function(req, res){
	if(typeof req.body.email_id == 'undefined')
	{
       console.log("not provided email id");
       return res.send({
       	statusCode: 400,
       	message: "not provided email id"
       })
	}
	else{
    // emailExistence.check(req.body.email_id, function(err,res){
    // 	if(err) throw err;
    //     console.log('res: '+res);
    // });
    SmEmployee.findOne({User_Email: req.body.email_id}, function(err, edoc){
    	if(err) throw err;
    	// if(edoc != null)
    	// {
    	// 	console.log("this email is already register");
    	// 	return res.send({
    	// 		statusCode: 400,
    	// 		message: "this email is already register"
    	// 	})
    	// }
    	// else{

            var otp = randomstring.generate({length: 6, charset: 'numeric'}); 
			var transporter = nodemailer.createTransport(smtpTransport({
				 service: 'gmail',
				 host: 'smtp.gmail.com',
				 proxy: 'http://localhost:3000/',
                 port: 25,
				 auth: {
				   user: config.email,
				   pass: config.password
				 }
			}));

			var mailOptions = {
			  from: 'Search4PharamcyJobs <'+config.email+'>',
			  to: req.body.email_id,
			  subject: 'Email Address Verification',
			 // text: 'your veryfication cod is ' +otp,
			  html:'Dear '+edoc.User_Fname+' '+edoc.User_Lname+',\
			  <br><br>Thank You for signing up With Search4PharamcyJobs<br><br>Your Email Verification Code is <b>' +otp+'.</b><br><i>*This verification code is valid for 1 hours</i><br>\
			  <br>Thanks & Regards,<br>Customer Support<br><b>Seach4PharmacyJobs</b>'
			};

			transporter.sendMail(mailOptions, function(error, info){
				if(error)
				{
					console.log("Failed to send mail "+error);
					logger.log.info(error);
					return res.send({
					    statusCode: 400,
					    message: "Failed to send mail"
					})
				}
				else{
					console.log('email is '+req.body.email_id+' and otp ' + otp);
					email_veryfication.findOne({verify_email: req.body.email_id}, function(err, doc){
						if(err){
							console.log("if err >>>>>", err);
							logger.log.info(err);
							return res.send({
								statusCode: 500,
								message: "something went wrong"
							})
						}
						if(doc == null){
							var email_very  = new email_veryfication();
							email_very.otp = otp;
							email_very.verify_email = req.body.email_id;
							email_very.date = Date.now();
							email_very.save(function(err){
								if(err) throw err;
								console.log("email and otp save successfully");
								return res.send({
									statusCode: 200,
									message: "please check your email for otp. email and otp save successfully"
								})
							}) 
						}
						else{
							doc.otp = otp;
							doc.date = Date.now();
							doc.save(function(err){
								if(err) throw err;
								console.log("email and otp update successfully");
								return res.send({
									statusCode: 200,
									message: "please check your email for otp. email and otp save successfully"
								})
							})
						}
					})
				}
			});               
    	// }
    })
   
	}
}


exports.change_email_address = function(req, res){
	if(typeof req.body.employee_id == "undefined"
	   || typeof req.body.password  == "undefined"
	   || typeof req.body.new_email_id == "undefined"
	   )
	{
		console.log("employee_id or password or new_email_id is not provided");
		return res.send({
			statusCode: 400,
			message: "Please fill all the medetory field."
		})
	}
	else{
		SmEmployee.findById({_id: req.body.employee_id}, function(err, datam){
			console.log("datam_file = "+datam.User_Pwd, "req.body  =  "+req.body.password);
			if(datam.User_Pwd == req.body.password)
			{
				console.log("password is match");
				if(datam.User_Email == req.body.new_email_id)
				{
					console.log("It is your email id");
					return res.send({
						statusCode: 400,
						message: "There is no change in your Email Address."
					})
				}
				else{
					SmEmployee.findOne({User_Email: req.body.new_email_id}, function(err, data){
						if(err) throw err;
						if(data == null)
						{
							SmEmployee.findOne({_id: req.body.employee_id}, function(err, doc){
								if(err) throw err;
								doc.User_Email = req.body.new_email_id;
								doc.is_email_valid = 0;
								doc.save(function(err){
									if(err) throw err;
									console.log("Your email is change successfully");
									return res.send({
										statusCode: 200,
										message: "Your Email has been change successfully"
									})
								})
							})

						}
						else{
							console.log("This email address is already using by othere persion");
							return res.send({
								statusCode: 400,
								message: "This Email Address is already registerd."
							})
						}
					})
					
				}
			}
			else{
				console.log("password is not match");
				return res.send({
					statusCode: 400,
					message: "Password is not match"
				})				
			}
			// if(datam.User_Email == req.body.new_email_id)
			// {
			// 	console.log("It is your email id");
			// 	return res.send({
			// 		statusCode: 400,
			// 		message: "It is your email id"
			// 	})
			// }
			// else{
			// 	SmEmployee.find({User_Email: req.body.new_email_id}, function(err, data){
			// 		if(err) throw err;
			// 		if(doc != null)
			// 		{
			// 			console.log("This email is already is use");
			// 			return res.send({
			// 				statusCode: 400,
			// 				message: "This email is already is use"
			// 			})
			// 		}
			// 		else{
			            
			// 		}
			// 	})
			// }
		})
	}
}


exports.mobile_veryfication_by_otp = function(req, res){
	if(typeof req.body.mobile_no == 'undefined')
	{
       console.log("not provided mobile number");
       return res.send({
       	statusCode: 400,
       	message: "not provided mobile number"
       })
	}
	else{
    // emailExistence.check(req.body.email_id, function(err,res){
    // 	if(err) throw err;
    //     console.log('res: '+res);
    // });
    SmEmployee.findOne({User_Contact: req.body.mobile_no}, function(err, edoc){
    	if(err) throw err;
    	// if(edoc != null)
    	// {
    	// 	console.log("this email is a lready register");
    	// 	return res.send({
    	// 		statusCode: 400,
    	// 		message: "this email is already register"
    	// 	})
    	// }
    	// else{
    		//+++++++++++++++ Testing Url +++++++++++++++
    	// request('http://www.global91sms.in/API/WebSMS/Http/v1.0a/index.php?username='+config.msgUname+'&password='+config.msgPass+'&sender=PHJOBS&to='+req.body.mobile_no+'&message=+Your+Verification Code is++'+otp+'.++ This OTP expires in 15 minute.++++++++++++Search4PharmacyJobs', function (err, done, body) {	
    	// http://sms.global91sms.in/api/mt/SendSMS?user='+config.msgUname+'&password='+config.msgPass+'&senderid=ORDMED&channel=trans&DCS=0&flashsms=0&number='+datam.mobile_number+'&text=
        var otp = randomstring.generate({length: 6, charset: 'numeric'}); 
        request('http://sms.global91sms.in/api/mt/SendSMS?user='+config.msgUname+'&password='+config.msgPass+'\
        	&senderid=PHJOBS&channel=trans&DCS=0&flashsms=0&number='+req.body.mobile_no+'&text=+Your+Verification Code is++'+otp+'.++ This OTP expires in 15 minute.++++++++++++Search4PharmacyJobs', function (err, done, body) {
				if(err)
				{
					console.log("if err -----> mobile_v "+err);
					logger.log.info(err);
					return res.send({
					    statusCode: 500,
					    message: "something went wrong"
					})
				}
				if(!done)
				{
					return res.send({
					    statusCode: 400,
					    message: "something went wrong"
					})
				}				
				else{
					console.log('email is '+req.body.mobile_no+' and otp ' + otp);		
					mobile_Veryfication.findOne({verify_mobile_number: req.body.mobile_no}, function(err, doc){
						if(err){
							console.log("if err >>>>>", err);
							logger.log.info(err);
							return res.send({
								statusCode: 500,
								message: "something went wrong"
							})
						}
						if(doc == null){
							var mobile_very  = new mobile_Veryfication();
							mobile_very.otp = otp;
							mobile_very.verify_mobile_number = req.body.mobile_no;
							mobile_very.date = Date.now();
							mobile_very.save(function(err){
								if(err) throw err;
								console.log("mobile number and otp save successfully");
								return res.send({
									statusCode: 200,
									message: "please check your mobile for otp. mobile number and otp save successfully"
								})
							}) 
						}
						else{
							doc.otp = otp;
							doc.date = Date.now();
							doc.save(function(err){
								if(err) throw err;
								console.log("mobile number and otp update successfully");
								return res.send({
									statusCode: 200,
									message: "please check your mobile for otp. mobile number and otp save successfully"
								})
							})
						}
					})
				}
			});               
    	// }
    })
   
	}
}


exports.match_otp_mobile = function(req, res){
	if(typeof req.body.mobile == 'undefined'
		|| typeof req.body.otp == 'undefined'
		)
	{
		console.log("Mobile Number or OTP is not provided")
		return res.send({
			statusCode: 400,
			message: "Mobile Number or OTP is not provided"
		})
	}
	else{
		mobile_Veryfication.findOne({verify_mobile_number: req.body.mobile}, function(err, doc){
			if(err) throw err;
			if(doc.otp != req.body.otp)
			{
				console.log("OTP is not match...... please enter right otp");
				return res.send({
					statusCode: 400,
					message: "OTP is not match. Please enter right OTP"
				})
			}
			else{
				console.log("otp is match.");
				SmEmployee.findOne({User_Contact: req.body.mobile}, function(err, doc){
					if(err) throw err;
					if(doc == null)
					{
						console.log("mobile number is not exists");
						return res.send({
							statusCode: 400,
							message: "mobile number is not exists"
						})
					}
					else{
						doc.is_contact_valid = 1;
						doc.save(function(err){
							if(err) throw err;

							console.log("Your mobile number verifivcation is done.");
							return res.send({
								statusCode: 200,
								message: "Your mobile number Verification Is Done."
							})
						})
					}
				})
			}
		})
	}
}


exports.get_latest_job = function(req, res){
	var sales_person;
	var pharmacist;
	var latest_job;
	var pharmacies;
	var account = [];
	AddVacancy.count({For_Position: 'pharmacist', job_status: 1}, function(err, pharmacistcoount){
		if(err) throw err;
		pharmacist = pharmacistcoount;		
		AddVacancy.count({For_Position: 'sales executive', job_status: 1}, function(err, sales_personcoount){
			if(err) throw err;
			sales_person = sales_personcoount;
			Store.find({},null,{sort: {CreateTime: -1}}, function(err, stores){
				if(err) throw err;
				pharmacies = stores;
				if(req.query.job_id == 1){
				AddVacancy.find({job_status: 1, For_Position: "sales executive"},null,{ sort :{ CreateTime : -1}}, function(err, docs){
					if(err) throw err;
					async.each(docs,function(doc, callback){
						if(doc == null)
						{
							callback();
						}
						else{
							var obj = {};
							obj._id = doc.id;
							obj.Title_Of_Vacancy = doc.Title_Of_Vacancy;
							obj.For_Position = doc.For_Position;
							obj.Number_Of_Vacancy = doc.Number_Of_Vacancy;
							obj.Total_Experience_Needed = doc.Total_Experience_Needed;
							obj.Salary_Range = doc.Salary_Range;
							obj.Qualification = doc.Qualification;
							obj.Joining_On = doc.Joining_On;
							obj.Age_Limit = doc.Age_Limit;
							obj.CreateTime = doc.CreateTime;
							CreateAcount.findById({_id: doc.AcountId}, function(err, datam){
								if(err) throw err;
								if(datam != null){
									obj.AcountId_detail = datam;
									Store.findById({_id: datam.Store_Id}, function(err, s_detail){  
										if(err)
										{
											console.log("if err ------>",err);
											logger.log.info(err);
											return res.send({
												statusCode: 500,
												message: "something Went wrong"
											})
										}
										if(s_detail != null){
											obj.store_detail = s_detail;
											account.push(obj);
											callback();
										}
										else{
											callback();
										}
									})									
								}
								else{
									callback();
								}							
							});
						}
					},
					function(err){
						if(err) throw err;
						if(account == null || account == ''){
							console.log('no vacancy');
							return res.send({
								statusCode: 400,
								message: 'no vacancy'
							})
						}					
						else{
							return res.send({
								statusCode: 200,
								sales_person: sales_person,
								pharmacist: pharmacist,
								latest_job: account,
								pharmacies: pharmacies
							})
						}
					})
				})}
				if(req.query.job_id == 2){
				AddVacancy.find({job_status: 1, For_Position: "pharmacist"},null,{ sort :{ CreateTime : -1}}, function(err, docs){
					if(err) throw err;
					async.each(docs,function(doc, callback){
						if(doc == null)
						{
							callback();
						}
						else{
							var obj = {};
							obj._id = doc.id;
							obj.Title_Of_Vacancy = doc.Title_Of_Vacancy;
							obj.For_Position = doc.For_Position;
							obj.Number_Of_Vacancy = doc.Number_Of_Vacancy;
							obj.Total_Experience_Needed = doc.Total_Experience_Needed;
							obj.Salary_Range = doc.Salary_Range;
							obj.Qualification = doc.Qualification;
							obj.Joining_On = doc.Joining_On;
							obj.Age_Limit = doc.Age_Limit;
							obj.CreateTime = doc.CreateTime;
							CreateAcount.findById({_id: doc.AcountId}, function(err, datam){
								if(err) throw err;
								if(datam != null){
									obj.AcountId_detail = datam;
									Store.findById({_id: datam.Store_Id}, function(err, s_detail){  
										if(err)
										{
											console.log("if err ------>",err);
											logger.log.info(err);
											return res.send({
												statusCode: 500,
												message: "something Went wrong"
											})
										}
		                              if(s_detail != null){
										obj.store_detail = s_detail;
										account.push(obj);
										callback();		                              	
		                              }
		                              else{
		                              	callback();
		                              }
									});										
								}
								else{
									callback();
								}						
							});
						}
					},
					function(err){
						if(err) throw err;
						if(account == null || account == ''){
							console.log('no vacancy');
							return res.send({
								statusCode: 400,
								message: 'no vacancy'
							})
						}						
						else{
							return res.send({
								statusCode: 200,
								sales_person: sales_person,
								pharmacist: pharmacist,
								latest_job: account,
								pharmacies: pharmacies
							})
						}
					})
				})}
			})
		})
	})
}


exports.change_mobile_number = function(req, res){
	if(typeof req.body.employee_id == "undefined"
	   || typeof req.body.password  == "undefined"
	   || typeof req.body.new_mobile == "undefined"
	   )
	{
		console.log("employee_id or password or new_mobile is not provided");
		return res.send({
			statusCode: 400,
			message: "Please fill all the medetory field."
		})
	}
	else{
		SmEmployee.findById({_id: req.body.employee_id}, function(err, datam){
			console.log("datam_file = "+datam.User_Pwd, "req.body  =  "+req.body.password);
			if(datam.User_Pwd == req.body.password)
			{
				console.log("password is match");
				if(datam.User_Contact == req.body.new_mobile)
				{
					console.log("It is your MObile Number");
					return res.send({
						statusCode: 400,
						message: "There is no change in your MObile Number"
					})
				}
				else{
					SmEmployee.findOne({User_Contact: req.body.new_mobile}, function(err, data){
						if(err) throw err;
						if(data == null)
						{
							SmEmployee.findOne({_id: req.body.employee_id}, function(err, doc){
								if(err) throw err;
								doc.User_Contact = req.body.new_mobile;
								doc.is_contact_valid = 0;
								doc.save(function(err){
									if(err) throw err;
									console.log("Your Mobile Number has been change successfully");
									return res.send({
										statusCode: 200,
										message: "Your Mobile Number has been change successfully"
									})
								})
							})

						}
						else{
							console.log("This Mobile Number is already using by othere persion");
							return res.send({
								statusCode: 400,
								message: "This Mobile Number is already registerd."
							})
						}
					})
					
				}
			}
			else{
				console.log("password is not match");
				return res.send({
					statusCode: 400,
					message: "Password is not match"
				})				
			}
			// if(datam.User_Email == req.body.new_email_id)
			// {
			// 	console.log("It is your email id");
			// 	return res.send({
			// 		statusCode: 400,
			// 		message: "It is your email id"
			// 	})
			// }
			// else{
			// 	SmEmployee.find({User_Email: req.body.new_email_id}, function(err, data){
			// 		if(err) throw err;
			// 		if(doc != null)
			// 		{
			// 			console.log("This email is already is use");
			// 			return res.send({
			// 				statusCode: 400,
			// 				message: "This email is already is use"
			// 			})
			// 		}
			// 		else{
			            
			// 		}
			// 	})
			// }
		})
	}
}


exports.contact_us = function(req, res){
	if(typeof req.body.name == 'undefined'
		|| typeof req.body.email == 'undefined'
		|| typeof req.body.subject == 'undefined'
		|| typeof req.body.comment == 'undefined'
		)
	{
		console.log("name or email or subject or comment is not provided");
		 return res.send({
		 	statusCode: 400,
		 	message: "name or email or subject or comment is not provided"
		 })
	}
	else{
		var transporter = nodemailer.createTransport(smtpTransport({
			service: 'gmail',
			host: 'smtp.gmail.com',
			proxy: 'http://localhost:3000/',
		    port: 25,
			auth: {
				user: config.email,
				pass: config.password
			}
			}));

			var mailOptions = {
				from: 'Search4PharamcyJobs<'+config.email+'>',
				to: config.email,
				subject: req.body.subject,
				html: 'There is new enquiry from Search4PharamcyJobs <br><br> Person Name: <b>'+req.body.name+'</b><br> Email : <b>'+req.body.email+'</b><br><br>Person Comment: '+req.body.comment+''
			};

			transporter.sendMail(mailOptions, function(error, info){
				if(error)
				{
					console.log("Failed to send mail "+error);
					logger.log.info(error);
					return res.send({
							statusCode: 400,
							message: "Failed to send mail"
					})
				}
				else{
					console.log("Thank you for submitting your request. We will get back to you soon.");
					return res.send({
						statusCode: 200,
						message: "Thank you for submitting your request. We will get back to you soon."
					})
				}
			})		
	}
}
