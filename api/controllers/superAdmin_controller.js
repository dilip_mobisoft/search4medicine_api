'use strict';
var admin = require("firebase-admin");
var config = require('../config/config');
var notificationFirebase = require('../notification/notificationFirebase');
var logger =  require('../logger/logger');
var node_mailer = require('../node_mailer/node_mailer');
var fs = require('fs');
var sum = require('sum-of-two-numbers');
var async = require('async');
var smtpTransport = require('nodemailer-smtp-transport');
var nodemailer = require('nodemailer');
var regexp = require('node-regexp');
var format  = require('node.date-time');
var emailformat = require('string-template');
var emailExistence = require('email-existence');
var mongoose = require('mongoose'),
   Store = mongoose.model('Store'),
   storeadvertisement = mongoose.model('storeadvertisement'),
   notification = mongoose.model('notification'),
   OwnerInfo = mongoose.model('OwnerInfo'),
   OperationalInfo = mongoose.model('OperationalInfo'),
   CreateAcount = mongoose.model('CreateAcount'),
   AddVacancy = mongoose.model('AddVacancy'),
   SmEmployee = mongoose.model('SmEmployee'),
   email_veryfication = mongoose.model('email_veryfication'),
   mobile_Veryfication = mongoose.model('mobile_Veryfication'),
   medicine_data = mongoose.model('medicine_data'),
   apply_job = mongoose.model('apply_job'),
   my_staff = mongoose.model('my_staff'),
   customer_cart = mongoose.model('customer_cart'),
   customer = mongoose.model('customer'),
   customer_delivery_detail = mongoose.model('customer_delivery_detail'),
   customer_order = mongoose.model('customer_order'),
   medicine_salt = mongoose.model('medicine_salt'),
   rejection = mongoose.model('rejection'),
   suspend = mongoose.model('suspend'),
   health_locker = mongoose.model('health_locker'), 
   medFinder = mongoose.model('medFinder'), 
   state = mongoose.model('state'),
   city = mongoose.model('city'),
   packagejob = mongoose.model('packagejob'),
   medicine_not_found = mongoose.model('medicine_not_found'),
   wrong_medicine = mongoose.model('wrong_medicine'),
   shortlist_candidiate = mongoose.model('shortlist_candidiate'),
   retailer_viewed_profile = mongoose.model('retailer_viewed_profile'),
   staff = mongoose.model('staff'),
   retailerPackagehistory = mongoose.model('retailerPackagehistory'),
   response_store = mongoose.model('response_store'),
   notificationcr = mongoose.model('notificationcr'),
   rejectedRetailer = mongoose.model('rejectedRetailer'),
   suspendRetailer = mongoose.model('suspendRetailer'),    
   super_admin = mongoose.model('super_admin');

// for Tesing
// exports.sendNotication = function(req, res){
//     var reqobj = {title : req.body.title,
//     bodyText :req.body.bodyText,
//     sound : req.body.sound,
//     fcm : req.body.fcm};
//    notificationFirebase.notificationFirebase(reqobj, function(err, data){
//       if(err) throw err;
//    })
// }

exports.super_admin = function(req, res){
   console.log('enter super_admin api');
   if(typeof req.body.email_id == 'undefined'
      || typeof req.body.password == 'undefined')
   {
      console.log("email and password is not provided");
      return res.send({
         statusCode: 400,
         message: "email and password is not provided"
      })
   }
   else{
      var superadmin = new super_admin();
      superadmin.name = req.body.name;
      superadmin.email_id = req.body.email_id;
      superadmin.password = req.body.password;
      superadmin.update_time = Date.now();
      superadmin.save(function(err){
         if(err){
            console.log("if err ----->", err);
            logger.log.info(err);
            return res.send({
               statusCode: 500,
               message: "something went wrong"
            })
         }
         console.log("super admin registerd successfully");
         return res.send({
            statusCode: 200,
            message: "super admin registerd successfully",
            //data: superadmin
         })
      })
   }
}

exports.super_admin_login = function(req, res){
   if(typeof req.body.email_address == 'undefined'
      || typeof req.body.password == 'undefined'
      )
   {
      console.log("email_address or password is not provided");
      logger.log.info("email_address or password is not provided");
      return res.send({
         statusCode: 400,
         message: "Email Address or Password is not provided."
      })
   }
   else{
      var data = {};
      super_admin.findOne({email_id: req.body.email_address, password: req.body.password}, function(err, doc){
         if(err){
            console.log("if err ----->", err);
            logger.log.info(err);
            return res.send({
               statusCode: 500,
               message: "something went wrong"
            })
         }
         else{
            if(!doc){
               console.log("email_id or password is not match");
               return res.send({
                  statusCode: 400,
                  message: "Email Address or Password is not match."
               })
            }
            else{
               data._id = doc.id;
               data.name = doc.name;
               data.email_id = doc.email_id;
               data.user_type = doc.user_type;
               console.log("super admin login successfully");
               return res.send({
                  statusCode: 200,
                  message: "Super Admin Login Successfully.",
                  data: data
               })
            }
         }
      })
   }
}


exports.all_emaployee_list = function(req, res){
   if(typeof req.body.query == 'undefined'){
      // if(req.body.pageNumber != '1'){
         // var page = 0;
         var page = Math.max(0, req.body.pageNumber); //req.body.pageNumber * 5;
         console.log("page", page)        
      // SmEmployee.find({},{},{ sort :{ '_id' : -1}}).skip(10 * page - 10).limit(10).exec(function(err, docs){
       // SmEmployee.find({},{},{ sort :{ User_Update : -1}}).skip(page).limit(10)         
      SmEmployee.find({},{},{ sort :{ '_id' : -1}}).exec(function(err, docs){
         if(err){
            console.log("if err ----->", err);
            logger.log.info(err);
            return res.send({
               statusCode: 500,
               message: "something went wrong"
            })
         }
         else{
            var smemply = [];
            async.each(docs, function(doc, callback){
               if(doc == null){
                  callback();
               }
               else{
                  var emdetail = {};
                  emdetail._id = doc.id;
                  emdetail.name = doc.User_Fname;
                  emdetail.position = doc.User_Profile;
                  emdetail.collegeName = doc.User_College;
                  emdetail.email_address = doc.User_Email;
                  emdetail.contact_number = doc.User_Contact;
                  emdetail.profile_status = doc.Profile_Status;
                  emdetail.create_date = doc.User_Create;
                  emdetail.update_date = doc.User_Update;
                  smemply.push(emdetail);
                  callback();
               }
            }, function(err){
               if(err) throw err;
               if(smemply == ''){
                  return res.send({
                     statusCode: 400,
                     message: "empty data found"
                  })
               }
               if(smemply == ''){
                  return res.send({
                     statusCode: 400,
                     message: "User not found"
                  })
               }
               else{
                  SmEmployee.count({}, function(err, cot){
                     if(err) throw err;
                        smemply.sort(function (a, b) {
                          return b.update_date - a.update_date;
                        });            
                     console.log("get all_emaployee_descending");
                     return res.send({
                        statusCode: 200,
                        message: "successfully get all registerd user",
                        data: smemply,
                        count: cot
                     })
                  })
               }     
            })
         }
      })
   }
   else{
      // var query = {Store_status: true, $and: [ { $or : [ {Contact_No_1:{ "$where": "function() { return this.Contact_No_1.toString().match(/"+req.body.query+"/) != null; }" }},  {Pharmacy_name: { $regex: '^' + req.body.query, $options:"i" }}, { DLN: { $regex: '^' + req.body.query, $options:"i" }}, { GST_Number: { $regex: '^' + req.body.query, $options:"i" }}] }]};
      var query = { $and: [ { $or : [ {User_Fname: { $regex: '^' + req.body.query, $options:"i" }}, {User_Lname: { $regex: '^' + req.body.query, $options:"i" }}, {User_Email: { $regex: '^' + req.body.query, $options:"i" }}, { User_Contact: { $regex: '^' + req.body.query, $options:"i" }}, { User_College: { $regex: '^' + req.body.query, $options:"i" }}] }]};

      SmEmployee.find(query).exec(function(err, docs){
         if(err) throw err;
         if(docs == ''){
            return res.send({
               statusCode: 400,
               message:"data not found.",
               data: docs
            })            
         }
         else{
            var data = [];
            async.each(docs, function(doc, callback){
               if(doc == null){
                  callback();
               }
               else{
                  var emdetail = {};
                  emdetail._id = doc.id;
                  emdetail.name = doc.User_Fname;
                  emdetail.position = doc.User_Profile;
                  emdetail.collegeName = doc.User_College;
                  emdetail.email_address = doc.User_Email;
                  emdetail.contact_number = doc.User_Contact;
                  emdetail.profile_status = doc.Profile_Status;
                  emdetail.create_date = doc.User_Create;
                  emdetail.update_date = doc.User_Update;
                  data.push(emdetail);
                  callback();
               }
            }, function(err){
               if(err) throw err;
               if(data == '' || data == null){
                  return res.send({
                     statusCode: 400,
                     message: "User not found",
                     data: data
                  })
               }
               else{
                     // smemply.sort(function (a, b) {
                     //   return a.update_date - b.update_date;
                     // });            
                  console.log("get all_emaployee_descending");
                  return res.send({
                     statusCode: 200,
                     message: "successfully get all registerd user",
                     data: data
                  })
               }     
            })            
         }
      })
   }   
}

// {_id: 1, Pharmacy_name:1, State:1, City:1, Pin_code:1, Contact_No_1:1, DLN: 1, GST_Number: 1 },
exports.getAllRetailer = function(req, res){
   if(typeof req.body.query == 'undefined'){
      Store.find({Store_status: true}, function(err, docs){
         if(err){
            console.log("if err------>", err);
            logger.log.info(err);
            return res.send({
               statusCode: 500,
               message: "Something went wrong"
            })
         }
         else{
            var data = [];
            async.each(docs, function(doc, callback){
               if(doc == null){
                  callback();
               }
               else{
                  var obj = {};   
                  state.findOne({state_id: doc.State}, function(err, state_n){
                     city.findOne({city_id: doc.City}, function(err, city_n){
                        customer.count({Store_Id: doc.id}, function(err, cot){
                           obj.state = state_n.state;
                           obj.city = city_n.city_name;                        
                           obj.storeName = doc.Pharmacy_name;
                           obj.drugLicenceNumber = doc.DLN;
                           obj.gstNumber = doc.GST_Number;
                           obj.pinCode = doc.Pin_code;
                           obj.createdTime = doc.CreateTime;
                           obj.Landmark = doc.Landmark;
                           obj.Address = doc.Address;
                           obj.customerCount = cot;
                           CreateAcount.findOne({Store_Id: doc._id}, function(err, crtac){
                              if(err) throw err;
                              if(crtac == null){
                                 callback();
                              }
                              else{
                                 AddVacancy.count({AcountId: crtac.id}, function(err, vacCount){
                                    if(err) throw err;
                                    obj.account_id = crtac.id;
                                    obj.phoneNumber = crtac.Phone_Number;
                                    obj.retailer_id = crtac._id;
                                    obj.is_active = crtac.is_active;
                                    obj.totalJobPost = vacCount;
                                    data.push(obj);
                                    callback();
                                 })                        
                              }
                           })
                        })
                     })
                  }) 
               }
            },
            function(err){
               if(err) throw err;
               data.sort(function (a, b) {
                 return b.createdTime - a.createdTime;
               });            
               return res.send({
                  statusCode: 200,
                  message: "Get all store detail successfully",
                  data: data
               })
            })
         }
      })
   }
   else{
      // var query = {Store_status: true, $and: [ { $or : [ {Contact_No_1:{ "$where": "function() { return this.Contact_No_1.toString().match(/"+req.body.query+"/) != null; }" }},  {Pharmacy_name: { $regex: '^' + req.body.query, $options:"i" }}, { DLN: { $regex: '^' + req.body.query, $options:"i" }}, { GST_Number: { $regex: '^' + req.body.query, $options:"i" }}] }]};
      //{ Contact_No_1: { $regex: '^' + req.body.query, $options:"i" }}
      //{Contact_No_1:{ "$where": "function() { return this.Contact_No_1.toString().match(/"+req.body.query+"/) != null; }" }}, 
     // query["$where"] = "function() { return this.Contact_No_1.toString().match(/"+req.body.query+"/) != null; }";
      var query = {Store_status: true, $and: [ { $or : [ {Pharmacy_name: { $regex: '^' + req.body.query, $options:"i" }}, {Contact_No_1: { $regex: '^' + req.body.query, $options:"i" }}, { DLN: { $regex: '^' + req.body.query, $options:"i" }}, { GST_Number: { $regex: '^' + req.body.query, $options:"i" }}] }]};

      Store.find(query).exec(function(err, docs){
         if(err) throw err;
         if(docs == ''){
            return res.send({
               statusCode: 400,
               message:"data not found.",
               // data: data
            })            
         }
         else{
            var data = [];
            async.each(docs, function(doc, callback){
               if(doc == null){
                  callback();
               }
               else{
                  var obj = {};   
                  state.findOne({state_id: doc.State}, function(err, state_n){
                     city.findOne({city_id: doc.City}, function(err, city_n){
                        console.log("doc.id+++++++> ", doc.id);
                           customer.count({Store_Id: doc.id}, function(err, cot){
                           obj.state = state_n.state;
                           obj.city = city_n.city_name;                        
                           obj.storeName = doc.Pharmacy_name;
                           obj.drugLicenceNumber = doc.DLN;
                           obj.gstNumber = doc.GST_Number;
                           obj.pinCode = doc.Pin_code;
                           obj.createdTime = doc.CreateTime;
                           obj.Landmark = doc.Landmark;
                           obj.Address = doc.Address;
                           obj.customerCount = cot;
                           CreateAcount.findOne({Store_Id: doc._id}, function(err, crtac){
                              if(err) throw err;
                              if(crtac == null){
                                 callback();
                              }
                              else{
                                    AddVacancy.count({AcountId: crtac.id}, function(err, vacCount){
                                    obj.phoneNumber = crtac.Phone_Number;
                                    obj.retailer_id = crtac._id;
                                    obj.is_active = crtac.is_active;
                                    obj.totalJobPost = vacCount;
                                    data.push(obj);
                                    callback(); 
                                 })                       
                              }
                           })
                        })
                     })
                  }) 
               }
            },
            function(err){
               if(err) throw err;
               data.sort(function (a, b) {
                 return b.createdTime - a.createdTime;
               });            
               return res.send({
                  statusCode: 200,
                  message: "Get all store detail successfully",
                  data: data
               })
            })            
         }
         // return res.send({
         //    statusCode: 200,
         //    message: "get detail suspendRetailer",
         //    data: doc
         // })
      })
   }
}

exports.getOneRetailerInformations = function(req, res){
   if(typeof req.body.retailer_id == 'undefined'){
      console.log('retailer_id is not provided');
      return res.send({
         statusCode: 400,
         message: "retailer_id is not provided"
      })
   }
   else{
      CreateAcount.findById({_id: req.body.retailer_id}, function(err, doc){
         if(err) throw err;
         var storeDetail = {};
         storeDetail.is_active = doc.is_active;  
         Store.findById({_id: doc.Store_Id}, function(err, sto){
            if(err) throw err;
            var obj = {}; 
            // obj.is_active = doc.is_active 
            obj.no_of_views = doc.no_of_jprofile_views;
            obj.remaining_view = doc.remaining_view;
            obj.phoneNumber = sto.Contact_No_1;
            if(sto.Contact_No_2 == null){
               obj.landlineNumber = ''
            }
            else{
               obj.landlineNumber = sto.Contact_No_2;
            }                   
            obj.Pharmacy_name = sto.Pharmacy_name;
            obj.DLN = sto.DLN;
            obj.GST_Number = sto.GST_Number;
            obj.Pin_code = sto.Pin_code;
            obj.CreateTime = sto.CreateTime;
            obj.Address = sto.Address;
            obj.Landmark = sto.Landmark
            obj.Area = sto.Area;
            obj.latitude = sto.latitude;
            obj.longitude = sto.longitude;
            obj.Premise = sto.Premise;
            obj.profile_image = sto.profile_image;
            obj.Store_status = sto.Store_status;            
            state.findOne({state_id: sto.State}, function(err, state_n){
               city.findOne({city_id: sto.City}, function(err, city_n){ 
                  obj.State = state_n.state;
                  obj.City = city_n.city_name;                          
                  storeDetail.storeInfo = obj;
                  OwnerInfo.findById({_id: doc.OwnerInfo_Id}, function(err, own){
                     if(err) throw err;
                     storeDetail.ownerInfo = own;
                     OperationalInfo.findById({_id: doc.OperationalInfo_Id}, function(err, opr){
                        if(err) throw err;
                        storeDetail.operationalInfo = opr;
                        rejectedRetailer.find({retailer_id: req.body.retailer_id},{},{ sort :{ create_time : -1}}, function(err, rrt){
                           storeDetail.rejectedRetailer = rrt;
                           suspendRetailer.find({retailer_id: req.body.retailer_id},{},{ sort :{ create_time : -1}}, function(err, srt){
                              storeDetail.suspendRetailer = srt;
                              return res.send({
                                 statusCode: 200,
                                 message: "successfully get all infomation of store",
                                 data: storeDetail
                              })                              
                           })
                        })
                     })
                  })
               })
            })      
         })
      })
   }
}

exports.activatedRetailer = function(req, res){
   if(typeof req.body.retailer_id == "undefined"){
      console.log("retailer_id is not provided");
       return res.send({
         statusCode: 400,
         message: "retaier_id is not provided"
       })
   }
   else{
      CreateAcount.findById({_id: req.body.retailer_id}, function(err, doc){
         if(err) throw err;
         doc.is_active = '1';
         doc.save(function(err){
            if(err) throw err;
            var templateData = {};
            templateData.accountAprove =" You account has been approved and activated";
             var requestObj = {
               templateName : "welcome-pharmacy",
               to : doc.Email_Id,
               subject : 'Welcome to Order Medicine - "The Med App"',
               templateData: templateData,
               senderEmail : config.email_m,
               senderPassword: config.password_m,
               fromJob : "OrderMedicine"
             }
              node_mailer.nodeMailer(requestObj,function(err,data){
               if(err){console.log("====ERROR====",err); logger.log.info(err);}
               console.log("====DATA====",data);
             });              
            return res.send({
               statusCode: 200,
               message: "Retailer activated successfully"
            })
         })
      })
   }
}

// exports.rejectRetailer = function(req, res){
//    if(typeof req.body.retailer_id == 'undefined'
//       || typeof req.body.reject_reson == 'undefined')
//    {
//       console.log("retailer_id or reject_reson is not provided");
//       return res.send({
//          statusCode: 400,
//          message: "retailer_id or reject_reson is not provided"
//       })
//    }
//    else{
//       CreateAcount.findById({_id: req.body.retailer_id}, function(err, doc){
//          if(err) throw err;
//          doc.reject_reson = req.body.reject_reson;
//          doc.is_active = false;
//          doc.save(function(err){
//             if(err) throw err;
//              return res.send({
//                statusCode: 200,
//                message: "Retailer reject successfully"
//              })
//          })
//       })
//    }
// }

exports.createPackageJob = function(req, res){
   if(typeof req.body.name_of_package == 'undefined'
      || typeof req.body.price_of_package == 'undefined'
      || typeof req.body.number_of_view == 'undefined'
      )
   {
      console.log("name_of_package or price_of_package");
       return res.send({
         statusCode: 400,
         message: "name_of_package or price_of_package"
       })
   }
   else{
      packagejob.findOne({name_of_package: req.body.name_of_package}, function(err, doc){
         if(doc == null){
            var packages = new packagejob();
            packages.name_of_package = req.body.name_of_package;
            packages.price_of_package = req.body.price_of_package;
            packages.number_of_view = req.body.number_of_view;
            packages.save(function(err){
               if(err){
                  console.log("if err ----->", err);
                  logger.log.info(err);
                  return res.send({
                     statusCode: 500,
                     message: "something went wrong"
                  })
               }
               else{
                  console.log("inserted successfully");
                  return res.send({
                     statusCode: 200,
                     message: "New subcription package added successfully.",
                     data: packages
                  });
               }
            })
         }
         else{
            console.log("package name is already used");
            return res.send({
               statusCode: 400,
               message: "This package name is already used."
            })
         }
      }) 
   }
} 


exports.getAllPackages = function(req, res){
   packagejob.find({package_status: 1},{},{sort: {package_updated: -1}}, function(err, doc){
      if(err){
         console.log("if err ----->", err);
         logger.log.info(err);
         return res.send({
            statusCode: 500,
            message: "something went wrong"
         })
      }
      if(doc == null){
         return res.send({
            statusCode: 400,
            message: "error"
         })
      }
      else{
         return res.send({
            statusCode: 200,
            message: "packege List found.",
            data: doc
         })
      }
   })
}


exports.getPackage = function(req, res){
   if(typeof req.body.packagejob_id == 'undefined')
   {
      console.log("packagejob_id is not provided");
      return res.send({
         statusCode: 400,
         message:"packagejob_id is not provided"
      })
   }
   else{
      packagejob.findById({_id: req.body.packagejob_id}, function(err, pkj){
         if(err){
            console.log("if err ----->", err);
            logger.log.info(err);
            return res.send({
               statusCode: 500,
               message: "something went wrong"
            })
         }
         return res.send({
            statusCode: 200,
            message: "get package successfully",
            data: pkj
         })       
      })
   }
}


exports.updatePackageJob = function(req, res){
   if(typeof req.body.packagejob_id == 'undefined'
      || typeof req.body.name_of_package == 'undefined'
      || typeof req.body.price_of_package == 'undefined'
      || typeof req.body.number_of_view == 'undefined'
      )
   {
      console.log("packagejob_id or AcountId is not provided");
      return res.send({
         statusCode: 400,
         message:"packagejob_id or AcountId is not provided"
      })
   }
   else{
      packagejob.findOne({name_of_package: req.body.name_of_package}, function(err, docs){
         if(docs == null){
            packagejob.findById({_id: req.body.packagejob_id}, function(err, pkj){
               if(err) throw err;
               pkj.name_of_package = req.body.name_of_package;
               pkj.price_of_package = req.body.price_of_package;
               pkj.number_of_view = req.body.number_of_view;
               pkj.package_updated = Date.now();
               pkj.save(function(err){
                  if(err) throw err;
                  console.log("inserted successfully");
                  return res.send({
                     statusCode: 200,
                     message: "inserted successfully",
                     data: pkj
                  });
               })                      
            })            
         }
         else{
            // async.each(docs, function(doc, callback){
            //    if(doc == null){
            //       callback();
            //    }
               if(docs._id == req.body.packagejob_id){
                  docs.name_of_package = req.body.name_of_package;
                  docs.price_of_package = req.body.price_of_package;
                  docs.number_of_view = req.body.number_of_view;
                  docs.package_updated = Date.now();
                  docs.save(function(err){
                     if(err) throw err;
                     // callback();
                     return res.send({
                        statusCode: 200,
                        message: "package update successfully."
                     })
                  })
               }
               else{
                  return res.send({
                     statusCode: 400,
                     message: "Package name is already used."
                  })                  
               }
            //    else{
            //       callback();
            //    }
            // },function(err){
            //    if(err) throw err;
            //    console.log("package update");
            //    return res.send({
            //       statusCode: 200,
            //       message: "This package update"
            //    })               
            // })
            // console.log("package name is already used");
            // return res.send({
            //    statusCode: 400,
            //    message: "This package name is already used"
            // })
         }
      })      
   }
}


exports.deletePackage = function(req, res){
   if(typeof req.body.packagejob_id == 'undefined')
   {
      console.log("packagejob_id is not provided");
      return res.send({
         statusCode: 400,
         message:"packagejob_id is not provided"
      })
   }
   else{
      packagejob.findById({_id: req.body.packagejob_id}, function(err, doc){
         if(err) throw err;
         doc.package_status = '0';
         doc.save(function(err){
            if(err) throw err;
            return res.send({
               statusCode: 200,
               message: "Delete package successfully"
            })             
         })      
      })
   }
}

exports.createRejectRetailer = function(req, res){
   if(typeof req.body.retailer_id == 'undefined'
      || typeof req.body.reject_reason == 'undefined')
   {
      console.log("retailer_id and rejecte_reason is not provided");
      return res.send({
         statusCode: 400,
         message:"all details is not provided"
      })
   }
   else{
      CreateAcount.findById({_id: req.body.retailer_id}, function(err, cta){
         if(err) throw err;
         var retaileEmail = cta.Email_Id;
         cta.is_active = '2';
         cta.save(function(err){
            if(err) throw err;
            var reject_Retailer = new rejectedRetailer();
            reject_Retailer.retailer_id = req.body.retailer_id;
            reject_Retailer.reject_reason = req.body.reject_reason;
            reject_Retailer.save(function(err){
               if(err) throw err;
               console.log("Retailer rejected successfully");
               var templateData = {};
               templateData.reasonName = "reject";
               templateData.reason = req.body.reject_reason;
                      var requestObj = {
                        templateName : "retailerRejectOrSuspend",
                        to : retaileEmail,
                        subject : 'Reject Retailer -"OrderMedicine"',
                        templateData : templateData,
                        senderEmail : config.email_m,
                        senderPassword: config.password_m,
                        fromJob : "OrderMedicine"
                      }
                       node_mailer.nodeMailer(requestObj,function(err,data){
                        if(err){console.log("====ERROR====",err); logger.log.info(err);}
                        console.log("====DATA====",data);
                      });               
               return res.send({
                  statusCode: 200,
                  message: "Retailer rejected successfully.",
                  data: reject_Retailer
               })            
            })            
         })
      })     
   }
}


exports.createSuspendRetailer = function(req, res){
   if(typeof req.body.retailer_id == 'undefined'
      || typeof req.body.suspend_reason == 'undefined')
   {
      console.log("retailer_id and suspend_reason is not provided");
      return res.send({
         statusCode: 400,
         message:"all details is not provided"
      })
   }
   else{
      CreateAcount.findById({_id: req.body.retailer_id}, function(err, cta){
         if(err) throw err;
         var retaileEmail = cta.Email_Id;
         cta.is_active = '3',
         cta.save(function(err){
            if(err) throw err;
            var suspend_Retailer = new suspendRetailer();
            suspend_Retailer.retailer_id = req.body.retailer_id;
            suspend_Retailer.suspend_reason = req.body.suspend_reason;
            suspend_Retailer.save(function(err){
               if(err) throw err;
               console.log("Retailer suspend successfully");
               var templateData = {};
               templateData.reasonName = "suspend";
               templateData.reason = req.body.suspend_reason;
                      var requestObj = {
                        templateName : "retailerRejectOrSuspend",
                        to : retaileEmail,
                        subject : 'Suspend Retailer-"OrderMedicine"',
                        templateData : templateData,
                        senderEmail : config.email_m,
                        senderPassword: config.password_m,
                        fromJob : "OrderMedicine"
                      }
                       node_mailer.nodeMailer(requestObj,function(err,data){
                        if(err){console.log("====ERROR====",err); logger.log.info(err);}
                        console.log("====DATA====",data);
                      });                
               return res.send({
                  statusCode: 200,
                  message: "Retailer suspend successfully.",
                  data: suspend_Retailer
               })            
            })            
         })
      })           
   }
}


exports.get_employee_by_employee_id = function(req, res){
   if(typeof req.query.employee_id == 'undefined')
   {
      console.log("employee_id is not provided");
      return res.send({
         statusCode: 400,
         message: "employee_id is not provided"
      })
   }
   else{
      SmEmployee.findById({_id: req.query.employee_id}, function(err, docs){
         if(err) throw err;
         if(docs == null)
         {
            return res.send({
               statusCode: 400,
               message: "employee id is not exists"
            })
         }
         else{
            // if(doc)
            var emp_info = {};
            // var obj = {};
            suspend.find({employee_id: req.query.employee_id}, function(err, sus){
               if(err) throw err;
               emp_info.suspend = sus;
               rejection.find({employee_id: req.query.employee_id}, function(err, rej){
                  if(err) throw err;
                  emp_info.reject = rej;
                     // emp_info.employee = doc
                  var state_name;
                  var city_name;
                  state.findOne({state_id: docs.User_State}, function(err, state_n){
                     if(state_n == null){ state_name = "" }
                        else{
                           state_name = state_n.state;
                        }
                     city.findOne({city_id: docs.User_City}, function(err, city_n){
                     if(city_n == null){ city_name = "" }
                        else{
                           city_name = city_n.city_name;
                        }                     
                        var obj = {};                  
                        obj.is_email_valid = docs.is_email_valid;
                        obj.is_contact_valid = docs.is_contact_valid;
                        obj.User_Experience = docs.User_Experience;
                        obj.Software_Data = docs.Software_Data;
                        obj.Profile_Status = docs.Profile_Status;
                        obj.Emp_Status = docs.Emp_Status;
                        obj._id = docs._id;
                        obj.User_Fname = docs.User_Fname;
                        obj.User_Lname = docs.User_Lname;
                        obj.User_Dob = docs.User_Dob;
                        obj.User_Gender = docs.User_Gender;
                        obj.User_Email = docs.User_Email;
                        obj.User_Pwd = docs.User_Pwd;
                        obj.User_Contact = docs.User_Contact;
                        obj.User_Image = docs.User_Image;
                        obj.User_Address1 =docs.User_Address1;
                        obj.User_Address2 = docs.User_Address2;
                        obj.User_Aadhar_No = docs.User_Aadhar_No;
                        obj.User_State = state_name;
                        obj.User_City = city_name;
                        obj.User_PIncode = docs.User_PIncode;
                        obj.User_Education = docs.User_Education;
                        obj.Certi_Image = docs.Certi_Image;
                        obj.User_College = docs.User_College;
                        obj.User_University = docs.User_University;
                        obj.User_Passing_Year = docs.User_Passing_Year;
                        obj.User_Percentage = docs.User_Percentage;
                        obj.User_Experience_Type = docs.User_Experience_Type;
                        obj.Fresher_Expected_Ctc = docs.Fresher_Expected_Ctc;
                        obj.Current_Ctc = docs.Current_Ctc;
                        obj.Expected_Ctc = docs.Expected_Ctc;
                        obj.Notice_Period = docs.Notice_Period;
                        obj.Computer_Knowledge = docs.Computer_Knowledge;
                        obj.Billing_Software = docs.Billing_Software;
                        obj.User_Create = docs.User_Create;
                        obj.User_Profile = docs.User_Profile;
                        obj.User_Update = docs.User_Update;
                        obj.total_experience = docs.total_experience;
                        obj.view_status = docs.view_status; 
                        // console.log(doc);
                        emp_info.emp = obj;
                        return res.send({
                           statusCode: 200,
                           message: "successfully get employee detail",
                           employee: emp_info
                        })                       
                     })
                  }) 
               })
            })
         }
      })
   }
}


// exports.get_employee_by_employee_id = function(req, res){
//    if(typeof req.query.employee_id == 'undefined')
//    {
//       console.log("employee_id is not provided");
//       return res.send({
//          statusCode: 400,
//          message: "employee_id is not provided"
//       })
//    }
//    else{
//       SmEmployee.findById({_id: req.query.employee_id}, function(err, docss){
//          if(err) throw err;
//          if(docss == null)
//          {
//             return res.send({
//                statusCode: 400,
//                message: "employee id is not exists",
//                employee: doc
//             })
//          }
//          else{ var obj = {};
//             async.each(docss, function(docs, callback){
//                if(docs == null){
//                   callback();
//                }
//                else{
//                   if(docs.User_State == '' || docs.User_City == ''){
//                      callback();
//                   }
//                   else{
                    
//                      state.findOne({state_id: docs.State}, function(err, state_n){
//                         console.log("state_n----->",state_n)
//                         city.findOne({city_id: docs.City}, function(err, city_n){ 
//                            obj.is_email_valid = docs.is_email_valid;
//                            obj.is_contact_valid = docs.is_contact_valid;
//                            obj.User_Experience = docs.User_Experience;
//                            obj.Software_Data = docs.Software_Data;
//                            obj.Profile_Status = docs.Profile_Status;
//                            obj.Emp_Status = docs.Emp_Status;
//                            obj._id = docs._id;
//                            obj.User_Fname = docs.User_Fname;
//                            obj.User_Lname = docs.User_Lname;
//                            obj.User_Dob = docs.User_Dob;
//                            obj.User_Gender = docs.User_Gender;
//                            obj.User_Email = docs.User_Email;
//                            obj.User_Pwd = docs.User_Pwd;
//                            obj.User_Contact = docs.User_Contact;
//                            obj.User_Image = docs.User_Image;
//                            obj.User_Address1 =docs.User_Address1;
//                            obj.User_Address2 = docs.User_Address2;
//                            obj.User_Aadhar_No = docs.User_Aadhar_No;
//                            obj.User_State = state_n.state;
//                            obj.User_City = city_n.city_name;
//                            obj.User_PIncode = docs.User_PIncode;
//                            obj.User_Education = docs.User_Education;
//                            obj.Certi_Image = docs.Certi_Image;
//                            obj.User_College = docs.User_College;
//                            obj.User_University = docs.User_University;
//                            obj.User_Passing_Year = docs.User_Passing_Year;
//                            obj.User_Percentage = docs.User_Percentage;
//                            obj.User_Experience_Type = docs.User_Experience_Type;
//                            obj.Fresher_Expected_Ctc = docs.Fresher_Expected_Ctc;
//                            obj.Current_Ctc = docs.Current_Ctc;
//                            obj.Expected_Ctc = docs.Expected_Ctc;
//                            obj.Notice_Period = docs.Notice_Period;
//                            obj.Computer_Knowledge = docs.Computer_Knowledge;
//                            obj.Billing_Software = docs.Billing_Software;
//                            obj.total_experience = docs.total_experience;
//                            obj.User_Create = docs.User_Create;
//                            obj.User_Profile = docs.User_Profile;
//                            obj.User_Update = docs.User_Update; 
//                            obj.view_status = docs.view_status;
//                            callback(); 

//                         })
//                      })                      

//                   }
//                }
//             }, function(err){
//                if(err) throw err;
//                return res.send({
//                   statusCode: 200,
//                   message: "successfully get employee detail",
//                   employee: obj
//                })               
//             })          
//             // console.log(doc);
//          }
//       })
//    }
// }

exports.superAdminDashbord = function(req, res){
   CreateAcount.count({}, function(err, storecount){
      if(err){
         return res.send({
            statusCode: 500,
            message: "Something went wrong"
         })         
      }
      else{
         SmEmployee.count({}, function(err, empcount){
            if(err){
               logger.log.info(err);
               return res.send({
                  statusCode: 500,
                  message: "Something went wrong"
               })         
            }
            else{
               customer_order.count({}, function(err, orderCounts){
                  if(err){
                     logger.log.info(err);
                     return res.send({
                        statusCode: 500,
                        message: "Something went wrong."
                     })
                  }                  
                  else{
                     customer.count({}, function(err, customerCount){
                        if(err){
                           logger.log.info(err);
                           return res.send({
                              statusCode: 500,
                              message: "Something went wrong."
                           })
                        }
                        else{
                           return res.send({
                              statusCode: 200,
                              message: "Get total count of stores, employee and orders.",
                              totalStore: storecount,
                              totalEmployee: empcount,
                              totalOrder: orderCounts,
                              totalCustomer: customerCount
                           })                           
                        }                         
                     })                     
                  }
               })
            }            
         })
      }
   })
}

exports.rejectionEmployee = function(req, res){
   if(typeof req.body.employee_id == 'undefined'
      || typeof req.body.reject_reason == 'undefined'
      )
   {
      console.log('employee_id or reject_reason is not provided');
      return res.send({
         statusCode: 400,
         message: 'employee_id or reject_reason is not provided'
      })
   }
   else
   {
      console.log("enter the else condition");
      var emp_rejection = new rejection();
      emp_rejection.employee_id = req.body.employee_id;
      emp_rejection.reject_reason = req.body.reject_reason;
      emp_rejection.save(function(err){
         if(err) throw err;
         var employee_email;
         SmEmployee.findById({_id: req.body.employee_id}, function(err, doc){
            if(err) throw err;
            employee_email =doc.User_Email;
            doc.Profile_Status = '2';
            doc.save(function(err){
               if(err) throw err;
               var templateData = {};
               templateData.reasonName = "rejected";
               templateData.reason = req.body.reject_reason;
                      var requestObj = {
                        templateName : "empSuspendOrReject",
                        to : employee_email,
                        subject : '"Account Rejected" - Search4PharmacyJobs.', //'Reject -"Search4PharamcyJobs"',
                        templateData : templateData,
                        senderEmail : config.email,
                        senderPassword: config.password,
                        fromJob : "Search4PharamcyJobs"
                      }
                       node_mailer.nodeMailer(requestObj,function(err,data){
                        if(err){console.log("====ERROR====",err); logger.log.info(err);}
                        console.log("====DATA====",data);
                      });  
               return res.send({
                  statusCode: 200,
                  message: "Successfully Send Reject Reason To Registered User"
               })                        
            })
         })
      })
   }
}

exports.suspendEmployee = function(req, res){
   if(typeof req.body.employee_id == 'undefined'
      || typeof req.body.suspend_reason == 'undefined'
      )
   {
      console.log('employee_id or suspend_reason is not provided');
      return res.send({
         statusCode: 400,
         message: 'employee_id or suspend_reason is not provided'
      })
   }
   else
   {
      var emp_suspend = new suspend();
      emp_suspend.employee_id = req.body.employee_id;
      emp_suspend.suspend_reason = req.body.suspend_reason;
      emp_suspend.save(function(err){
         if(err) throw err;
         var employee_email;
         SmEmployee.findById({_id: req.body.employee_id}, function(err, doc){
            if(err) throw err;
            employee_email =doc.User_Email;
            doc.Profile_Status = '3';
            doc.save(function(err){
               if(err) throw err;
               var templateData = {};
               templateData.reasonName = "Suspend";
               templateData.reason = req.body.suspend_reason;
                      var requestObj = {
                        templateName : "empSuspendOrReject",
                        to : employee_email,
                        subject : 'Suspend -"Search4PharamcyJobs"',
                        templateData : templateData,
                        senderEmail : config.email,
                        senderPassword: config.password,
                        fromJob : "Search4PharamcyJobs"
                      }
                       node_mailer.nodeMailer(requestObj,function(err,data){
                        if(err){console.log("====ERROR====",err); logger.log.info(err);}
                        console.log("====DATA====",data);
                      });
               console.log("successfully send suspend reason");
               return res.send({
                  statusCode: 200,
                  message: "Successfully Send Suspend Reason To Registered User"
               })
            })
         })
      })
   }
}

exports.activationEmployee = function(req, res){
   if(typeof req.body.employee_id == 'undefined')
   {
      console.log("employee_id is not provided");
      return res.send({
         statusCode: 400,
         message: "employee_id is not provided"
      })
   }
   else{
      SmEmployee.findById({_id: req.body.employee_id}, function(err, doc){
         if(doc == null)
         {
            console.log("this type of employee id not exists");
            return res.send({
               stataus: 400,
               message: "this type of employee id not exists"
            })
         }
         else{
            console.log("Successfully update statusCode 4");
            doc.Profile_Status = '4';
            doc.save(function(err){
               if(err) throw err;
               console.log("Successfully update statusCode 4");
               return res.send({
                  statusCode: 200,
                  message: "Employee Account Successfully Activated"
               })
            })
         }
      })
   }
}


exports.getRetailersPackageHistory = function(req, res){
   retailerPackagehistory.find({retailer_package_status: '1'}, function(err, docs){
      if(err) throw err;
      var data = [];
      async.each(docs, function(doc, callback){
         if(doc == null){
            callback();
         }
         else{
            var obj = {};
            CreateAcount.findById({_id: doc.account_id}, function(err, cat){
               if(err) throw err;
               obj.no_of_jprofile_views = cat.no_of_jprofile_views;
               obj.remaining_view = cat.remaining_view;
               packagejob.findById({_id: doc.package_id}, function(err, pak){
                  if(err) throw err;
                  obj.rph_id = doc.id;
                  obj.is_aprove = doc.is_aprove;
                  obj.packageName = pak.name_of_package;
                  obj.packagePrice = pak.price_of_package;
                  obj.date = doc.create_date;
                  CreateAcount.findById({_id: doc.account_id}, function(err, crt){
                     if(err) throw err;
                     Store.findById({_id: crt.Store_Id}, function(err, sto){
                        if(err) throw err;
                       // obj.rph = doc;
                        state.findOne({state_id: sto.State}, function(err, state_n){
                           city.findOne({city_id: sto.City}, function(err, city_n){
                              obj.storeName = sto.Pharmacy_name;
                              obj.emailId = crt.Email_Id;
                              obj.phoneNumber = crt.Phone_Number;
                              //obj.phoneNumber = sto.phone_number;
                              obj.area = sto.Area;
                              obj.city = city_n.city_name;
                              obj.state = state_n.state;
                              obj.pinCode = sto.Pin_code;
                              data.push(obj);
                              callback();                           
                           })
                        })
                     })
                  })
               })//
            })
         }
      }, function(err){
         if(err) throw err;
            data.sort(function (a, b) {
              return b.date - a.date;
            });         
         return res.send({
            statusCode: 200,
            message: "Get Retailer Package history successfully.",
            data: data
         })
      })
   })
}

exports.addRetailerPackage = function(req, res){
   if(typeof req.body.retailer_id == 'undefined'
      || typeof req.body.package_id == 'undefined'
      )
   {
      console.log("Account_id or package_id is not provided");
      return res.send({
         statusCode: 400,
         message: "Account_id or package_id is not provided"
      })
   }
   else{
      var adminpackage = new retailerPackagehistory();
      adminpackage.account_id = req.body.retailer_id;
      adminpackage.package_id = req.body.package_id;
      adminpackage.save(function(err){
         if(err){
            console.log("if err ----->", err);
            logger.log.info(err);
            return res.send({
               statusCode: 500,
               message: "something went wrong"
            })
         }
         else{
            return res.send({
               statusCode: 200,
               message: "Package added successfully."
            })
         }
      })
   }
}

exports.acceptPackage = function(req, res){
   if(typeof req.body.rph_id == 'undefined')
   {
      console.log("rph_id is not provided");
      return res.send({
         statusCode: 400,
         message: "rph_id is not provided"
      })
   }
   else{
      console.log("tesing--------.");
      retailerPackagehistory.findById({_id: req.body.rph_id}, function(err, doc){
         if(err) throw err;
         doc.is_aprove = '1';
         doc.save(function(err){
            if(err) throw err;
            packagejob.findById({_id: doc.package_id}, function(err, pkj){
               if(err) throw err;
               CreateAcount.findById({_id: doc.account_id}, function(err, crt){
                  if(err) throw err;
                  var totalView;
                  var totalremaining_view;
                  var jprofilen = Number(pkj.number_of_view);
                  // console.log("jprofilen", jprofilen);
                  // console.log("crt.no_of_jprofile_views", crt.no_of_jprofile_views);
                  totalView = sum(jprofilen, crt.no_of_jprofile_views);
                  totalremaining_view = sum(jprofilen, crt.remaining_view);
                  crt.no_of_jprofile_views = totalView;
                  crt.remaining_view = totalremaining_view;
                  crt.save(function(err){
                     if(err) throw err;
                     return res.send({
                        statusCode: 200,
                        message: "Accept package successfully.",
                        data: crt
                     })
                  })
               })
            })            
         })
      })
   }
}

exports.getOrdersOfOrderMedicine = function(req, res){
   customer_order.find({store_name:"Shashank Pharmacy"}, function(err, docs){
      if(err) throw err;
      var data = [];
      async.each(docs, function(doc, callback){
         if(err) throw err;
         var obj = {}
         var item = doc.cart_item_list;
         obj.orederId = doc._id;
         obj.cart_item_list = doc.cart_item_list;
         obj.prescription = doc.prescription;
         obj.customer_orders_status = doc.customer_orders_status;
         obj.order_price = doc.order_price;
         obj.comment = doc.comment;
         obj.order_no = doc.order_no;
         obj.itemCount = item.length;
         obj.update_date = doc.update_date;
         // obj.final_bill_amount = doc.final_bill_amount;
         customer_delivery_detail.findById({_id: doc.deliver_address_id}, function(err, cdd){
            if(err) throw err;
            obj.customerName = cdd.name;
            obj.customerMObilenumber = cdd.mobile_number;
            data.push(obj);
            callback(); 
         })   
      }, function(err){
         if(err) throw err;
         data.sort(function (a, b) {
           return b.update_date - a.update_date;
         });          
         return res.send({
            statusCod: 200,
            message: data
         })      
      })
   })
}

exports.getOrderListByOrederId = function(req, res){
   if(typeof req.body.orederId == 'undefined'){
      console.log("cutomerOrederid is not provided");
      return res.send({
         statusCode: 400,
         message: "cutomerOrederid is not provided"
      })
   }
   else{       
      async.waterfall([
         function(callback){
            var obj = {};
            customer_order.findById({_id: req.body.orederId}, function(err, doc){
               if(err) callback(err);
               obj.orderDetail = doc;
               callback(null, obj, doc);
            })       
         },
         function(obj, doc, callback){
            customer_delivery_detail.findById({_id: doc.deliver_address_id}, function(err, cdd){
               if(err) callback(err);
               obj.customerDeliverydetail = cdd;
               callback(null, obj);
            })
         }],
         function(err, result){
            if(err) throw err;
            return res.send({
            statusCode: 200,
            message: "Get customer order detail successfully.",
            data: result
         })           
      })
   }
}


// exports.getOrderListByOrederId = function(req, res){
//    if(typeof req.body.orederId == 'undefined'){
//       console.log("cutomerOrederid is not provided");
//       return res.send({
//          statusCode: 400,
//          message: "cutomerOrederid is not provided"
//       })
//    }
//    else{
//       var obj = {};
//       customer_order.findById({_id: req.body.orederId}, function(err, doc){
//          if(err) throw err;
//          obj.orderDetail = doc;
//          console.log(doc);
//          customer_delivery_detail.findById({_id: doc.deliver_address_id}, function(err, cdd){
//             if(err) throw err;
//             obj.customerDeliverydetail = cdd;
//             return res.send({
//                statusCode: 200,
//                message: "Get customer order detail successfully.",
//                data: obj
//             })
//          })
//       })
//    }
// }

// exports.getAllordrdetail = function(req, res){
//    customer_order.find({}, function(err, docs){
//       if(err) throw err;
//       var data = [];
//       async.each(docs, function(doc, callback){
//          if(doc == null){
//             callback();
//          }
//          else{
//             var obj = {};
//             obj.orderNumber = doc.order_no;
//             obj.storeName = doc.store_name;
//             Store.findOne({_id: doc.Store_Id}, function(err, sto){
//                if(err) throw err;
//                console.log("stoDetail----->",sto);
//                if(sto.Contact_No_1 == null)
//                   {obj.storeContactNumber = ''}
//                else{obj.storeContactNumber = sto.Contact_No_1;}
//                customer.findById({_id: doc.customer_id}, function(err, cut){
//                   if(err) throw err;
//                   console.log("cutDetail----->",cut);
//                   if(cut.Phone_Number == null)
//                   {obj.customerContactNumber = ''}
//                else{obj.customerContactNumber = cut.Phone_Number;}
//                   obj.orderItemCount = doc.order_item_count;
//                   obj.order_price = doc.order_price;
//                   obj.final_bill_amount = doc.final_bill_amount;
//                   obj.update_date = doc.update_date;
//                   data.push(obj);
//                   callback();
//                })
//             })
//          }
//       }, function(err){
//          if(err) throw err;
//          return res.send({
//             statusCode: 200,
//             message: data
//          })
//       })
//    })
// }

// exports.getAllordrdetail = function(req, res){
//    customer_order.find({}, function(err, docs){
//       if(err) throw err;
//       var data = [];
//       async.each(docs, function(doc, callback){
//          if(doc == null){
//             callback();
//          }
//          else{
//             var obj = {};
//             obj.orderNumber = doc.order_no;
//             obj.storeName = doc.store_name;
//             // obj.status = doc.customer_orders_status;
//             if(doc.customer_orders_status == '1')
//                obj.Orderstatus = 'Pending'
//             else if(doc.customer_orders_status == '2')
//                obj.Orderstatus = 'Accepted'
//             else if(doc.customer_orders_status == '3')
//                obj.Orderstatus = 'Canceled'
//             else if(doc.customer_orders_status == '4')
//                obj.Orderstatus = 'Dispatched'
//             else if(doc.customer_orders_status == '5')
//                obj.Orderstatus = 'Rejected'                                                         
            
//             if(doc.final_bill_amount)
//             {
//                obj.final_bill_amount = doc.final_bill_amount;
//             }
//             else{
//                obj.final_bill_amount = '';
//             }
//             Store.findOne({_id: doc.Store_Id}, function(err, sto){
//                if(err) throw err;
//                console.log("stoDetail----->",sto);
//                if(sto.Contact_No_1 == null)
//                   {
//                      obj.storeContactNumber = '';
//                      obj.Pincode = sto.Pin_code;

//                      customer.findById({_id: doc.customer_id}, function(err, cut){
//                         if(err) throw err;
//                         console.log("cutDetail----->",cut);
//                         if(cut.phone_number == null)
//                         {
//                            obj.customerContactNumber = '';
//                            obj.orderItemCount = doc.order_item_count;
//                            obj.order_price = doc.order_price;
//                            obj.update_date = doc.update_date;
//                            obj.create_date = doc.create_date;
//                            data.push(obj);
//                            callback();                              
//                         }
//                         else{
//                            obj.customerContactNumber = cut.phone_number;
//                            obj.orderItemCount = doc.order_item_count;
//                            obj.order_price = doc.order_price;
//                            obj.update_date = doc.update_date;
//                            obj.create_date = doc.create_date;
//                            data.push(obj);
//                            callback();                           
//                         }
//                      })                     
//                   }
//                else{
//                   obj.storeContactNumber = sto.Contact_No_1;
//                   obj.Pincode = sto.Pin_code
//                   customer.findById({_id: doc.customer_id}, function(err, cut){
//                      if(err) throw err;
//                      if(cut.phone_number == null)
//                      {
//                         obj.customerContactNumber = '';
//                         obj.orderItemCount = doc.order_item_count;
//                         obj.order_price = doc.order_price;
//                         obj.update_date = doc.update_date;
//                         obj.create_date = doc.create_date;
//                         data.push(obj);
//                         callback();                              
//                      }
//                      else{
//                         obj.customerContactNumber = cut.phone_number;
//                         obj.orderItemCount = doc.order_item_count;
//                         obj.order_price = doc.order_price;
//                         obj.update_date = doc.update_date;
//                         obj.create_date = doc.create_date;
//                         data.push(obj);
//                         callback();                           
//                      }
//                   })                  
//                }
//             })
//          }
//       }, function(err){
//          if(err) throw err;
//          data.sort(function (a, b) {
//            return b.update_date - a.update_date;
//          })         
//          return res.send({
//             statusCode: 200,
//             message: "Get all orders successfully.",
//             orderData: data
//          })
//       })
//    })
// }

exports.getAllordrdetail = function(req, res){
   if(typeof req.body.query == 'undefined'){
      customer_order.find({}, function(err, docs){
         if(err) throw err;
         var data = [];
         async.each(docs, function(doc, callback){
            if(doc == null){
               callback();
            }
            else{
               var obj = {};
            if(doc.customer_orders_status == '1')
               obj.Orderstatus = 'Pending'
            else if(doc.customer_orders_status == '2')
               obj.Orderstatus = 'Accepted'
            else if(doc.customer_orders_status == '3')
               obj.Orderstatus = 'Canceled'
            else if(doc.customer_orders_status == '4')
               obj.Orderstatus = 'Dispatched'
            else if(doc.customer_orders_status == '5')
               obj.Orderstatus = 'Rejected'               
               if(doc.final_bill_amount)
               {
                  obj.final_bill_amount = doc.final_bill_amount;
                  obj.orderNumber = doc.order_no;
                  obj.storeName = doc.store_name;
                  obj.storeContactNumber = doc.storeContactNumber;                                 
                  obj.customerContactNumber = doc.customerContactNumber;
                  obj.orderItemCount = doc.order_item_count;
                  obj.order_price = doc.order_price;
                  obj.update_date = doc.update_date;
                  obj.create_date = doc.create_date;
                  data.push(obj);
                  callback();

               }
               else{
                  obj.final_bill_amount = '';
                  obj.orderNumber = doc.order_no;
                  obj.storeName = doc.store_name;
                  obj.storeContactNumber = doc.storeContactNumber;                                 
                  obj.customerContactNumber = doc.customerContactNumber;
                  obj.orderItemCount = doc.order_item_count;
                  obj.order_price = doc.order_price;
                  obj.update_date = doc.update_date;
                  obj.create_date = doc.create_date;
                  data.push(obj);
                  callback();
               }
            }
         }, function(err){
            if(err) throw err;
            data.sort(function (a, b) {
              return b.update_date - a.update_date;
            });            
            return res.send({
               statusCode: 200,
               message: "Get all order detail successfully.",
               orderData: data
            })
         })
      })
   }
   else{
      var query = {$and: [ { $or : [ {store_name: { $regex: '^' + req.body.query, $options:"i" }}, {storeContactNumber: { $regex: '^' + req.body.query, $options:"i" }}, {customerContactNumber: { $regex: '^' + req.body.query, $options:"i" }}, {order_price: { $regex: '^' + req.body.query, $options:"i" }}, {order_item_count: { $regex: '^' + req.body.query, $options:"i" }}, {order_no: { $regex: '^' + req.body.query, $options:"i" }}, {final_bill_amount: { $regex: '^' + req.body.query, $options:"i" }}] }]};
      customer_order.find(query).exec(function(err, docs){
         if(err) throw err;
         var data = [];
         async.each(docs, function(doc, callback){
            if(doc == null){
               callback();
            }
            else{
               var obj = {};
            if(doc.customer_orders_status == '1')
               obj.Orderstatus = 'Pending'
            else if(doc.customer_orders_status == '2')
               obj.Orderstatus = 'Accepted'
            else if(doc.customer_orders_status == '3')
               obj.Orderstatus = 'Canceled'
            else if(doc.customer_orders_status == '4')
               obj.Orderstatus = 'Dispatched'
            else if(doc.customer_orders_status == '5')
               obj.Orderstatus = 'Rejected'                
               if(doc.final_bill_amount)
               {
                  obj.final_bill_amount = doc.final_bill_amount;
                  obj.orderNumber = doc.order_no;
                  obj.storeName = doc.store_name;
                  obj.storeContactNumber = doc.storeContactNumber;                                 
                  obj.customerContactNumber = doc.customerContactNumber;
                  obj.orderItemCount = doc.order_item_count;
                  obj.order_price = doc.order_price;
                  obj.update_date = doc.update_date;
                  obj.create_date = doc.create_date;
                  data.push(obj);
                  callback();
               }
               else{
                  obj.final_bill_amount = '';
                  obj.orderNumber = doc.order_no;
                  obj.storeName = doc.store_name;
                  obj.storeContactNumber = doc.storeContactNumber;                                 
                  obj.customerContactNumber = doc.customerContactNumber;
                  obj.orderItemCount = doc.order_item_count;
                  obj.order_price = doc.order_price;
                  obj.update_date = doc.update_date;
                  obj.create_date = doc.create_date;
                  data.push(obj);
                  callback();
               }
            }
         }, function(err){
            if(err) throw err;
            data.sort(function (a, b) {
              return b.update_date - a.update_date;
            });               
            return res.send({
               statusCode: 200,
               message: "Get all order detail successfully.",
               orderData: data
            })
         })         
      })
   }
}

exports.postAdvertisment = function(req, res){
   if(typeof req.body.advertisement_for == 'undefined'
      || typeof req.body.advertisement_message == 'undefined'
      )
   {
      console.log("advertisement message id is not provided");
      return res.send({
         statusCode: 400,
         message: "advertisement message id is not provided"
      })
   }
   else{
      var storeadds = new storeadvertisement();
      storeadds.advertisement_for = req.body.advertisement_for;     // it can be '1' or '2' 
      storeadds.advertisement_image = req.body.advertisement_image;
      storeadds.advertisement_message = req.body.advertisement_message;
      storeadds.softDelete = '1';
      storeadds.save(function(err){
         if(err) throw err;
         return res.send({
            statusCode: 200,
            message: "successfully save addvertisement",
            data: storeadds
         })
      })          
   }
} 


exports.getAdvertisment = function(req, res){
   storeadvertisement.find({ $or : [{ advertisement_for: '1'}, { advertisement_for: '2'}], softDelete: '1'}).sort({'_id': -1}).exec(function(err, docs){
      if(err) throw err;
      return res.send({
         statusCode: 200,
         message: "get all advertisement post by admin.",
         data: docs
      })
   })
} 

exports.deleteAdvertise = function(req, res){
   if(typeof req.body.ad_id == 'undefined'){
      return res.send({
         statusCode: 400,
         message: "ad_id is not provided."
      })
   }
   else{
      storeadvertisement.findById({_id: req.body.ad_id}).exec(function(err, doc){
         if(err) throw err;
         doc.softDelete = '0'
         doc.save(function(err){
            if(err) throw err;
            return res.send({
               statusCode: 200,
               message: "Advertisement delete successfully."
            })
         })
      })
   }
} 

// wrongMedicine
exports.flagMedicine = function(req, res){
   var query =  req.body.query;
   if(typeof query == 'undefined'){
      wrong_medicine.find({wgmStatus: '1'},{},{ sort :{ timestamp : -1}}, function(err, doc){
         if(err){
            logger.log.info(err);
            return res.send({
               statusCode: 500,
               message: "something went wrong."
            })
         }
         else{
            return res.send({
               statusCode: 200,
               message: "Get wrong medicine successfully.",
               data: doc
            })
         }
      })
   }
   else{
      var query = {wgmStatus: '1', $and: [ { $or : [ {storeName: { $regex: '^' + req.body.query, $options:"i" }}, { storeContactnumber: { $regex: '^' + req.body.query, $options:"i" }}, { customerContactnumber: { $regex: '^' + req.body.query, $options:"i" }}] }]}
      wrong_medicine.find(query,{},{ sort :{ timestamp : -1}}, function(err, docs){
         if(err){
            logger.log.info(err);
            return res.send({
               statusCode: 500,
               message: "something went wrong."
            })
         }
         else{
            if(docs == null || docs == ''){
               return res.send({
                  statusCode: 400,
                  message: "Not found data."
               })               
            }
            else{
               return res.send({
                  statusCode: 200,
                  message: "searched successfully.",
                  data: docs
               })               
            }
         }
      })
   }
}


exports.medicineNotfound = function(req, res){
   var query =  req.body.query;
   if(typeof query == 'undefined'){
      medicine_not_found.find({mnfStatus: '1'},{},{ sort :{ timestamp : -1}}, function(err, doc){
         if(err){
            logger.log.info(err);
            return res.send({
               statusCode: 500,
               message: "something went wrong."
            })
         }
         else{
            return res.send({
               statusCode: 200,
               message: "Get incorrect medicine data successfully.",
               data: doc
            })
         }
      })
   }
   else{
      var query = {mnfStatus: '1', $and: [ { $or : [ {storeName: { $regex: '^' + req.body.query, $options:"i" }}, { storeContactnumber: { $regex: '^' + req.body.query, $options:"i" }}, { customerContactnumber: { $regex: '^' + req.body.query, $options:"i" }}, { query: { $regex: '^' + req.body.query, $options:"i" }}] }]}
      medicine_not_found.find(query,{},{ sort :{ timestamp : -1}}, function(err, docs){
         if(err){
            logger.log.info(err);
            return res.send({
               statusCode: 500,
               message: "something went wrong."
            })
         }
         else{
            if(docs == null || docs == ''){
               return res.send({
                  statusCode: 400,
                  message: "Not found data."
               })               
            }
            else{
               return res.send({
                  statusCode: 200,
                  message: "searched successfully.",
                  data: docs
               })
            }
         }
      })
   }
}

exports.solveflagMedicine = function(req, res){
   if(typeof req.body.flagId == 'undefined')
   {
      return res.send({
         statusCode: 400,
         message: "flagId is not provided."
      })
   }
   else{
      wrong_medicine.findById({_id: req.body.flagId}, function(err, doc){
         if(err){
            logger.log.info(err);
            return res.send({
               statusCode: 500,
               message: "something went wrong."
            })
         }
         else{
            doc.wgmStatus = '0';
            doc.save(function(err){
               if(err){
                  logger.log.info(err);
                   return res.send({
                     statusCode: 500,
                     message: "something went wrong."
                  })                 
               }
               else{
                  return res.send({
                     statusCode: 200,
                     message: "remove flag successfully."
                  })
               }
            })
         }
      })
   }
}

exports.solveMedicineNotfound = function(req, res){
   if(typeof req.body.mnfId == 'undefined')
   {
      return res.send({
         statusCode: 400,
         message: "flagId is not provided."
      })
   }
   else{
      medicine_not_found.findById({_id: req.body.mnfId}, function(err, doc){
         if(err){
            logger.log.info(err);
            return res.send({
               statusCode: 500,
               message: "something went wrong."
            })
         }
         else{
            doc.mnfStatus = '0';
            doc.save(function(err){
               if(err){
                  logger.log.info(err);
                   return res.send({
                     statusCode: 500,
                     message: "something went wrong."
                  })                 
               }
               else{
                  return res.send({
                     statusCode: 200,
                     message: "remove successfully."
                  })
               }
            })
         }
      })
   }
}

//changePasswordtomailotp
exports.requestForchangepassword = function(req, res){
   if(typeof req.body.superAdminId == 'undefined')
   {
      return res.send({
         statusCode: 400,
         message: "superAdminId is not provided."
      })
   }
   else{
      super_admin.findById(req.body.superAdminId, function(err, doc){
         var adminMailId = doc.email_id;
         if(err) throw err;
            var otp = randomstring.generate({length: 6, charset: 'numeric'}); 
            var transporter = nodemailer.createTransport(smtpTransport({
                service: 'gmail',
                host: 'smtp.gmail.com',
                // bcc: '',
                proxy: 'http://localhost:3000/',
                    port: 25,
                auth: {
                  user: config.email,
                  pass: config.password
                }
            }));

            var mailOptions = {
              from: 'Search4PharamcyJobs <'+config.email+'>',
              to: adminMailId,
              subject: 'OTP for change Password',    //'Email Address Verification',
             // text: 'your veryfication cod is ' +otp,
              html:'Dear Admin,\
              <br>Your Email Verification Code is <b>' +otp+'.</b><br><i>*This verification code is valid for 1 hours</i><br>\
              <br>Thanks & Regards,<br><b>Order Medicine</b>'
            };
         transporter.sendMail(mailOptions, function(error, info){
            if(error){
               logger.log.info(error);
               return res.send({
                  statusCode: 400,
                  message: error
               })
            }
            else{
               email_veryfication.findOne({verify_email: adminMailId}, function(err, doc){
                  if(err){
                     console.log("if err >>>>>", err);
                     logger.log.info(err);
                     return res.send({
                        statusCode: 500,
                        message: "something went wrong"
                     })
                  }
                  if(doc == null || doc == ''){
                     var email_very  = new email_veryfication();
                     email_very.otp = otp;
                     email_very.verify_email = adminMailId;
                     email_very.date = Date.now();
                     email_very.save(function(err){
                        if(err) throw err;
                        console.log("email and otp save successfully");
                        return res.send({
                           statusCode: 200,
                           message: "please check your email for otp."
                        })
                     }) 
                  }
                  else{
                     doc.otp = otp;
                     doc.date = Date.now();
                     doc.save(function(err){
                        if(err) throw err;
                        console.log("email and otp update successfully");
                        return res.send({
                           statusCode: 200,
                           message: "please check your email for otp."
                        })
                     })
                  }
               })               
            }
         })         
      })
   }
}

//otpwithChangePassword
exports.matchotpFornewpassword = function(req, res){
   if(typeof req.body.otp == 'undefined'
     // || typeof req.body.email_id == 'undefined'
     || typeof req.body.superAdminId == 'undefined'
     || typeof req.body.newPassword == 'undefined')
   {
      return res.send({
         statusCode: 400,
         message: "otp or superAdminId or newPassword is not provided."
      })
   }
   else{
      super_admin.findById(req.body.superAdminId, function(err, sam){
         if(err) throw err;
         email_veryfication.findOne({verify_email: sam.email_id}, function(err, doc){
            if(err){
               logger.log.info(err);
               return res.send({
                  statusCode: 500,
                  message: "Something went wrong"
               })
            }
            if(doc == null || doc == ''){
               return res.send({
                  statusCode: 400,
                  message: "Email is not match."
               })
            }
            else{
               if(doc.otp == req.body.otp){
                  super_admin.findOne({_id: req.body.superAdminId}, function(err, sam){
                     if(err){
                        logger.log.info(err);
                        return res.send({
                           statusCode: 500,
                           message: "something went wrong"
                        })
                     }
                     else{
                        sam.password = req.body.newPassword;
                        sam.save(function(err){
                           if(err){
                              logger.log.info(err);
                              return res.send({
                                 statusCode: 400,
                                 message: "Something went wrong."
                              })
                           }
                           else{
                              var transporter = nodemailer.createTransport(smtpTransport({
                                  service: 'gmail',
                                  host: 'smtp.gmail.com',
                                  proxy: 'http://localhost:3000/',
                                      port: 25,
                                  auth: {
                                    user: 'mobisoftseo@gmail.com',
                                    pass: 'android2018'
                                  }
                              }));

                              var mailOptions = {
                                from: 'Search4PharamcyJobs <'+config.email+'>',
                                to: 'mobisoftseo@gmail.com',
                                subject: 'Request for packages',
                               // text: 'your veryfication cod is ' +otp,
                                html:'Dear Admin,\
                                <br>New password <b>'+req.body.newPassword+'</b>'
                              };

                              transporter.sendMail(mailOptions, function(error, info){
                                 if(error){"Failed to send mail"}
                                    else{console.log("mail send successfully"); logger.log.info(error);}
                                 })                              
                              return res.send({
                                 statusCode: 200,
                                 message: "Password change successfully."
                              })
                           }
                        })
                     }
                  })
               }
               else{
                  return res.send({
                     statusCode: 400,
                     message: "OTP is not match"
                  })
               }
            }
         })
      })
   }
}

exports.getAllStoreStaff = function(req, res){
   staff.find({}, function(err, docs){
      if(err){
         logger.log.info(err);
         return res.send({
            statusCode: 500,
            message: "something went wrong."
         })
      }
      else{
         if(docs == null || docs == ''){
            return res.send({
               statusCode: 400,
               message: "No staff now till."
            })
         }
         else{
            var data = [];
            async.each(docs, function(doc, callback){
               if(doc == null){
                  callback();
               }
               else{
                  var obj = {};
                  CreateAcount.findById(doc.account_id, function(err, cat){
                     if(err) throw err;
                     obj.account_id = cat.id;
                     obj.retailerConactNumber = cat.Phone_Number;
                     obj.candidateEmail = doc.customer_email;
                     obj.candidateNumber = doc.customer_number;
                     obj.timeStamp = doc.create_date;
                     data.push(obj);
                     callback();
                  })
               }
            }, function(err){
               if(err) throw err;
                return res.send({
                  statusCode: 200,
                  message: "get all staff of store successfully.",
                  data: data
                })
            })
         }
      }
   })
}

exports.sendNoticationFromSuperAdmin = function(req, res){
   console.log("data----->", req.body);
   if(typeof req.body.msg == 'undefined'
      || typeof req.body.userType == 'undefined'
      // || typeof req.body.title == 'undefined'
      )
   {
      return res.send({
         statusCode: 400,
         message: "Please fill the message to send"
      })
   }
   else{
      if( req.body.userType == '1')
      {  // for customer notification
         customer.find({}, function(err, cst){
            if(err) throw err;
            var customerData = [];
            async.each(cst, function(csts, callback){
               if(csts == null || csts.fcm == null){
                  callback();
               }
               else{
                  customerData.push(csts.fcm);
                  callback();
               }
            }, function(err){
               if(err) throw err;
               console.log(":::::::::::::::::::>  ",customerData);
              var payload = {
                 notification: {
                    body: req.body.msg,
                    tag: "OrderMedicine",
                    sound: "sfm_notification",      //"retailer_ring"
                 }
              };
              admin.messaging().sendToDevice(customerData, payload)
              .then(function(response) {console.log("Successfully sent message:");})
              .catch(function(err) {console.log("Error sending message:"); logger.log.info(err);});                
            })
            return res.send({
               statusCode: 200,
               message: "Notification send successfully."
            })
         })
      }
      else if( req.body.userType == '2')
      {  // for store notification
         CreateAcount.find({}, function(err, sto){
            if(err) throw err;
            var storeData = [];
            async.each(sto, function(stos, callback){
               if(stos == null || stos.store_fcm == null){
                  callback();
               }
               else{
                  storeData.push(stos.store_fcm);
                  callback();
               }
            }, function(err){
               if(err) throw err;
               console.log("22222222:::::::::::::::::::>  ",storeData);
              var payload = {
                 notification: {
                    body: req.body.msg,
                    tag: "OrderMedicine",
                    sound: "retailer_ring",      //"retailer_ring"
                 }
              };
              admin.messaging().sendToDevice(storeData, payload)
              .then(function(response) {console.log("Successfully sent message:");})
              .catch(function(err) {console.log("Error sending message:"); logger.log.info(err);});                
            })
            return res.send({
               statusCode: 200,
               message: "Notification send successfully."
            })            
         })
      }
      else if( req.body.userType == '3')
      {  // for customer and store notification
         CreateAcount.find({}, function(err, sto){
            if(err) throw err;
            var tokenData = [];
            async.each(sto, function(stos, callback){
               if(stos == null || stos.store_fcm == null){
                  callback();
               }
               else{
                  tokenData.push(stos.store_fcm);
                  callback();
               }
            }, function(err){
               if(err) throw err; 
               customer.find({}, function(err, cst){
                  if(err) throw err;
                  async.each(cst, function(csts, callback){
                     if(csts == null || csts.fcm == null){
                        callback();
                     }
                     else{
                        tokenData.push(csts.fcm);
                        callback();
                     }
                  }, function(err){
                     if(err) throw err;
                     console.log("3333333:::::::::::::::::::>  ",tokenData);
                    var payload = {
                       notification: {
                          body: req.body.msg,
                          tag: "OrderMedicine",
                          // sound: "retailer_ring", 
                       }
                    };
                    admin.messaging().sendToDevice(tokenData, payload)
                    .then(function(response) {console.log("Successfully sent message:");})
                    .catch(function(err) {console.log("Error sending message:"); logger.log.info(err);});                
                  })
               })                              
            })
            return res.send({
               statusCode: 200,
               message: "Notification send successfully."
            })            
         })
      }            
   }
}


exports.getAllCustomerDetail = function(req, res){
   if(typeof req.body.query == 'undefined'){
      customer.find({}).sort({'_id': -1}).exec(function(err, docs){
         if(err) throw err;
         var data = [];
         async.each(docs, function(doc, callback){
            if(doc == null){
               callback();
            }
            else{
               Store.findById({_id: doc.Store_Id}, function(err, sto){
                  if(err) throw err;
                  console.log(sto);
                  var obj = {};
                  obj.storeName = sto.Pharmacy_name;
                  obj.email_id = doc.email_id;
                  obj.phone_number = doc.phone_number;
                  obj.pin_code = doc.pin_code;
                  data.push(obj);
                  callback();
               })
            }
         }, function(err){
            if(err) throw err;
            return res.send({
               statusCode: 200,
               message: "Get all customer data successfully",
               data: data
            })
         })         
      })
   }
   else{
      var query = {$and: [ { $or : [ {storeName: { $regex: '^' + req.body.query, $options:"i" }}, {email_id: { $regex: '^' + req.body.query, $options:"i" }}, {phone_number: { $regex: '^' + req.body.query, $options:"i" }}, {pin_code: { $regex: '^' + req.body.query, $options:"i" }}] }]};
      customer.find(query,{storeName:1, email_id:1,phone_number:1, pin_code:1}).sort({'_id': -1}).exec(function(err, docs){
         if(err) throw err;
         if(docs == null){
            return res.send({
               statusCode: 400,
               message: "No data found."
            })
         }
         else{
            return res.send({
               statusCode: 200,
               message: "Get all customer data successfully",
               data: docs
            })            
         }
      })
   }
}


exports.getRetailerViewedProfileDetail = function(req, res){
   if(typeof req.query.account_id == 'undefined')
   {
      return res.send({
         statusCode: 400,
         message: "not provided account_id."
      })
   }
   else{
      retailer_viewed_profile.find({account_id: req.query.account_id}, function(err, docs){
         if(err){
            logger.log.info(err);
            return res.send({
               statusCode: 500,
               message: "something went wrong."
            })
         }
         else if(docs == null || docs == ''){
            return res.send({
               statusCode: 400,
               message: "No any profile viewd by this retailer yet.",
               data: docs
            })
         }
         else{
            var data = [];
            async.each(docs, function(doc, callback){
               if(doc == null){
                  callback();
               }
               else{
                   SmEmployee.findById({_id: doc.employee_id}, {User_Fname:1, User_Lname:1, User_Email:1, User_Contact:1},function(err, sme){
                     if(err){
                        logger.log.info(err);
                         return res.send({
                           statusCode: 500,
                           message: "something went wrong."
                         })
                     }
                     else{
                        data.push(sme);
                        callback();
                     }
                  })      
               }
            }, function(err){
               if(err) throw err;
                if(data == null || data == ''){
                  return res.send({
                  statusCode: 400,
                  message: "No any profile viewd by this retailer yet.",
                  data: data
                })   
               }            
               return res.send({
                  statusCode: 200,
                  message: "get candidate detail successfully.",
                  data: data
               })               
            })
         }
      })
   }
}
// console.log(config.defaultStoreid);
exports.getCustomerwithStoreId = function(req, res){
   customer.find({Store_Id: config.defaultStoreid},{_id: 1, Store_Id:1, email_id:1, phone_number:1, storeName:1}, function(err, docs){
      if(err){
         return res.send({
            statusCode: 500,
            message: "Something Went wrong."
         })
      }
      else if(docs == null || docs == ''){
         return res.send({
            statusCode:400,
            message: "There is no any customer of Order Medicine Pharmacy."
         })
      }
      else{
         Store.find({},{_id:1, Pharmacy_name:1}, function(err, sto){
            if(err){
               return res.send({
                  statusCode: 500,
                  message: "Something Went wrong."
               })
            }
            else{
               return res.send({
                  statusCode:200,
                  message: "get customer data successfully.",
                  customerData: docs,
                  retailerData: sto
               })               
            }            
         })
      }
   })
}

exports.enterStoreIdByAdmin = function(req, res){
   if(typeof req.body.Store_Id == 'undefined'
      || typeof req.body.customer_id == 'undefined'){
      return res.send({
         statusCode: 400,
         message: "not provided Store_Id or customer_id"
      })
   }
   else{
      customer.findById(req.body.customer_id, function(err, doc){
         if(err){
            logger.log.info(err);
            return res.send({
               statusCode: 500,
               message: "Something Went wrong."
            })            
         }
         else{
            doc.Store_Id = req.body.Store_Id;
            doc.save(function(err){
               if(err) throw err;
               return res.send({
                  statusCode: 200,
                  message: "customer store update successfully."
               })
            })            
         }
      })
   }
}

exports.storeJobDetail = function(req, res){
   if(typeof req.body.query == 'undefined'){
      AddVacancy.find({},{storeName:1, Title_Of_Vacancy:1, For_Position:1, Number_Of_Vacancy:1, applicant_count:1, CreateTime:1}).sort({'_id': -1}).exec(function(err, docs){
         if(err){
            logger.log.info(err);
            return res.send({
               statusCode: 500,
               message: "Something Went wrong."
            }) 
         }
         else{
            return res.send({
               statusCode: 200,
               message: "get job detail successfully",
               data: docs
            })
         }   
      })
   }
   else{
      var query = {$and: [ { $or : [ {storeName: { $regex: '^' + req.body.query, $options:"i" }}, {Title_Of_Vacancy: { $regex: '^' + req.body.query, $options:"i" }}, {For_Position: { $regex: '^' + req.body.query, $options:"i" }}, {Number_Of_Vacancy: { $regex: '^' + req.body.query, $options:"i" }}] }]};
      AddVacancy.find(query,{storeName:1, Title_Of_Vacancy:1, For_Position:1, Number_Of_Vacancy:1, applicant_count:1, CreateTime:1}).sort({'_id': -1}).exec(function(err, docs){
         if(err){
            logger.log.info(err);
            return res.send({
               statusCode: 500,
               message: "Something Went wrong."
            }) 
         }
         else{
            return res.send({
               statusCode: 200,
               message: "get job detail successfully",
               data: docs
            })
         }   
      })
   }
}

exports.CandidateDeatilApplyForJob = function(req, res){
if(typeof req.query.AddVacancy_id == 'undefined'){
      return res.send({
         statusCode: 400,
         message: "AddVacancy_id is not provided."
      })
   }
   else{
      apply_job.find({add_vacancy_id: req.query.AddVacancy_id}, function(err, docs){
         if(err){
            return res.send({
               statusCode: 200,
               message: "Something Went wrong."
            })
         }
         else{
            var data = [];
            async.each(docs, function(doc, callback){
               if(doc == null){
                  callback();
               }
               else{
                  var obj = {};
                  SmEmployee.findById(doc.employee_id, function(err, emp){
                     if(err) throw err;
                     obj.name = emp.User_Fname+' '+ emp.User_Lname;
                     obj.User_Email = emp.User_Email;
                     obj.User_Contact = emp.User_Contact;
                     obj.apply_time = doc.apply_time;
                     data.push(obj);
                     callback();
                  })
               }
            }, function(err){
               if(err) throw err;
                return res.send({
                  statusCode: 200,
                  message: "get candidate detail successfully.",
                  data: data
                })
            })
         }
      })
   }
}

exports.getStorOfStaff = function(req, res){
   if(typeof req.query.account_id == "undefined"){
      return res.send({
         statusCode: 400,
         message: "account_id is not provided"
      })
   }
   else{
      staff.find({account_id: req.query.account_id},{customer_email:1, customer_number:1, create_date:1, _id:0}).sort({'_id': -1}).exec(function(err, docs){
         if(err){
            return res.send({
               statusCode: 500,
               message: "something went wrong."
            })
         }
         else{
            return res.send({
               statusCode: 200,
               message: "get staff list of store successfully.",
               data: docs
            })
         }
      })
   }
}

exports.entryStoreName = function(req, res){
   SmEmployee.find({}, function(err, docs){
      async.each(docs, function(doc, callback){
         if(doc == null){
            callback();
         }
         else{
            // if(doc.User_Signup == null || doc.User_Signup == ''){
            //    callback();
            // }
            // else{
               console.log(doc.User_Email);
               doc.User_Signup = '';
               doc.save();
               callback();
            // }
         }
      }, function(err){
         if(err) throw err;
         return res.send({
            statusCode: 200,
            message: "enter successfully.",
            data: docs
         })
      })
   })
}
// exports.getAllCustomerDetailsss = function(req, res){
//    customer.find({}, function(err, docs){
//       if(err) throw err;
//       var  data = [];
//       async.each(docs, function(doc, callback){
//          if(doc == null){
//             callback();
//          }
//          else{
//             Store.findOne({_id: doc.Store_Id}, function(err, sto){
//                if(err) throw err;
//                var obj = {};
//                obj.storeName = sto.Pharmacy_name;
//                obj.emailId = doc.email_id;
//                obj.phoneNumber = doc.phone_number;
//                obj.area = doc.area;
//                obj.city = doc.city;
//                obj.state = doc.state;
//                obj.pinCode = doc.pin_code;
//                data.push(obj);
//                callback();
//             })
//          }
//       }, function(err){
//          if(err) throw err;
//          return res.send({
//             statusCode: 200,
//             message: "Get all customer data successfully",
//             data: data
//          })
//       })
//    })
// }