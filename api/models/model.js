'use strict';
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;


var storeSchema = new Schema({
    Pharmacy_name: {type: String},
    Address: {type: String},
    Landmark: {type: String},
    Area: {type: String},
    State: {type: String},
    City: {type: String},
    Pin_code: {type: String},
    Email: {type: String}, 
    Contact_No_1: {type: String},// this is a mobileNumber
    Contact_No_2: {type: String},// this is a landlineNumber (optional)
    no_of_branches: {type: String},
    DLN: {type:String},
    latitude: {type: Number},
    longitude: {type: Number},
    GST_Number: {type:String, max:50},
    Premise: {type:String, max:50},
    profile_image: {type:String},
    CreateTime: {type:Date, default: Date.now},
    updateTime: {type:Date, default: Date.now},
    Store_status: {type: Boolean, default: false}
});

module.exports = mongoose.model('Store',storeSchema);

var storeadvertisementSchema = new Schema({
    store_id: {type: Schema.Types.ObjectId, ref: 'Store'},
    advertisement_image: {type: String},
    advertisement_message: {type: String},
    softDelete: {type: String, enum: ['0', '1'], default: '1'},        // soft delete only for super admin (0 = delete, 1 = active);
    advertisement_for: {type: String, enum: ['1', '2']}, // 1 = customerAdds  2 = retailerAdds
    create_date: {type: Date,  default: Date.now},
    update_date: {type: Date,  default: Date.now}
})

module.exports = mongoose.model('storeadvertisement', storeadvertisementSchema);


var notificationSchema = new Schema({
    store_id: {type: Schema.Types.ObjectId, ref: 'Store'},
    title: {type: String},
    message: {type: String},
    create_time: {type: Date, default: Date.now}
})

module.exports = mongoose.model('notification', notificationSchema);


var OwnerInfoSchema = new Schema({
	store_id: {type: Schema.Types.ObjectId, ref: 'Store'},
    Firm_Type: {type:String},
    Owner_list: {type:String},
    CreateTime: {type:Date, default: Date.now},
    updateTime: {type:Date, default: Date.now},    
});

module.exports = mongoose.model('OwnerInfo',OwnerInfoSchema);


var OperationalInfoSchema = new Schema({
    Store_Start_Time: {type:String},
    Store_Break_Time_From: {type:String},
    Store_Break_Time_To: {type:String},
    Store_Close_Time: {type:String},
    Holidays: {type:String},
    Shift_Day: {type:String},
    Number_Of_Staff: {type: String},
    Number_Of_Pharmcist: {type:String},
    Number_Of_Whole_Sellers: {type:String}, 
    Home_Delevary: {type:String},
    Min_Order_Rs: {type:String},
    Discount_Scheme: {type:String},
    Flate_Discount: {type: String},
    Slab_Discount: {type: String},
    CreateTime: {type:Date, default: Date.now},
    updateTime: {type:Date, default: Date.now},    
});

module.exports = mongoose.model('OperationalInfo',OperationalInfoSchema);


var Create_Acount = new Schema({
	Store_Id: {type: Schema.Types.ObjectId, ref: 'Store', required: true},
	OwnerInfo_Id: {type: Schema.Types.ObjectId, ref: 'OwnerInfo', required: true},
	OperationalInfo_Id: {type: Schema.Types.ObjectId, ref: 'OperationalInfo', required: true},
   // Opackagejob_id: {type: Schema.Types.ObjectId, ref: 'packagejob', required: true},
    no_of_jprofile_views: {type: Number, default: 0},
    remaining_view: {type: Number, default: 0},
	Email_Id: {type: String},
	Phone_Number: {type: String},
    store_fcm: {type: String},
    is_active: {type: String, enum: ['0', '1', '2', '3'], default: '0'},
    //1= 'acivation', 2= 'reject', 3= 'suspend'
})

module.exports = mongoose.model('CreateAcount', Create_Acount);

var Add_VacancySchema = new Schema({
    AcountId: {type: Schema.Types.ObjectId, ref: 'CreateAcount', required: true},
    storeName: {type: String},
    Title_Of_Vacancy: {type: String},
    For_Position: {type: String},
    Number_Of_Vacancy: {type: String},
    Gender: {type: String},
    Total_Experience_Needed: {type: String},
    Salary_Range: {type: String},
    Qualification: {type: String},
    Joining_On: {type: String},
    Age_Limit: {type: String},
    Description: {type: String},
    applicant_count: {type: Number, default: 0},
    CreateTime: {type:Date},
    job_status : {type: String, enum: ['0', '1'], default: '1'}
});

module.exports = mongoose.model('AddVacancy', Add_VacancySchema);

var Sm_Employee = new Schema({
    User_Fname:{type: String},
    User_Lname:{type: String},
    User_Dob:{type: String},
    User_Email:{type: String},
    is_email_valid:{type: String, enum: ['0', '1'], default: '0'},
    User_Contact:{type: String},
    is_contact_valid:{type: String, enum: ['0', '1'], default: '0'},
    User_Address1:{type: String},
    User_Address2:{type: String},
    User_Aadhar_No:{type: String},
    User_State:{type: String},
    User_City:{type: String},
    User_PIncode:{type: String},
    User_Gender:{type: String},
    User_Pwd:{type: String},
   // User_Profile:{type: String},
    User_Education:{type: String},
    Certi_Image:{type: String},
    User_College:{type: String},
    User_University:{type: String},
    User_Passing_Year:{type: String},
    User_Percentage:{type: String},
    User_Experience_Type:{type: String},
    Fresher_Expected_Ctc:{type: String},
    User_Experience:[{
                       employeer_name: {type: String},
                       joining_date: {type: String},
                       relieving_date: {type: String},
                       contact_person:{type: String},
                       contact_number:{type: String},
                       total_experience:{type: String}
                   }],
    Current_Ctc:{type: String},
    Expected_Ctc:{type: String},
    Notice_Period:{type: String},
    Computer_Knowledge:{type: String},
    Billing_Software:{type: String, enum: ['0', '1']},
    Software_Data: [{
                      soft_name:{type: String},
                      soft_from_date:{type: String},
                      soft_to_date:{type: String}
                  }],
    User_Image:{type: String},
    User_Profile:{type: String, enum: ['1', '2', '3', '']},
    Profile_Status:{type: String, enum: ['1','2','3','4'], default: '1'},//1=register, 2=reject, 3=suspend, 4= accept, on update time status ==1
    User_Signup:{type:String},
    User_Create:{type:Date},
    User_Update:{type:Date},
    current_employeer: {type: String},
    current_employeer_contact: {type: String},
    current_join_date: {type: String},
    total_experience: {type: String},
    current_employeer_person: {type: String},
    Emp_Status:{type: Boolean, default: true}

});

module.exports = mongoose.model('SmEmployee', Sm_Employee);

var My_Staff = new Schema({
    account_id: {type: Schema.Types.ObjectId, ref: 'CreateAcount'},
    employee_id: {type: Schema.Types.ObjectId, ref: 'SmEmployee'},
    my_staff_status:{type: Boolean, default: true},
    create_date: {type:Date},
    update_date: {type:Date}
})

module.exports = mongoose.model('my_staff', My_Staff);


var Email_Veryfication = new Schema({
    otp: {type: String},
    verify_email: {type: String},
    date: {type: Date, expires: 3600} //900
})

module.exports = mongoose.model('email_veryfication', Email_Veryfication);


var mobile_number_veryfication = new Schema({
    otp: {type: String},
    verify_mobile_number: {type: String},
    date: {type: Date, expires: 3600}  //900
})

module.exports = mongoose.model('mobile_Veryfication', mobile_number_veryfication);


var medicine_datas = new Schema({
    name: {type: String},
    packing: {type: String},
    type: {type: String},
    category: {type: String},
    strength: [{
                 salt: {type: String}, 
                 quantity: {type: String}
              }],
    mrp: [{
            from: {type:Date},
             price: {type: Number}, 
             currency: {type: String}
         }],
    companyNmae: {type: String},

}, { collection : 'medicine_data' });
// Medicine_Data.index({name: 'text'});

module.exports = mongoose.model('medicine_data', medicine_datas);

var Apply_Job = new Schema({
    add_vacancy_id: {type: Schema.Types.ObjectId, ref: 'AddVacancy'},
    employee_id: {type: Schema.Types.ObjectId, ref: 'SmEmployee'},
    apply_time: {type: Date}
})

module.exports = mongoose.model('apply_job', Apply_Job);


var customer_profile = new Schema({
    Store_Id: {type: Schema.Types.ObjectId, ref: 'Store', required: true},
    storeName: {type: String},
    email_id : {type: String},
    fcm: {type: String},
    name: {type: String},
    image: {type: String},
    //email: {type: String},
    phone_number: {type: String},
    address: {type: String},
    area: {type: String},    
    state: {type: String},
    city: {type: String},
    pin_code: {type: String}

})

module.exports = mongoose.model('customer', customer_profile);


var customer_delivery_details  = new Schema({
    customer_id: {type: Schema.Types.ObjectId, ref: 'customer', required: true},
    name: {type: String},
    email_id : {type: String},
    mobile_number: {type: String},
    address1: {type: String},
    address2: {type: String},
    landmark: {type: String},
    city: {type: String},
    state: {type: String},
    pin_code: {type: String},
    delivery_boy_name: {type: String},
    delivery_boy_number: {type: String},
    delivery_time: {type: String},
    create_date: {type: Date, default: Date.now},
    update_date: {type: Date, default: Date.now}
})

module.exports = mongoose.model('customer_delivery_detail', customer_delivery_details);


var customer_carts = new Schema({
    customer_order_id: {type: Schema.Types.ObjectId, ref: 'customer_order'},
    customer_id: {type: Schema.Types.ObjectId, ref: 'customer'},
    Store_Id: {type: Schema.Types.ObjectId, ref: 'Store'},
    medicine_id: {type: Schema.Types.ObjectId, ref: 'medicine_data'},
    med_name: {type: String},
    quantity: {type: String},
    price: {type: String},
    date: {type: Date},
    cart_status: {type:  String, default: '0', enum: ['0', '1'], default: '0' }
})

module.exports = mongoose.model('customer_cart', customer_carts);


var customer_orders = new Schema({
    customer_id : {type: Schema.Types.ObjectId, ref: 'customer'},
    Store_Id: {type: Schema.Types.ObjectId, ref: 'Store'},
    deliver_address_id: {type: Schema.Types.ObjectId, ref: 'customer_delivery_detail'},
    store_name: {type: String},
    storeContactNumber: {type: String},
    customerContactNumber: {type: String},
    cart_item_list: [{ medicine_id: {type: String}, type: {type: String}, med_name: {type: String}, quantity: {type: String}, price: {type: String}, medicine_manufacturer: {type: String}, packing: {type:String}}],
    prescription: [{image_url: {type:String}}],
    customer_orders_status: {type: String, enum: ['1', '2', '3', '4', '5'], default: '1'},
    order_price: {type: String},
    delivery_time: {type: String},
    delivery_body_name: {type: String},
    delivery_body_number: {type: String},
    comment: {type: String},
    order_item_count: {type: String},
    order_no: {type: String},
    final_bill_amount: {type: String},
    update_date: {type: Date, default: Date.now},
    create_date: {type: Date, default: Date.now}
})

module.exports = mongoose.model('customer_order', customer_orders);

var super_admins = new Schema({
    name: {type: String},
    email_id: {type: String},
    password: {type: String},
    super_admin_status: {type: String, enum: ['0', '1'], default: '1'},
    user_type: {type: Number},
    update_time: {type: Date}
})

module.exports = mongoose.model('super_admin', super_admins);

var medicine_salts = new Schema({
    salt: {type: String},
    quantity: {type: String}
})

module.exports = mongoose.model('medicine_salt', medicine_salts);

var rejections = new Schema({
    employee_id: {type: Schema.Types.ObjectId, ref: 'SmEmployee'},
    reject_reason: {type: String},
    rejection_time_stamp: {type: Date, default: Date.now}
})

module.exports = mongoose.model('rejection', rejections);

var educations = new Schema({
    edu_name: {type: String},
    edu_status: {type: String},
    edu_time_stamp: {type: Date, default: Date.now}
})

module.exports = mongoose.model('education', educations);

var suspends = new Schema({
    employee_id: {type: Schema.Types.ObjectId, ref: 'SmEmployee'},
    suspend_reason: {type: String},
    suspend_time_stamp: {type: Date, default: Date.now}
})

module.exports = mongoose.model('suspend', suspends);

var healthlocker = new Schema({
    customer_id: {type: Schema.Types.ObjectId, ref: 'customer'},
    img_url: {type: String},
    patient_name: {type: String},
    doc_name: {type: String},
    type: {type: String},
    create_date: {type: Date},
    update_date: {type: Date}
})

module.exports = mongoose.model('health_locker', healthlocker); 
 

var all_state_of_india = new Schema({
    State_id: {type: String},
    state: {type: String},
}, { collection : 'state' });

module.exports = mongoose.model('state', all_state_of_india);

var all_city_of_india = new Schema({
    State_id: {type: String},
    city_id: {type: String},
    city_name: {type: String}
}, { collection : 'city' });

module.exports = mongoose.model('city', all_city_of_india);

var Packagess = new Schema({
    name_of_package: {type: String},
    price_of_package: {type: String},
    number_of_view: {type: String},
    package_status: {type: String, enum: ['0', '1'], default: '1'},
    package_created: {type: Date, default: Date.now},
    package_updated: {type: Date}
})

module.exports = mongoose.model('packagejob', Packagess);

var med_not_found = new Schema({
    customer_id: {type: Schema.Types.ObjectId, ref: 'customer'},
    store_id: {type: Schema.Types.ObjectId,  ref: 'Store'},
    storeName: {type: String},
    storeContactnumber: {type: String},
    customerContactnumber: {type: String},
    seach_for: {type: String},
    query: {type: String},
    mnfStatus: {type: String, enum: ['0','1'], default: '1'},  // 0 = is deactive , 1 = active
    timestamp: {type: Date, default: Date.now}
})

module.exports = mongoose.model('medicine_not_found', med_not_found);


var wrong_med = new Schema({
    customer_id: {type: Schema.Types.ObjectId, ref: 'customer'},
    store_id: {type: Schema.Types.ObjectId,  ref: 'Store'},
    medicine_id: {type: String},
    storeName: {type: String},
    storeContactnumber: {type: String},
    customerContactnumber: {type: String},    
    // query: {type: String},
    wgmStatus: {type: String, enum: ['0','1'], default: '1'},    // 0 = is deactive , 1 = active
    timestamp: {type: Date, default: Date.now}
})

module.exports = mongoose.model('wrong_medicine', wrong_med);

var short_list_candidiate = new Schema({
    account_id : {type: Schema.Types.ObjectId, ref:'CreateAcount'},
    employee_id : {type: Schema.Types.ObjectId, ref:'employee_id'}
})

module.exports = mongoose.model('shortlist_candidiate', short_list_candidiate);


var retailerViewedProfile = new Schema({
    account_id : {type: Schema.Types.ObjectId, ref:'CreateAcount'},
    employee_id : {type: Schema.Types.ObjectId, ref:'employee_id'},
    create_date: {type: Date, default: Date.now},
})

module.exports = mongoose.model('retailer_viewed_profile', retailerViewedProfile)


var retailer_Package_history = new Schema({
    account_id : {type: Schema.Types.ObjectId, ref:'CreateAcount'},
    package_id : {type: Schema.Types.ObjectId, ref:'packagejob'},
    retailer_package_status: {type: String, enum: ['0', '1'], default: '1'},
    is_aprove : {type: String, enum: ['0', '1'], default: '0'},
    create_date : {type: Date, default: Date.now},
})

module.exports = mongoose.model('retailerPackagehistory', retailer_Package_history);

var staffSchema = new Schema({
    account_id : {type: Schema.Types.ObjectId, ref:'CreateAcount'},
    customer_id : {type: Schema.Types.ObjectId, ref:'customer'},
    customer_store_id : {type: Schema.Types.ObjectId,  ref: 'Store'},
    customer_email: {type: String},
    customer_number: {type: String},
    customer_fcm: {type: String },
    create_date: {type: Date, default: Date.now},
})

module.exports = mongoose.model('staff', staffSchema);


// var med_finder = new Schema({
//     customer_id: {type: Schema.Types.ObjectId, ref: 'customer'},
//     med_name: {type: String},
//     quantity: {type: String},
//     //counter: {type: String},
//     request: [{store_id: {type: Schema.Types.ObjectId, ref: 'Store'}}],
//     create_date: {type: Date, default: Date.now}
// })

// module.exports = mongoose.model('medFinder', med_finder); 

var med_finder = new Schema({
    customer_id: {type: Schema.Types.ObjectId, ref: 'customer'},
    med_name: {type: String},
    quantity: {type: String},
    latitude: {type: Number},
    longitude: {type: Number},
    status: {type: String, enum: ['0', '1', '2', '3'], default: '0'},
    searchingradius: {type: Number},    
    create_date: {type: Date, default: Date.now},
    response_count: {type: Number, default: 0}
    //0 = SEARCHING,  1 = FOUND, 2 = NOTFOUND,  3= CANCELED
})

module.exports = mongoose.model('medFinder', med_finder); 

var responsestore = new Schema({
    enquiry_id: {type: Schema.Types.ObjectId, ref: 'medFinder'},
    store_id: {type: Schema.Types.ObjectId, ref: 'Store'},
    timestamp: {type: Date, default: Date.now},
    response_status: {type: String, enum: ['0', '1', '2', '3'], default: '0'},
    // 0= Notansered,  1= yes or accept,   2= No or regect,   3= closed
})

module.exports = mongoose.model('response_store', responsestore); 

var notificationretailcustomer = new Schema({
    store_id: {type: Schema.Types.ObjectId, ref: 'Store'},
    customer_id: {type: Schema.Types.ObjectId, ref: 'customer_id'},
    title: {type: String},
    message: {type: String},
    flag: {type: String, enum: ['1', '2', '3']},   // 1 = retaileNotification, 2 = customerNotification
    create_time: {type: Date, default: Date.now}

})

module.exports = mongoose.model('notificationcr', notificationretailcustomer);


var rejected_Retailer = new Schema({
    retailer_id: {type: Schema.Types.ObjectId, ref: 'CreateAcount'},
    reject_reason: {type: String},
    create_time: {type: Date, default: Date.now}

})

module.exports = mongoose.model('rejectedRetailer', rejected_Retailer);


var suspend_Retailer = new Schema({
    retailer_id: {type: Schema.Types.ObjectId, ref: 'CreateAcount'},
    suspend_reason: {type: String},
    create_time: {type: Date, default: Date.now}
})  

module.exports = mongoose.model('suspendRetailer', suspend_Retailer);